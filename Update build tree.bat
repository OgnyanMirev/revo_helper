Rem Copy over all of the Revo Uninstaller (Pro) executables.

IF EXIST %1 (
	echo %1 > RUPRelease64Location.txt
	xcopy %1 build/Pro/Release/x64/ /q /Y
)
ELSE (
	IF EXISTS type RUPRelease64Location.txt (		
		xcopy %1 build/Pro/Release/x64/ /q /Y
	)
	ELSE ( 
		echo "Invalid RUPRelease64Location"
		EXIT
	)
)


Rem Copy over all of the language files from ./lang

xcopy lang build\Pro\Debug\x64\lang /q /Y
xcopy lang build\Pro\Debug\x86\lang /q /Y
xcopy lang build\Pro\Release\x64\lang /q /Y
xcopy lang build\Pro\Release\x86\lang /q /Y

xcopy lang build\Free\Debug\x64\lang /q /Y
xcopy lang build\Free\Debug\x86\lang /q /Y
xcopy lang build\Free\Release\x64\lang /q /Y
xcopy lang build\Free\Release\x86\lang /q /Y



Rem Copy over all of the driver files from Pro to Free since they only build in Pro

xcopy build/Pro/Release/x86/RevoProcessDetector.sys build/Free/Release/x86/RevoProcessDetector.sys /q /Y
xcopy build/Pro/Release/x86/RevoProcessDetector.inf build/Free/Release/x86/RevoProcessDetector.inf /q /Y

xcopy build/Pro/Debug/x86/RevoProcessDetector.sys build/Free/Debug/x86/RevoProcessDetector.sys /q /Y
xcopy build/Pro/Debug/x86/RevoProcessDetector.inf build/Free/Debug/x86/RevoProcessDetector.inf /q /Y

xcopy build/Pro/Release/x64/RevoProcessDetector.sys build/Free/Release/x86/RevoProcessDetector.sys /q /Y
xcopy build/Pro/Release/x64/RevoProcessDetector.inf build/Free/Release/x86/RevoProcessDetector.inf /q /Y

xcopy build/Pro/Debug/x64/RevoProcessDetector.sys build/Free/Debug/x64/RevoProcessDetector.sys /q /Y
xcopy build/Pro/Debug/x64/RevoProcessDetector.inf build/Free/Debug/x64/RevoProcessDetector.inf /q /Y




