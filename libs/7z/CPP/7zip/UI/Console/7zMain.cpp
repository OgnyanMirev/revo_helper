// Main.cpp

#include "StdAfx.h"

#include "../../../Common/MyWindows.h"

#ifdef _WIN32
#include <Psapi.h>
#endif

#include "../../../../C/CpuArch.h"

#include "../../../Common/MyInitGuid.h"

#include "../../../Common/CommandLineParser.h"
#include "../../../Common/IntToString.h"
#include "../../../Common/MyException.h"
#include "../../../Common/StringConvert.h"
#include "../../../Common/StringToInt.h"
#include "../../../Common/UTFConvert.h"

#include "../../../Windows/ErrorMsg.h"

#include "../../../Windows/TimeUtils.h"

#include "../Common/ArchiveCommandLine.h"
#include "../Common/Bench.h"
#include "../Common/ExitCode.h"

#ifdef EXTERNAL_CODECS
#include "../Common/LoadCodecs.h"
#endif

#include "../../Common/RegisterCodec.h"

#include "BenchCon.h"
#include "ConsoleClose.h"
#include "ExtractCallbackConsole.h"
#include "List.h"


CStdOutStream* g_StdStream = NULL;
CStdOutStream* g_ErrStream = NULL;

HINSTANCE g_hInstance;

void ListArchive(const wchar_t* file, std::vector<fs::path>& Result)
{
    static struct const_data
    {
        CObjectVector<COpenType> types;
        CMyComPtr<IUnknown> __codecsRef;
        CCodecs* codecs;
        CIntVector excludedFormats;
        NWildcard::CCensorNode censor;
        CStdOutStream* percentsStream = NULL;
        const_data()
        {
            g_ErrStream = &g_StdErr;
            g_StdStream = &g_StdOut;

            codecs = new CCodecs;
            __codecsRef = codecs;
            codecs->Load();
        }

        void list(const wchar_t* file, std::vector<fs::path>& Result)
        {
            UINT64 n_err, n_warn;

            UStringVector ArchivePathsSorted;
            UStringVector ArchivePathsFullSorted;

            UStringVector commandStrings;
            auto cmdln = (std::wstring(L"l \"") + file) + L"\"";

            NCommandLineParser::SplitCommandLine(cmdln.c_str(), commandStrings);


            CArcCmdLineOptions options;


            CArcCmdLineParser parser;

            parser.Parse1(commandStrings, options);
            parser.Parse2(options);


            g_StdOut.IsTerminalMode = false;
            g_StdErr.IsTerminalMode = false;

            if (options.StdInMode)
            {
                ArchivePathsSorted.Add(options.ArcName_for_StdInMode);
                ArchivePathsFullSorted.Add(options.ArcName_for_StdInMode);
            }
            else
            {
                CExtractScanConsole scan;

                scan.Init(options.EnableHeaders ? g_StdStream : NULL, g_ErrStream, percentsStream);
                scan.SetWindowWidth(1);

                CDirItemsStat st;

                scan.StartScanning();

                auto hresultMain = EnumerateDirItemsAndSort(
                    options.arcCensor,
                    NWildcard::k_RelatPath,
                    UString(), // addPathPrefix
                    ArchivePathsSorted,
                    ArchivePathsFullSorted,
                    st,
                    &scan);

                scan.CloseScanning();

            }

            auto hresultMain = ListArchives(
                codecs,
                types,
                excludedFormats,
                options.StdInMode,
                ArchivePathsSorted,
                ArchivePathsFullSorted,
                options.ExtractOptions.NtOptions.AltStreams.Val,
                options.AltStreams.Val, // we don't want to show AltStreams by default
                censor,
                options.EnableHeaders,
                options.TechMode,
#ifndef _NO_CRYPTO
                options.PasswordEnabled,
                options.Password,
#endif
                & options.Properties,
                n_err, n_warn, Result);

        }

    } data;

    try
    {
        data.list(file, Result);
    }
    catch (...) {}
}