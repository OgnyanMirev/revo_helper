
#include "ntddk.h"
#define SIOCTL_TYPE 40000
#define IOCTL_GETPROCESS\
 CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)

const WCHAR deviceNameBuffer[] = L"\\Device\\RevoDetector";
const WCHAR deviceSymLinkBuffer[] = L"\\DosDevices\\RevoDetector";

HANDLE ProcessId, ParentProcessId;
BOOLEAN NewProcess = FALSE;
char CommandLine[8192];
size_t CommandLineSize = 0;
KEVENT ProcessCreatedEvent;
FAST_MUTEX NotificationSync;
PDEVICE_OBJECT g_MyDevice; // Global pointer to our device object


void SetCommandLine(PVOID buffer, size_t buffer_size)
{
	if (buffer_size > 8192) buffer_size = 8192;

	RtlZeroMemory(CommandLine, 8192);
	RtlCopyMemory(CommandLine, buffer, buffer_size);
	CommandLineSize = buffer_size;
}

void CreateProcessNotify(PEPROCESS Process, HANDLE ProcessId_, PPS_CREATE_NOTIFY_INFO CNInfo)
{
	UNREFERENCED_PARAMETER(Process);
	// We need to synchronize this part of the code since multiple processes may be created at the same time
	// We need to further synchronize this writing thread with the reading thread from the Helper
	ExAcquireFastMutex(&NotificationSync);

	ProcessId = ProcessId_;

	if (!CNInfo)
		ParentProcessId = 0;
	else if (CNInfo->CommandLine)
	{
		ParentProcessId = CNInfo->ParentProcessId;
		SetCommandLine(CNInfo->CommandLine->Buffer, CNInfo->CommandLine->Length);
	}
	
	KeSetEvent(&ProcessCreatedEvent, 0, FALSE);
	ExReleaseFastMutex(&NotificationSync);
}


VOID OnUnload(IN PDRIVER_OBJECT pDriverObject)
{
	UNICODE_STRING symLink;

	RtlInitUnicodeString(&symLink, deviceSymLinkBuffer);

	PsSetCreateProcessNotifyRoutineEx(CreateProcessNotify, TRUE);
	IoDeleteSymbolicLink(&symLink);
	IoDeleteDevice(pDriverObject->DeviceObject);
}

NTSTATUS Function_IRP_MJ_CREATE(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);
	
	//DbgPrint("IRP MJ CREATE received.");
	return STATUS_SUCCESS;
}

NTSTATUS Function_IRP_MJ_CLOSE(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);
	//DbgPrint("IRP MJ CLOSE received.");
	return STATUS_SUCCESS;
}
NTSTATUS Function_IRP_DEVICE_CONTROL(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);

	PIO_STACK_LOCATION pIoStackLocation;
	char* pBuf = Irp->AssociatedIrp.SystemBuffer;
	NTSTATUS status = STATUS_SUCCESS;
	size_t BytesCopied = 0;

	pIoStackLocation = IoGetCurrentIrpStackLocation(Irp);

	KeWaitForSingleObject(&ProcessCreatedEvent, UserRequest, KernelMode, TRUE, NULL);

	ExAcquireFastMutex(&NotificationSync);
	if (CommandLineSize)
	{
		// Write the PID to the first 4/8 bytes
		RtlCopyMemory(pBuf, &ProcessId, sizeof(HANDLE));
		BytesCopied += sizeof(HANDLE);
		pBuf += sizeof(HANDLE);

		// Write the ParentPID to the second 4/8 bytes
		RtlCopyMemory(pBuf, &ParentProcessId, sizeof(HANDLE));
		BytesCopied += sizeof(HANDLE);
		pBuf += sizeof(HANDLE);

		RtlCopyMemory(pBuf, CommandLine, CommandLineSize);
		BytesCopied += CommandLineSize;
		CommandLineSize = 0;
	}

	KeClearEvent(&ProcessCreatedEvent);
	ExReleaseFastMutex(&NotificationSync);


	// Finish the I/O operation by simply completing the packet and returning
	// the same status as in the packet itself.
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = BytesCopied;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return status;
}


NTSTATUS DriverEntry(IN PDRIVER_OBJECT pDriverObject,
					 IN PUNICODE_STRING pRegistryPath)
{
	UNREFERENCED_PARAMETER(pRegistryPath);

	NTSTATUS ntStatus = 0;
	UNICODE_STRING deviceNameUnicodeString, deviceSymLinkUnicodeString;

	// Normalize name and symbolic link.
	RtlInitUnicodeString(&deviceNameUnicodeString,
						 deviceNameBuffer);
	RtlInitUnicodeString(&deviceSymLinkUnicodeString,
						 deviceSymLinkBuffer);

	// Create the device.
	ntStatus = IoCreateDevice(pDriverObject,
							  0, // For driver extension
							  &deviceNameUnicodeString,
							  FILE_DEVICE_UNKNOWN,
							  FILE_DEVICE_UNKNOWN,
							  FALSE,
							  &g_MyDevice);

	// Create the symbolic link
	ntStatus = IoCreateSymbolicLink(&deviceSymLinkUnicodeString,
									&deviceNameUnicodeString);

	pDriverObject->DriverUnload = OnUnload;
	pDriverObject->MajorFunction[IRP_MJ_CREATE] = Function_IRP_MJ_CREATE;
	pDriverObject->MajorFunction[IRP_MJ_CLOSE] = Function_IRP_MJ_CLOSE;
	pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = Function_IRP_DEVICE_CONTROL;

	KeInitializeEvent(&ProcessCreatedEvent, NotificationEvent, FALSE);
	ExInitializeFastMutex(&NotificationSync);
	PsSetCreateProcessNotifyRoutineEx(CreateProcessNotify, FALSE);
	return STATUS_SUCCESS;
}
