
#include "ntddk.h"

typedef UINT32 DWORD;
#define SIOCTL_TYPE 40000
#define IOCTL_GETPROCESS\
 CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)

const WCHAR deviceNameBuffer[] = L"\\Device\\RevoDetector";
const WCHAR deviceSymLinkBuffer[] = L"\\DosDevices\\RevoDetector";

KEVENT ProcessCreatedEvent;
KGUARDED_MUTEX NotificationSync;
PDEVICE_OBJECT g_MyDevice; // Global pointer to our device object

#define OUTPUT_BUFFER_MAX_SIZE 8192


PUCHAR OutputBuffer;
unsigned BytesCopied;
INT Read = FALSE;

void CreateProcessNotify(PEPROCESS Process, HANDLE ProcessId_, PPS_CREATE_NOTIFY_INFO CNInfo)
{
	UNREFERENCED_PARAMETER(Process);
	if (!Read) return;


	// Since any thread in the system can create a process
    // we need to synchronize creations to avoid threads writing over each other
	KeAcquireGuardedMutex(&NotificationSync);


	if (CNInfo && CNInfo->CommandLine && CNInfo->CommandLine->Length <= (OUTPUT_BUFFER_MAX_SIZE - sizeof(DWORD) * 2))
	{
		DWORD PID = (DWORD)ProcessId_;
		memcpy(OutputBuffer, &PID, sizeof(DWORD));

		PID = (DWORD)CNInfo->ParentProcessId;

		memcpy(OutputBuffer + sizeof(DWORD), &PID, sizeof(HANDLE));
		
		memcpy(OutputBuffer + (sizeof(DWORD)*2), CNInfo->CommandLine->Buffer, CNInfo->CommandLine->Length);
		BytesCopied = CNInfo->CommandLine->Length + sizeof(DWORD) * 2;
		Read = FALSE;
		KeSetEvent(&ProcessCreatedEvent, 0, FALSE);
	}

	KeReleaseGuardedMutex(&NotificationSync);
}


VOID OnUnload(IN PDRIVER_OBJECT pDriverObject)
{
	UNICODE_STRING symLink;

	RtlInitUnicodeString(&symLink, deviceSymLinkBuffer);

	PsSetCreateProcessNotifyRoutineEx(CreateProcessNotify, TRUE);

	ExFreePool(OutputBuffer);
	IoDeleteSymbolicLink(&symLink);
	IoDeleteDevice(pDriverObject->DeviceObject);
}

NTSTATUS Function_IRP_MJ_CREATE(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);

	//DbgPrint("IRP MJ CREATE received.");
	return STATUS_SUCCESS;
}

NTSTATUS Function_IRP_MJ_CLOSE(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);
	//DbgPrint("IRP MJ CLOSE received.");
	return STATUS_SUCCESS;
}
NTSTATUS Function_IRP_DEVICE_CONTROL(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	PIO_STACK_LOCATION pIoStackLocation;
	pIoStackLocation = IoGetCurrentIrpStackLocation(Irp);

	Read = TRUE;
	KeClearEvent(&ProcessCreatedEvent);
	KeWaitForSingleObject(&ProcessCreatedEvent, UserRequest, KernelMode, TRUE, NULL);

	memcpy(Irp->AssociatedIrp.SystemBuffer, OutputBuffer, BytesCopied);
	// Finish the I/O operation by simply completing the packet and returning
	// the same status as in the packet itself.
	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = BytesCopied;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}


NTSTATUS DriverEntry(IN PDRIVER_OBJECT pDriverObject,
					 IN PUNICODE_STRING pRegistryPath)
{
	UNREFERENCED_PARAMETER(pRegistryPath);

	NTSTATUS ntStatus = 0;
	UNICODE_STRING deviceNameUnicodeString, deviceSymLinkUnicodeString;

	// Normalize name and symbolic link.
	RtlInitUnicodeString(&deviceNameUnicodeString,
						 deviceNameBuffer);
	RtlInitUnicodeString(&deviceSymLinkUnicodeString,
						 deviceSymLinkBuffer);

	// Create the device.
	ntStatus = IoCreateDevice(pDriverObject,
							  0, // For driver extension
							  &deviceNameUnicodeString,
							  FILE_DEVICE_UNKNOWN,
							  FILE_DEVICE_UNKNOWN,
							  FALSE,
							  &g_MyDevice);

	// Create the symbolic link
	ntStatus = IoCreateSymbolicLink(&deviceSymLinkUnicodeString,
									&deviceNameUnicodeString);
	pDriverObject->DriverUnload = OnUnload;
	pDriverObject->MajorFunction[IRP_MJ_CREATE] = Function_IRP_MJ_CREATE;
	pDriverObject->MajorFunction[IRP_MJ_CLOSE] = Function_IRP_MJ_CLOSE;
	pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = Function_IRP_DEVICE_CONTROL;

	Read = FALSE;
	BytesCopied = 0;
	OutputBuffer = ExAllocatePool(PagedPool, OUTPUT_BUFFER_MAX_SIZE);

	if (OutputBuffer)
	{
		KeInitializeEvent(&ProcessCreatedEvent, NotificationEvent, FALSE);
		KeSetEvent(&ProcessCreatedEvent, 0, 0);
		KeInitializeGuardedMutex(&NotificationSync);
		PsSetCreateProcessNotifyRoutineEx(CreateProcessNotify, FALSE);
	}

	return STATUS_SUCCESS;
}
