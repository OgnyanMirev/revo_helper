#include "stdafx.h"
#include "UninstallerUtils.h"
#include "Registry.h"
#include "Utils.h"


#include <shlwapi.h>
#include <Sddl.h>
#include <Softpub.h>
#include <wincrypt.h>
#include <wintrust.h>


// Link with the Wintrust.lib file.
#pragma comment (lib, "wintrust")


//extern CEvent g_eMainWindowClosing;
//extern CEvent g_eClosingUninstaller;
//extern CCriticalSection g_csUninstaller;

int g_nCalls = 0;

void CheckGroupingMode(FILETIME* ftCalcTimeToCompare, BOOL* bOldNewByDays)
{

	DWORD dwNewProgramsType = 0;
	DWORD dwNewDays = 7;

	if (VSGetSettings(CAT_UNINSTALLER, VAL_NEWPROGRAMSTYPE, dwNewProgramsType) == FALSE)
		VSSetSettings(CAT_UNINSTALLER, VAL_NEWPROGRAMSTYPE, dwNewProgramsType);

	if (dwNewProgramsType == 0)
	{
		*bOldNewByDays = TRUE;

		if (VSGetSettings(CAT_UNINSTALLER, VAL_NEWIFDAYS, dwNewDays) == FALSE)
			VSSetSettings(CAT_UNINSTALLER, VAL_NEWIFDAYS, dwNewDays);
	}
	else
	{
		*bOldNewByDays = FALSE;
		return;
	}

	FILETIME stCurrentTime;
	GetSystemTimeAsFileTime(&stCurrentTime);
	SYSTEMTIME stUTCReg, stLocalReg;
	FileTimeToSystemTime(&stCurrentTime, &stUTCReg);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTCReg, &stLocalReg);
	SystemTimeToFileTime(&stLocalReg, &stCurrentTime);


	if (bOldNewByDays)//if check for new date is based on days
	{
		ULONGLONG qwResult;

		// Copy the time into a quadword.
		qwResult = (((ULONGLONG)stCurrentTime.dwHighDateTime) << 32) + stCurrentTime.dwLowDateTime;

		// remove days.
		qwResult -= dwNewDays * _DAY;

		// Copy the result back into the FILETIME structure.
		stCurrentTime.dwLowDateTime = (DWORD)(qwResult & 0xFFFFFFFF);
		stCurrentTime.dwHighDateTime = (DWORD)(qwResult >> 32);
	}
	//If the check for 
	*ftCalcTimeToCompare = stCurrentTime;
}

BOOL SearchForMSIIconPath(HKEY hRoot, LPCTSTR strFullKey, CString* strRetIconPath, int* nIconIndex, REGSAM flagsOpen)
{

	CRegistry regMSIData(hRoot);
	if (regMSIData.Open(strFullKey, flagsOpen) == FALSE)
		return FALSE;


	CString strIconPath;
	if (regMSIData.GetStringValue(TEXT("ProductIcon"), strIconPath) == TRUE)
	{
		///If the icon has an index in the file try to get the correct icon
		int nIconPos = strIconPath.Find(TEXT(','));
		if (nIconPos != -1)
		{
			CString strIndex = strIconPath.Mid(nIconPos + 1, 1);
			(*nIconIndex) = _ttoi(strIndex);
			CString strNewDisplayIcon = strIconPath.Left(nIconPos);
			//hExtIcon = ::ExtractIcon(AfxGetInstanceHandle( ), strNewDisplayIcon, nIconIndex);
			*strRetIconPath = strNewDisplayIcon;

		}
		else
			*strRetIconPath = strIconPath;
		//	hExtIcon = ::ExtractIcon(AfxGetInstanceHandle( ),strDisplayIcon,0);
	}
	else
		return FALSE;

	return TRUE;

}
BOOL GetMSIAppIcon(CString& strAppKey, CString* strRetIconPath, int* nIconIndex, REGSAM flagsOpen)
{

	HKEY hRoot = HKEY_CLASSES_ROOT;
	CString strKeyPath = TEXT("Installer\\Products\\") + strAppKey;
	if (SearchForMSIIconPath(hRoot, strKeyPath, strRetIconPath, nIconIndex, flagsOpen) == TRUE)
		return TRUE;
	else
	{
		hRoot = HKEY_CURRENT_USER;
		strKeyPath.Empty();
		strKeyPath = TEXT("SOFTWARE\\Microsoft\\Installer\\Products\\") + strAppKey;
		if (SearchForMSIIconPath(hRoot, strKeyPath, strRetIconPath, nIconIndex, flagsOpen) == TRUE)
			return TRUE;
	}

	return FALSE;
}

void GetArpCachePath(LPCTSTR pszAppKeyName, CString& srtPathExe, REGSAM flagsOpen)
{
	CRegistry regArpCache(HKEY_LOCAL_MACHINE);
	CString strARPCache = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Management\\ARPCache\\");
	strARPCache += pszAppKeyName;
	if (regArpCache.Open(strARPCache, flagsOpen) == FALSE)
		return;
	else
	{
		TCHAR	szArpCacheData[1024] = { 0 };
		DWORD	lpcKey = sizeof(szArpCacheData);
		if (::RegQueryValueEx(regArpCache.GetHandle(), TEXT("SlowInfoCache"), NULL, NULL, (LPBYTE)(szArpCacheData), &lpcKey) == ERROR_SUCCESS)
		{
			//TCHAR szInfo[4096] = {0};
			CString temp = szArpCacheData + 14;
			GetPathFromString(temp, srtPathExe);
			//if(szTempRezult)
			//	_tcscpy_s(szInfo,4096,szTempRezult);
			//delete [] szTempRezult;
			//srtPathExe = szInfo;
			return;
		}
	}

}

/**Read application properties from the MSI section or Uninstall section in the Registry
*
* \param hkeyRoot - Registry root of the strFullKey that is to be opened
* \param strFullKey - Full path to MSI Section in the Registry SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData
* \param strConvertedKeyName - converted keyname from GUID to number
* \param strOriginKeyName - original keyname from Uninstall section as a GUID
* \param IsMSI - is the application MSI or not
* \return TRUE if the data have been get successfully
*		   FALSE otherwise
*/
BOOL ReadAppProps(HKEY hkeyRoot, LPCTSTR strFullKey, LPCTSTR strConvertedKeyName, LPCTSTR strOriginKeyName, BOOL IsMSI, AppList& listCachedApps, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags)
{

	BOOL bJustUpdated = FALSE;

	BOOL bIsProg64Bit = FALSE;
	if (dwFlags == KEY_WOW64_64KEY)
		bIsProg64Bit = TRUE;

	DWORD dwCheckFastLM = 0;
#ifdef PORTABLE
	if (VSGetSettings(CAT_PORTABILITY, VAL_PORT_FASTLOADMODE, dwCheckFastLM) == FALSE)
		VSSetSettings(CAT_PORTABILITY, VAL_PORT_FASTLOADMODE, dwCheckFastLM);
#endif
	BOOL bReadingMSISection = FALSE;
	CString strFullPath = strFullKey;
	if (strFullPath.Find(TEXT("\\Installer\\UserData\\")) != -1)
		bReadingMSISection = TRUE;


	REGSAM flagsOpen = KEY_READ | dwFlags;

	auto pAppProps = make_shared<CInstalledAppData>();

	CRegistry regAppData(hkeyRoot);
	if (regAppData.Open(strFullKey, flagsOpen) == FALSE)
		return FALSE;

	CString strDisplayName;
	CString strUninstallString = TEXT("");

	//////Get Uninstall String
	if (regAppData.GetStringValue(TEXT("UninstallString"), strUninstallString) == TRUE)
	{
		if (IsRealString(strUninstallString) == FALSE)
		{
			if (IsMSI == TRUE)
			{
				strUninstallString += TEXT("MsiExec.exe /X");
				strUninstallString += strOriginKeyName;
			}
			else
				return FALSE;
		}
	}
	else
	{
		if (IsMSI == TRUE)
		{
			strUninstallString += TEXT("MsiExec.exe /X");
			strUninstallString += strOriginKeyName;
		}
		else
			return FALSE;
	}

	//FILETIME ftRegApp = regAppData.GetLastModifiedDate();// get the date of the kay ofthe current app
	//													 //////Get Installation Date !!!
	CString strRegInstallDate;

	TCHAR szString[MAX_PATH] = { 0 };
	FILETIME ftLastWriteTimeInfo;

	DWORD dwUseInstallDate = 0;
	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_USEREGINSTALLDATE, dwUseInstallDate) == FALSE)
		VSSetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_USEREGINSTALLDATE, dwUseInstallDate);
	if (dwUseInstallDate)
	{
		if (regAppData.GetStringValue(TEXT("InstallDate"), strRegInstallDate))
		{
			int nDay = 0, nMonth = 0, nYear = 0;
			nYear = _ttoi(strRegInstallDate.Left(4));
			nMonth = _ttoi(strRegInstallDate.Mid(4, 2));
			nDay = _ttoi(strRegInstallDate.Right(2));

			COleDateTime timeRegInstallDate(nYear, nMonth, nDay, 0, 0, 0);
			if (timeRegInstallDate.GetStatus() != 0)
			{
				ftLastWriteTimeInfo = regAppData.GetLastModifiedDate();
			}
			else
			{
				SYSTEMTIME stimeInstallDate;
				timeRegInstallDate.GetAsSystemTime(stimeInstallDate);
				if (IsValidTime(&stimeInstallDate) == FALSE)
					ftLastWriteTimeInfo = regAppData.GetLastModifiedDate();
				else
					SystemTimeToFileTime(&stimeInstallDate, &ftLastWriteTimeInfo);
				//ftLastWriteTimeInfo =
			}


		}
		else
			ftLastWriteTimeInfo = regAppData.GetLastModifiedDate();
	}
	else
		ftLastWriteTimeInfo = regAppData.GetLastModifiedDate();
	///////Get Display Name
	if (regAppData.GetStringValue(TEXT("DisplayName"), strDisplayName) == TRUE)
	{

		//if(strDisplayName.Find(TEXT("Adobe Reader")) != -1)
		//	int a = 9;

		CString strVersion;
		regAppData.GetStringValue(TEXT("DisplayVersion"), strVersion);

		if (bReadingMSISection) // to avoid duplication of an app already readed and listed from the regular Uninstll section - Skype, Adobre Reader XI
			IsMSIProgramInList(strDisplayName, listCachedApps, strVersion);

		if (IsStringGUID((LPCTSTR)strDisplayName) == TRUE)
			return FALSE;
		if (IsRealString(strDisplayName) == FALSE)/// if the string is empty or only with spaces get the Key name as a DisplayName
		{
			if (IsMSI == FALSE)
			{
				if (IsStringGUID(strOriginKeyName) == FALSE)
					strDisplayName = strOriginKeyName;
				else
					return FALSE;
			}
			else
				return FALSE;
		}

		if (bCheckingForNewApps == TRUE) // if we are comparing cache and Registry to find new or updated progs
		{
			auto pFoundApp = FoundKeyInList(strFullKey, listCachedApps, bIsProg64Bit, hkeyRoot);

			if (pFoundApp != NULL)
			{
				if (pFoundApp->statMatchtoReg != only_in_cache)
					return TRUE;

				if (bOldNewByDays)
				{

					if ((pFoundApp->nAddedtoGroup == 0) || (pFoundApp->nAddedtoGroup == 100000))
					{
						if ((CompareFileTime(&ftLastWriteTimeInfo, &ftCalcTimeToCompare) == 1)
							|| (CompareFileTime(&ftLastWriteTimeInfo, &ftCalcTimeToCompare) == 0))
							pFoundApp->nAddedtoGroup = 0;
						else
							pFoundApp->nAddedtoGroup = 100000;
					}
				}
				else
				{
					if ((pFoundApp->nAddedtoGroup == 0) || (pFoundApp->nAddedtoGroup == 100000))
						pFoundApp->nAddedtoGroup = 100000;
				}

				//if the dates of the entry in the list and the real key in the Registry are the same - the program is not modified
				if (CompareFileTime(&(pFoundApp->ftLastWriteTime), &ftLastWriteTimeInfo) == 0)
				{
					pFoundApp->statMatchtoReg = existing_everywhere;
					return TRUE;
				}
				else // the Registry entry is modified so the program is updated
				{
					pAppProps = pFoundApp;
					bJustUpdated = TRUE;
				}

			}//if(pFoundApp != NULL)

		}//(bCheckingForNewApps == TRUE)
		else
		{
			auto pFoundApp = FoundKeyInList(strFullKey, listCachedApps, bIsProg64Bit, hkeyRoot);
			if (pFoundApp != NULL)
			{
				if (pFoundApp->statMatchtoReg != only_in_cache)
					return TRUE;
			}

		}

		if (bJustUpdated == FALSE)
		{
			CString strVersion;
			regAppData.GetStringValue(TEXT("DisplayVersion"), strVersion);

			DWORD dwIsSysComp = 0;
			BOOL bSysComp = FALSE;
			if (regAppData.GetDoubleWordValue(TEXT("SystemComponent"), dwIsSysComp) == TRUE)
			{
				if (dwIsSysComp == 1)
					bSysComp = TRUE;
				else
					bSysComp = FALSE;
			}
			else
				bSysComp = FALSE;

			if (IsProgramInList(strDisplayName, listCachedApps, strVersion, bSysComp, bIsProg64Bit, bReadingMSISection))
				return TRUE;
		}
	} //if(regAppData.GetStringValue(TEXT("DisplayName"),strDisplayName) == TRUE)
	else
		return FALSE;
	pAppProps->statMatchtoReg = new_app;

	if (bJustUpdated == FALSE)
	{
		if (bOldNewByDays)
		{
			if ((CompareFileTime(&ftLastWriteTimeInfo, &ftCalcTimeToCompare) == 1) || (CompareFileTime(&ftLastWriteTimeInfo, &ftCalcTimeToCompare) == 0))
				pAppProps->nAddedtoGroup = 0;
			else
				pAppProps->nAddedtoGroup = 100000;
		}
		else
			pAppProps->nAddedtoGroup = 0;
	}

	/////////Set Key Name	
	pAppProps->strKeyName = strOriginKeyName;

	///////Get Root Name	
	regAppData.GetRegistryName(pAppProps->strRoot);

	pAppProps->strFullKeyName = strFullKey;
	pAppProps->strAppName = strDisplayName;
	pAppProps->strUninstString = strUninstallString;
	CString strUninstallPathOnly;

	GetPathFromString(strUninstallString, strUninstallPathOnly);
	if (strUninstallPathOnly.Right(8).CompareNoCase(TEXT("ctor.dll")) == 0)
		strUninstallPathOnly.Empty();
	//TCHAR* pszUninstallPathOnly = strUninstallString.GetBuffer();
	//MessageBox(::AfxGetMainWnd()->GetSafeHwnd(),strDisplayName,pszUninstallPathOnly,MB_OK);


	///////Get Display Icon
	if (dwCheckFastLM == 0)
	{
		if ((bJustUpdated == FALSE) || (pAppProps->strIconPath.IsEmpty()) || (::PathFileExists(pAppProps->strIconPath) == FALSE))
		{

			CString strDisplayIcon;
			int nIconIndex = 0;
			if (IsMSI == TRUE)
			{
				CString strConvertedKeyNameTemp = strConvertedKeyName;
				GetMSIAppIcon(strConvertedKeyNameTemp, &strDisplayIcon, &nIconIndex, flagsOpen);
				TCHAR szExpandedIconPath[4096] = { 0 };
				ExpandEnvironmentStrings(strDisplayIcon, szExpandedIconPath, 4096);
				strDisplayIcon = szExpandedIconPath;
			}
			if (strDisplayIcon.IsEmpty() || (::PathFileExists(strDisplayIcon) == FALSE))
			{
				if (regAppData.GetStringValue(TEXT("DisplayIcon"), strDisplayIcon) == TRUE)
				{
					///If the icon has an index in the file try to get the correct icon
					int nIconPos = strDisplayIcon.Find(TEXT(','));
					if (nIconPos != -1)
					{
						CString strIndex = strDisplayIcon.Mid(nIconPos + 1, 1);
						nIconIndex = _ttoi(strIndex);
						CString strNewDisplayIcon = strDisplayIcon.Left(nIconPos);
						//hExtIcon = ::ExtractIcon(AfxGetInstanceHandle( ), strNewDisplayIcon, nIconIndex);
						strDisplayIcon = strNewDisplayIcon;

					}
				}
				else
				{
					strDisplayIcon = strUninstallPathOnly;
				}
			}
			::PathUnquoteSpaces(strDisplayIcon.GetBuffer());
			strDisplayIcon.ReleaseBuffer();

			if ((strDisplayIcon.IsEmpty()) || (::PathFileExists(strDisplayIcon) == FALSE))
			{
				GetArpCachePath(strOriginKeyName, strDisplayIcon, flagsOpen);
				nIconIndex = 0;
			}

			TCHAR szExpandedIconPath[4096] = { 0 };
			ExpandEnvironmentStrings(strDisplayIcon, szExpandedIconPath, 4096);
			strDisplayIcon = szExpandedIconPath;

			pAppProps->strIconPath = strDisplayIcon;
			pAppProps->nIconIndex = nIconIndex;
		}
	}


	//////Get DisplayVersion
	CString strVersion;
	regAppData.GetStringValue(TEXT("DisplayVersion"), strVersion);
	pAppProps->strVersion = strVersion;

	/////Get InstallLocation
	CString strInstallLoc, strAltInstallLoc, strIconPath, strOrigInstLocValue;
	BOOL bUseAsInstallLoc = FALSE;
	regAppData.GetStringValue(TEXT("InstallLocation"), strInstallLoc);

	if (strInstallLoc.IsEmpty() == FALSE)// Check if the installloation value is file no folder as in iobit uninstaller 3.0
	{
		strOrigInstLocValue = strInstallLoc;
		if (VSFileExists(strInstallLoc))
		{
			TCHAR szTmp[8192] = { 0 };
			StringCchCopy(szTmp, 8192, strInstallLoc);
			::PathRemoveFileSpec(szTmp);
			strInstallLoc = szTmp;
		}

	}

	if ((strInstallLoc.IsEmpty() == FALSE) && (IsFolderAllowedForSearchBasic(strInstallLoc) == FALSE))
		strInstallLoc.Empty();

	if (dwCheckFastLM == 0)
	{
		if ((strInstallLoc.IsEmpty()) || (pAppProps->strIconPath.IsEmpty()))
			AltSearchForInstallLoc(pAppProps->strAppName, strIconPath, strAltInstallLoc, bUseAsInstallLoc);
	}

	if ((pAppProps->strIconPath.IsEmpty()) && (strIconPath.IsEmpty() == FALSE))
		pAppProps->strIconPath = strIconPath;

	if ((strInstallLoc.IsEmpty()) && (strAltInstallLoc.IsEmpty() == FALSE) && (bUseAsInstallLoc))
	{
		if (IsFolderAllowedForSearch(strAltInstallLoc))
			pAppProps->strInstallLocation = strAltInstallLoc;
		else
			strAltInstallLoc.Empty();
	}
	else
		pAppProps->strInstallLocation = strInstallLoc;

	if (bUseAsInstallLoc == FALSE)
		strAltInstallLoc.Empty();
	//////Get the calculated Size or EstimatedSize
	TCHAR	pszSize[64] = { 0 };
	DWORD64 dxFolderSize = 0;
	GetProgramDirSize(regAppData, strOriginKeyName, strUninstallPathOnly, strAltInstallLoc, &dxFolderSize, flagsOpen);
	//strUninstallString.ReleaseBuffer();
	pAppProps->nSize = dxFolderSize;
	if ((dxFolderSize <= 0) || (dxFolderSize >1099511627776))
		_tcsnset(pszSize, 0, lstrlen(pszSize));
	else
		StrFormatMBSize(dxFolderSize, pszSize, sizeof(pszSize) / sizeof(TCHAR));

	pAppProps->strOurSize = pszSize;

	//-----

	//////Get Installation Date !!!
	SYSTEMTIME stUTC, stLocal;
	CString strDate;

	BOOL res = FileTimeToSystemTime(&ftLastWriteTimeInfo, &stUTC);
	if (res == FALSE)
	{
		FILETIME stCurrentTime;
		GetSystemTimeAsFileTime(&stCurrentTime);
		FileTimeToSystemTime(&stCurrentTime, &stUTC);
		ftLastWriteTimeInfo = stCurrentTime;
	}
	else
	{
		if (IsValidTime(&stUTC) == FALSE)
		{
			FILETIME stCurrentTime;
			GetSystemTimeAsFileTime(&stCurrentTime);
			FileTimeToSystemTime(&stCurrentTime, &stUTC);
			ftLastWriteTimeInfo = stCurrentTime;
		}
	}

	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	if (IsValidTime(&stLocal) == FALSE)
	{
		FILETIME stCurrentTime;
		GetSystemTimeAsFileTime(&stCurrentTime);
		FileTimeToSystemTime(&stCurrentTime, &stUTC);
		SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);
		ftLastWriteTimeInfo = stCurrentTime;
	}

	_stprintf_s(szString, MAX_PATH, TEXT("%02d.%02d.%d"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear);

	pAppProps->strDate = szString;
	pAppProps->ftLastWriteTime = ftLastWriteTimeInfo;
	//---------

	//////Get Publisher !!!
	CString strPublisher;
	regAppData.GetStringValue(TEXT("Publisher"), strPublisher);
	pAppProps->strPublisher = strPublisher;

	//////Get HelpLink!!!
	CString strHelpLink;
	regAppData.GetStringValue(TEXT("HelpLink"), strHelpLink);
	pAppProps->strHelpLink = strHelpLink;

	//////Get Comments !!!
	CString strComments;
	regAppData.GetStringValue(TEXT("Comments"), strComments);
	pAppProps->strComment = strComments;

	//////Get is update(has parent key name)
	CString strParentName;
	if (regAppData.GetStringValue(TEXT("ParentKeyName"), strParentName) == TRUE)
	{
		if (strParentName.IsEmpty() == FALSE)
			pAppProps->bHasParentKeyName = TRUE;
		else
			pAppProps->bHasParentKeyName = FALSE;

	}
	else
		pAppProps->bHasParentKeyName = FALSE;

	//////Get is system component
	DWORD dwIsSysComp = 0;
	if (regAppData.GetDoubleWordValue(TEXT("SystemComponent"), dwIsSysComp) == TRUE)
	{
		if (dwIsSysComp == 1)
			pAppProps->bIsSystemComponent = TRUE;
		else
			pAppProps->bIsSystemComponent = FALSE;
	}
	else
		pAppProps->bIsSystemComponent = FALSE;

	////Set 64 bit flag
	if (dwFlags == KEY_WOW64_64KEY)
		pAppProps->b64Bit = TRUE;
	else
		pAppProps->b64Bit = FALSE;

	//if(Is64BitWindows() && (strOrigInstLocValue.IsEmpty() == FALSE))
	//{
	//	
	//	if(VerifyProgramBits(strOrigInstLocValue,bIsProg64Bit)== FALSE)
	//		pAppProps->b64Bit = bIsProg64Bit;
	//		
	//}

	if (IsMSI == TRUE)
		pAppProps->bIsMSI = TRUE;
	else
		pAppProps->bIsMSI = FALSE;

	if (bJustUpdated == FALSE)
		listCachedApps.push_back(pAppProps);

	return TRUE;

}

/**If the application is an MSI based then that function is going to try to get all data from MSI sections
* in the Registry.
*
* \param strSubKey - the MSI key for the section that is going to be read
*/
void ReadMSISections(CString& strSubKey, AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags, CTime* arrCacheTimes, BOOL bAllUserSection, BOOL bForcedRefresh)
{
	if (bForcedRefresh)
		bCheckingForNewApps = FALSE;

	BOOL bIs64Bit = FALSE;
	if (dwFlags == KEY_WOW64_64KEY)
		bIs64Bit = TRUE;

	CTime tReadedFromSubKeys;

	CRegistry regMSIRoot(HKEY_LOCAL_MACHINE);
	if (regMSIRoot.Open(strSubKey, KEY_READ | dwFlags) == TRUE)
	{
		CTime timeKey(regMSIRoot.GetLastModifiedDate());
		if (bCheckingForNewApps)
		{
			CTime timeCacheRead;
			if (bAllUserSection)
				timeCacheRead = arrCacheTimes[2];
			else
				timeCacheRead = arrCacheTimes[3];


			if (timeKey <= timeCacheRead)
			{
				//Check if there is a key with a newer date that the parent key, if so we need it to replace our in the cache and to update the data
				if (CheckSubKeysModDates(HKEY_LOCAL_MACHINE, timeCacheRead, strSubKey, dwFlags, tReadedFromSubKeys) == FALSE)
				{
					CString strRoot;
					regMSIRoot.GetRegistryName(strRoot);
					//MarkCacheItemsAsExisting(listCachedApps,strRoot,strSubKey);
					UpdateItemStatus(listCachedApps, strRoot, strSubKey, ftCalcTimeToCompare, bOldNewByDays, bIs64Bit);
					return;
				}
				else
					timeKey = tReadedFromSubKeys;
			}
		}
		if (bAllUserSection)
			arrCacheTimes[2] = timeKey;
		else
			arrCacheTimes[3] = timeKey;
		for (DWORD index = 0; index < regMSIRoot.GetNumberOfSubkeys(); index++)
		{
			CString strName, strClassName;

			/*if ((::WaitForSingleObject(g_eMainWindowClosing, 0) == WAIT_OBJECT_0)
				|| (::WaitForSingleObject(g_eClosingUninstaller, 0) == WAIT_OBJECT_0))
				return;*/

			if (regMSIRoot.EnumerateKeys(index, strName, strClassName) == TRUE)
			{

				CString strFullMISAppPath = strSubKey + strName + TEXT("\\InstallProperties");
				CString strGUIDName;
				TCHAR szCoverted[128] = { 0 };
				if (strName.GetLength() != 32)
					continue;
				unsquash_guid(strName, szCoverted);
				strGUIDName = szCoverted;

				if (bForcedRefresh)
					bCheckingForNewApps = TRUE;

				ReadAppProps(HKEY_LOCAL_MACHINE, strFullMISAppPath, strName, strGUIDName, TRUE, listCachedApps, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays, dwFlags);

			}
		}
	}
}

void GetMSIAppData(AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags, CTime* arrCacheTimes, BOOL bForcedRefresh)
{
	CString strMSIAllUsers = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Products\\");
	CString strMSICurrentUsers = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\")
		+ GetCurrentUserSID() + TEXT("\\Products\\");

	ReadMSISections(strMSIAllUsers, listCachedApps, modTime, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays, dwFlags, arrCacheTimes, TRUE, bForcedRefresh);
	ReadMSISections(strMSICurrentUsers, listCachedApps, modTime, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays, dwFlags, arrCacheTimes, FALSE, bForcedRefresh);
}

BOOL GetUninstallSectionAppData(HKEY hRoot, AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags, CTime* arrCacheTimes, BOOL bForcedRefresh)
{
	REGSAM flagsOpen = KEY_READ | dwFlags;
	CRegistry regKey(hRoot);

	BOOL bIs64Bit = FALSE;
	if (dwFlags == KEY_WOW64_64KEY)
		bIs64Bit = TRUE;

	if (bForcedRefresh)
		bCheckingForNewApps = FALSE;

	CTime tReadedFromSubKeys;

	CString strUninstallKey = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");

	if (regKey.Open(strUninstallKey, flagsOpen) == TRUE)
	{
		CTime timeKey(regKey.GetLastModifiedDate());
		if (bCheckingForNewApps)
		{
			CTime timeCacheRead;
			if (hRoot == HKEY_LOCAL_MACHINE)
			{
				if (dwFlags == KEY_WOW64_32KEY)
					timeCacheRead = arrCacheTimes[0];
				else
					timeCacheRead = arrCacheTimes[4];
			}
			if (hRoot == HKEY_CURRENT_USER)
			{
				if (dwFlags == KEY_WOW64_32KEY)
					timeCacheRead = arrCacheTimes[1];
				else
					timeCacheRead = arrCacheTimes[5];
			}
			if (timeKey <= timeCacheRead)
			{
				//Check if there is a key with a newer date that the parent key, if so we need it to replace our in the cache and to update the data
				if (CheckSubKeysModDates(hRoot, timeCacheRead, strUninstallKey, dwFlags, tReadedFromSubKeys) == FALSE)
				{
					CString strRoot;
					regKey.GetRegistryName(strRoot);
					//MarkCacheItemsAsExisting(listCachedApps,strRoot,strUninstallKey);
					UpdateItemStatus(listCachedApps, strRoot, strUninstallKey, ftCalcTimeToCompare, bOldNewByDays, bIs64Bit);
					return TRUE;
				}
				else
					timeKey = tReadedFromSubKeys;
			}
		}

		if (hRoot == HKEY_LOCAL_MACHINE)
		{
			if (dwFlags == KEY_WOW64_32KEY)
				arrCacheTimes[0] = timeKey;
			else
				arrCacheTimes[4] = timeKey;
		}
		if (hRoot == HKEY_CURRENT_USER)
		{
			if (dwFlags == KEY_WOW64_32KEY)
				arrCacheTimes[1] = timeKey;
			else
				arrCacheTimes[5] = timeKey;
		}


		for (DWORD index = 0; index < regKey.GetNumberOfSubkeys(); index++)
		{
			CString strName, strClassName;

			/*if ((::WaitForSingleObject(g_eMainWindowClosing, 0) == WAIT_OBJECT_0)
				|| (::WaitForSingleObject(g_eClosingUninstaller, 0) == WAIT_OBJECT_0))
				return FALSE;*/

			if (regKey.EnumerateKeys(index, strName, strClassName) == TRUE)
			{

				CRegistry regUninstallAppKey(hRoot);
				CString strAppKey = strUninstallKey + TEXT("\\") + strName;
				CString strDisplayName, strUninstallString;

				if (regUninstallAppKey.Open(strAppKey, flagsOpen) == TRUE)
				{
					BOOL IsMSI = FALSE;
					DWORD dwIsWinInstaller = 0;
					regUninstallAppKey.GetDoubleWordValue(TEXT("WindowsInstaller"), dwIsWinInstaller);
					CString strConverted;
					if (dwIsWinInstaller == 1)
					{
						IsMSI = TRUE;
						TCHAR szConverted[128] = { 0 };
						squash_guid(strName, szConverted);
						strConverted = szConverted;
					}

					if (bForcedRefresh)
						bCheckingForNewApps = TRUE;

					ReadAppProps(hRoot, strAppKey, strConverted, strName, IsMSI, listCachedApps, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays, dwFlags);
				}

			}///if(EnumerateKeys(index,strName,strClassName) == TRUE)
		}///for(int index = 0; index < regKey.GetNumberOfSubkeys(); index++)
	} /// regKey.Open(strUninstallKey) == TRUE)
	else /// if(regKey.Open(strUninstallKey) == TRUE)
		return FALSE;

	return TRUE;
}
BOOL WriteCacheDataToFile(AppList& listCachedApps, CTime* arrCacheTimes, CIniData* pIniData, BOOL bClosing)
{
	CFile fileCacheData;
	CString strAppPath;
	GetLocalAppData(strAppPath);
	strAppPath += TEXT("\\data\\");
	//CreateDirectory(strAppPath,NULL);
	VSCreateDirectory(strAppPath);
	strAppPath += TEXT("cachedata.dat");

	CFileException peError;
	if (fileCacheData.Open(strAppPath, CFile::modeWrite | CFile::modeCreate, &peError)>0)
	{
		CArchive ar(&fileCacheData, CArchive::store);
		float fRUVer = RUP_VER;
		ar << fRUVer;
		ar << GetItemsToWrite(listCachedApps);
		for (int i = 0; i<4; i++)
			ar << arrCacheTimes[i];
		if (Is64BitWindows())
		{
			ar << arrCacheTimes[4];
			ar << arrCacheTimes[5];
		}
		for (auto& temp : listCachedApps)
		{
			//TRACE("Num:%d Start AppName-%s\n",temp->nAddedtoGroup,temp->strAppName);
			if (temp->statMatchtoReg != only_in_cache)
			{
				TRY
				{
					ar << temp->ftLastWriteTime.dwHighDateTime << temp->ftLastWriteTime.dwLowDateTime
					<< temp->nIconIndex << temp->strAppName << temp->strComment << temp->strDate << temp->strHelpLink
					<< temp->strIconPath << temp->strInstallLocation << temp->strKeyName << temp->strPublisher
					<< temp->strRoot << temp->strOurSize << temp->nSize << temp->strUninstString << temp->strVersion
					<< (int)temp->bHasParentKeyName << (int)temp->bIsSystemComponent
					<< temp->strFullKeyName << temp->nAddedtoGroup << temp->b64Bit << temp->bIsMSI;
				}
					CATCH(CFileException, pEx)
				{
					CString strText;
					pIniData->GetValue(IDS_ERRORNOTSPACECACHE, &strText);
					if ((pEx->m_cause == CFileException::diskFull) && (bClosing == FALSE))
						AfxMessageBox(strText);
					else
					{
						CString strText;
						pIniData->GetValue(IDS_ERRORWRITINGCACHE, &strText);

						TCHAR szError[1024];
						pEx->GetErrorMessage(szError, 1024);
						strText += TEXT("\n");
						strText += szError;
						AfxMessageBox(strText);
					}

					fileCacheData.Abort();
					CFile::Remove(strAppPath);
					ar.Abort();
					return FALSE;
				}
				END_CATCH
			}
		}
		TRY
		{
			ar.Close();
		}
			CATCH(CFileException, pEx)
		{
			CString strText;
			pIniData->GetValue(IDS_ERRORNOTSPACECACHE, &strText);
			if ((pEx->m_cause == CFileException::diskFull) && (bClosing == FALSE))
				AfxMessageBox(strText);
			else
			{
				CString strText;
				pIniData->GetValue(IDS_ERRORWRITINGCACHE, &strText);

				TCHAR szError[1024];
				pEx->GetErrorMessage(szError, 1024);
				strText += TEXT("\n");
				strText += szError;
				AfxMessageBox(strText);
			}
			fileCacheData.Abort();
			CFile::Remove(strAppPath);
			ar.Abort();
			return FALSE;
		}
		END_CATCH

	}
	else
	{
		CString strText;
		pIniData->GetValue(IDS_ERRORWRITINGCACHE, &strText);

		TCHAR szError[1024];
		peError.GetErrorMessage(szError, 1024);
		strText += TEXT("\n");
		strText += szError;
		AfxMessageBox(strText);
		fileCacheData.Abort();
		return FALSE;
	}


	return TRUE;
}

BOOL ReadCacheDataFromFile(AppList& listCachedApps, CTime* timeLastMod, CTime* arrCacheTimes, CIniData* pIniData)
{
	CFile fileCacheData;
	CString strAppPath;
	GetLocalAppData(strAppPath);
	strAppPath += TEXT("\\data\\cachedata.dat");

	_int64 n64FileSize = 0;
	CTime time;
	GetFileData(strAppPath, &n64FileSize, &time);

	if (n64FileSize == 0)
		return FALSE;

	//CFileException e;
	if (fileCacheData.Open(strAppPath, CFile::modeRead | CFile::modeNoTruncate)>0)
	{
		CFileStatus status;
		fileCacheData.GetStatus(status);
		*timeLastMod = status.m_mtime;
		CArchive ar(&fileCacheData, CArchive::load);
		int nAppCount = 0;
		float fRUVer = 0;
		TRY
		{
			ar >> fRUVer;
		ar >> nAppCount;
		for (int i = 0; i<4; i++)
			ar >> arrCacheTimes[i];
		if (Is64BitWindows())
		{
			ar >> arrCacheTimes[4];
			ar >> arrCacheTimes[5];
		}
		}
			CATCH(CException, pEx)
		{
			//CString strText;
			//pIniData->GetValue(IDS_ERRORREADINGCACHE,&strText);
			//AfxMessageBox(strText);
			listCachedApps.clear();
			fileCacheData.Abort();
			CFile::Remove(strAppPath);
			ar.Abort();
			return FALSE;
		}
		END_CATCH
			for (int i = 0; i<nAppCount; i++)
			{
				/*if ((::WaitForSingleObject(g_eMainWindowClosing, 0) == WAIT_OBJECT_0)
					|| (::WaitForSingleObject(g_eClosingUninstaller, 0) == WAIT_OBJECT_0))
				{
					listCachedApps.clear();
					return FALSE;
				}
*/
				auto temp = make_shared<CInstalledAppData>();
				if (fRUVer == 2.0)
				{
					TRY
					{
						ar >> temp->ftLastWriteTime.dwHighDateTime >> temp->ftLastWriteTime.dwLowDateTime
						>> temp->nIconIndex >> temp->strAppName >> temp->strComment >> temp->strDate >> temp->strHelpLink
						>> temp->strIconPath >> temp->strInstallLocation >> temp->strKeyName >> temp->strPublisher
						>> temp->strRoot >> temp->strOurSize >> temp->nSize >> temp->strUninstString >> temp->strVersion
						>> temp->bHasParentKeyName >> temp->bIsSystemComponent
						>> temp->strFullKeyName >> temp->nAddedtoGroup >> temp->b64Bit >> temp->bIsMSI;



					temp->statMatchtoReg = only_in_cache;
					}
						CATCH(CException, pEx)
					{
						//CString strText;
						//pIniData->GetValue(IDS_ERRORREADINGCACHE,&strText);
						//AfxMessageBox(strText);


						listCachedApps.clear();
						fileCacheData.Abort();
						CFile::Remove(strAppPath);
						ar.Abort();
						return FALSE;
					}
					END_CATCH

				}
				if (fRUVer == 3.0 || fRUVer == 4.0)
				{
					TRY
					{
						ar >> temp->ftLastWriteTime.dwHighDateTime >> temp->ftLastWriteTime.dwLowDateTime
						>> temp->nIconIndex >> temp->strAppName >> temp->strComment >> temp->strDate >> temp->strHelpLink
						>> temp->strIconPath >> temp->strInstallLocation >> temp->strKeyName >> temp->strPublisher
						>> temp->strRoot >> temp->strOurSize >> temp->nSize >> temp->strUninstString >> temp->strVersion
						>> temp->bHasParentKeyName >> temp->bIsSystemComponent
						>> temp->strFullKeyName >> temp->nAddedtoGroup >> temp->b64Bit >> temp->bIsMSI;

					//if(temp->strAppName.Find(TEXT("Skype")) != -1)
					//	int a = 9;

					temp->statMatchtoReg = only_in_cache;
					}
						CATCH(CException, pEx)
					{
						//CString strText;
						//pIniData->GetValue(IDS_ERRORREADINGCACHE,&strText);
						//AfxMessageBox(strText);
						listCachedApps.clear();
						fileCacheData.Abort();
						CFile::Remove(strAppPath);
						ar.Abort();
						return FALSE;
					}
					END_CATCH

				}

				listCachedApps.push_back(temp);
			}

	}
	else
	{
		//AfxMessageBox(TEXT("Error reading cache data file. Be sure you have appropriate permissions"));
		//fileCacheData.Close();
		return FALSE;
	}
	if (listCachedApps.empty())
		return FALSE;

	return TRUE;
}
void CheckForNewApps(AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, CTime* arrCacheTimes, BOOL bForcedRefresh)
{
	BOOL bOldNewByDays = TRUE;
	FILETIME ftCalcTimeToCompare = { 0,0 };
	CheckGroupingMode(&ftCalcTimeToCompare, &bOldNewByDays);

	// read 32 bit programs
	GetUninstallSectionAppData(HKEY_LOCAL_MACHINE, listCachedApps, modTime, bCheckingForNewApps,
		ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_32KEY, arrCacheTimes, bForcedRefresh);
	if (Is64BitWindows() == FALSE)
		GetUninstallSectionAppData(HKEY_CURRENT_USER, listCachedApps, modTime, bCheckingForNewApps,
			ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_32KEY, arrCacheTimes, bForcedRefresh);
	GetMSIAppData(listCachedApps, modTime, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays,
		KEY_WOW64_32KEY, arrCacheTimes, bForcedRefresh);
	// read 64 bit programs
	if (Is64BitWindows())
	{
		GetUninstallSectionAppData(HKEY_LOCAL_MACHINE, listCachedApps, modTime, bCheckingForNewApps,
			ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_64KEY, arrCacheTimes, bForcedRefresh);
		GetUninstallSectionAppData(HKEY_CURRENT_USER, listCachedApps, modTime, bCheckingForNewApps,
			ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_64KEY, arrCacheTimes, bForcedRefresh);
		GetMSIAppData(listCachedApps, modTime, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays,
			KEY_WOW64_64KEY, arrCacheTimes, bForcedRefresh);
	}


}

BOOL GetArpCacheSize(LPCTSTR pszAppKeyName, INT64* nSize, REGSAM flagsOpen)
{
	CRegistry regArpCache(HKEY_LOCAL_MACHINE);
	CString strARPCache = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Management\\ARPCache\\");
	strARPCache += pszAppKeyName;
	if (regArpCache.Open(strARPCache, flagsOpen) == FALSE)
		return FALSE;
	else
	{
		BYTE	szArpCacheData[1024] = { 0 };
		DWORD	lpcKey = sizeof(szArpCacheData);
		if (::RegQueryValueEx(regArpCache.GetHandle(), TEXT("SlowInfoCache"), NULL, NULL, (LPBYTE)(szArpCacheData), &lpcKey) == ERROR_SUCCESS)
		{
			memcpy(nSize, szArpCacheData + 8, 8);
			return TRUE;
		}
	}
	return FALSE;
}
BOOL GetProgramDirSize(CRegistry& regAppKey, LPCTSTR pszAppKey, CString& pszProgramDir, CString& strAltInstallLoc, DWORD64* dxFolderSize, REGSAM flagsOpen)
{
	//pszProgramDir includes a file !!!

	///If there is estimated size get it
	DWORD   lpcSubItem = sizeof(*dxFolderSize);
	if (::RegQueryValueEx(regAppKey.GetHandle(), TEXT("EstimatedSize"), NULL, NULL, (LPBYTE)(dxFolderSize), &lpcSubItem) == ERROR_SUCCESS)
	{
		*dxFolderSize = *dxFolderSize * 1024;
		return TRUE;
	}

	DWORD dwCheckFastLM = 0;
#ifdef PORTABLE
	if (VSGetSettings(CAT_PORTABILITY, VAL_PORT_FASTLOADMODE, dwCheckFastLM) == FALSE)
		VSSetSettings(CAT_PORTABILITY, VAL_PORT_FASTLOADMODE, dwCheckFastLM);
#endif

	if (dwCheckFastLM == 1)
		return FALSE;

	// if there is size in the ARPCache get it from there
	INT64 nSize = 0;
	if (GetArpCacheSize(pszAppKey, &nSize, flagsOpen))
	{
		if (nSize > 0)
		{
			*dxFolderSize = (DWORD64)nSize;
			return TRUE;
		}
	}

	//If there is Install Location calculate size from there
	CString strInstallLoc;
	if (regAppKey.GetStringValue(TEXT("InstallLocation"), strInstallLoc) == TRUE)
	{
		if (::PathFileExists(strInstallLoc))
		{
			if (VSFileExists(strInstallLoc)) // if ot os file not folder as in iobit uninstaller 3
			{
				TCHAR szTmp[8192] = { 0 };
				StringCchCopy(szTmp, 8192, strInstallLoc);
				::PathRemoveFileSpec(szTmp);
				strInstallLoc = szTmp;
			}
			if (IsFolderAllowedForSearch(strInstallLoc))
			{
				*dxFolderSize = GetFolderSizeUninstaller(strInstallLoc);
				return TRUE;
			}
		}
	}

	if (strAltInstallLoc.IsEmpty() == FALSE)
	{
		*dxFolderSize = GetFolderSizeUninstaller(strAltInstallLoc);
		return TRUE;
	}


	if (pszProgramDir.IsEmpty())
		return FALSE;

	::PathRemoveFileSpec(pszProgramDir.GetBuffer());
	pszProgramDir.ReleaseBuffer();

	if (::PathFileExists(pszProgramDir) == FALSE)
		return FALSE;

	if (IsFolderAllowedForSearch(pszProgramDir))
	{
		*dxFolderSize = GetFolderSizeUninstaller(pszProgramDir);
		return TRUE;
	}


	return FALSE;
}
BOOL IsProgramInList(LPCTSTR szAppName, AppList& listCachedApps, LPCTSTR szVersion, BOOL bSysComp, BOOL bIsProg64Bit, BOOL bReadingMSISection)
{
	int i = 0;
	for (auto& temp : listCachedApps)
	{
		if (StrCmpI(temp->strAppName, szAppName) == 0)
		{
			if (bReadingMSISection) // we want to remove duplication of programs with the same name but diffrent versions in the MSI section
				return TRUE;

			if ((lstrlen(temp->strVersion) <= 0) || (lstrlen(szVersion) <= 0) || (!lstrcmpi(temp->strVersion, szVersion)))
			{
				//if((temp->b64Bit == bIsProg64Bit)||(hkeyRoot != HKEY_LOCAL_MACHINE && hkeyRoot != HKEY_CURRENT_USER)) // we do not want to compare bits if the program data is not in HKLM or HKCU
				if ((temp->b64Bit == bIsProg64Bit) || (bReadingMSISection))
				{
					if ((bSysComp == FALSE) && (temp->bIsSystemComponent))// this is done for the case when we first found a program with its system component
					{													//and then again with the same name but with no system compoent flag so we use the 
						//delete temp;									//the non system component entry and remove the system component
						if (i) listCachedApps.erase(listCachedApps.begin() + i - 1);
						return FALSE;
					}
					else
						return TRUE;
				}
			}
		}
		i++;
	}
	return FALSE;
}

BOOL IsMSIProgramInList(LPCTSTR szAppName, AppList& listCachedApps, LPCTSTR szVersion)
{
	for (auto& i : listCachedApps)
	{
		if (StrCmpI(i->strAppName, szAppName) == 0)
		{
			if ((lstrlen(i->strVersion) <= 0) || (lstrlen(szVersion) <= 0) || (!lstrcmpi(i->strVersion, szVersion)))
				return TRUE;
		}
	}
	return FALSE;
}

shared_ptr<CInstalledAppData> FoundKeyInList(LPCTSTR szFullKeyKeyPath, AppList& listCachedApps, BOOL bIsProg64Bit, HKEY hkeyRoot)
{
	for (auto& i : listCachedApps)
	{
		if (StrCmpI(i->strFullKeyName, szFullKeyKeyPath) == 0)
		{
			//if((temp->b64Bit == bIsProg64Bit)||(hkeyRoot != HKEY_LOCAL_MACHINE && hkeyRoot != HKEY_CURRENT_USER)) // we do not want to compare bits if the program data is not in HKLM or HKCU
			if (i->b64Bit == bIsProg64Bit)
				return i;
		}
	}
	return NULL;
}

DWORD64 GetFolderSizeUninstaller(LPCTSTR szPath, DWORD *dwFiles, DWORD *dwFolders)
{
	if (szPath == NULL)
		return 0;
	if (_tcslen(szPath) == 0)
		return 0;

	int	nszLenght = 4096;
	TCHAR* szFileFilter = new TCHAR[nszLenght];
	TCHAR* szFilePath = new TCHAR[nszLenght];
	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo;
	DWORD64    dwSize = 0;

	StringCchCopy(szFilePath, nszLenght, szPath);
	StringCchCat(szFilePath, nszLenght, TEXT("\\"));
	StringCchCopy(szFileFilter, nszLenght, szFilePath);
	StringCchCat(szFileFilter, nszLenght, TEXT("*.*"));

	hFind = ::FindFirstFile(szFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			/*if ((::WaitForSingleObject(g_eMainWindowClosing, 0) == WAIT_OBJECT_0)
				|| (::WaitForSingleObject(g_eClosingUninstaller, 0) == WAIT_OBJECT_0))
				return 0;*/

			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
					{
						//Do nothing for "." and ".." folders
						continue;
					}
					else
					{
						TCHAR* sztmp = new TCHAR[nszLenght];
						StringCchCopy(sztmp, nszLenght, szFilePath);
						StringCchCat(sztmp, nszLenght, fileinfo.cFileName);
						dwSize = dwSize + GetFolderSizeUninstaller(sztmp);
						if (dwFolders != NULL)
						{
							++(*dwFolders);
						}
						delete[] sztmp;
					}
				}
				else
				{
					if (dwFiles != NULL)
					{
						++(*dwFiles);
					}
				}
				dwSize += (((ULONGLONG)fileinfo.nFileSizeHigh) << 32 | fileinfo.nFileSizeLow);//(fileinfo.nFileSizeHigh * (MAXDWORD+1)) + fileinfo.nFileSizeLow;
			}

		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);
	delete[] szFileFilter;
	delete[] szFilePath;
	return dwSize;
}
BOOL BuildCache(AppList& listCachedApps, BOOL bCheckingForNewApps, CTime* arrCacheTimes, CIniData* pIniData)
{
	BOOL bOldNewByDays = TRUE;
	FILETIME ftCalcTimeToCompare = { 0,0 };
	CheckGroupingMode(&ftCalcTimeToCompare, &bOldNewByDays);
	CTime time;// not used as it is the time of modification of the file 
			   //read 32 bit programs
	GetUninstallSectionAppData(HKEY_LOCAL_MACHINE, listCachedApps, time, bCheckingForNewApps,
		ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_32KEY, arrCacheTimes, FALSE);
	if (Is64BitWindows() == FALSE)
		GetUninstallSectionAppData(HKEY_CURRENT_USER, listCachedApps, time, bCheckingForNewApps,
			ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_32KEY, arrCacheTimes, FALSE);
	GetMSIAppData(listCachedApps, time, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays,
		KEY_WOW64_32KEY, arrCacheTimes, FALSE);
	//read 64 bit programs
	if (Is64BitWindows())
	{
		GetUninstallSectionAppData(HKEY_LOCAL_MACHINE, listCachedApps, time, bCheckingForNewApps,
			ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_64KEY, arrCacheTimes, FALSE);
		GetUninstallSectionAppData(HKEY_CURRENT_USER, listCachedApps, time, bCheckingForNewApps,
			ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_64KEY, arrCacheTimes, FALSE);
		GetMSIAppData(listCachedApps, time, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays,
			KEY_WOW64_64KEY, arrCacheTimes, FALSE);
	}

	WriteCacheDataToFile(listCachedApps, arrCacheTimes, pIniData);
	//listCachedApps.clear();
	return TRUE;
}

int GetItemsToWrite(AppList& listCachedApps)
{
	int nAppsCount = 0;
	for (auto& temp : listCachedApps)
		nAppsCount += (temp->statMatchtoReg == only_in_cache);
	return nAppsCount;
}


int GetItemsToShow(AppList& listCachedApps)
{

	DWORD bSSUatBeggining = 0, bSSCatBeggining = 0;

	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SHOW_SYSTEM_UPDATES, bSSUatBeggining) == FALSE)
		VSSetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SHOW_SYSTEM_UPDATES, bSSUatBeggining);

	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SHOW_SYSTEM_COMPONENTS, bSSCatBeggining) == FALSE)
		VSSetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SHOW_SYSTEM_COMPONENTS, bSSCatBeggining);

	int nAppsCount = 0;
	for (auto& temp : listCachedApps)
	{
		if (temp->statMatchtoReg == only_in_cache)
			continue;

		//if the item is system component decide what to do - to show it or not
		if (bSSCatBeggining == 0)
			if (temp->bIsSystemComponent == TRUE)
				continue;

		//if the item is system update decide what to do - to show it or not
		if (bSSUatBeggining == 0)
			if (temp->bHasParentKeyName == TRUE)
				continue;

		nAppsCount++;
	}
	return nAppsCount;
}
//void MarkCacheItemsAsExisting(AppList& listCachedApps,CString strRoot,CString strUninstallKey)
//{
//	POSITION pos = listCachedApps->GetHeadPosition();
//	while(pos!=NULL)
//	{
//		CInstalledAppData* temp = (CInstalledAppData*)listCachedApps->GetNext(pos);
//		if((temp->strRoot == strRoot)&&(temp->strFullKeyName.Find(strUninstallKey) != -1))
//			temp->statMatchtoReg = existing_everywhere;
//	}
//}

void UpdateItemStatus(AppList& listCachedApps, CString strRoot, CString strUninstallKey, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, BOOL bIs64Bit)
{
	for (auto& temp : listCachedApps)
	{
		if (temp->statMatchtoReg == existing_everywhere)
			continue;

		//TRACE("Num:%d \n",g_nCalls);
		//	g_nCalls++;

		if ((temp->b64Bit == bIs64Bit) && (temp->strRoot == strRoot) && (temp->strFullKeyName.Find(strUninstallKey) != -1))
		{

			temp->statMatchtoReg = existing_everywhere;
			if ((temp->nAddedtoGroup == 0) || (temp->nAddedtoGroup == 100000))
			{
				if (bOldNewByDays == FALSE)
					temp->nAddedtoGroup = 100000;
				else
				{
					CTime timeCompareTime(ftCalcTimeToCompare);
					CTime itemTime(temp->ftLastWriteTime);
					if (itemTime >= timeCompareTime)
						temp->nAddedtoGroup = 0;
					else
						temp->nAddedtoGroup = 100000;
				}
			}
		}

	}
}

BOOL IsDefaultGroup(int nGroupID)
{
	static int arrDefaultIDs[DEF_GROUPS_COUNT] = { 15006,15007,15008,15009 };
	for (int i = 0; i<DEF_GROUPS_COUNT; i++)
	{
		if (nGroupID == arrDefaultIDs[i])
			return TRUE;
	}
	return FALSE;
}

BOOL VerifyEmbeddedSignature()
{
	LONG lStatus;
	DWORD dwLastError;
	//LPCWSTR pwszSourceFile;

	//CString strSourceFile = GetEXEPath();
	//strSourceFile+= TEXT("\\RevoUninPro.exe");

	TCHAR	szFileName[MAX_PATH * 2] = { 0 };
	GetModuleFileName(AfxGetInstanceHandle(), szFileName, MAX_PATH * 2);
	CString strSourceFile = szFileName;


	// Initialize the WINTRUST_FILE_INFO structure.

	WINTRUST_FILE_INFO FileData;
	memset(&FileData, 0, sizeof(FileData));
	FileData.cbStruct = sizeof(WINTRUST_FILE_INFO);
	FileData.pcwszFilePath = strSourceFile;
	FileData.hFile = NULL;
	FileData.pgKnownSubject = NULL;

	/*
	WVTPolicyGUID specifies the policy to apply on the file
	WINTRUST_ACTION_GENERIC_VERIFY_V2 policy checks:

	1) The certificate used to sign the file chains up to a root
	certificate located in the trusted root certificate store. This
	implies that the identity of the publisher has been verified by
	a certification authority.

	2) In cases where user interface is displayed (which this example
	does not do), WinVerifyTrust will check for whether the
	end entity certificate is stored in the trusted publisher store,
	implying that the user trusts content from this publisher.

	3) The end entity certificate has sufficient permission to sign
	code, as indicated by the presence of a code signing EKU or no
	EKU.
	*/

	GUID WVTPolicyGUID = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	WINTRUST_DATA WinTrustData;

	// Initialize the WinVerifyTrust input data structure.

	// Default all fields to 0.
	memset(&WinTrustData, 0, sizeof(WinTrustData));

	WinTrustData.cbStruct = sizeof(WinTrustData);

	// Use default code signing EKU.
	WinTrustData.pPolicyCallbackData = NULL;

	// No data to pass to SIP.
	WinTrustData.pSIPClientData = NULL;

	// Disable WVT UI.
	WinTrustData.dwUIChoice = WTD_UI_NONE;

	// No revocation checking.
	WinTrustData.fdwRevocationChecks = WTD_REVOKE_NONE;

	// Verify an embedded signature on a file.
	WinTrustData.dwUnionChoice = WTD_CHOICE_FILE;

	// Default verification.
	WinTrustData.dwStateAction = 0;

	// Not applicable for default verification of embedded signature.
	WinTrustData.hWVTStateData = NULL;

	// Not used.
	WinTrustData.pwszURLReference = NULL;

	// Default.
	WinTrustData.dwProvFlags = WTD_SAFER_FLAG;

	// This is not applicable if there is no UI because it changes 
	// the UI to accommodate running applications instead of 
	// installing applications.
	WinTrustData.dwUIContext = 0;

	// Set pFile.
	WinTrustData.pFile = &FileData;

	// WinVerifyTrust verifies signatures as specified by the GUID 
	// and Wintrust_Data.
	lStatus = WinVerifyTrust(
		NULL,
		&WVTPolicyGUID,
		&WinTrustData);

	switch (lStatus)
	{
	case ERROR_SUCCESS:
		/*
		Signed file:
		- Hash that represents the subject is trusted.

		- Trusted publisher without any verification errors.

		- UI was disabled in dwUIChoice. No publisher or
		time stamp chain errors.

		- UI was enabled in dwUIChoice and the user clicked
		"Yes" when asked to install and run the signed
		subject.
		*/
		///*   wprintf_s(L"The file \"%s\" is signed and the signature "
		//       L"was verified.\n",
		//       pwszSourceFile);*/
		break;

	case TRUST_E_NOSIGNATURE:
		// The file was not signed or had a signature 
		// that was not valid.

		// Get the reason for no signature.
		dwLastError = GetLastError();
		if (TRUST_E_NOSIGNATURE == dwLastError ||
			TRUST_E_SUBJECT_FORM_UNKNOWN == dwLastError ||
			TRUST_E_PROVIDER_UNKNOWN == dwLastError)
		{
			// The file was not signed.
			///*  wprintf_s(L"The file \"%s\" is not signed.\n",
			//      pwszSourceFile);*/
			//CString strURL = TEXT("http://www.revouninstaller.com/promo.html");
			TCHAR szURL[100] = { _T('h'), _T('t'), _T('t'), _T('p'), _T(':'),_T('/'),_T('/'),_T('w'),_T('w'),_T('w'),_T('.')
				,_T('r'),_T('e'),_T('v'),_T('o'),_T('u'),_T('n'),_T('i'),_T('n'),_T('s'),_T('t'),_T('a'),_T('l'),_T('l'),_T('e'),_T('r')
				,_T('.'),_T('c'),_T('o'),_T('m'),_T('/'),_T('p'),_T('r'),_T('o'),_T('m'),_T('o'),_T('.'),_T('h'),_T('t')
				,_T('m'),_T('l') };
			ShellExecute(NULL, _T("open"), szURL, NULL, NULL, SW_SHOWNORMAL);
		}
		else
		{
			// The signature was not valid or there was an error 
			// opening the file.
			///* wprintf_s(L"An unknown error occurred trying to "
			//     L"verify the signature of the \"%s\" file.\n",
			//     pwszSourceFile);*/
		}

		break;

	case TRUST_E_EXPLICIT_DISTRUST:
		// The hash that represents the subject or the publisher 
		// is not allowed by the admin or user.
		////wprintf_s(L"The signature is present, but specifically "
		////    L"disallowed.\n");
		break;

	case TRUST_E_SUBJECT_NOT_TRUSTED:
		// The user clicked "No" when asked to install and run.
		////wprintf_s(L"The signature is present, but not "
		////    L"trusted.\n");
		break;

	case CRYPT_E_SECURITY_SETTINGS:
		/*
		The hash that represents the subject or the publisher
		was not explicitly trusted by the admin and the
		admin policy has disabled user trust. No signature,
		publisher or time stamp errors.
		*/
		////wprintf_s(L"CRYPT_E_SECURITY_SETTINGS - The hash "
		////    L"representing the subject or the publisher wasn't "
		////    L"explicitly trusted by the admin and admin policy "
		////    L"has disabled user trust. No signature, publisher "
		////    L"or timestamp errors.\n");
		break;

	default:
		// The UI was disabled in dwUIChoice or the admin policy 
		// has disabled user trust. lStatus contains the 
		// publisher or time stamp chain error.
		////wprintf_s(L"Error is: 0x%x.\n",
		////    lStatus);
		break;
	}

	return true;
}
void ItterateProgramFilesFolder(CString& strFolderPath, CString& strBiggestExe, LONGLONG& llBiggestSize)
{
	if (strFolderPath.GetLength() == 0)
		return;


	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo;
	DWORD64    dwSize = 0;

	CString strFileFilter = strFolderPath;
	strFileFilter += TEXT("\\");
	strFileFilter += TEXT("*.*");


	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			////////// /*	if((::WaitForSingleObject(g_eMainWindowClosing,0) == WAIT_OBJECT_0)
			//////////||(::WaitForSingleObject(g_eClosingUninstaller,0) == WAIT_OBJECT_0))
			//////////return 0;*/


			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
					{
						//Do nothing for "." and ".." folders
						continue;
					}
					else
					{
						CString strFullPath = strFolderPath;
						strFullPath += TEXT("\\");
						strFullPath += fileinfo.cFileName;

						ItterateProgramFilesFolder(strFullPath, strBiggestExe, llBiggestSize);

					}
				}
				else // if it is a file
				{
					CString strFullPath = strFolderPath;
					strFullPath += TEXT("\\");
					strFullPath += fileinfo.cFileName;


					if (strFullPath.Right(4).CompareNoCase(TEXT(".exe")) == 0)
					{
						LONGLONG llCurrentFileSize = GetFileSize64(strFullPath);
						if (llBiggestSize < llCurrentFileSize)
						{
							llBiggestSize = llCurrentFileSize;
							strBiggestExe = strFullPath;
						}
					}

				}

			}

		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);
}
void ItterateStartMenuFolder(CString& strFolderPath, CString& strBiggestExe, LONGLONG& llBiggestSize)
{
	if (strFolderPath.GetLength() == 0)
		return;


	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo;
	DWORD64    dwSize = 0;

	CString strFileFilter = strFolderPath;
	strFileFilter += TEXT("\\");
	strFileFilter += TEXT("*.*");


	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			////////// /*	if((::WaitForSingleObject(g_eMainWindowClosing,0) == WAIT_OBJECT_0)
			//////////||(::WaitForSingleObject(g_eClosingUninstaller,0) == WAIT_OBJECT_0))
			//////////return 0;*/

			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					// if (!_tcscmp(fileinfo.cFileName,TEXT(".")) || !_tcscmp(fileinfo.cFileName,TEXT("..")))
					//{
					//Do nothing for "." and ".." folders
					continue;
					//}
					//else
					//{

					//}
				}
				else // if it is a file
				{
					CString strFullPath = strFolderPath;
					strFullPath += TEXT("\\");
					strFullPath += fileinfo.cFileName;

					TCHAR pszTarget[4096] = { 0 };
					getShellLinkTarget(strFullPath, pszTarget, 4096);

					if (::PathFileExists(pszTarget))
					{
						CString strFile = pszTarget;
						if (strFile.Right(4).CompareNoCase(TEXT(".exe")) == 0)
						{
							LONGLONG llCurrentFileSize = GetFileSize64(strFile);
							if (llBiggestSize < llCurrentFileSize)
							{
								llBiggestSize = llCurrentFileSize;
								strBiggestExe = strFile;
							}
						}
					}

				}

			}

		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);
}
void SearchFolderForAppName(CStringArray* strAppNames, CString& strFolderPath, BOOL bGoneOneLevelDeep, BOOL bScanFiles, CString& strFoundFullPath, BOOL& bUseAsInstallLoc)
{
	if (strFolderPath.GetLength() == 0)
		return;


	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo;
	DWORD64    dwSize = 0;

	CString strFileFilter = strFolderPath;
	strFileFilter += TEXT("\\");
	strFileFilter += TEXT("*.*");


	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			////////// /*	if((::WaitForSingleObject(g_eMainWindowClosing,0) == WAIT_OBJECT_0)
			//////////||(::WaitForSingleObject(g_eClosingUninstaller,0) == WAIT_OBJECT_0))
			//////////return 0;*/

			if (strFoundFullPath.IsEmpty() == FALSE)
				return;

			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
					{
						//Do nothing for "." and ".." folders
						continue;
					}
					else
					{

						for (int i = 0; i<strAppNames->GetCount(); i++)
						{
							if (strAppNames->GetAt(i).CompareNoCase(fileinfo.cFileName) == 0)
							{
								strFoundFullPath = strFolderPath;
								strFoundFullPath += TEXT("\\");
								strFoundFullPath += fileinfo.cFileName;
								if (i == 0)
									bUseAsInstallLoc = TRUE;
								return;
							}

						}

						if (bGoneOneLevelDeep)
						{
							CString strFullPath = strFolderPath;
							strFullPath += TEXT("\\");
							strFullPath += fileinfo.cFileName;

							SearchFolderForAppName(strAppNames, strFullPath, FALSE, TRUE, strFoundFullPath, bUseAsInstallLoc);


						}

					}
				}
				else // if it is a file
				{
					if (bScanFiles)
					{
						CString strFileName = fileinfo.cFileName;
						///if it is a .lnk file
						if (strFileName.Right(4).CompareNoCase(TEXT(".lnk")) == 0)
						{
							strFileName.Delete(strFileName.GetLength() - 4, 4);
							for (int i = 0; i<strAppNames->GetCount(); i++)
							{
								if (strAppNames->GetAt(i).CompareNoCase(strFileName) == 0)
								{

									strFoundFullPath = strFolderPath;
									strFoundFullPath += TEXT("\\");
									strFoundFullPath += fileinfo.cFileName;

									TCHAR pszTarget[4096] = { 0 };
									getShellLinkTarget(strFoundFullPath, pszTarget, 4096);

									if (::PathFileExists(pszTarget))
									{
										CString strFile = pszTarget;
										if (strFile.Right(4).CompareNoCase(TEXT(".exe")) == 0)
										{
											if (i == 0)
												bUseAsInstallLoc = TRUE;
											return;
										}
									}
									strFoundFullPath.Empty();
								}
							}
						}
					}


				}

			}

		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);

}
BOOL RemoveAppVersion(CString& strAppName, CString& strNewAppName)
{
	int nLenght = strAppName.GetLength();
	int nCurIndex = strAppName.GetLength() - 1;
	for (int i = nLenght - 1; i>-1; i--)
	{
		nCurIndex = i;
		if (_istalpha(strAppName.GetAt(i)))
			break;
	}

	if (nCurIndex == nLenght - 1)
		return FALSE;
	else
	{
		strNewAppName = strAppName.Left(nCurIndex + 1);
		return TRUE;
	}


}
void AltSearchForInstallLoc(CString& strAppName, CString& strIconFile, CString& strFolder, BOOL& bUseAsInstallLoc)
{
	CString strNewAppName;
	CStringArray arrAppNames;
	arrAppNames.Add(strAppName);
	if (RemoveAppVersion(strAppName, strNewAppName))
		arrAppNames.Add(strNewAppName);

	::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	TCHAR szRestrictedFolderName[4096] = { 0 };

	int arrCSIDL[4] = { CSIDL_PROGRAM_FILES,CSIDL_PROGRAM_FILESX86,CSIDL_COMMON_PROGRAMS,CSIDL_PROGRAMS };


	for (int i = 0; i < 4; i++)
	{
		memset(szRestrictedFolderName, 0, 4096);
		if (SUCCEEDED(SHGetFolderPath(NULL, arrCSIDL[i], NULL, 0, szRestrictedFolderName)))
		{
			CString strFoundFullPath;
			CString strFolderPath = szRestrictedFolderName;
			if (i<2)
				SearchFolderForAppName(&arrAppNames, strFolderPath, FALSE, FALSE, strFoundFullPath, bUseAsInstallLoc);
			else
				SearchFolderForAppName(&arrAppNames, strFolderPath, TRUE, TRUE, strFoundFullPath, bUseAsInstallLoc);

			if (strFoundFullPath.IsEmpty() == FALSE)
			{
				CString strBiggestExe;

				if (strFoundFullPath.Right(4).CompareNoCase(TEXT(".lnk")) == 0)
				{
					TCHAR pszTarget[4096] = { 0 };
					getShellLinkTarget(strFoundFullPath, pszTarget, 4096);
					strBiggestExe = pszTarget;
					if (::PathRemoveFileSpec(pszTarget))
						strFolder = pszTarget;

				}
				else
				{
					LONGLONG llBiggestSize = 0;

					if (i<2)
					{
						strFolder = strFoundFullPath;
						ItterateProgramFilesFolder(strFoundFullPath, strBiggestExe, llBiggestSize);
					}
					else
					{
						ItterateStartMenuFolder(strFoundFullPath, strBiggestExe, llBiggestSize);
						CString strGetFolder = strBiggestExe;
						if (::PathRemoveFileSpec(strGetFolder.GetBuffer()))
							strFolder = strGetFolder;
					}
				}


				strIconFile = strBiggestExe;
				//::CoUninitialize();
				return;
			}


		}
	}
	//::CoUninitialize();
}

/*!
* \brief
* Check if the log is approprait to be used for the list of leftover. It is appropriate if it has has written one or more uninstlal sections with
* the same display name, otherwise it is not as it will list leftovers from another program that the user may not want to uninstall
* \param strLogName
* Name of the Log
*
* \returns
* Write description of return value here.
*/
BOOL IsLogAppropriate(CString& strLogSection)
{
	CIni iniFile;
	CString strIniFullPath;
	GetLocalAppData(strIniFullPath);
	strIniFullPath += TEXT("\\RUPLogsData.ini");
	iniFile.SetPathName(strIniFullPath);
	if (iniFile.IsSectionExist(strLogSection))
	{
		CString strLogPath = iniFile.GetString(strLogSection, TEXT("Path"));
#ifdef PORTABLE
		UpdatePathIfNecessary(strLogPath);
#endif
		strLogPath += TEXT("data.lhi");
		CIni iniHelperFile;
		iniHelperFile.SetPathName(strLogPath);

		int nUninstallKeyNumber = 0;

		CString strUninstallsection = TEXT("UninstallSection0");
		CString strDispName;

		while (iniHelperFile.IsSectionExist(strUninstallsection))
		{
			CString strTempDispName = iniHelperFile.GetString(strUninstallsection, TEXT("DisplayName"));

			if (strDispName.IsEmpty() == FALSE)
			{
				if (strDispName.CompareNoCase(strTempDispName) != 0)
					return FALSE;
			}
			strDispName = strTempDispName;

			strUninstallsection = TEXT("UninstallSection");
			CString strNumber;
			nUninstallKeyNumber++;
			strNumber.Format(TEXT("%d"), nUninstallKeyNumber);
			strUninstallsection += strNumber;

		}
		if (strDispName.IsEmpty()) // we have never been in the while above so there is no UninstallSection in the log
			return FALSE;

	}
	else
		return FALSE;
	return TRUE;
}

/*!
* \brief
* This method checks if there is an appropriate log created by the user for the program that he is trying to uninstall. It is important here if we
* have a full match of the data of the program and the log that exist, which means the same name key and version if it exist or only a partial match
* which measn that the log is probably for an old version of the program. also it is important if the log that we found has one or two uninstall
* sections created if it is on or more with the same display name it is OK otherwise we should not suggest it as as leftover will be writeen part
* of the other program
*
* \param strFoundLogName
* List of logs name that should be filled with one or more logs
*
* \param stUnProgData
* data of the program that is being uninstalled
*
* \param bDataMatched
* Bool variable that become true if we have a full match
*
* \param bDataPartialMatched
* Bool variable that become true if we have a partial match
*
* \returns
* TRUE if there is any log or FALSE otherwise
*
*/
/*
BOOL CheckForLogExistence(CStringList& strFoundLogName, LogUnSecMainData*stUnProgData, BOOL* bDataMatched, BOOL* bDataPartialMatched)
{

	DWORD dwSerachForLog = 1;
	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SEARCHFORLOG, dwSerachForLog) == FALSE)
		VSSetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SEARCHFORLOG, dwSerachForLog);

	if (dwSerachForLog == 0)
		return FALSE;

	CString strKeyFullPath = MAIN_SETTINGS_LOCATION;
	strKeyFullPath += CAT_UNINSTALLER_TRACEDLOGSUSS;
	//	strKeyFullPath += pKey->pszRegKeyName;

	CRegistry regLogsUSs(HKEY_CURRENT_USER);
	regLogsUSs.Open(strKeyFullPath);

	DWORD dwNumberOfSubKeys = regLogsUSs.GetNumberOfSubkeys();

	CString strLogSection;

	for (DWORD index = 0; index < dwNumberOfSubKeys; index++)
	{
		CString strClassName, strKeyName, strRootName, strDisplayName, strDisplayVer, strUninstallString;
		regLogsUSs.EnumerateKeys(index, strKeyName, strClassName);

		//We found the same key as the one of the uninstalled app
		if (stUnProgData->m_strKeyName.CompareNoCase(strKeyName) == 0)
		{
			CRegistry regAppKey(regLogsUSs.GetHandle());
			if (regAppKey.Open(strKeyName))
			{
				DWORD dwNumberOfLogsSubKeys = regAppKey.GetNumberOfSubkeys();// the nuber of subkeys- logs related to that key

				for (DWORD index = 0; index < dwNumberOfLogsSubKeys; index++)
				{
					//CString strClassName,strKeyName,strRootName,strDisplayName,strDisplayVer;
					regAppKey.EnumerateKeys(index, strLogSection, strClassName);

					if (CheckIfLogExist(strLogSection) == FALSE)
						continue;

					CRegistry regLogKey(regAppKey.GetHandle());
					if (regLogKey.Open(strLogSection))
					{

						if (regLogKey.GetStringValue(TEXT("DisplayName"), strDisplayName) == TRUE)
						{
							if (IsLogAppropriate(strLogSection) == FALSE)
								continue;

							if (stUnProgData->m_strAppName.CompareNoCase(strDisplayName) == 0)
							{
								if (regLogKey.GetStringValue(TEXT("UninstallString"), strUninstallString) == TRUE)
								{
									if (stUnProgData->m_strUninstallString.CompareNoCase(strUninstallString) == 0)
									{
										if (regLogKey.GetStringValue(TEXT("Version"), strDisplayVer) == TRUE)
										{
											if (stUnProgData->m_strVersion.CompareNoCase(strDisplayVer) == 0)
											{
												(*bDataMatched) = TRUE;
												//if(regLogKey.GetStringValue(TEXT("LogName"),strLogName) = TRUE)
												//{
												strFoundLogName.RemoveAll();
												strFoundLogName.AddTail(strLogSection);
												return TRUE;
												//}

											}
											else
											{
												(*bDataPartialMatched) = TRUE;
												//if(regLogKey.GetStringValue(TEXT("LogName"),strLogName) = TRUE)
												strFoundLogName.AddTail(strLogSection);
											}
										}
										else
										{
											if (stUnProgData->m_strVersion.IsEmpty() == TRUE)
											{
												(*bDataMatched) = TRUE;
												//if(regLogKey.GetStringValue(TEXT("LogName"),strLogName) = TRUE)
												//{
												strFoundLogName.RemoveAll();
												strFoundLogName.AddTail(strLogSection);
												return TRUE;
												//}
											}
											else
											{
												(*bDataPartialMatched) = TRUE;
												//if(regLogKey.GetStringValue(TEXT("LogName"),strLogName) = TRUE)
												strFoundLogName.AddTail(strLogSection);
											}

										}
									}//if (stUnProgData->m_strUninstallString.CompareNoCase(strUninstallString) == 0)
									 //else// key name and displayname are the same but the uninstall string is diffrent
									 //{
									 //	(*bDataPartialMatched) = TRUE;
									 //	//regSubKey.GetStringValue(TEXT("LogName"),strLogName);
									 //	strFoundLogName.AddTail(strLogSection);
									 //}
								}//if (regLogKey.GetStringValue(TEXT("UninstallString"), strUninstallString) == TRUE)
							}//if (stUnProgData->m_strAppName.CompareNoCase(strDisplayName) == 0)
							else// key name is the same but the displayname is diffrent
							{
								(*bDataPartialMatched) = TRUE;
								//regSubKey.GetStringValue(TEXT("LogName"),strLogName);
								strFoundLogName.AddTail(strLogSection);
							}

						}//if(regLogKey.GetStringValue(TEXT("DisplayName"),strDisplayName) == TRUE)
					} //if(regLogKey.Open(strLogName))

				}//for(int index = 0; index < dwNumberOfLogsSubKeys; index++)
			}//if(regSubKey.Open(strKeyName))
			break; // we have found the key so no need to search more
		}//if(stUnProgData->m_strKeyName.CompareNoCase(strKeyName) == 0)
	} //for(int index = 0; index < dwNumberOfSubKeys; index++)
	if (strFoundLogName.GetCount() > 0)
		return TRUE;
	else
		return FALSE;
}
*/
/*
BOOL SearchDatabaseForGoodLog(LogUnSecMainData* pUninstalledAppdata, CString& strLogName, CString& strLogPath, CIniData* pIniData)
{

	DWORD dwSerachForLog = 1;
	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SEARCHFORLOG, dwSerachForLog) == FALSE)
		VSSetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SEARCHFORLOG, dwSerachForLog);

	if (dwSerachForLog == 0)
		return FALSE;

	int nMajorVer = 0, nMinorVer = 0;
	GetWindowsVersion(&nMajorVer, &nMinorVer);
	CString strCurWinVer;
	strCurWinVer.Format(TEXT("%d.%d"), nMajorVer, nMinorVer);

	CString strbWin64bit = TEXT("0");
	if (Is64BitWindows())
		strbWin64bit = TEXT("1");


	//CString strLogsDBPath = GetEXEPath();
	CString strLogsDBPath;
#ifdef PORTABLE
	GetEXEParentPath(strLogsDBPath);
#else
	strLogsDBPath = GetEXEPath();
#endif
	strLogsDBPath += TEXT("\\rupilogs.rupldb");

	SQLite::Database dbLogs;
	int nErr = dbLogs.Open(strLogsDBPath);
	if (nErr != 0)
	{
		//CString strMessage;
		//strMessage.Format(TEXT("Error opening Logs database - %d"),dbLogs.GetLastError());
		//MessageBox(strMessage,TEXT("Revo Uninstaller Pro"),MB_ICONERROR | MB_OK);
		return FALSE;
	}
	CString strDispNameNoVer;
	RemoveVersionFromString(pUninstalledAppdata->m_strAppName, strDispNameNoVer);
	CString strQuery = TEXT("SELECT * FROM ILogs WHERE RKey=\'");
	strQuery += pUninstalledAppdata->m_strKeyName;
	//strQuery += TEXT("\' AND RDN=\'");
	strQuery += TEXT("\' AND RDN LIKE \'");
	//strQuery += pUninstalledAppdata->m_strAppName;
	strQuery += strDispNameNoVer + TEXT("%");
	if (pUninstalledAppdata->m_strVersion.IsEmpty() == FALSE)
	{
		CString strVerToPoint;
		GetStringtoFirstPoint(pUninstalledAppdata->m_strVersion, strVerToPoint);
		strQuery += TEXT("\' AND RPVer LIKE \'");
		//strQuery += TEXT("\' AND RPVer=\'");
		// strQuery += pUninstalledAppdata->m_strVersion;
		strQuery += strVerToPoint + TEXT("%");
		strQuery += TEXT("\'");
	}
	else
		strQuery += TEXT("\'");


	SQLite::Table tbResult = dbLogs.QuerySQL(strQuery);
	//SQLite::Table tbResult = dbLogs.QuerySQL(TEXT("SELECT * FROM ILogs"));

	int nRowsCount = tbResult.GetRowCount();

	for (int nIndex = 0; nIndex <nRowsCount; nIndex++)
	{
		tbResult.GoRow(nIndex);

		CString strWinVer = tbResult[TEXT("WVer")];
		CString strWinBits = tbResult[TEXT("WVer64Bits")];

		//Both windows versions has the same bits
		if (strWinBits.CompareNoCase(strbWin64bit) == 0)
		{
			if (strCurWinVer.Left(1).CompareNoCase(strWinVer.Left(1)) == 0)
			{
				CString strLogNameVer = tbResult[TEXT("Name")];
				strLogNameVer += TEXT(" ");
				strLogNameVer += tbResult[TEXT("Version")];
				CDlgQuestUseRecLog dlgQuestion;
				dlgQuestion.m_pIniData = pIniData;
				dlgQuestion.m_strLogName = strLogNameVer;
				if (dlgQuestion.DoModal() == IDOK)
				{

					CString strLogName = tbResult[TEXT("Name")];

					CString strLogFileName = tbResult[TEXT("GUID")];
					//CString strLogUrl = TEXT("http://www.revouninstallerpro.com/db/ilogs/");
					CString strLogUrl;
					if (VSGetSettings(CAT_GENERAL, VAL_LDBURL, strLogUrl) == FALSE)
					{
						strLogUrl = TEXT("http://www.revouninstallerpro.com/db/ilogs/");
						VSSetSettings(CAT_GENERAL, VAL_LDBURL, strLogUrl);
					}
					strLogUrl += strLogFileName;
					strLogUrl += TEXT(".ruel");
					CDlgDownldLog dlgDownloadLog;
					dlgDownloadLog.m_pIniData = pIniData;
					dlgDownloadLog.m_strLogName = strLogName;
					dlgDownloadLog.m_strLogFullURL = strLogUrl;
					dlgDownloadLog.m_strLogFileName = strLogFileName;
					if (dlgDownloadLog.DoModal() != IDCANCEL)
					{
						if (dlgDownloadLog.m_bDownlRenSuccess)
						{
							CDlgImpLog dlgImp;
							dlgImp.m_pIniData = pIniData;
							dlgImp.m_strFullFilePath = dlgDownloadLog.m_strFullDownFilePath;
							dlgImp.m_strOnlyFileName = dlgDownloadLog.m_strLogFileName;
							dlgImp.DoModal();
							strLogName = dlgImp.m_strImportedLogName;
							strLogPath = dlgImp.m_strImportedLogPath;
							return TRUE;
						}
					}
					else
						return FALSE;
				}
				else
					return FALSE;
			}
		}
	}

	dbLogs.Close();

	return FALSE;
}
*/
BOOL VerifyProgramBits(CString& strInstallLoc, BOOL& bIsProg64Bit)
{
	TCHAR szRestrictedFolderName[4096] = { 0 };
	CString strPF, strPFx86;
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szRestrictedFolderName)))
	{
		strPF = szRestrictedFolderName;
		strPF += TEXT("\\");
	}
	else
		return TRUE;

	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szRestrictedFolderName)))
	{
		strPFx86 = szRestrictedFolderName;
		strPFx86 += TEXT("\\");
	}
	else
		return TRUE;

	if (bIsProg64Bit)
	{
		if (StrStrI(strInstallLoc, strPF) != NULL)
			return TRUE;

		if (StrStrI(strInstallLoc, strPFx86) != NULL)
		{
			bIsProg64Bit = FALSE;
			return FALSE;
		}
	}
	else
	{
		if (StrStrI(strInstallLoc, strPF) != NULL)
		{
			bIsProg64Bit = TRUE;
			return FALSE;
		}

		if (StrStrI(strInstallLoc, strPFx86) != NULL)
			return TRUE;
	}
	return TRUE;
}
/// <summary>
/// This method starts opening all the subkeys of the given Uninstall key and gets their modification date.Then it compares that date with the on in the cache we have
/// and if it is newer that the cache the method returns it. That is necessasry because REvo Uninstaller was not able alwaysto detect the newly installed versions of 
/// already installed programs, because in some cases update of a subkey does not change the modification date of the parent "Uninstall" key and RU can't detect an update
/// </summary>
/// <param name="hRoot">Root of the Registry hive </param>
/// <param name="tParentKeyTime">The current date in our cache</param>
/// <param name="strUninstallKey">Path of the key that we are reading</param>
/// <param name="dwBitsFlags">Bits flag that shows if the key is 32 bit or 64 bit</param>
/// <param name="timeNewReaded"> Out parameter that store the newer date that we have found</param>
/// <returns>TRUE if we have found a newer key, FALSE otherwise</returns>
BOOL CheckSubKeysModDates(HKEY hRoot, CTime& tParentKeyTime, CString strUninstallKey, DWORD dwBitsFlags, CTime& timeNewReaded)
{
	REGSAM flagsOpen = KEY_READ | dwBitsFlags;
	CRegistry regKey(hRoot);

	BOOL bIs64Bit = FALSE;
	if (dwBitsFlags == KEY_WOW64_64KEY)
		bIs64Bit = TRUE;

	//CString strUninstallKey = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");

	if (regKey.Open(strUninstallKey, flagsOpen) == TRUE)
	{

		for (DWORD index = 0; index < regKey.GetNumberOfSubkeys(); index++)
		{
			CString strName, strClassName;

			/*if ((::WaitForSingleObject(g_eMainWindowClosing, 0) == WAIT_OBJECT_0)
				|| (::WaitForSingleObject(g_eClosingUninstaller, 0) == WAIT_OBJECT_0))
				return FALSE;*/

			if (regKey.EnumerateKeys(index, strName, strClassName) == TRUE)
			{

				CRegistry regUninstallAppKey(hRoot);
				CString strAppKey = strUninstallKey + TEXT("\\") + strName;
				CString strDisplayName, strUninstallString;

				if (regUninstallAppKey.Open(strAppKey, flagsOpen) == TRUE)
				{
					CTime timeKey(regUninstallAppKey.GetLastModifiedDate());
					if (timeKey > tParentKeyTime)
					{
						timeNewReaded = timeKey;
						return TRUE;
					}
				}
			}
		}
	}
	return FALSE;
}

#ifdef PORTABLE
/*!
* \brief
* This method checks if there is an appropriate log created by the user for the program that he is trying to uninstall. It is important here if we
* a full match of the data of the program and the log that exist, which means the same name key and version if it exist or only a partial match
* which measn that the log is probably for an old version of the program. also it is important if the log that we found has one or two uninstall
* sections created if it is one or more with the same display name it is OK otherwise we should not suggest it as as leftover will be writeen part
* of the other program
*
* \param strFoundLogName
* List of logs name that should be filled with one or more logs
*
* \param stUnProgData
* data of the program that is being uninstalled
*
* \param bDataMatched
* Bool variable that become true if we have a full match
*
* \param bDataPartialMatched
* Bool variable that become true if we have a partial match
*
* \returns
* TRUE if there is any log or FALSE otherwise
*
*/
BOOL CheckForLogExistencePort(CStringList& strFoundLogName, LogUnSecMainData*stUnProgData, BOOL* bDataMatched, BOOL* bDataPartialMatched)
{

	DWORD dwSerachForLog = 1;
	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SEARCHFORLOG, dwSerachForLog) == FALSE)
		VSSetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_SEARCHFORLOG, dwSerachForLog);

	if (dwSerachForLog == 0)
		return FALSE;

	CString strLogsUsHelperPath;
	GetLocalAppData(strLogsUsHelperPath);
	strLogsUsHelperPath += TEXT("\\logsus.dat");

	if (PathFileExists(strLogsUsHelperPath) == FALSE)
		return FALSE;

	CIni iniLogsUsHelperFile;
	iniLogsUsHelperFile.SetPathName(strLogsUsHelperPath);

	//we have found the same section as the uninstall key of the unisntalled program
	if (iniLogsUsHelperFile.IsSectionExist(stUnProgData->m_strKeyName))
	{
		BOOL bNoMoreLogs = FALSE;
		int nUniqueIndex = 1;
		CString strTempLogNumber = TEXT("Log0");

		do
		{
			//if(iniFile.IsSectionExist(strTempLogName)== TRUE)
			if ((iniLogsUsHelperFile.IsKeyExist(stUnProgData->m_strKeyName, strTempLogNumber) == TRUE))
			{
				CString strLogSection = iniLogsUsHelperFile.GetString(stUnProgData->m_strKeyName, strTempLogNumber);
				if (CheckIfLogExist(strLogSection) == FALSE)
				{
					CString strUniqueIndex;
					strUniqueIndex.Format(TEXT("Log%d"), nUniqueIndex);
					strTempLogNumber = strUniqueIndex;
					nUniqueIndex++;
					continue;
				}

				CString strLogsusDNKey = strTempLogNumber + TEXT("DisplayName");
				CString strLogsusDNValue = iniLogsUsHelperFile.GetString(stUnProgData->m_strKeyName, strLogsusDNKey);
				if (strLogsusDNValue.IsEmpty() == FALSE)
				{
					if (IsLogAppropriate(strLogSection) == FALSE)
					{
						CString strUniqueIndex;
						strUniqueIndex.Format(TEXT("Log%d"), nUniqueIndex);
						strTempLogNumber = strUniqueIndex;
						nUniqueIndex++;
						continue;
					}

					if (stUnProgData->m_strAppName.CompareNoCase(strLogsusDNValue) == 0)
					{
						CString strLogsusVerKey = strTempLogNumber + TEXT("Version");
						CString strLogsusVerValue = iniLogsUsHelperFile.GetString(stUnProgData->m_strKeyName, strLogsusVerKey);
						if (strLogsusVerValue.IsEmpty() == FALSE)
						{
							if (stUnProgData->m_strVersion.CompareNoCase(strLogsusVerValue) == 0)
							{
								(*bDataMatched) = TRUE;
								strFoundLogName.RemoveAll();
								strFoundLogName.AddTail(strLogSection);
								return TRUE;


							}
							else
							{
								(*bDataPartialMatched) = TRUE;

								strFoundLogName.AddTail(strLogSection);
							}
						}
						else
						{
							if (stUnProgData->m_strVersion.IsEmpty() == TRUE)
							{
								(*bDataMatched) = TRUE;

								strFoundLogName.RemoveAll();
								strFoundLogName.AddTail(strLogSection);
								return TRUE;

							}
							else
							{
								(*bDataPartialMatched) = TRUE;

								strFoundLogName.AddTail(strLogSection);
							}

						}
					}
					else// key name is the same but the displayname is diffrent
					{
						(*bDataPartialMatched) = TRUE;

						strFoundLogName.AddTail(strLogSection);
					}
				}//if(regLogKey.GetStringValue(TEXT("DisplayName"),strDisplayName) == TRUE)

				CString strUniqueIndex;
				strUniqueIndex.Format(TEXT("Log%d"), nUniqueIndex);
				strTempLogNumber = strUniqueIndex;
				nUniqueIndex++;
			}
			else
				bNoMoreLogs = TRUE;
		} while (bNoMoreLogs == FALSE);
	}
	if (strFoundLogName.GetCount() > 0)
		return TRUE;
	else
		return FALSE;

}

/*!
* \brief
* This method checks if read log path is valid and if it is not, it tries to modified it guessing that the portable version is just now running on
* a different partition, because the device on which it operates is mounted on different partiion. To do the update it just replace the path to the log
* with the current path to the logs
*
* \param strReadedLogPath
* Currently written in the main Log file path to the log
*
* \returns
* void
*
*/
void UpdatePathIfNecessary(CString& strReadLogPath)
{
	if (::PathFileExists(strReadLogPath) == FALSE)
	{
		CString strCurrentRoot, strLogPathNoRoot, strCurrentLogPath;
		strCurrentLogPath = strReadLogPath;
		GetEXEParentPath(strCurrentRoot);
		PathStripToRoot(strCurrentRoot.GetBuffer());
		strCurrentRoot.ReleaseBuffer();
		strReadLogPath = strCurrentRoot + PathSkipRoot(strCurrentLogPath);
	}
}
#endif

void RemoveVersionFromString(CString& strOrigString, CString& strNoVer)
{
	if (strOrigString.IsEmpty())
		return;

	CString strSource = strOrigString;
	int nLengthFor = strSource.GetLength() - 1;
	for (int nCount = nLengthFor; nCount >= 0; nCount--)
	{
		if ((IsCharAlpha(strSource.GetAt(nCount)) == FALSE) && (strSource.GetAt(nCount) != TEXT(' ')))
		{
			strSource.Delete(nCount);
			//nCount--;
		}
		else
			break;
	}
	strSource.Trim(TEXT(" "));
	strNoVer = strSource;
}