﻿// OLD - used with .NET interface

#include "stdafx.h"
#include "ProcessUtils.h"
#include "DialogTask.h"
#include "Exports.h"



/*
	Before ListenForEvents returns, it will set this variable to the current event.
	In the event that the user wants to proceed with the event, ExecuteEvent will
	use the information in CurrentEvent to implement the execution.
*/
shared_ptr<DialogTask> CurrentEvent;
DWORD NeverShowAgainVar = 0;
time_t StartingTime;

void ExecuteEvent()
{
	auto& CurrentTask = CurrentEvent;
	static wstring IPCString;

	//MessageBox((L"Execute event with event type = " + to_wstring(CurrentTask->mode)).c_str());
	if (CurrentTask)
	{
		switch (CurrentTask->mode)
		{
		case MODE_NEWVERSION:
			ExecuteIPCRequest(GetSysConstant(C_RUPEXE).wstring() + L" /update");
			break;
		case MODE_CONTINUE_UNINSTALL:
			ExecuteIPCRequest(GetSysConstant(C_RUPEXE).wstring() + L"/continue");
			break;
		case MODE_INDIE_UNINSTALL:
			CurrentTask->scanner->CleanLeftovers();
			break;
#ifdef INSTALLER_DETECTION_ENABLED
		case MODE_INSTALL_TRACED:
			ExecuteIPCRequest(GetSysConstant(C_RUPDIR) /"RevoAppBar.exe", L"/pid " + to_wstring(CurrentTask->process->PID));
			break;
#endif
		}
	}
}


#include <taskschd.h>

Event ListenForEventsInternal(char* button_name)
{
	Event e;
	CurrentEvent = state.GetTask();

	wstring button;
	
	switch (CurrentEvent->mode)
	{
	case MODE_ADVERT:
		e.AddMember(CurrentEvent->advert_info->ImageURL);
		e.AddMember(CurrentEvent->advert_info->LinkURL);
		e.advert_mode = true;
		return e;
	case MODE_NEWVERSION:
		e.AddMember(LangString(strid::new_version_title));
		button = LangString(strid::update_button);
		break;
	case MODE_CONTINUE_UNINSTALL:
		e.AddMember(LangString(strid::unfinished_uninstall_title));
		button = LangString(strid::continue_button);
		break;
	case MODE_INSTALL_TRACED:
		e.AddMember(LangString(strid::untraced_install_title));
		button = LangString(strid::trace_button);
		break;
	case MODE_INDIE_UNINSTALL:
		e.AddMember(LangString(strid::uninstall_leftovers_title));
		button = LangString(strid::clean_button);
		break;
	}
	memcpy(button_name, button.c_str(), button.size()*sizeof(wchar_t));
	
	
	switch (CurrentEvent->mode)
	{
	case MODE_NEWVERSION:
		e.AddMember(LangString(strid::current_version_str) + (L": " + CurrentEvent->update_info->CurrentVersion));
		e.AddMember(LangString(strid::new_version_str) + (L": " + CurrentEvent->update_info->NewVersion));
		e.AddMember(LangString(strid::release_date_str) + (L": " + CurrentEvent->update_info->Date));
		break;
	case MODE_CONTINUE_UNINSTALL:
		e.AddMember(CurrentEvent->int_unin_info->m_strAppName.GetString());
		break;
	case MODE_INSTALL_TRACED:
		break;
	case MODE_INDIE_UNINSTALL:
		e.AddMember(LangString(strid::program_str) + (wstring(L": ") + CurrentEvent->scanner->m_appData->strAppName.GetString()));
		//e.AddMember((to_wstring(CurrentEvent->files_n) + L" files to delete."));
		e.AddMember((to_wstring(CurrentEvent->registry_n) + L" ") + LangString(strid::registry_items_str));
		e.AddMember((to_wstring(CurrentEvent->files_mb) + L"MB ") + LangString(strid::to_clean_str));
		break;
	}
	return e;
}

int ListenForEvents(char* buffer, char* button_name)
{
	return ListenForEventsInternal(button_name).Format(buffer);
}

void NeverShowAgain()
{
	NeverShowAgainVar |= (1 <<CurrentEvent->mode);
	VSSetSettings(CAT_HELPER, VAL_NEVER_SHOW_AGAIN, NeverShowAgainVar);
}

void InitStrings(char* strOpenRevo, char* strSettings, char* strExit, char* strNeverShowAgain, char* strToolTip)
{
	auto cpy_str = [](char* dst, const wchar_t* src) { memset(dst, 0, 128); memcpy(dst, src, lstrlenW(src) * sizeof(wchar_t)); };
	cpy_str(strOpenRevo, LangString(open_revo_str));
	cpy_str(strSettings, LangString(settings_str));
	cpy_str(strExit, LangString(exit_str));
	cpy_str(strNeverShowAgain, LangString(never_show_again_str));
	cpy_str(strToolTip, LangString(tool_tip_str));
}

int GetMode()
{
	return APPMODE;
}