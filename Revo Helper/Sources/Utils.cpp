﻿#include "stdafx.h"
#include "Utils.h"
#include "VSSettings.h"
#include "rapidxml.h"
#include "UninstallerUtils.h"
#include <string>
#include <wininet.h>
#include <mstask.h>

#pragma comment(lib, "mstask.lib")
#pragma comment(lib, "wininet.lib")

#pragma region APPLIST
AppList::AppList()
{
	reserve(1024);
	CFile fileCacheData;
	const static struct CDPath : public fs::path
	{
		using fs::path::operator=;
		CDPath()
		{
			*this = L"%localappdata%\\VS Revo Group\\Revo Uninstaller Pro\\data\\cachedata.dat";
			EnvExpand(*this);
		}
	} Src;

	if (!fs::exists(Src))
		return;

	auto ReadCache = [&]()
	{
		CTime* arrCacheTimes = new CTime[6];

		if (fileCacheData.Open(Src.c_str(), CFile::modeRead | CFile::modeNoTruncate) > 0)
		{
			CArchive ar(&fileCacheData, CArchive::load);
			int nAppCount = 0;
			float fRUVer = 0;
			TRY
			{
				ar >> fRUVer;
			ar >> nAppCount;

			for (int i = 0; i < 4; i++)
			{
				ar >> arrCacheTimes[i];
			}

			if (Is64BitWindows())
			{
				ar >> arrCacheTimes[4];
				ar >> arrCacheTimes[5];
			}

			}
				CATCH(CException, pEx)
			{
				fileCacheData.Abort();
				ar.Abort();
				return false;
			}
			END_CATCH
				for (int i = 0; i < nAppCount; i++)
				{
					shared_ptr<CInstalledAppData> temp = make_shared<CInstalledAppData>();
					if (fRUVer == 2.0)
					{
						TRY
						{
							ar >> temp->ftLastWriteTime.dwHighDateTime >> temp->ftLastWriteTime.dwLowDateTime
							>> temp->nIconIndex >> temp->strAppName >> temp->strComment >> temp->strDate >> temp->strHelpLink
							>> temp->strIconPath >> temp->strInstallLocation >> temp->strKeyName >> temp->strPublisher
							>> temp->strRoot >> temp->strOurSize >> temp->nSize >> temp->strUninstString >> temp->strVersion
							>> temp->bHasParentKeyName >> temp->bIsSystemComponent
							>> temp->strFullKeyName >> temp->nAddedtoGroup >> temp->b64Bit >> temp->bIsMSI;



						temp->statMatchtoReg = only_in_cache;
						}
							CATCH(CException, pEx)
						{
							fileCacheData.Abort();
							ar.Abort();
							return false;
						}
						END_CATCH

					}
					if (fRUVer == 3.0 || fRUVer == 4.0)
					{
						TRY
						{
							ar >> temp->ftLastWriteTime.dwHighDateTime >> temp->ftLastWriteTime.dwLowDateTime
							>> temp->nIconIndex >> temp->strAppName >> temp->strComment >> temp->strDate >> temp->strHelpLink
							>> temp->strIconPath >> temp->strInstallLocation >> temp->strKeyName >> temp->strPublisher
							>> temp->strRoot >> temp->strOurSize >> temp->nSize >> temp->strUninstString >> temp->strVersion
							>> temp->bHasParentKeyName >> temp->bIsSystemComponent
							>> temp->strFullKeyName >> temp->nAddedtoGroup >> temp->b64Bit >> temp->bIsMSI;

						temp->statMatchtoReg = only_in_cache;
						}
							CATCH(CException, pEx)
						{
							fileCacheData.Abort();
							ar.Abort();
							return false;
						}
						END_CATCH

					}

					push_back(temp);
				}
		}
		return true;
	};
	auto CheckForNewApps = [&](AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, CTime* arrCacheTimes, BOOL bForcedRefresh)
	{
		BOOL bOldNewByDays = TRUE;
		FILETIME ftCalcTimeToCompare = { 0,0 };
		CheckGroupingMode(&ftCalcTimeToCompare, &bOldNewByDays);
		// read 32 bit programs

		GetUninstallSectionAppData(HKEY_LOCAL_MACHINE, listCachedApps, modTime, bCheckingForNewApps,
								   ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_32KEY, arrCacheTimes, bForcedRefresh);
		if (Is64BitWindows() == FALSE)
			GetUninstallSectionAppData(HKEY_CURRENT_USER, listCachedApps, modTime, bCheckingForNewApps,
									   ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_32KEY, arrCacheTimes, bForcedRefresh);
		GetMSIAppData(listCachedApps, modTime, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays,
					  KEY_WOW64_32KEY, arrCacheTimes, bForcedRefresh);
		// read 64 bit programs
		if (Is64BitWindows())
		{
			GetUninstallSectionAppData(HKEY_LOCAL_MACHINE, listCachedApps, modTime, bCheckingForNewApps,
									   ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_64KEY, arrCacheTimes, bForcedRefresh);
			GetUninstallSectionAppData(HKEY_CURRENT_USER, listCachedApps, modTime, bCheckingForNewApps,
									   ftCalcTimeToCompare, bOldNewByDays, KEY_WOW64_64KEY, arrCacheTimes, bForcedRefresh);
			GetMSIAppData(listCachedApps, modTime, bCheckingForNewApps, ftCalcTimeToCompare, bOldNewByDays,
						  KEY_WOW64_64KEY, arrCacheTimes, bForcedRefresh);
		}
	};

	if (!ReadCache())
	{
		clear();
		CTime arrCacheTimes[6];

		CheckForNewApps(*this, arrCacheTimes[0], TRUE, arrCacheTimes, TRUE);
	}
}

void AppList::push_back(shared_ptr<CInstalledAppData> appdata)
{
	if (appdata->strUninstString.IsEmpty()) return;
	int dot_exe = appdata->strUninstString.Find(L".exe");

	if (dot_exe == -1) dot_exe = appdata->strUninstString.Find(L".EXE");

	if (dot_exe > 0)
	{
		int relevant_space = appdata->strUninstString.Find(' ', dot_exe);
		if (relevant_space > -1)
		{
			wstring path = appdata->strUninstString.Left(relevant_space).GetString();
			appdata->UninstallStringArguments = appdata->strUninstString.Right(appdata->strUninstString.GetLength() - relevant_space - 1).GetString();
			path.erase(remove(path.begin(), path.end(), L'"'), path.end());
			to_lower(path);
			appdata->UninstallStringPath = path;
		}
		else
		{
			wstring path = appdata->strUninstString.GetString();
			path.erase(remove(path.begin(), path.end(), L'"'), path.end());
			to_lower(path);
			appdata->UninstallStringPath = path;
		}
	}
	static wofstream ofs("C:\\Temp\\progs.txt", ios::trunc);
	ofs << appdata->UninstallStringPath << L"  " << appdata->UninstallStringArguments << endl;

	vector<shared_ptr<CInstalledAppData>>::push_back(appdata);
}


AppList* GetCache()
{
	static AppList list;
	return&list;
}


void WriteUnicodetoFile(const fs::path& myFile, wstring& ws)
{
	ofstream outFile(myFile, std::ios::out | std::ios::binary);
	outFile.write((char*)ws.c_str(), ws.length() * sizeof(wchar_t));
}

inline void stream_write(wstring& ofs, CString& data)
{
	ofs += data.GetBuffer();
	ofs += L'\n';
}

template<typename T>
inline void stream_write(wstring& ofs, T n)
{
	ofs += to_wstring(n) + L'\n';
}

inline void stream_write(wstring& ofs, FILETIME& ft)
{
	stream_write(ofs, ft.dwLowDateTime);
	stream_write(ofs, ft.dwHighDateTime);
}
bool WriteIAppDataToFile(fs::path const& filename, shared_ptr<CInstalledAppData>& AppData)
{
	wstring ofs;
#define write(x) stream_write(ofs, x)

	write(AppData->b64Bit);
	write(AppData->bHasParentKeyName);
	write(AppData->bIsMSI);
	write(AppData->bIsSystemComponent);
	write(AppData->ftLastWriteTime);
	write(AppData->nAddedtoGroup);
	write(AppData->nIconIndex);
	write(AppData->nSize);
	write(AppData->statMatchtoReg);
	write(AppData->strAppName);
	write(AppData->strComment);
	write(AppData->strDate);
	write(AppData->strFullKeyName);
	write(AppData->strHelpLink);
	write(AppData->strIconPath);
	write(AppData->strInstallLocation);
	write(AppData->strKeyName);
	write(AppData->strOurSize);
	write(AppData->strPublisher);
	write(AppData->strRoot);
	write(AppData->strUninstString);
	write(AppData->strVersion);
	WriteUnicodetoFile(filename, ofs);

#undef write
	return true;
}
#pragma endregion

fs::path GetTemporaryFilename()
{
	static struct temp_path_
	{
		wchar_t path[MAX_PATH * 2] = { 0 };
		temp_path_()
		{
			GetTempPath(MAX_PATH * 2, path);
		}
		operator wchar_t* () { return path;}
	} temp_path;

	wchar_t temp[MAX_PATH * 2] = { 0 };
	GetTempFileName(temp_path, L"rup_", 0, temp);

	return temp;
}

BOOL DWReadRestartDataFile(const fs::path &strFilePath, int& nWizardMode, DWORD& dwLogUninstallMethod, CStringArray* parrMatchedLogPaths
	, UninstallInfo* pUninstallerData, int& nUninstallMode)
{
	CFile fileCacheData;

	_int64 n64FileSize = 0;
	CTime time;
	GetFileData(strFilePath.c_str(), &n64FileSize, &time);

	if (n64FileSize == 0)
		return FALSE;

	//CFileException e;
	if (fileCacheData.Open(strFilePath.c_str(), CFile::modeRead | CFile::modeNoTruncate)>0)
	{

		CArchive ar(&fileCacheData, CArchive::load);
		int nMatchedLogsCount = 0;
		CString strLogPath;
		TRY
		{
			ar >> nWizardMode;
		ar >> dwLogUninstallMethod;
		ar >> nMatchedLogsCount;
		for (int i = 0; i < nMatchedLogsCount; i++)
		{
			ar >> strLogPath;
			if (parrMatchedLogPaths)
			parrMatchedLogPaths->Add(strLogPath);
		}


		ar >> pUninstallerData->m_strAppName;
		ar >> pUninstallerData->m_strUninstString >> pUninstallerData->m_strKeyName
			>> pUninstallerData->m_strFullKeyPath >> pUninstallerData->m_strInstallLocation >> pUninstallerData->m_strMSIGUID
			>> pUninstallerData->m_strDispIcon >> pUninstallerData->m_bMSI >> pUninstallerData->m_b64Bit >> nUninstallMode;


		}
			CATCH(CException, pEx)
		{
			//CString strText;
			//m_IniData->GetValue(IDS_ERRORREADINGREGFILE, &strText);
			//AfxMessageBox(LangString(strid::error_reading_file));
			fileCacheData.Abort();
			CFile::Remove(strFilePath.c_str());
			ar.Abort();
			return FALSE;
		}
		END_CATCH

	}
	else
	{
		AfxMessageBox(TEXT("Error reading data file. Be sure you have appropriate permissions"));
		//fileCacheData.Close();
		return FALSE;
	}

	return TRUE;
}

rapidxml::xml_document<char>* GetXMLConfigFile()
{
	static struct _
	{
		ReadFileToBuffer* buffer = nullptr;
		rapidxml::xml_document<char>* doc = nullptr;
		_()
		{
			HRESULT   hr;
			CString   sURL, sFile;
			//buffer = new ReadFileToBuffer("C:\\Users\\ognian\\Desktop\\updatepro4.xml");
			//goto parse;
			

			sURL = UPDATE_XML_FILE;

			//DeleteUrlCacheEntry(sURL); -

			DWORD	dwBufSize = 4096;
			TCHAR	szTempName[MAX_PATH * 2];
			TCHAR	lpPathBuffer[4096];

			GetTempPath(dwBufSize,   // length of the buffer
						lpPathBuffer);      // buffer for path 

											// Create a temporary file. 
			GetTempFileName(lpPathBuffer, // directory for temp files 
							_T("REV"),                // temp file name prefix 
							0,                        // create unique name 
							szTempName);              // buffer for name 

			sFile = szTempName;

			DeleteUrlCacheEntry(sURL);

			hr = URLDownloadToFile(NULL,      // ptr to ActiveX container
								   sURL,      // URL to get
								   sFile,     // file to store data in
								   0,         // reserved
								   0  // ptr to IBindStatusCallback
			);

			if (SUCCEEDED(hr))
				buffer = new ReadFileToBuffer(sFile.GetString());

			DeleteFile(sFile);
			if (buffer && buffer->b)
			{
				doc = new rapidxml::xml_document<char>;
				doc->parse<0>(buffer->b);
			}
		}
		~_()
		{
			if (doc) delete doc;
			if (buffer) delete buffer;
		}
	} s;
	return s.doc;
}


time_t SecondsElapsedSince(const wchar_t* RegistryValueName)
{
	static time_t today = time(0);
	QWORD Value;
	if (!VSGetSettings(CAT_HELPER, RegistryValueName, Value)) return 0xffffffffffff;
	return today - Value;
}

time_t DaysElapsedSince(const wchar_t* RegistryValueName)
{
	return SecondsElapsedSince(RegistryValueName) / DAY;
}

void SetToNow(const wchar_t* RegistryValueName)
{
	QWORD t = time(0);
	VSSetSettings(CAT_HELPER, RegistryValueName, t);
}


shared_ptr<UpdateInfo> CheckUpdate()
{
	if (SecondsElapsedSince(VAL_LAST_UPDATE_NOTIFICATION) < UPDATE_NOTIFICATION_INTERVAL)
		return nullptr;

	
	shared_ptr<UpdateInfo> ui = nullptr;


	auto doc = GetXMLConfigFile();
	if (doc)
	{
		auto n = doc->first_node("UNINSTALLER");
		if (!n) goto __fail;
		n = n->first_node("INFO");
		if (!n) goto __fail;
		ui = make_shared<UpdateInfo>();
		string version = n->first_node("VERSION")->value();
#ifdef PRO_ENABLED
		string date = n->first_node("DATE")->value();
		ui->Date = wstring(date.begin(), date.end());
#endif
		ui->NewVersion = wstring(version.begin(), version.end());

 	}


	// Check if the versions are the same, return nullptr in that case
	if (ui)
	{
		CRegistry reg(HKEY_LOCAL_MACHINE);
		reg.Open(REG_UNINSTALL_KEY);
		wchar_t Buffer[1024] = { 0 };
		DWORD size = 1024;
		CRegistry::KeyValueTypes kvt = CRegistry::KeyValueTypes::typeString;
		reg.QueryValue(L"DisplayVersion", kvt, (LPBYTE)Buffer, size);

		ui->CurrentVersion = Buffer;
		if (accumulate(ui->CurrentVersion.begin(), ui->CurrentVersion.end(), 0, [](int& a, wchar_t const& b) { if (b == L'.') return ++a; return a; }) < 3)
			ui->CurrentVersion += L".0";

		if (ui->CurrentVersion == ui->NewVersion) return nullptr;
		ui->SetToNowKey = (wchar_t*)VAL_LAST_UPDATE_NOTIFICATION;
	}

	__fail:
	return ui;
}


const activation_state& GetActivationState()
{
#ifndef PRO_ENABLED
	static activation_state activation(false,false,true,0);
	return activation;
#else

	static time_t LastCheckTime = 0;
	static activation_state activation = { 0,0,0,0};


	if ((time(0) - LastCheckTime) > 3600)
	{
		LastCheckTime = time(0);
		activation.ActivationDays = 0;

		VSDeleteSettings(CAT_HELPER, VAL_REMAINING_DAYS_PRO);
		VSDeleteSettings(CAT_GENERAL, VAL_TRIALEXPDAY);
		VSDeleteSettings(CAT_GENERAL, VAL_TRIALEXPMONTH);
		VSDeleteSettings(CAT_GENERAL, VAL_TRIALEXPYEAR);
		VSDeleteSettings(CAT_GENERAL, VAL_TRIALEXP);
		VSDeleteSettings(CAT_GENERAL, VAL_EXPIREDON);


		HANDLE hProcess;
		bool failed = true;
		if (InvokeRevo(L"/chactivation", &hProcess))
		{
			WaitForSingleObject(hProcess, INFINITE);
			CloseHandle(hProcess);
			failed = !VSGetSettings(CAT_HELPER, VAL_REMAINING_DAYS_PRO, activation.ActivationDays);
			VSDeleteSettings(CAT_HELPER, VAL_REMAINING_DAYS_PRO);
			activation.ActivationDays++;
		}

		if (failed || activation.ActivationDays == -1)
		{
			failed = !VSGetSettings(CAT_GENERAL, VAL_TRIALEXP, activation.ActivationDays);

			if (failed || activation.ActivationDays == -1)
			{
				DWORD iDay = 1, iMonth = 1, iYear = 1970;
				if (!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPDAY, iDay) ||
					!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPMONTH, iMonth) ||
					!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPYEAR, iYear))
					return activation;

				tm time_point;
				memset(&time_point, 0, sizeof(time_point));
				time_point.tm_year = iYear - 1900;
				time_point.tm_mday = iDay;
				time_point.tm_mon = iMonth - 1;

				time_t c = _mkgmtime(&time_point);
				time_t t0 = time(0);

				if (t0 > c)
				{
					activation.Trial = true;
					activation.Licensed = false;
					activation.ActivationDays = 0;
					return activation;
				}
				c -= t0;
				activation.ActivationDays = c / DAY;
			}
		}

#ifdef _DEBUG	
		cout << "Licensed days: " << activation.ActivationDays << endl;
#endif
	}


	//MessageBox(to_wstring(dwRemainingDays).c_str());
	return activation;
#endif
}


bool GlobalActivated()
{
	auto ActState = GetActivationState();
	return (ActState.Licensed || ActState.Trial) && ActState.ActivationDays > 0;
}

bool subscription_about_to_expire()
{
#ifdef PRO_ENABLED
	auto act = GetActivationState();
	return act.Licensed && act.ActivationDays <= SUBSCRIPTION_EXPIRY_WARNING_DAYS;
#else
	return false;
#endif
}

shared_ptr<Advertisement> advert_from_xml(rapidxml::xml_node<char>* node)
{
	auto attr = node->first_node("IMG");
	if (!attr) return nullptr;

	auto advert = make_shared<Advertisement>();
	advert->ImageURL = attr->value();
	attr = node->first_node("LINK");
	if (!attr) return nullptr;

	advert->LinkURL = attr->value();
	return advert;
}

shared_ptr<Advertisement> GetAdvert()
{
	auto doc = GetXMLConfigFile();
	if (!doc) return nullptr;
	auto uninstaller = doc->first_node("UNINSTALLER");
	if (!uninstaller) return nullptr;
	auto advertising = uninstaller->first_node("ADVERTISING");
	if (!advertising) return nullptr;
	

	/*auto universal_adverts = advertising->first_node("UNIVERSAL");
	if (universal_adverts)
	{

	}*/

#ifdef PRO_ENABLED
	if (subscription_about_to_expire() && 
		SecondsElapsedSince(VAL_LAST_SUBSCRIPTION_EXPIRY_WARNING) > SUBSCRIPTION_EXPIRY_WARNING_INTERVAL)
	{
		auto renew_subscription_adverts = (advertising->first_node("RENEW"));

		if (renew_subscription_adverts)
		{
			auto ad = advert_from_xml(renew_subscription_adverts);
			ad->SetToNowKey = VAL_LAST_SUBSCRIPTION_EXPIRY_WARNING;
			return ad;
		}
	}
	else
#else
	if (SecondsElapsedSince(VAL_LAST_FREE_ADVERT) > ADVERT_NOTIFICATION_INTERVAL)
	{
		auto free_adverts = advertising->first_node("FREE");
		if (free_adverts)
		{
			auto ad = advert_from_xml(free_adverts);
			ad->SetToNowKey = VAL_LAST_FREE_ADVERT;
			return ad;
		}
	}
	
#endif
	
	return nullptr;
}



bool IsSubDirectory(const wstring& ToTest, const wstring& Parent)
{
	if (!ToTest.size() || !Parent.size()) return false;
	return ToTest.find(Parent) == 0;
}


UninstallInfo CIAD2UninstallInfo(CInstalledAppData* pElem)
{
	UninstallInfo oUninstallerData;
	oUninstallerData.m_strAppName = pElem->strAppName;
	oUninstallerData.m_strInstallLocation = pElem->strInstallLocation;
	//oUninstallerData.m_strUninstString = pElem->strUninstString ;
	/*if (m_bForcedMSIUninstall)
	//	oUninstallerData.m_strUninstString = m_strForcedUnCommand;
	else*/ oUninstallerData.m_strUninstString = pElem->strUninstString;
	oUninstallerData.m_strKeyName = pElem->strKeyName;
	oUninstallerData.m_strFullKeyPath = pElem->strRoot + TEXT("\\") + pElem->strFullKeyName;
	if (pElem->bIsMSI)
		oUninstallerData.m_strMSIGUID = pElem->strKeyName;
	oUninstallerData.m_bMSI = pElem->bIsMSI;
	oUninstallerData.m_b64Bit = pElem->b64Bit;
	oUninstallerData.m_strDispIcon = pElem->strIconPath;
	
	return oUninstallerData;
}


CInstalledAppData::CInstalledAppData()
{
	strRoot = TEXT("");
	strKeyName = TEXT("");
	strAppName = TEXT("");
	strUninstString = TEXT("");
	strIconPath = TEXT("");
	nIconIndex = 0;
	strInstallLocation = TEXT("");
	strOurSize = TEXT("");
	nSize = 0;
	strVersion = TEXT("");
	strDate = TEXT("");
	strPublisher = TEXT("");
	strHelpLink = TEXT("");
	strComment = TEXT("");
	ftLastWriteTime.dwHighDateTime = 0;
	ftLastWriteTime.dwLowDateTime = 0;
	bHasParentKeyName = FALSE;
	bIsSystemComponent = FALSE;
	statMatchtoReg = AppStatus::only_in_cache;
	nAddedtoGroup = -1;
	b64Bit = FALSE;
	bIsMSI = FALSE;
}


/*
void ReadCache(CIniData* m_IniData, )
{
	//g_eDefUnCaching.ResetEvent();
	CTime timeLastMod;
	//g_csUninstaller.Lock();
	if (ReadCacheDataFromFile(&m_listCachedApps, &timeLastMod, m_arrCacheTimes, m_IniData) == FALSE)
	{
		m_bCheckingForNewApps = FALSE;
		RebuildCache(m_arrCacheTimes);
	}
	else
	{
		m_bCheckingForNewApps = TRUE;
		CheckForNewApps(&m_listCachedApps, timeLastMod, m_bCheckingForNewApps, m_arrCacheTimes, m_bForcedRefresh);

	}
	//m_nItemsToShow = GetItemsToShow(&m_listCachedApps);
	//g_csUninstaller.Unlock();

	//g_eDefUnCaching.SetEvent();
}
*/

bool DetectedUninstallerFromRevo(CString const& strAppName)
{
	CString CurrentlyUninstalling;
	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGS, VAL_CURRENTLY_UNINSTALLING, CurrentlyUninstalling))
	{
		VSDeleteSettings(CAT_UNINSTALLER_ALLPROGS, VAL_CURRENTLY_UNINSTALLING);
		return CurrentlyUninstalling == strAppName;
	}
	return false;
}




int windows_system(std::wstring cmd)
{
	PROCESS_INFORMATION p_info;
	STARTUPINFO s_info;
	DWORD ReturnValue = -1;

	memset(&s_info, 0, sizeof(s_info));
	memset(&p_info, 0, sizeof(p_info));
	s_info.cb = sizeof(s_info);

	if (CreateProcess(NULL, (wchar_t*)cmd.c_str(), NULL, NULL, 0, 0, NULL, NULL, &s_info, &p_info))
	{
		WaitForSingleObject(p_info.hProcess, INFINITE);
		GetExitCodeProcess(p_info.hProcess, &ReturnValue);
		CloseHandle(p_info.hProcess);
		CloseHandle(p_info.hThread);
	}
	return ReturnValue;
}

void check_set_autorun()
{
	//if (task_exists(L"RevoHelperStartup"))
	//	return;
	if (windows_system(L"schtasks /QUERY /tn " HELPER_AUTOSTART_TASK_NAME)) // Query if task exists, schtasks returns non-zero if it errors.
	{

		const wchar_t* task_xml = L"<?xml version=\"1.0\" encoding=\"UTF-16\" ?>"
			"<Task xmlns=\"http://schemas.microsoft.com/windows/2004/02/mit/task\">"
			"<Triggers>\n"
			"<LogonTrigger>\n"
			"<Enabled>true</Enabled>\n"
			"</LogonTrigger>\n"
			"</Triggers>\n"
			"<Principals>\n"
			"<Principal>\n"
			"<RunLevel>HighestAvailable</RunLevel>\n"
			"</Principal>\n"
			"</Principals>\n"
			"<Settings>\n"
			"<Enabled>true</Enabled>\n"
			"<DisallowStartIfOnBatteries>false</DisallowStartIfOnBatteries>"
			"</Settings>\n"
			"<Actions>\n"
			"<Exec>\n"
			"<Command>\"";


		wchar_t MFN[4096] = { 0 };
		GetModuleFileName(NULL, MFN, 4096);

		wstring ws = task_xml;
		ws += MFN;
		ws += L"\"</Command></Exec></Actions></Task>";

		ofstream ofs("temp.xml", ios::binary | ios::trunc);
		ofs.write("\xff\xfe", 2);
		ofs.write((char*)ws.c_str(), ws.size() * sizeof(wchar_t));
		ofs.close();

		windows_system(L"schtasks /create /XML temp.xml /tn " HELPER_AUTOSTART_TASK_NAME);

		DeleteFile(L"temp.xml");
	}

}

DWORD GetRUPID()
{
	auto th32 = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);


	PROCESSENTRY32W proc_entry;
	memset(&proc_entry, 0, sizeof(proc_entry));
	proc_entry.dwSize = sizeof(proc_entry);
	
	if (Process32First(th32, &proc_entry)) do
	{
		fs::path exe = proc_entry.szExeFile;
		if (exe.filename() == "RevoUninPro.exe" || exe.filename() == "RevoUnin.exe")
		{
			CloseHandle(th32);
			return proc_entry.th32ProcessID;
		}
	}
	while (Process32Next(th32, &proc_entry));
	
	CloseHandle(th32);
	return NULL;
}


DWORD GetAnyTID(DWORD dwPID)
{
	auto hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 th32;
		th32.dwSize = sizeof(THREADENTRY32);
		BOOL bOK = TRUE;
		for (bOK = Thread32First(hThreadSnap, &th32); bOK;
			 bOK = Thread32Next(hThreadSnap, &th32))
		{
			if (th32.th32OwnerProcessID == dwPID)
			{
				CloseHandle(hThreadSnap);
				return th32.th32ThreadID;
			}
		}
	}
	CloseHandle(hThreadSnap);
	return 0;
}

void SetToForeground(DWORD dwPID)
{

	EnumWindows([](HWND Window, LPARAM Param) -> BOOL
	{
		//if (!::IsWindowVisible(Window)) return true;

		DWORD* RUPID = (DWORD*)Param;

		DWORD PID;
		GetWindowThreadProcessId(Window, &PID);

		if (PID == *RUPID)
		{
			SetForegroundWindow(Window);
		}

		return TRUE;

	}, (LPARAM)&dwPID);


}


bool InvokeRevo(wstring const& args, HANDLE* phProcessHandle)
{
	if (args.empty())
	{
		auto dwPID = GetRUPID();

		if (dwPID)
		{
			SetToForeground(dwPID);
			return true;
		}

	}

	wstring Request = GetWorkingDirectory() / GetSysConstant(C_RUPEXE);
	if (args.size()) Request += L" " + args;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroObj(si);
	ZeroObj(pi);
	if (CreateProcess(NULL, (wchar_t*)Request.c_str(), NULL, NULL, FALSE, 0, NULL, GetWorkingDirectory().c_str(), &si, &pi))
	{
		CloseHandle(pi.hThread);
		if (phProcessHandle)
		{
			*phProcessHandle = pi.hProcess;
			return true;
		}

		CloseHandle(pi.hProcess);

		return true;
	}
	return false;
}

bool ShouldRun()
{
	SetLastError(0);
	CreateMutexA(0, TRUE, MUTEX_NAME); // The handle will be freed automatically on process exit so no need to deal with the variable.
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		ErrorMsg(LangString(already_running_str).c_str());
		return false;
	}
	return true;
}

void ErrorMsg(wstring&& str)
{
	MessageBoxW(0, str.c_str(), 0, 0);
}


void ErrorMsg(const wchar_t* str)
{
	MessageBoxW(0, str, 0, 0);
}



activation_state::activation_state(bool Trial, bool Licensed, bool Free, DWORD ActivationDays) : Trial(Trial), Licensed(Licensed), Free(Free), ActivationDays(ActivationDays)
{}