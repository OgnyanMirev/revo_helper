#include "stdafx.h"
#include "DriverClient.h"
#include "gui.h"
#include <Psapi.h>
#include "ProcessUtils.h"

#define PROCESS_CREATION_NOTIFY_DEAD_TREE -1


/*
	This piece of abstraction is largely superfluous in the current version of the tracer, 
	but I've decided to leave it as a convenient place to make future changes in the interconnect 
	between the static and dynamic analysis.
*/

struct InlineTracer
{
	CDriverClient driver;
	bool CurrentlyTracing = false,
		bValid = false;


	//thread t;

	InlineTracer()
	{
		bValid = driver.Initialize();
	}
	bool TraceProcess(shared_ptr<ProcessInfo> Process, bool AlreadyDetectedInstaller)
	{
		/*
		auto ret =  WaitForSingleObject(t.native_handle(), 0);

		if (ret == WAIT_TIMEOUT)
		{
			if (driver.DetectedInstaller()) return false;
			else
			{

			}
		}*/

		return driver.StartLogging(Process, AlreadyDetectedInstaller);
	}

	~InlineTracer()
	{
		driver.StopLogging();
		driver.Deinitialize();
		driver.DeinstallDriver();
	}
};


void TraceProcess(shared_ptr<ProcessInfo> Process, bool AlreadyDetectedInstaller)
{
	static InlineTracer tracer;
	if (!tracer.bValid) return;
	tracer.TraceProcess(Process, AlreadyDetectedInstaller);
}
