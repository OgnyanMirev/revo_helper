#include "stdafx.h"
#include "VSSettings.h"
#include "Registry.h"
#include "Ini.h"

BOOL VSGetSettings(LPCTSTR psztCategoryName, CStringArray &retArrayValueNames, CStringArray &retArrayData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

#ifdef PORTABLE
	//Read from XML

	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	oIni.GetKeyNames(psztCategoryName, &retArrayValueNames);

	BOOL bRes = FALSE;
	for (int i = 0; i<retArrayValueNames.GetCount(); i++)
	{
		bRes = TRUE;
		retArrayData.Add(oIni.GetString(psztCategoryName, retArrayValueNames.GetAt(i)));

	}
	return bRes;

#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Open(pszFullCatPath))
	{
		//The name and the count of values is unknow so get everything in retArray
		Reg.QueryInfo();
		DWORD dwValNum = Reg.GetNumberOfValues();

		for (DWORD dwIndex = 0; dwIndex<dwValNum; dwIndex++)
		{
			CRegistry::KeyValueTypes value_type;
			CString strName;
			DWORD dwDataSize = Reg.GetValueDataSize();
			LPBYTE data = new BYTE[dwDataSize];
			::ZeroMemory(data, dwDataSize);
			Reg.EnumerateValues(dwIndex, strName, value_type, data, dwDataSize);
			switch (value_type)
			{
			case REG_SZ:
			case REG_MULTI_SZ:
			case REG_EXPAND_SZ:
				retArrayData.Add((TCHAR*)data);
				retArrayValueNames.Add(strName);
				break;
			case REG_DWORD:
			{
				TCHAR szBuff[12] = { 0 };
				_tcprintf_s(szBuff, _T("%d"), *data);
				retArrayData.Add(szBuff);

				retArrayValueNames.Add(strName);
			}
			break;
			}

			delete[] data;
		}
		Reg.Close();

		delete[] pszFullCatPath;
		return TRUE;
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}
BOOL VSSetSettings(LPCTSTR psztCategoryName, CStringArray &setArrayValueNames, CStringArray &setArrayData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

	INT_PTR nNameArrLen = setArrayValueNames.GetSize();
	INT_PTR nDataArrLen = setArrayData.GetSize();

	if (nDataArrLen != nNameArrLen)
		return FALSE;

#ifdef PORTABLE
	//Write to XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	for (int i = 0; i<setArrayValueNames.GetCount(); i++)
	{
		oIni.WriteString(psztCategoryName, setArrayValueNames.GetAt(i), setArrayData.GetAt(i));
	}

	return TRUE;

#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Create(pszFullCatPath))
	{
		for (int i = 0; i<nNameArrLen; i++)
			Reg.SetStringValue(setArrayValueNames.GetAt(i), setArrayData.GetAt(i));

		Reg.Close();

		delete[] pszFullCatPath;
		return TRUE;
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}

BOOL VSGetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, DWORD &retData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

	if (_tcslen(psztValueNames) == 0)
		return FALSE;

#ifdef PORTABLE
	//Read from XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	DWORD dwRet = oIni.GetUInt(psztCategoryName, psztValueNames, 100001);
	if (100001 == dwRet)
		return FALSE;
	else
	{
		retData = dwRet;
		return TRUE;
	}

#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Open(pszFullCatPath))
	{
		if (Reg.GetDoubleWordValue(psztValueNames, retData))
		{
			Reg.Close();
			delete[] pszFullCatPath;
			return TRUE;
		}
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;

}
BOOL VSSetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, DWORD setData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

	if (_tcslen(psztValueNames) == 0)
		return FALSE;

#ifdef PORTABLE
	//Write to XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	oIni.WriteUInt(psztCategoryName, psztValueNames, setData);
	return TRUE;
#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Create(pszFullCatPath))
	{
		Reg.SetDoubleWordValue(psztValueNames, setData);
		Reg.Close();

		delete[] pszFullCatPath;
		return TRUE;
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}

BOOL VSGetSettings(LPCTSTR psztCategoryName, CStringArray &retArrayValueNames, CUIntArray &retArrayData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

#ifdef PORTABLE
	//Read from XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	oIni.GetKeyNames(psztCategoryName, &retArrayValueNames);

	BOOL fRes = FALSE;
	for (int i = 0; i<retArrayValueNames.GetCount(); i++)
	{
		fRes = TRUE;
		retArrayData.Add(oIni.GetUInt(psztCategoryName, retArrayValueNames.GetAt(i), 0));
	}

	return fRes;

#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Open(pszFullCatPath))
	{
		//The name and the count of values is unknow so get everything in retArray
		Reg.QueryInfo();
		DWORD dwValNum = Reg.GetNumberOfValues();

		for (DWORD dwIndex = 0; dwIndex<dwValNum; dwIndex++)
		{
			CRegistry::KeyValueTypes value_type;
			CString strName;
			DWORD dwDataSize = sizeof(DWORD);
			DWORD dwData;
			Reg.EnumerateValues(dwIndex, strName, value_type, (LPBYTE)&dwData, dwDataSize);
			retArrayData.Add(dwData);
			retArrayValueNames.Add(strName);
		}

		Reg.Close();

		delete[] pszFullCatPath;
		return TRUE;
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}

BOOL VSSetSettings(LPCTSTR psztCategoryName, CStringArray &setArrayValueNames, CUIntArray &setArrayData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

	INT_PTR nNameArrLen = setArrayValueNames.GetSize();
	INT_PTR nDataArrLen = setArrayData.GetSize();

	if (nDataArrLen != nNameArrLen)
		return FALSE;

#ifdef PORTABLE
	//Write to XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	for (int i = 0; i<setArrayValueNames.GetCount(); i++)
	{
		oIni.WriteUInt(psztCategoryName, setArrayValueNames.GetAt(i), setArrayData.GetAt(i));
	}

	return TRUE;

#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Create(pszFullCatPath))
	{
		for (int i = 0; i<nNameArrLen; i++)
			Reg.SetDoubleWordValue(setArrayValueNames.GetAt(i), setArrayData.GetAt(i));

		Reg.Close();

		delete[] pszFullCatPath;
		return TRUE;
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}


BOOL VSSetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, LPCTSTR setData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

	if (_tcslen(psztValueNames) == 0)
		return FALSE;

#ifdef PORTABLE
	//Write to XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	return oIni.WriteString(psztCategoryName, psztValueNames, setData);
#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Create(pszFullCatPath))
	{
		Reg.SetStringValue(psztValueNames, setData);

		Reg.Close();
		delete[] pszFullCatPath;
		return TRUE;
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}


BOOL VSGetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, CString &retData, HKEY hKey)
{
	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

	if (_tcslen(psztValueNames) == 0)
		return FALSE;

#ifdef PORTABLE
	//Read from XML
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	retData = oIni.GetString(psztCategoryName, psztValueNames);
	if (retData.GetLength())
		return TRUE;
#else
	//Create full path of the settings by adding category name to main location
	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	CRegistry Reg(hKey);
	if (Reg.Open(pszFullCatPath))
	{
		if (Reg.GetStringValue(psztValueNames, retData))
		{
			Reg.Close();
			delete[] pszFullCatPath;
			return TRUE;
		}
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;

}



BOOL VSGetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, QWORD& retData, HKEY hKey)
{
	fs::path reg_key = MAIN_SETTINGS_LOCATION;
	reg_key /= psztCategoryName;

	if (RegOpenKeyW(HKEY_CURRENT_USER, reg_key.c_str(), &hKey) != ERROR_SUCCESS) return FALSE;

	DWORD ValType = REG_QWORD, QWORD_size = sizeof(QWORD);
	BOOL ret = TRUE;
	if (RegQueryValueExW(hKey, psztValueNames, 0, &ValType, (PBYTE)&retData, &QWORD_size) != ERROR_SUCCESS)
		ret = FALSE;

	RegCloseKey(hKey);
	return ret;
}


BOOL VSSetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, QWORD& setData, HKEY hKey)
{
	fs::path reg_key = MAIN_SETTINGS_LOCATION;
	reg_key /= psztCategoryName;

	auto ec = RegOpenKeyW(HKEY_CURRENT_USER, reg_key.c_str(), &hKey);
	if (ec != 0)
	{
		if (ec == ERROR_FILE_NOT_FOUND)
		{
			if (RegCreateKey(HKEY_CURRENT_USER, reg_key.c_str(), &hKey) != 0) return FALSE;
		}
		else return FALSE;
	}

	BOOL ret = TRUE;
	if (RegSetKeyValueW(hKey, 0, psztValueNames, REG_QWORD, (LPCWSTR)&setData, sizeof(QWORD)) != ERROR_SUCCESS)
		ret = FALSE;

	RegCloseKey(hKey);
	return ret;
}


BOOL VSDeleteSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, BOOL bRemoveWholeCategory, HKEY hKey)
{

	if (_tcslen(psztCategoryName) == 0)
		return FALSE;

#ifdef PORTABLE
	CIni oIni;
	CString strPath;//= GetEXEPath();
	GetEXEParentPath(strPath);
	strPath += SETTINGS_FILE;
	if (::PathFileExists(strPath) == FALSE)
		WriteBOM(strPath);
	oIni.SetPathName(strPath);
	if (psztValueNames == NULL)
		return oIni.DeleteSection(psztCategoryName);
	else
		return oIni.DeleteKey(psztCategoryName, psztValueNames);
#else

	size_t stFullCatPathLen, stFullSettingsLocLen, stFinalLen;
	StringCbLength(psztCategoryName, MAX_CATEGORY_NAME_LENGTH, &stFullCatPathLen);
	StringCbLength(MAIN_SETTINGS_LOCATION, MAX_LOCATION_PATH_LENGTH, &stFullSettingsLocLen);
	stFinalLen = stFullCatPathLen + stFullSettingsLocLen + sizeof(TCHAR);

	TCHAR* pszFullCatPath = new TCHAR[stFinalLen];
	ZeroMemory(pszFullCatPath, stFinalLen);

	StringCchCopy(pszFullCatPath, stFinalLen, MAIN_SETTINGS_LOCATION);
	StringCchCat(pszFullCatPath, stFinalLen, psztCategoryName);

	if (psztValueNames)
	{
		if (SHDeleteValue(hKey, pszFullCatPath, psztValueNames) == ERROR_SUCCESS)
		{
			delete[] pszFullCatPath;
			return TRUE;
		}
		else
		{
			delete[] pszFullCatPath;
			return FALSE;
		}
	}

	if (bRemoveWholeCategory)
	{
		if (SHDeleteKey(hKey, pszFullCatPath) == ERROR_SUCCESS)
		{
			delete[] pszFullCatPath;
			return TRUE;
		}
	}
	else
	{
		CRegistry Reg(hKey);
		if (Reg.Open(pszFullCatPath))
		{
			//The name and the count of values is unknow so get everything in strArray
			Reg.QueryInfo();
			DWORD dwValNum = Reg.GetNumberOfValues();

			CStringArray strArray;
			for (DWORD dwIndex = 0; dwIndex<dwValNum; dwIndex++)
			{
				DWORD dwDataSize = Reg.GetValueDataSize();
				LPBYTE data = new BYTE[dwDataSize];
				::ZeroMemory(data, dwDataSize);

				CString strName;
				CRegistry::KeyValueTypes value_type;
				Reg.EnumerateValues(dwIndex, strName, value_type, data, dwDataSize);
				strArray.Add(strName);

				delete[] data;
			}
			Reg.Close();

			for (int i = 0; i<strArray.GetCount(); i++)
				SHDeleteValue(hKey, pszFullCatPath, strArray.GetAt(i));

			delete[] pszFullCatPath;
			return TRUE;
		}
	}
	delete[] pszFullCatPath;
#endif

	return FALSE;
}

