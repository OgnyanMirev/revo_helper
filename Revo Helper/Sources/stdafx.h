
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once
//#define _AFXDLL
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxdisp.h>        // MFC Automation classes
#include "IniData.h"

#include "defines.h"

namespace globals
{
	extern const CIniData* pLangIniSelected;
	extern const CIniData* pLangIniEnglish;

	extern const CString* pstrAppPath;
	extern const CString* pstrLangPath;
	extern double dDPICorrection;
	extern		 CFont* pfontGlobalPM;
}


#define _SECOND ((_int64) 10000000)
#define _MINUTE (60 * _SECOND)
#define _HOUR   (60 * _MINUTE)
#define _DAY    (24 * _HOUR)

#define MAINFONTSIZE	-13

#define RUP_VER 4.0

#define UNINSTALLERCOLUMNS 10
#define AUTORUNREALCOLUMNS 6

#define EXCLITEMSCOUNT 28
#define JFCEXTCOUNT 18
#define TRACED_INC_PROCESS_COUNT 5
#define TRACED_EXC_REGKEYS_COUNT 36
#define ALL_EXC_REGKEYS_COUNT 19
#define ALL_EXC_FOLDERS_COUNT 1
#define THEMESCOUNT 9
#define MAINTOOLBARDEFBUTS 5
#define BIGLOGFILESLIMIT 20000
#define BIGLOGSIZELIMIT 5000000

//#ifndef _AFX_NO_OLE_SUPPORT
//#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
//#endif
//#ifndef _AFX_NO_AFXCMN_SUPPORT
//#include <afxcmn.h>             // MFC support for Windows Common Controls
//#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxmt.h>
//#include <afxcontrolbars.h>     // MFC support for ribbons and control bars
#include <Windows.h>
#include <strsafe.h>
#include <fstream>
#include <experimental/filesystem>
#include <unordered_map>
#include <memory>
#include <string>
#include <map>
#include <numeric>
#include <algorithm>
#include "Resource.h"
#include "VSSettings.h"
#include "OldUtils.h"
#include "defs.h"

using namespace std;
namespace fs = std::experimental::filesystem::v1;
#undef MessageBox


#define ZeroObj(obj) ZeroMemory(&obj, sizeof(obj))

#define DEF_GROUPS_COUNT 4
#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

