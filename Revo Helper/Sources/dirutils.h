#pragma once
#include "defines.h"
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem::v1;

#define C_USERPROFILE 0
#define C_PROGRAMFILES 1
#define C_PROGRAMFILESX86 2
#define C_SYSTEMROOT 3
#define C_APPDATA 4
#define C_LOCALAPPDATA 5
#define C_RUPDIR 6
#define C_RUPEXE 7

const fs::path& GetSysConstant(unsigned ID);
const fs::path& GetWorkingDirectory();