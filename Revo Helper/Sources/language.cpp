﻿#include "defines.h"
#include "language.h"
#include "stdafx.h"
#include "dirutils.h"
#include <vector>
#include <string>
#include <codecvt>
#include <locale>
#include <mutex>


std::string cvt2utf8(std::wstring const& ws)
{
	static std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.to_bytes(ws);
}
struct ini
{
	CIniData cIni;
	BOOL bValid;
	wstring SelectedIni;

	std::mutex mutex;
	bool LangRefresh()
	{
		mutex.lock();
		CString strSelectedLangIni;
		cIni.ReleaseMemory();

		if (VSGetSettings(CAT_GENERAL, VAL_LANGUAGE_FILE, strSelectedLangIni) == FALSE)//if there is no setting in the Registry load the default
		{
			strSelectedLangIni = _T("english.ini");
			VSSetSettings(CAT_GENERAL, VAL_LANGUAGE_FILE, strSelectedLangIni);
		}
		
		if (strSelectedLangIni.GetString() != SelectedIni)
		{
			SelectedIni = strSelectedLangIni;
			auto lang_path = GetWorkingDirectory() / "lang" / strSelectedLangIni.GetString();
			CString cs = lang_path.c_str();
			if (fs::exists(lang_path))
				bValid = cIni.InitData(cs);
			mutex.unlock();
			return true;
		}
		mutex.unlock();
		return false;
	}

	ini()
	{
		LangRefresh();
	}
	std::wstring GetString(int id)
	{
		mutex.lock();
		CString res;
		cIni.GetValue(id, &res);
		mutex.unlock();

		return res.GetString();
	}
} lang;

std::wstring LangString(strid id)
{
	return lang.GetString(id);
}


std::string LangStringUTF8(strid id)
{
	return cvt2utf8(LangString(id));
}

bool LangRefresh()
{
	return lang.LangRefresh();
}