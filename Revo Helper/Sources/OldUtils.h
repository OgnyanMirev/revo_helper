#pragma once

#define REG_CHECKED           0x0004
#define REG_INCLUDESUBKEYS    0x0008

struct RegExcludeInfo
{
	CString m_strRegKey;
	DWORD	m_dwLoad;
};

struct FolderExcludeInfo
{
	CString m_strFolderPath;
	DWORD	m_dwLoad;
};

struct stGroups
{
	CString strName;
	int		nGroupID;
};

enum ReferenceKnownFolder
{
	LocalAppData,
	RoamingAppData
};

struct stBrowsersPath
{
	CString strBrowserName;
	CString strBrowserPath;
};

struct stDuplicatedIEItem
{
	CString strItemName;
	int nItemID;
};


CString GetEXEPath();
void GetEXEParentPath(CString& strExeParentPath);

//CString GetFileOnly(CString& strPath);

//DWORD64 GetFolderSize(LPCTSTR szPath, DWORD *dwFiles = NULL, DWORD *dwFolders = NULL);

void CmdRouteMenu(CWnd* pWnd, CMenu* pPopupMenu);

BOOL GetPathFromString(CString& strString, CString& strResult);
int GetPointPos(CString& strString, int nPos);
int GetPointCount(CString& strString);

LPTSTR StrFormatMBSize(LONGLONG qdw, LPWSTR pwszBuf, UINT cchBuf);

LPTSTR MyStrFormatKBSize(LONGLONG qdw, LPWSTR pwszBuf, UINT cchBuf);

void RegeditJump(LPCTSTR pszCompressedKey);
void OpenRegEditBits(LPCTSTR pszKeyPath, BOOL bIsKey64Bit);
void OpenRegEdit(LPCTSTR pszKeyPath);

//TCHAR* GetDirFromPath(TCHAR* pszPath);

//void PeekMessageLoop();
//void WaitForThreadToTerminate(HANDLE hThread);
BOOL VSAtlWaitWithMessageLoop(HANDLE hEvent);
void SearchAtGoogle(LPCTSTR pszToken);
BOOL RemoveWhiteSpaces(CString* strSource, CString* strDest);
BOOL RemoveSpecSymbols(CString* strSource, CString* strDest);
void CenterWindowToWorkArea(HWND hWnd);

BOOL IsStringInList(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase = TRUE);

//BOOL GetInstalledFolderName(CString* strInstLoc, CString* strFolderName);

BOOL GetLastFolderName(CString* strInstLoc, CString* strFolderName);
//BOOL IsPatternAllowedForSearch(LPCTSTR szPattern);
BOOL IsFolderAllowedForSearch(LPCTSTR pszFolderName);
BOOL IsFolderAllowedForSearchBasic(LPCTSTR pszFolderName);

BOOL GetLastFolderPath(LPCTSTR pszString, CString* strResultPath);

bool getShellLinkTarget(LPCTSTR pszLinkFile, LPTSTR pszTargetPath, DWORD buffSize);
void shellFree(void* p);

BOOL GetStartMenuItems(LPCTSTR pFolder, CStringArray* pArray);
//BOOL GetFilesFolders(LPCTSTR pFolder, CStringArray* pArray, BOOL fRecursive = TRUE);
//BOOL FindFileOrFolder(LPCTSTR pFolder, CStringArray* arrNames, BOOL fFolder, CStringArray* pArray, BOOL fRecursive = TRUE);
BOOL GetSeparatedKey(LPCTSTR strKeysPath, int nKeyNumber, CString* strOneKeyOnly);
BOOL squash_guid(LPCWSTR in, LPWSTR out);
BOOL unsquash_guid(LPCWSTR in, LPWSTR out);

BOOL IsRealString(LPCTSTR pszString);
BOOL IsRealStringForced(LPCTSTR pszString);
CString GetCurrentUserSID();

void GetWindowsVersion(int* nMajor, int* nMinor);
void GetRootHKEYByName(CString strRootName, HKEY& hRootKey);
BOOL CheckIfMSI_Has_I(CString strCommand);

BOOL SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
);
BOOL TakeRegKeyOwnership(HKEY hRoot, LPCTSTR lpszOwnKey, SE_OBJECT_TYPE eObjType);
///void ReplaceMSIItoXParameter(LPTSTR pszCommand); // obsolete
BOOL VSDeleteRegKey(HKEY hRoot, LPCTSTR pszKeyName, BOOL bIs64Bit);
BOOL VSRegDeleteKeyNoSubs(HKEY hRoot, LPCTSTR pszKeyName, REGSAM dwFlag);
#ifdef _WIN64
DWORD VSRegDeleteKeyExRecur(HKEY hStartKey, LPCTSTR pKeyName, REGSAM samFlag);
#endif
BOOL VSModifyValue(HKEY hRoot, LPCTSTR pszKeyName, HKEY hParentKey, LPCTSTR pszValueName, DWORD dwDataType,
	DWORD dwSize, LPBYTE pData, REGSAM dwFlag);

BOOL VSDeleteValue(HKEY hRoot, LPCTSTR pszKeyName, HKEY hParentKey, LPCTSTR pszValueName, REGSAM dwFlag);
BOOL VSCreateRegKey(HKEY hRoot, LPCTSTR pszParentPath, HKEY hParent, LPCTSTR pszKeyName, REGSAM dwFlag);
BOOL VSOpenRegKey(HKEY hRoot, LPCTSTR pszKeyName, PHKEY hCurrent, REGSAM dwFlag);

BOOL GetRegKeysFromString(CString &strSource, CStringArray &arrReturn);
BOOL GetFileData(const LPCTSTR szFileName, _int64* nSize, CTime* timeMod);
BOOL GetFileSize(LPCTSTR szFileName, _int64* nSize);

void GetDateAttribFromString(CString strDate, int* nDay, int* nMonth, int* nYear);
void GetHourAttribFromString(CString strHour, int* nHour, int* nMinutes, int* nSeconds);

BOOL IsStringGUID(LPCTSTR strChecked);
HKEY GetRootFromPath(CString strOurPath);
HKEY GetRootFromPath(CString strOurPath, CString* strRoot);
void RemoveRootFromPath(CString strOurPath, CString& strResultPath);

void ConvertByteArrayToCString(BYTE* arrBytes, int narrSize, CString* strResult);
int GetPathDepth(CString& strPath);

void ConvertStringListToRegPath(CStringList* strList, CString& strResult);

void ConvertMultiSzToArray(TCHAR* pszMultySz, DWORD dwSize, CStringArray& arrResult);
void ConvertMultiSzToString(CByteArray* pByteArray, DWORD dwSize, CString& arrResult);
void ConvertMultiSzToStringOld(TCHAR* pszMultySz, DWORD dwSize, CString& strResult);

BOOL GetLastKeyNameAndItsPath(CString& strFullPath, CString& strLastKeyName, CString& strLastKeyPath);

_int64 GetFileSize64(LPCTSTR szFileName);

BOOL Is64BitWindows();

//void SortTree(CExtTreeCtrl* pTree, HTREEITEM hItem);

BOOL GetSubKey(LPCTSTR pszKeysPath, int nTillKeyNumber, CString* strRetKeyPath);
BOOL TakeFileOwnership(LPTSTR lpszOwnFile);
BOOL VSRemoveDirectory(LPTSTR pszParentPath);
int VSSHFileOperation(LPSHFILEOPSTRUCT pSFO);

//nVSType = 0 - Information message; nVSType = 1 - Warning message
int VSMessageBox(HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType, int nVSType);

HRESULT ModifyPrivilege(IN LPCTSTR szPrivilege, IN BOOL fEnable);
BOOL DevicePathToDosPath(const TCHAR* pszDevicePath, CString* pstrDosPath);
//BOOL CreateFullRegBackup(LPCTSTR pszBackupFolderPath, CEvent* pEventNTU_SkipBU);

void GetLocalAppData(CString& strPath);

BOOL VSCreateDirectory(LPCTSTR szPath);

BOOL IsValidTime(LPSYSTEMTIME stUTC);

BOOL VS_IsExistingDirectory(LPCTSTR szPath);

BOOL GetRegKeyForOpen(LPCTSTR pszFullKey, CString& strKeyToOpen);

void  GetNumberofSubFolders(CString& strFolderName, int& nCounter, CString& strSubfolder); // gets also subfolder name if it is only one

BOOL RemoveLastItemFromPath(CString& strPath, CString& strResPath);

BOOL VSFileExists(LPCTSTR szPath);
BOOL VSPathExists(LPCTSTR szPath);

void GetErrorText(DWORD dwErr, CString& strErrorText);

void GetValueTypeText(DWORD dwType, CString& strType);

void UpdateRegPathIfOld(CString& strNewPath);

void FolderRemoveBadSymbols(CString& strFolder, BOOL bAllowPath = FALSE);

BOOL Is_Win81();
BOOL Is_Win10();
BOOL GetOsVersion(RTL_OSVERSIONINFOEXW* pk_OsVer);

BOOL IsTextURL(CString& strText);

BOOL RemoveLastBrackets(CString& strText);
/////------------- Revo Library
/////Folder
BOOL FindAppDataFolder(CString& p_appDataFolderLocation, ReferenceKnownFolder p_referenceKnownFolder);
void FindAllFolders(CString& strFolderName, CStringArray& parrLogsHDDItems);
//int DeleteFolder(CString folderToDelete);
//File
int RevoRemoveFile(CString fileToDelete);
int RevoRemoveFileOwnership(CString fileToDelete);
void FindAllFiles(CString& strFolderName, CStringArray& parrLogsHDDItems);

void WriteBOM(LPCTSTR szFileName);
BOOL IsStringOnlyDigits(CString& strText);

void GetStringtoFirstPoint(CString& strOrigVer, CString& strVerToPoint);

void GetArrayofKeys(CString strAllKeys, CStringArray& arrAllKeys);

#include <filesystem>
namespace fs = std::experimental::filesystem::v1;

void EnvExpand(fs::path& Path);

std::vector<std::wstring> SplitW(const std::wstring& s, wchar_t delimiter);


void FilterWString(std::wstring& Str, const std::wstring& Filter);