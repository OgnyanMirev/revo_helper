#ifndef _DRIVERCLIENT_H_INCLUDED_
#define _DRIVERCLIENT_H_INCLUDED_
#pragma once
#include "mspyLog.h"
#include "VSRegTree.h"
#include "VSFileLog.h"
#include "ProcessUtils.h"
#include <fltUser.h>
#include <mutex>

typedef struct _DRIVER_MESSAGE {

    //
    //  Required structure header.
    //

    FILTER_MESSAGE_HEADER MessageHeader;


    //
    //  Private scanner-specific fields begin here.
    //

    COMMAND_MESSAGE cmdMessage;

    //
    //  Overlapped structure: this is not really part of the message
    //  However we embed it instead of using a separately allocated overlap structure
    //

    OVERLAPPED Ovlp;
    
} DRIVER_MESSAGE, *PDRIVER_MESSAGE;




class CDriverClient
{
public:
	//Temporary for testing 
	HWND	m_hWndList;

	shared_ptr<ProcessInfo> TracedProcess;


	bool AbortTracingIfUncategorizedProgram();
public:
	CDriverClient();
	~CDriverClient();
	
	BOOL Initialize();
	bool StartLogging(shared_ptr<ProcessInfo> Process, bool already_detected_installer);
	BOOL StopLogging();

	void HandleTracingDialogs();
	
	// ID = Installer detector functions
	void ID_CheckFileRecord(VSFileLog* log);
	void ID_CheckRegRecord(PREG_RECORD reg);
	void ID_MonitorTracing();

	void ClearAllData();



	void Deinitialize();
	void DeinstallDriver();

	
	void SaveRegRecords(LPCTSTR pszFullFilePath);
	void SaveFileRecords(LPCTSTR pszFullFilePath);

	void AddDoNotLogPath(CStringArray& arrIgnoreList)
	{
		m_arrDoNotLogPaths.RemoveAll();
		m_arrDoNotLogPaths.Append(arrIgnoreList);
	}

	void AddLogPath(LPCTSTR pszPath)
	{
		m_arrDoNotLogPaths.Add(pszPath);
	}

	void AddLogPath(CStringArray& arrLogList)
	{
		m_arrLogPaths.RemoveAll();
		m_arrLogPaths.Append(arrLogList);
	}

	void AddDoNotLogPath(LPCTSTR pszPath)
	{
		m_arrLogPaths.Add(pszPath);
	}

	void GetRegLogs(CPtrList* pList)
	{
		CSingleLock singleLock(&m_CritSecReg);
		singleLock.Lock();
		if(pList)
			pList->AddHead(&m_listRegistryLog);
		singleLock.Unlock();
	}
	
	void GetFileLogs(CPtrList* pList)
	{
		if(pList)
			pList->AddHead(&m_listFileLog);
	}

	void ToggleCapturing(bool bCapFlag);

private:
	static unsigned __stdcall GetLogRecordsThread(LPVOID pThis)
	{
		((CDriverClient*)pThis)->GetLogRecords();
			return 0;
	};
	static unsigned __stdcall GetRegRecordsThread(LPVOID pThis)
	{
		((CDriverClient*)pThis)->GetRegRecords();
			return 0;
	};
	static unsigned __stdcall ProcessRegRecordsThread(LPVOID pThis)
	{
		((CDriverClient*)pThis)->ProcessRegRecords();
			return 0;
	};
	static unsigned __stdcall GetPIDRecordsThread(LPVOID pThis)
	{
		((CDriverClient*)pThis)->GetPIDRecords();
			return 0;
	};
	void GetPIDRecords();
	void GetLogRecords();
	void GetRegRecords();
	void ProcessRegRecords();
	void PeekMessageLoop();
	void WaitForThreadToTerminate(HANDLE hThread);
	DWORD* GetRunningPrcesses(DWORD& m_dwPIDsCount);
	void SetExcludedProcesses();
	void FillTree(HKEY hKey, PREGNODE pRegTree);
	void DeleteRegTree(PREGNODE pTree);
	BOOL DevicePathToDosPath(const TCHAR* pszDevicePath, CString* pstrDosPath);

	BOOLEAN TranslateFileTag(__in PLOG_FILE_RECORD logRecord);
	BOOLEAN AttachDevices(BOOL bAttach);

	BOOL EnableDebugPrivilege();
	BOOL IsStringInList(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase = TRUE);
	BOOL SendRunningPids();
	BOOL IsPIDAllowed(DWORD dwPID);
	BOOL GetFileAssociations();
	BOOL AddStringInList(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase);
	BOOL ProcessPID(PPID_RECORD pPIDRec);
	BOOL ConvertNtNametoDosName(CString& strName);

	TCHAR* GetPathFromString(TCHAR* szString);
	TCHAR* GetPointPos(TCHAR* szString,int nPos);
	int GetPointCount(TCHAR* szString);

	ULONG IsAttachedToVolume(__in PWCHAR VolumeName);

	void TerminateTracing();
	void BeginTracing();

private:

	bool bIsCapturing = false, tracing_done_flag = false, detected_installer = false, initial_check_done;
	
	shared_ptr<ProcessInfo> ServicesBase = nullptr;


	CWinThread*							m_thGetLogRecordsThread;
	CWinThread*							m_thGetRegRecordsThread;
	CWinThread*							m_thProcessRegRecordsThread;
	CWinThread*							m_thGetPIDRecordsThread;

	HANDLE								m_hPort;	

	CEvent*								m_pStopThreadsEvent;

	CPtrList							m_listRegistryLog;
	CTypedPtrList<CObList, VSFileLog*>	m_listFileLog;

	CStringArray						m_arrShellExecPaths;
	CStringArray						m_arrDoNotLogPaths;
	CStringArray						m_arrLogPaths;

	CCriticalSection					m_CritSecReg;
	CCriticalSection					m_CritSecPIDs;

	VSRegTree*							m_pRegTree;

	FILETIME							m_ftStartTime;
};

#endif /* _DRIVERCLIENT_H_INCLUDED_ */