#include "stdafx.h"
//#include <Shlwapi.h>
#include <msi.h>
#include <intshcut.h>
#include <Sddl.h>
#include <aclapi.h>


//#define MAX_KEY_LENGTH 256

#pragma comment(lib, "Msi.lib")


// Get the target of a file:
// If the file is a shortcut, return its target; handle all known cases:
// - installer shortcuts (MSI API)
// - IUniformResourceLocator shortcuts
// - IShellLink shortcuts
// If the file is a normal file, return its path unchanged
bool getShellLinkTarget(LPCTSTR pszLinkFile, LPTSTR pszTargetPath, DWORD buffSize)
{
	*pszTargetPath = _T('\0');

	TCHAR pszProductCode[39] = { 0 };
	TCHAR pszComponentCode[39] = { 0 };
	DWORD buf = buffSize;
	if (ERROR_SUCCESS == MsiGetShortcutTarget(pszLinkFile, pszProductCode, NULL, pszComponentCode))
		MsiGetComponentPath(pszProductCode, pszComponentCode, pszTargetPath, &buf);
	if (*pszTargetPath)
		return true;

#ifndef UNICODE
	WCHAR wsz[MAX_PATH];
	MultiByteToWideChar(CP_ACP, 0, pszLinkFile, -1, wsz, nbArray(wsz));
#endif
	LPCTSTR wszLinkFile = pszLinkFile;

	// Resolve the shortcut using the IUniformResourceLocator and IPersistFile interfaces
	IUniformResourceLocator *purl;
	if (SUCCEEDED(CoCreateInstance(CLSID_InternetShortcut, NULL, CLSCTX_INPROC_SERVER,
		IID_IUniformResourceLocator, (LPVOID*)&purl))) {
		IPersistFile *ppf;
		if (SUCCEEDED(purl->QueryInterface(IID_IPersistFile, (void**)&ppf))) {
			LPTSTR pszURL;
			HRESULT hr1 = ppf->Load(wszLinkFile, STGM_READ);
			HRESULT hr2 = purl->GetURL(&pszURL);
			if ((S_OK == hr1) && (S_OK == hr2))
				lstrcpy(pszTargetPath, pszURL);
			if (hr2 == S_OK)
				shellFree(pszURL);
			ppf->Release();
		}
		purl->Release();

		if (*pszTargetPath)
		{
			return true;
		}
	}
	DWORD dwErr = GetLastError();
	// Resolve the shortcut using the IShellLink and IPersistFile interfaces
	IShellLink *psl;
	if (SUCCEEDED(CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
		IID_IShellLink, (LPVOID*)&psl))) {
		IPersistFile *ppf;
		if (SUCCEEDED(psl->QueryInterface(IID_IPersistFile, (void**)&ppf))) {
			if (SUCCEEDED(ppf->Load(wszLinkFile, STGM_READ)) &&
				SUCCEEDED(psl->Resolve(NULL, MAKELONG(SLR_NO_UI | SLR_NOUPDATE | SLR_NOTRACK | SLR_NOSEARCH | SLR_NOLINKINFO, 1000))))
				psl->GetPath(pszTargetPath, buffSize, NULL, 0);
			ppf->Release();
		}
		psl->Release();

		if (*pszTargetPath)
		{
			return true;
		}
	}
	dwErr = GetLastError();
	//CoUninitialize();
	return false;
}

// Use IMalloc to free a memory block
void shellFree(void* p)
{
	LPMALLOC pMalloc;
	if (SUCCEEDED(SHGetMalloc(&pMalloc))) {
		pMalloc->Free(p);
		pMalloc->Release();
	}
}

CString GetEXEPath()
{
	TCHAR szApp[2048] = {};
	::GetModuleFileName(NULL, szApp, 2048);
	::PathRemoveFileSpec(szApp);
	CString strAppPath = szApp;
	return strAppPath;
}
///if there is a problem to get the parent exe path returns empty path
void GetEXEParentPath(CString& strExeParentPath)
{
	CString strExePath = GetEXEPath(), strParentPath;
	GetLastFolderPath(strExePath, &strParentPath);
	strExeParentPath = strParentPath;
}

//Not Used used PathStripPath
////CString GetFileOnly(CString& strPath)
////{
////	int index = strPath.ReverseFind('\\');
////	CString strResult;
////	if ( index != -1)
////	{
////		strResult = strPath.Right(strPath.GetLength() - index - 1);
////	}
////	return strResult;
////}

void CmdRouteMenu(CWnd* pWnd, CMenu* pPopupMenu)
{
	CCmdUI state;
	state.m_pMenu = pPopupMenu;
	state.m_pParentMenu = pPopupMenu;
	state.m_nIndexMax = pPopupMenu->GetMenuItemCount();

	for (state.m_nIndex = 0;
		state.m_nIndex < state.m_nIndexMax;
		state.m_nIndex++)
	{
		state.m_nID = pPopupMenu->GetMenuItemID(state.m_nIndex);

		// menu separator or invalid cmd - ignore it
		if (state.m_nID == 0) continue;

		if (state.m_nID == (UINT)-1)
		{
			// possibly a popup menu, route to child menu if so
			CMenu* pSub = pPopupMenu->GetSubMenu(state.m_nIndex);
			if (pSub) CmdRouteMenu(pWnd, pSub);
		}
		else
		{
			// normal menu item, Auto disable if command is 
			// _not_ a system command.
			state.m_pSubMenu = NULL;
			state.DoUpdate(pWnd, FALSE);
		}
	}
}
/*DWORD64 GetFolderSize(LPCTSTR szPath, DWORD *dwFiles, DWORD *dwFolders)
{
if(szPath == NULL)
return 0;

if(_tcslen(szPath) == 0)
return 0;

TCHAR szFileFilter[4096];
TCHAR szFilePath[4096];
HANDLE hFind = NULL;
WIN32_FIND_DATA fileinfo;
DWORD64    dwSize = 0;

_tcscpy(szFilePath,szPath);
_tcscat(szFilePath,TEXT("\\"));
_tcscpy(szFileFilter,szFilePath);
_tcscat(szFileFilter,TEXT("*.*"));


hFind = ::FindFirstFile(szFileFilter,&fileinfo);
if (hFind != INVALID_HANDLE_VALUE)
{
do
{
if ( ( fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT ) == FALSE)
{
if(fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
{
if (!_tcscmp(fileinfo.cFileName,TEXT(".")) || !_tcscmp(fileinfo.cFileName,TEXT("..")))
{
//Do nothing for "." and ".." folders
}
else
{
TCHAR sztmp[4096];
_tcscpy(sztmp,szFilePath);
_tcscat(sztmp,fileinfo.cFileName);
dwSize = dwSize + GetFolderSize(sztmp);
if(dwFolders != NULL)
{
++(*dwFolders);
}
}
}
else
{
if(dwFiles != NULL)
{
++(*dwFiles);
}
}


dwSize += (((ULONGLONG)fileinfo.nFileSizeHigh) << 32 | fileinfo.nFileSizeLow);//(fileinfo.nFileSizeHigh * (MAXDWORD+1)) + fileinfo.nFileSizeLow;
}

}while(::FindNextFile(hFind,&fileinfo));
}
::FindClose(hFind);
return dwSize;

}*/
//int GetPointCount(TCHAR* szString)
//{
//	if(szString == NULL)
//		return 0;
//	int count = 0;
//	int point = TEXT('.');
//	int isymbols = lstrlen(szString);
//	for(int i = 0;i<isymbols;i++)
//	{
//		if(szString[i] == point)
//			count++;
//	}
//	return count;
//}


int GetPointCount(CString& strString)
{
	if (strString.IsEmpty())
		return 0;
	int count = 0;
	TCHAR tchPoint = TEXT('.');
	int isymbols = strString.GetLength();
	for (int i = 0; i<isymbols; i++)
	{
		if (strString.GetAt(i) == tchPoint)
			count++;
	}
	return count;
}

//TCHAR* GetPointPos(TCHAR* szString,int nPos)
//{
//	if(szString == NULL)
//		return NULL;
//
//	int count = 0;
//	int point = TEXT('.');
//	int isymbols = lstrlen(szString);
//	for(int i = 0;i<isymbols;i++)
//	{
//		if(szString[i] == point)
//		{
//			count++;
//			if(count == nPos)
//				return _tcsninc(szString,i);
//		}
//	}
//
//	return NULL;
//}
int GetPointPos(CString& strString, int nPos)
{
	if (strString.IsEmpty())
		return 0;

	int count = 0;
	TCHAR tchPoint = TEXT('.');
	int isymbols = strString.GetLength();
	for (int i = 0; i<isymbols; i++)
	{
		if (strString.GetAt(i) == tchPoint)
		{
			count++;
			if (count == nPos)
				return i;
		}
	}

	return 0;
}

//TCHAR* GetPathFromString(TCHAR* szString)
//{
//	if(szString == NULL)
//		return NULL;
//
//	if(_tcslen(szString) == 0)
//		return NULL;
//
//	int point = TEXT('.');
//	int twopoints = TEXT(':');
//
//	TCHAR * szPointPos, *sz2PointsPos, *szExtension;
//	TCHAR *szResultString = NULL;
//
//	szExtension = new TCHAR[4];
//	_tcsnset(szExtension,0,4);
//
//	sz2PointsPos = _tcschr(szString,twopoints);
//	if(sz2PointsPos == NULL)
//	{
//		delete [] szExtension;
//		return NULL;
//	}
//
//	int nPointsCount = GetPointCount(szString);
//    for(int nindex = 1; nindex<nPointsCount+1;nindex++)
//	{
//		if(szResultString != NULL)
//			delete [] szResultString;
//		szResultString = new TCHAR[4096];
//		_tcsnset(szResultString,0,4096);
//
//		szPointPos = GetPointPos(szString,nindex);//_tcschr(szString,point);
//		if(szPointPos == NULL)
//			break;
//			
//		if((szPointPos != NULL) && (sz2PointsPos != NULL))
//		{
//				if(szPointPos<sz2PointsPos)
//					continue;
//
//				_tcsncpy(szExtension,szPointPos+1,3);
//				INT_PTR npos = szPointPos - (sz2PointsPos-1) + 4;
//
//				_tcsncpy(szResultString,(sz2PointsPos-1),npos);
//				if(::PathIsDirectory(szResultString))
//					continue;
//				if(::PathFileExists(szResultString))
//				{
//					delete [] szExtension;
//					return szResultString;
//				}
//		}
//
//	}
//	delete [] szExtension;
//	delete [] szResultString;
//	return NULL;
//}
BOOL GetPathFromString(CString& strString, CString& strResult)
{
	if (strString.IsEmpty())
		return FALSE;


	TCHAR strPoint = TEXT('.');
	TCHAR strTwoPoints = TEXT(':');
	int nPointPos = 0, n2PointsPos = 0;

	//TCHAR * szPointPos, *sz2PointsPos, *szExtension;
	//TCHAR *szResultString = NULL;

	//szExtension = new TCHAR[4];
	//_tcsnset(szExtension,0,4);

	//sz2PointsPos = _tcschr(szString,twopoints);
	n2PointsPos = strString.Find(strTwoPoints);
	if (n2PointsPos < 0)
		return FALSE;


	int nPointsCount = GetPointCount(strString);
	for (int nindex = 1; nindex<nPointsCount + 1; nindex++)
	{
		//if(szResultString != NULL)
		//	delete [] szResultString;
		//szResultString = new TCHAR[4096];
		//_tcsnset(szResultString,0,4096);

		nPointPos = GetPointPos(strString, nindex);//_tcschr(szString,point);
		if (nPointPos < 0)
			return FALSE;

		if ((nPointPos > 0) && (n2PointsPos > 0))
		{
			if (nPointPos<n2PointsPos)
				continue;

			//_tcsncpy(szExtension,szPointPos+1,3);
			INT_PTR npos = nPointPos - (n2PointsPos - 1) + 4;

			//_tcsncpy(szResultString,(sz2PointsPos-1),npos);
			strResult = strString.Mid(n2PointsPos - 1, npos);
			if (::PathIsDirectory(strResult))
				continue;
			if (::PathFileExists(strResult))
			{
				//delete [] szExtension;
				//return szResultString;
				return TRUE;
			}
		}

	}
	//delete [] szExtension;
	//delete [] szResultString;
	return NULL;
}

LPTSTR StrFormatMBSize(LONGLONG qdw, LPTSTR pwszBuf, UINT cchBuf)
{
	TCHAR buf[128] = { 0 };
	long double ddw = ((long double)qdw) / (1024 * 1024);
	_stprintf_s(buf, 128, TEXT("%.2Lf MB"), ddw);
	_tcscpy_s(pwszBuf, cchBuf, buf);
	//lstrcpyn (pwszBuf, buf, cchBuf/2);
	return pwszBuf;
}

LPTSTR MyStrFormatKBSize(LONGLONG qdw, LPTSTR pwszBuf, UINT cchBuf)
{
	TCHAR buf[128] = { 0 };
	long double ddw = ((long double)qdw) / 1024;
	_stprintf_s(buf, 128, TEXT("%.0Lf KB"), ddw);
	_tcscpy_s(pwszBuf, cchBuf, buf);
	//lstrcpyn (pwszBuf, buf, cchBuf/2);
	return pwszBuf;
}

/******************************************************************************
*
*	FUNCTION:	RegeditJump
*
*	PURPOSE:	Opens Regedit and navigates the desired key
*
*****************************************************************************/

// length in ms we wait for Regedit to update its display 
#define	REGEDITSLOWWAIT		50

void RegeditJump(LPCTSTR pszCompressedKey)
{
	int		pos;
	TCHAR	* ch;
	HWND	regeditHwnd, regeditMainHwnd;
	TCHAR	compressPath[MAX_PATH];
	HKEY	hKey;
	TCHAR	*value = NULL;
	DWORD	status;
	TCHAR	RegPath[MAX_PATH];
	DEVMODE	devMode;

	// Get the color depth, which can effect the speed that Regedit
	// responds to keyboard commands
	devMode.dmSize = sizeof(DEVMODE);
	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &devMode);

	memset(compressPath, 0, MAX_PATH);
	lstrcpy(compressPath, pszCompressedKey);

	// If the key is a handle reference, tell the user we're sorry
	if (compressPath[0] == '0') {

		//MessageBox( hWnd, "The full name of the selected key or value is not available.",
		//	APPNAME, MB_OK|MB_ICONWARNING );
		return;
	}

	// We need to uncompress abbreviations
	if (!_tcsnccmp(compressPath, _T("HKLM"), _tcslen(_T("HKLM")))) {

		_stprintf(RegPath, _T("\\HKEY_LOCAL_MACHINE%s"), &compressPath[_tcslen(_T("HKLM"))]);
		status = RegOpenKey(HKEY_LOCAL_MACHINE, &compressPath[_tcslen(_T("HKLM")) + 1], &hKey);

	}
	else if (!_tcsnccmp(compressPath, _T("HKCU"), _tcslen(_T("HKCU")))) {

		_stprintf(RegPath, _T("\\HKEY_CURRENT_USER%s"), &compressPath[_tcslen(_T("HKCU"))]);
		status = RegOpenKey(HKEY_CURRENT_USER, &compressPath[_tcslen(_T("HKCU")) + 1], &hKey);

	}
	else if (!_tcsnccmp(compressPath, _T("HKCC"), _tcslen(_T("HKCC")))) {

		_stprintf(RegPath, _T("\\HKEY_CURRENT_CONFIG%s"), &compressPath[_tcslen(_T("HKCC"))]);
		status = RegOpenKey(HKEY_CURRENT_CONFIG, &compressPath[_tcslen(_T("HKCC")) + 1], &hKey);

	}
	else if (!_tcsnccmp(compressPath, _T("HKCR"), _tcslen(_T("HKCR")))) {

		_stprintf(RegPath, _T("\\HKEY_CLASSES_ROOT%s"), &compressPath[_tcslen(_T("HKCR"))]);
		status = RegOpenKey(HKEY_CLASSES_ROOT, &compressPath[_tcslen(_T("HKCR")) + 1], &hKey);

	}
	else if (!_tcsnccmp(compressPath, _T("HKU"), _tcslen(_T("HKU")))) {

		_stprintf(RegPath, _T("\\HKEY_USERS%s"), &compressPath[_tcslen(_T("HKU"))]);
		status = RegOpenKey(HKEY_USERS, &compressPath[_tcslen(_T("HKU")) + 1], &hKey);

	}
	else {

		lstrcpy(RegPath, compressPath);
		status = FALSE;
	}

	// Is it a value or a key?
	if (status == ERROR_SUCCESS) {

		RegCloseKey(hKey);
		lstrcat(RegPath, _T("\\"));

	}
	else {

		value = _tcsrchr(RegPath, _T('\\')) + 1;
		*_tcsrchr(RegPath, _T('\\')) = 0;
	}

	// Open RegEdit
	regeditMainHwnd = ::FindWindow(_T("RegEdit_RegEdit"), NULL);
	if (regeditMainHwnd == NULL) {
		SHELLEXECUTEINFO info;
		memset(&info, 0, sizeof info);
		info.cbSize = sizeof info;
		info.fMask = SEE_MASK_NOCLOSEPROCESS;
		info.lpVerb = _T("open");
		info.lpFile = _T("regedit.exe");
		info.nShow = SW_SHOWNORMAL;
		ShellExecuteEx(&info);
		WaitForInputIdle(info.hProcess, INFINITE);
		regeditMainHwnd = ::FindWindow(_T("RegEdit_RegEdit"), NULL);
	}


	if (regeditMainHwnd == NULL) {

		//MessageBox( hWnd, _T("Regmon was unable to launch Regedit."),
		//	APPNAME, MB_OK|MB_ICONERROR );
		return;
	}
	ShowWindow(regeditMainHwnd, SW_SHOWNORMAL);
	SetForegroundWindow(regeditMainHwnd);

	// Get treeview
	regeditHwnd = FindWindowEx(regeditMainHwnd, NULL, _T("SysTreeView32"), NULL);
	SetForegroundWindow(regeditHwnd);
	SetFocus(regeditHwnd);

	// Close it up
	for (pos = 0; pos < 30; ++pos) {
		UINT vk = VK_LEFT;
		SendMessage(regeditHwnd, WM_KEYDOWN, vk, 0);
	}

	// wait for slow displays - 
	// Regedit takes a while to open keys with lots of subkeys
	// when running at high color depths 
	if (devMode.dmBitsPerPel > 8) Sleep(REGEDITSLOWWAIT);

	// Open path
	for (ch = RegPath; *ch; ++ch) {
		if (*ch == '\\') {
			UINT vk = VK_RIGHT;
			SendMessage(regeditHwnd, WM_KEYDOWN, vk, 0);

			// wait for slow displays - 
			// Regedit takes a while to open keys with lots of subkeys
			// when running at high color depths 
			if (devMode.dmBitsPerPel > 8) Sleep(REGEDITSLOWWAIT);

		}
		else {
			UINT vk = toupper(*ch);

			SendMessage(regeditHwnd, WM_CHAR, vk, 0);
		}
	}

	// If its a value select the value
	if (value) {
		UINT vk = VK_HOME;

		regeditHwnd = FindWindowEx(regeditMainHwnd, NULL, _T("SysListView32"), NULL);
		SetForegroundWindow(regeditHwnd);
		SetFocus(regeditHwnd);
		Sleep(REGEDITSLOWWAIT); // have to wait for Regedit to update listview
		SendMessage(regeditHwnd, WM_KEYDOWN, vk, 0);

		for (ch = value; *ch; ++ch) {
			UINT vk = toupper(*ch);

			SendMessage(regeditHwnd, WM_CHAR, vk, 0);
		}
	}

	SetForegroundWindow(regeditMainHwnd);
	SetFocus(regeditMainHwnd);
}
//Use full path i.e. HKEY_LOCAL_MACHINE\Software\...
void OpenRegEditBits(LPCTSTR pszKeyPath, BOOL bIsKey64Bit)
{
	DWORD dwFlag;
	if (bIsKey64Bit)
		dwFlag = KEY_WOW64_64KEY;
	else
		dwFlag = KEY_WOW64_32KEY;

	REGSAM dwOpenFlag = KEY_READ | dwFlag;

	BOOL	bWin81 = FALSE, bWin10 = FALSE;
	int nMajorVer = 0, nMinorVer = 0;
	GetWindowsVersion(&nMajorVer, &nMinorVer);
	if (nMajorVer == 10)
		bWin10 = TRUE;
	else if ((nMajorVer >= 6) && (nMinorVer >= 2)) // Windows 8 and up
	{
		HKEY hKeyWinVer;
		TCHAR szWinVerNumber[MAX_PATH] = { 0 };
		if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
			_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"),
			0,
			KEY_READ,
			&hKeyWinVer))
		{
			DWORD dwDataSize = MAX_PATH * sizeof(TCHAR);

			if (ERROR_SUCCESS == ::RegQueryValueEx(hKeyWinVer, _T("CurrentVersion"), 0, NULL, (LPBYTE)szWinVerNumber, &dwDataSize))
			{
				CString strReadedRes = szWinVerNumber;
				if (strReadedRes.CompareNoCase(TEXT("6.3")) == 0)
					bWin81 = TRUE;

			}

			::RegCloseKey(hKeyWinVer);
		}
	}

	HWND regeditMainHwnd = ::FindWindow(_T("RegEdit_RegEdit"), NULL);
	::SendMessage(regeditMainHwnd, WM_CLOSE, 0, 0);
	//::Sleep(100);

	TCHAR szMyComputer[MAX_PATH] = { 0 };
	TCHAR szMyComputerRes[MAX_PATH] = { 0 };

	if (bWin81 || bWin10)
	{
		StringCchCopy(szMyComputerRes, MAX_PATH, TEXT("@%SystemRoot%\\system32\\shell32.dll,-33010"));
		if (S_OK != SHLoadIndirectString(szMyComputerRes, szMyComputer, MAX_PATH, NULL))
			return;
	}
	else
	{
		HKEY hKeyMyComp;
		//TCHAR szMyComputer[MAX_PATH] = {0};
		//TCHAR szMyComputerRes[MAX_PATH] = {0};
		if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
			_T("SOFTWARE\\Classes\\CLSID\\{20D04FE0-3AEA-1069-A2D8-08002B30309D}"),
			0,
			KEY_READ,
			&hKeyMyComp))
		{
			DWORD dwDataSize = MAX_PATH * sizeof(TCHAR);

			if (ERROR_SUCCESS == ::RegQueryValueEx(hKeyMyComp, _T("LocalizedString"), 0, NULL, (LPBYTE)szMyComputerRes, &dwDataSize))
			{
				CString strReadedRes = szMyComputerRes;
				if (strReadedRes.Find(TEXT("9216")) == -1)
					StringCchCopy(szMyComputerRes, MAX_PATH, TEXT("@%SystemRoot%\\system32\\shell32.dll,-9216")); //8.1 33010 - Computer
				if (S_OK != SHLoadIndirectString(szMyComputerRes, szMyComputer, MAX_PATH, NULL))
					return;
			}

			::RegCloseKey(hKeyMyComp);
		}
		else
		{
			StringCchCopy(szMyComputerRes, MAX_PATH, TEXT("@%SystemRoot%\\system32\\shell32.dll,-9216"));
			if (S_OK != SHLoadIndirectString(szMyComputerRes, szMyComputer, MAX_PATH, NULL))
				return;
		}
	}


	CString strFullPath = szMyComputer;
	strFullPath += TEXT("\\");
	strFullPath += pszKeyPath;


	CString		strInitialPath, strKeyPath, strRoot;
	strInitialPath = pszKeyPath;
	HKEY hRoot = GetRootFromPath(strInitialPath, &strRoot);
	if (hRoot == NULL)
		return;
	RemoveRootFromPath(strInitialPath, strKeyPath);
	HKEY hTestKey;

	if (ERROR_SUCCESS == ::RegOpenKeyEx(hRoot, strKeyPath, 0, dwOpenFlag, &hTestKey))
	{

		HKEY hKey;
		DWORD dwDisp;
		if (ERROR_SUCCESS == ::RegCreateKeyEx(HKEY_CURRENT_USER,
			_T("Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit"),
			0, NULL, REG_OPTION_NON_VOLATILE,
			KEY_ALL_ACCESS, NULL, &hKey, &dwDisp))
		{
			if (ERROR_SUCCESS == ::RegSetValueEx(hKey, _T("LastKey"), 0, REG_SZ, (LPBYTE)(LPCTSTR)strFullPath, (strFullPath.GetLength() + 1) * sizeof(TCHAR)))
			{
				TCHAR pszWinDir[1024] = { 0 };
				::GetWindowsDirectory(pszWinDir, 1024);
				CString strFullPath = pszWinDir;
				if (bIsKey64Bit == FALSE)
					strFullPath += TEXT("\\SysWOW64");
				strFullPath += TEXT("\\regedit.exe");

				SHELLEXECUTEINFO info;
				memset(&info, 0, sizeof info);
				info.cbSize = sizeof info;
				info.fMask = SEE_MASK_NOCLOSEPROCESS;
				info.lpVerb = _T("open");
				info.lpFile = strFullPath;
				info.nShow = SW_SHOWNORMAL;
				ShellExecuteEx(&info);
			}
			::RegCloseKey(hKey);
		}
		::RegCloseKey(hTestKey);
	}

	else
	{
		TCHAR szWOW64FolderPath[4096] = { 0 };
		// C:\Windows\sysWOW64
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_SYSTEMX86, NULL, 0, szWOW64FolderPath)))
		{
			CString strRegEdit32bitPath = szWOW64FolderPath;
			strRegEdit32bitPath += TEXT("\\regedit.exe");
			HKEY hKey;
			DWORD dwDisp;
			if (ERROR_SUCCESS == ::RegCreateKeyEx(HKEY_CURRENT_USER,
				_T("Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit"),
				0, NULL, REG_OPTION_NON_VOLATILE,
				KEY_ALL_ACCESS, NULL, &hKey, &dwDisp))

			{
				if (ERROR_SUCCESS == ::RegSetValueEx(hKey, _T("LastKey"), 0, REG_SZ, (LPBYTE)(LPCTSTR)strFullPath, (strFullPath.GetLength() + 1) * sizeof(TCHAR)))
				{
					SHELLEXECUTEINFO info;
					memset(&info, 0, sizeof info);
					info.cbSize = sizeof info;
					info.fMask = SEE_MASK_NOCLOSEPROCESS;
					info.lpVerb = _T("open");
					info.lpFile = strRegEdit32bitPath;
					info.nShow = SW_SHOWNORMAL;
					ShellExecuteEx(&info);
				}
				::RegCloseKey(hKey);
			}
		}
	}
}
void OpenRegEdit(LPCTSTR pszKeyPath)
{

	BOOL	bWin81 = FALSE, bWin10 = FALSE;
	int nMajorVer = 0, nMinorVer = 0;
	GetWindowsVersion(&nMajorVer, &nMinorVer);
	if (nMajorVer == 10)
		bWin10 = TRUE;
	else if ((nMajorVer >= 6) && (nMinorVer >= 2)) // Windows 8 and up
	{
		HKEY hKeyWinVer;
		TCHAR szWinVerNumber[MAX_PATH] = { 0 };
		if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
			_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"),
			0,
			KEY_READ,
			&hKeyWinVer))
		{
			DWORD dwDataSize = MAX_PATH * sizeof(TCHAR);

			if (ERROR_SUCCESS == ::RegQueryValueEx(hKeyWinVer, _T("CurrentVersion"), 0, NULL, (LPBYTE)szWinVerNumber, &dwDataSize))
			{
				CString strReadedRes = szWinVerNumber;
				if (strReadedRes.CompareNoCase(TEXT("6.3")) == 0)
					bWin81 = TRUE;

			}

			::RegCloseKey(hKeyWinVer);
		}
	}

	HWND regeditMainHwnd = ::FindWindow(_T("RegEdit_RegEdit"), NULL);
	::SendMessage(regeditMainHwnd, WM_CLOSE, 0, 0);
	//::Sleep(100);

	TCHAR szMyComputer[MAX_PATH] = { 0 };
	TCHAR szMyComputerRes[MAX_PATH] = { 0 };
	if (bWin81 || bWin10)
	{
		StringCchCopy(szMyComputerRes, MAX_PATH, TEXT("@%SystemRoot%\\system32\\shell32.dll,-33010"));
		if (S_OK != SHLoadIndirectString(szMyComputerRes, szMyComputer, MAX_PATH, NULL))
			return;
	}
	else
	{
		HKEY hKeyMyComp;
		//TCHAR szMyComputer[MAX_PATH] = {0};
		//TCHAR szMyComputerRes[MAX_PATH] = {0};
		if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
			_T("SOFTWARE\\Classes\\CLSID\\{20D04FE0-3AEA-1069-A2D8-08002B30309D}"),
			0,
			KEY_READ,
			&hKeyMyComp))
		{
			DWORD dwDataSize = MAX_PATH * sizeof(TCHAR);

			if (ERROR_SUCCESS == ::RegQueryValueEx(hKeyMyComp, _T("LocalizedString"), 0, NULL, (LPBYTE)szMyComputerRes, &dwDataSize))
			{
				CString strReadedRes = szMyComputerRes;
				if (strReadedRes.Find(TEXT("9216")) == -1)
					StringCchCopy(szMyComputerRes, MAX_PATH, TEXT("@%SystemRoot%\\system32\\shell32.dll,-9216")); //8.1 33010 - Computer
				if (S_OK != SHLoadIndirectString(szMyComputerRes, szMyComputer, MAX_PATH, NULL))
					return;
			}

			::RegCloseKey(hKeyMyComp);
		}
		else
		{
			StringCchCopy(szMyComputerRes, MAX_PATH, TEXT("@%SystemRoot%\\system32\\shell32.dll,-9216"));
			if (S_OK != SHLoadIndirectString(szMyComputerRes, szMyComputer, MAX_PATH, NULL))
				return;
		}
	}


	CString strFullPath = szMyComputer;
	strFullPath += TEXT("\\");
	strFullPath += pszKeyPath;


	CString		strInitialPath, strKeyPath, strRoot;
	strInitialPath = pszKeyPath;
	HKEY hRoot = GetRootFromPath(strInitialPath, &strRoot);
	if (hRoot == NULL)
		return;
	RemoveRootFromPath(strInitialPath, strKeyPath);
	HKEY hTestKey;

	if (ERROR_SUCCESS == ::RegOpenKeyEx(hRoot, strKeyPath, 0, KEY_READ, &hTestKey))
	{

		HKEY hKey;
		DWORD dwDisp;
		if (ERROR_SUCCESS == ::RegCreateKeyEx(HKEY_CURRENT_USER,
			_T("Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit"),
			0, NULL, REG_OPTION_NON_VOLATILE,
			KEY_ALL_ACCESS, NULL, &hKey, &dwDisp))
		{
			if (ERROR_SUCCESS == ::RegSetValueEx(hKey, _T("LastKey"), 0, REG_SZ, (LPBYTE)(LPCTSTR)strFullPath, (strFullPath.GetLength() + 1) * sizeof(TCHAR)))
			{
				TCHAR pszWinDir[1024] = { 0 };
				::GetWindowsDirectory(pszWinDir, 1024);
				CString strFullPath = pszWinDir;
				strFullPath += TEXT("\\regedit.exe");

				SHELLEXECUTEINFO info;
				memset(&info, 0, sizeof info);
				info.cbSize = sizeof info;
				info.fMask = SEE_MASK_NOCLOSEPROCESS;
				info.lpVerb = _T("open");
				info.lpFile = strFullPath;
				info.nShow = SW_SHOWNORMAL;
				ShellExecuteEx(&info);
			}
			::RegCloseKey(hKey);
		}
		::RegCloseKey(hTestKey);
	}

	else
	{
		TCHAR szWOW64FolderPath[4096] = { 0 };
		// C:\Windows\sysWOW64
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_SYSTEMX86, NULL, 0, szWOW64FolderPath)))
		{
			CString strRegEdit32bitPath = szWOW64FolderPath;
			strRegEdit32bitPath += TEXT("\\regedit.exe");
			HKEY hKey;
			DWORD dwDisp;
			if (ERROR_SUCCESS == ::RegCreateKeyEx(HKEY_CURRENT_USER,
				_T("Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit"),
				0, NULL, REG_OPTION_NON_VOLATILE,
				KEY_ALL_ACCESS, NULL, &hKey, &dwDisp))

			{
				if (ERROR_SUCCESS == ::RegSetValueEx(hKey, _T("LastKey"), 0, REG_SZ, (LPBYTE)(LPCTSTR)strFullPath, (strFullPath.GetLength() + 1) * sizeof(TCHAR)))
				{
					SHELLEXECUTEINFO info;
					memset(&info, 0, sizeof info);
					info.cbSize = sizeof info;
					info.fMask = SEE_MASK_NOCLOSEPROCESS;
					info.lpVerb = _T("open");
					info.lpFile = strRegEdit32bitPath;
					info.nShow = SW_SHOWNORMAL;
					ShellExecuteEx(&info);
				}
				::RegCloseKey(hKey);
			}
		}
	}
}
/*TCHAR* GetDirFromPath(TCHAR* pszPath)
{
if(pszPath == NULL)
return NULL;

if(_tcslen(pszPath) == 0)
return NULL;

TCHAR* pszDirName = new TCHAR[4096];
_tcsnset(pszDirName, 0, 4096);

int nSlash = TEXT('\\');
TCHAR* pszSlash = NULL;
pszSlash = _tcsrchr(pszPath,nSlash);

if(pszPath[0] == _T('"'))
pszPath++;
int result = (int)(pszSlash - pszPath + 1);
_tcsncpy(pszDirName, pszPath, result);
return pszDirName;
}
*/

//void PeekMessageLoop()
//{
//    MSG msg;
//    while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
//    {
//        TranslateMessage(&msg);
//        DispatchMessage(&msg);
//    }
//}
//void WaitForThreadToTerminate(HANDLE hThread)
//{
//	if(hThread == NULL)
//		return;
//
//    DWORD dwRet;
//    do
//    {
//        dwRet = ::MsgWaitForMultipleObjects(1, &hThread, FALSE, 
//            INFINITE, QS_ALLINPUT);
//        if (dwRet != WAIT_OBJECT_0)
//        {
//            PeekMessageLoop();
//        }
//    } while ((dwRet != WAIT_OBJECT_0) && (dwRet != WAIT_FAILED));
//}
BOOL VSAtlWaitWithMessageLoop(HANDLE hEvent)
{
	DWORD dwRet;
	MSG msg;

	while (1)
	{
		dwRet = MsgWaitForMultipleObjects(1, &hEvent, FALSE, INFINITE, QS_ALLINPUT);

		if (dwRet == WAIT_OBJECT_0)
			return TRUE;    // The event was signaled

		if (dwRet != WAIT_OBJECT_0 + 1)
			break;          // Something else happened

							// There is one or more window message available. Dispatch them
		while (PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE))
		{
			// check for unicode window so we call the appropriate functions
			BOOL bUnicode = ::IsWindowUnicode(msg.hwnd);
			BOOL bRet;

			if (bUnicode)
				bRet = ::GetMessageW(&msg, NULL, 0, 0);
			else
				bRet = ::GetMessageA(&msg, NULL, 0, 0);

			if (bRet > 0)
			{
				::TranslateMessage(&msg);

				if (bUnicode)
					::DispatchMessageW(&msg);
				else
					::DispatchMessageA(&msg);
			}

			if (WaitForSingleObject(hEvent, 0) == WAIT_OBJECT_0)
				return TRUE; // Event is now signaled.
		}
	}
	return FALSE;
}

BOOL IsStringInList(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase)
{
	for (int i = 0; i<arrStringList->GetCount(); i++)
	{
		CString str = arrStringList->GetAt(i);
		if (bMatchCase)
		{
			if (StrCmpI(pszString, arrStringList->GetAt(i)) == 0)
				return TRUE;
		}
		else
		{
			if (StrStrI(pszString, arrStringList->GetAt(i)))
				return TRUE;
		}
	}
	return FALSE;
}
////////BOOL IsPatternAllowedForSearch(LPCTSTR szPattern)
////////{
////////	if(_tcslen(szPattern) < 4)
////////		return FALSE;
////////
////////	CStringArray arrDeniedStrings;
////////	arrDeniedStrings.Add(_T(".net"));
////////	arrDeniedStrings.Add(_T("account"));
////////	arrDeniedStrings.Add(_T("accounts"));
////////	arrDeniedStrings.Add(_T("admin"));
////////	arrDeniedStrings.Add(_T("administrator"));
////////	arrDeniedStrings.Add(_T("advanced"));
////////	arrDeniedStrings.Add(_T("application"));
////////	arrDeniedStrings.Add(_T("applications"));
////////	arrDeniedStrings.Add(_T("arpcache"));
////////	arrDeniedStrings.Add(_T("audio"));
////////	arrDeniedStrings.Add(_T("backup"));
////////	arrDeniedStrings.Add(_T("backups"));
////////	arrDeniedStrings.Add(_T("bin"));
////////	arrDeniedStrings.Add(_T("boot"));
////////	arrDeniedStrings.Add(_T("cache"));
////////	arrDeniedStrings.Add(_T("cached"));
////////	arrDeniedStrings.Add(_T("calendar"));
////////	arrDeniedStrings.Add(_T("calendars"));
////////	arrDeniedStrings.Add(_T("center"));
////////	arrDeniedStrings.Add(_T("class"));
////////	arrDeniedStrings.Add(_T("classes"));
////////	arrDeniedStrings.Add(_T("clean"));
////////	arrDeniedStrings.Add(_T("cleaner"));
////////	arrDeniedStrings.Add(_T("client"));
////////	arrDeniedStrings.Add(_T("close"));
////////	arrDeniedStrings.Add(_T("command"));
////////	arrDeniedStrings.Add(_T("commands"));
////////	arrDeniedStrings.Add(_T("common"));
////////	arrDeniedStrings.Add(_T("compact"));
////////	arrDeniedStrings.Add(_T("company"));
////////	arrDeniedStrings.Add(_T("component"));
////////	arrDeniedStrings.Add(_T("components"));
////////	arrDeniedStrings.Add(_T("config"));
////////	arrDeniedStrings.Add(_T("configuration"));
////////	arrDeniedStrings.Add(_T("configure"));
////////	arrDeniedStrings.Add(_T("connection"));
////////	arrDeniedStrings.Add(_T("connections"));
////////	arrDeniedStrings.Add(_T("console"));
////////	arrDeniedStrings.Add(_T("control panel"));
////////	arrDeniedStrings.Add(_T("control"));
////////	arrDeniedStrings.Add(_T("controls"));
////////	arrDeniedStrings.Add(_T("data"));
////////	arrDeniedStrings.Add(_T("date"));
////////	arrDeniedStrings.Add(_T("debug"));
////////	arrDeniedStrings.Add(_T("default"));
////////	arrDeniedStrings.Add(_T("defaults"));
////////	arrDeniedStrings.Add(_T("desktop"));
////////	arrDeniedStrings.Add(_T("desktops"));
////////	arrDeniedStrings.Add(_T("device"));
////////	arrDeniedStrings.Add(_T("devices"));
////////	arrDeniedStrings.Add(_T("disk"));
////////	arrDeniedStrings.Add(_T("disks"));
////////	arrDeniedStrings.Add(_T("document"));
////////	arrDeniedStrings.Add(_T("documents"));
////////	arrDeniedStrings.Add(_T("domain"));
////////	arrDeniedStrings.Add(_T("domains"));
////////	arrDeniedStrings.Add(_T("driver"));
////////	arrDeniedStrings.Add(_T("drivers"));
////////	arrDeniedStrings.Add(_T("drivers32"));
////////	arrDeniedStrings.Add(_T("edit"));
////////	arrDeniedStrings.Add(_T("edition"));
////////	arrDeniedStrings.Add(_T("emulator"));
////////	arrDeniedStrings.Add(_T("execute"));
////////	arrDeniedStrings.Add(_T("exit"));
////////	arrDeniedStrings.Add(_T("explore"));
////////	arrDeniedStrings.Add(_T("explorer"));
////////	arrDeniedStrings.Add(_T("express"));
////////	arrDeniedStrings.Add(_T("extension"));
////////	arrDeniedStrings.Add(_T("extensions"));
////////	arrDeniedStrings.Add(_T("free"));
////////	arrDeniedStrings.Add(_T("freeware"));
////////	arrDeniedStrings.Add(_T("file"));
////////	arrDeniedStrings.Add(_T("files"));
////////	arrDeniedStrings.Add(_T("firewall"));
////////	arrDeniedStrings.Add(_T("folder"));
////////	arrDeniedStrings.Add(_T("folders"));
////////	arrDeniedStrings.Add(_T("font"));
////////	arrDeniedStrings.Add(_T("fonts"));
////////	arrDeniedStrings.Add(_T("full"));
////////	arrDeniedStrings.Add(_T("game"));
////////	arrDeniedStrings.Add(_T("games"));
////////	arrDeniedStrings.Add(_T("gateway"));
////////	arrDeniedStrings.Add(_T("gateways"));
////////	arrDeniedStrings.Add(_T("group"));
////////	arrDeniedStrings.Add(_T("hardware"));
////////	arrDeniedStrings.Add(_T("help"));
////////	arrDeniedStrings.Add(_T("host"));
////////	arrDeniedStrings.Add(_T("hosting"));
////////	arrDeniedStrings.Add(_T("hotfix"));
////////	arrDeniedStrings.Add(_T("icon"));
////////	arrDeniedStrings.Add(_T("icons"));
////////	arrDeniedStrings.Add(_T("install"));
////////	arrDeniedStrings.Add(_T("installer"));
////////	arrDeniedStrings.Add(_T("interactive"));
////////	arrDeniedStrings.Add(_T("internet"));
////////	arrDeniedStrings.Add(_T("internet explorer"));
////////	arrDeniedStrings.Add(_T("license"));
////////	arrDeniedStrings.Add(_T("light"));
////////	arrDeniedStrings.Add(_T("log"));
////////	arrDeniedStrings.Add(_T("logs"));
////////	arrDeniedStrings.Add(_T("manager"));
////////	arrDeniedStrings.Add(_T("media"));
////////	arrDeniedStrings.Add(_T("message"));
////////	arrDeniedStrings.Add(_T("messenger"));
////////	arrDeniedStrings.Add(_T("microsoft"));
////////	arrDeniedStrings.Add(_T("mobile"));
////////	arrDeniedStrings.Add(_T("module"));
////////	arrDeniedStrings.Add(_T("modules"));
////////	arrDeniedStrings.Add(_T("monitor"));
////////	arrDeniedStrings.Add(_T("multimedia"));
////////	arrDeniedStrings.Add(_T("network"));
////////	arrDeniedStrings.Add(_T("object"));
////////	arrDeniedStrings.Add(_T("objects"));
////////	arrDeniedStrings.Add(_T("office"));
////////	arrDeniedStrings.Add(_T("open"));
////////	arrDeniedStrings.Add(_T("option"));
////////	arrDeniedStrings.Add(_T("options"));
////////	arrDeniedStrings.Add(_T("panel"));
////////	arrDeniedStrings.Add(_T("phone"));
////////	arrDeniedStrings.Add(_T("platform"));
////////	arrDeniedStrings.Add(_T("player"));
////////	arrDeniedStrings.Add(_T("point"));
////////	arrDeniedStrings.Add(_T("policies"));
////////	arrDeniedStrings.Add(_T("policy"));
////////	arrDeniedStrings.Add(_T("portable"));
////////	arrDeniedStrings.Add(_T("print"));
////////	arrDeniedStrings.Add(_T("printers"));
////////	arrDeniedStrings.Add(_T("process"));
////////	arrDeniedStrings.Add(_T("processor"));
////////	arrDeniedStrings.Add(_T("product"));
////////	arrDeniedStrings.Add(_T("products"));
////////	arrDeniedStrings.Add(_T("professional"));
////////	arrDeniedStrings.Add(_T("profile"));
////////	arrDeniedStrings.Add(_T("profiles"));
////////	arrDeniedStrings.Add(_T("program"));
////////	arrDeniedStrings.Add(_T("programs"));
////////	arrDeniedStrings.Add(_T("properties"));
////////	arrDeniedStrings.Add(_T("reader"));
////////	arrDeniedStrings.Add(_T("register"));
////////	arrDeniedStrings.Add(_T("registration"));
////////	arrDeniedStrings.Add(_T("registry"));
////////	arrDeniedStrings.Add(_T("reinstall"));
////////	arrDeniedStrings.Add(_T("remote"));
////////	arrDeniedStrings.Add(_T("remove"));
////////	arrDeniedStrings.Add(_T("resource"));
////////	arrDeniedStrings.Add(_T("resources"));
////////	arrDeniedStrings.Add(_T("restore"));
////////	arrDeniedStrings.Add(_T("route"));
////////	arrDeniedStrings.Add(_T("router"));
////////	arrDeniedStrings.Add(_T("run"));
////////	arrDeniedStrings.Add(_T("script"));
////////	arrDeniedStrings.Add(_T("scripts"));
////////	arrDeniedStrings.Add(_T("secure"));
////////	arrDeniedStrings.Add(_T("secured"));
////////	arrDeniedStrings.Add(_T("security"));
////////	arrDeniedStrings.Add(_T("server"));
////////	arrDeniedStrings.Add(_T("servers"));
////////	arrDeniedStrings.Add(_T("service"));
////////	arrDeniedStrings.Add(_T("services"));
////////	arrDeniedStrings.Add(_T("setting"));
////////	arrDeniedStrings.Add(_T("settings"));
////////	arrDeniedStrings.Add(_T("setup"));
////////	arrDeniedStrings.Add(_T("share"));
////////	arrDeniedStrings.Add(_T("shared"));
////////	arrDeniedStrings.Add(_T("shell"));
////////	arrDeniedStrings.Add(_T("software"));
////////	arrDeniedStrings.Add(_T("sound"));
////////	arrDeniedStrings.Add(_T("sounds"));
////////	arrDeniedStrings.Add(_T("source"));
////////	arrDeniedStrings.Add(_T("sources"));
////////	arrDeniedStrings.Add(_T("storage"));
////////	arrDeniedStrings.Add(_T("store"));
////////	arrDeniedStrings.Add(_T("style"));
////////	arrDeniedStrings.Add(_T("styles"));
////////	arrDeniedStrings.Add(_T("system"));
////////	arrDeniedStrings.Add(_T("system32"));
////////	arrDeniedStrings.Add(_T("systems"));
////////	arrDeniedStrings.Add(_T("time"));
////////	arrDeniedStrings.Add(_T("tool"));
////////	arrDeniedStrings.Add(_T("toolbar"));
////////	arrDeniedStrings.Add(_T("toolbars"));
////////	arrDeniedStrings.Add(_T("tools"));
////////	arrDeniedStrings.Add(_T("trial"));
////////	arrDeniedStrings.Add(_T("unin"));
////////	arrDeniedStrings.Add(_T("uninst"));
////////	arrDeniedStrings.Add(_T("uninstall"));
////////	arrDeniedStrings.Add(_T("uninstaller"));
////////	arrDeniedStrings.Add(_T("update"));
////////	arrDeniedStrings.Add(_T("updates"));
////////	arrDeniedStrings.Add(_T("user"));
////////	arrDeniedStrings.Add(_T("users"));
////////	arrDeniedStrings.Add(_T("util"));
////////	arrDeniedStrings.Add(_T("utilities"));
////////	arrDeniedStrings.Add(_T("utility"));
////////	arrDeniedStrings.Add(_T("value"));
////////	arrDeniedStrings.Add(_T("values"));
////////	arrDeniedStrings.Add(_T("version"));
////////	arrDeniedStrings.Add(_T("versions"));
////////	arrDeniedStrings.Add(_T("video"));
////////	arrDeniedStrings.Add(_T("window"));
////////	arrDeniedStrings.Add(_T("windows"));
////////	arrDeniedStrings.Add(_T("winlogon"));
////////	arrDeniedStrings.Add(_T("wizard"));
////////	arrDeniedStrings.Add(_T("wizards"));
////////
////////	for(int i=0; i<arrDeniedStrings.GetCount(); i++)
////////	{
////////		if(StrCmpI(szPattern, arrDeniedStrings.GetAt(i)) == 0)
////////			return FALSE;
////////	}
////////
////////	return TRUE;
////////}
////////BOOL GetInstalledFolderName(CString* strInstLoc, CString* strFolderName)
////////{
////////	if((strInstLoc == NULL) || (strFolderName == NULL))
////////		return FALSE;
////////
////////	BOOL bInclude = TRUE;
////////	int nLen = strInstLoc->GetLength();
////////	if(nLen)
////////	{
////////		CString strTemp = *strInstLoc;
////////		CString strLast = strTemp.GetAt(nLen-1);
////////		if(strLast.Compare(_T("\\")) == 0)
////////		{
////////			strTemp.Delete(nLen-1);
////////			nLen -= 1;
////////		}
////////
////////		int nSlash = strTemp.ReverseFind(_T('\\'));	
////////		int nFolderNameLen = nLen - nSlash - 1;
////////		
////////		*strFolderName = strTemp.Right(nFolderNameLen);
////////
////////		if(IsPatternAllowedForSearch(strFolderName->GetBuffer()))
////////		{
////////			strFolderName->ReleaseBuffer();
////////			return TRUE;
////////		}
////////		strFolderName->ReleaseBuffer();
////////	}
////////
////////	strFolderName->Empty();
////////	return FALSE;
////////}
BOOL IsFolderAllowedForSearchBasic(LPCTSTR pszFolderName)
{
	if (pszFolderName == NULL)
		return FALSE;

	if (_tcslen(pszFolderName) == 0)
		return FALSE;

	CString strFolderName = pszFolderName;
	::PathUnquoteSpaces(strFolderName.GetBuffer());
	strFolderName.ReleaseBuffer();

	if (strFolderName.IsEmpty())
		return FALSE;

	if (strFolderName.GetAt(1) != TEXT(':'))
		return FALSE;

	::PathRemoveBackslash(strFolderName.GetBuffer());
	strFolderName.ReleaseBuffer();

	int nLen = strFolderName.GetLength() * 2;
	TCHAR* pszTmp = new TCHAR[nLen];
	pszTmp[0] = 0;
	::ExpandEnvironmentStrings(strFolderName, pszTmp, nLen);
	strFolderName = pszTmp;
	delete[] pszTmp;

	if (PathFileExists(strFolderName) != FALSE)
	{
		pszTmp = new TCHAR[nLen];
		pszTmp[0] = 0;
		::GetLongPathName(strFolderName, pszTmp, nLen);
		strFolderName = pszTmp;
		delete[] pszTmp;
	}

	if (::PathIsRoot(strFolderName))
		return FALSE;

	TCHAR szRestrictedFolderName[4096] = { 0 };
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szRestrictedFolderName)))
	{
		// C:\Program Files
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// C:\Windows
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_WINDOWS, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	return TRUE;
}
BOOL IsFolderAllowedForSearch(LPCTSTR pszFolderName)
{
	if (pszFolderName == NULL)
		return FALSE;

	if (_tcslen(pszFolderName) == 0)
		return FALSE;

	CString strFolderName = pszFolderName;
	::PathUnquoteSpaces(strFolderName.GetBuffer());
	strFolderName.ReleaseBuffer();

	if (strFolderName.IsEmpty())
		return FALSE;

	if (strFolderName.GetAt(1) != TEXT(':'))
		return FALSE;

	::PathRemoveBackslash(strFolderName.GetBuffer());
	strFolderName.ReleaseBuffer();

	int nLen = strFolderName.GetLength() * 2;
	TCHAR* pszTmp = new TCHAR[nLen];
	pszTmp[0] = 0;
	::ExpandEnvironmentStrings(strFolderName, pszTmp, nLen);
	strFolderName = pszTmp;
	delete[] pszTmp;

	if (PathFileExists(strFolderName) != FALSE)
	{
		pszTmp = new TCHAR[nLen];
		pszTmp[0] = 0;
		::GetLongPathName(strFolderName, pszTmp, nLen);
		strFolderName = pszTmp;
		delete[] pszTmp;
	}

	if (::PathIsRoot(strFolderName))
		return FALSE;

	//Read excluded folders from the Registry to strarrFolders
	CStringArray strarrFolders;
	CUIntArray dwarrData;

	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGSFOLDEREXCLUDE, strarrFolders, dwarrData) == FALSE)
	{
		FolderExcludeInfo arrAllExcFolderInfo[ALL_EXC_FOLDERS_COUNT];

		CString strLogsFolder;
		GetLocalAppData(strLogsFolder);
		strLogsFolder += TEXT("\\Logs\\");

		CString strLogsDir;
		if (VSGetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsDir) == FALSE)
		{
			VSSetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsFolder);
			strLogsDir = strLogsFolder;
		}

		if (strLogsDir.Right(1) == TEXT("\\"))
			strLogsDir.Delete(strLogsDir.GetLength() - 1);


		arrAllExcFolderInfo[0].m_strFolderPath = strLogsDir;
		arrAllExcFolderInfo[0].m_dwLoad = 1;

		for (int i = 0; i<ALL_EXC_FOLDERS_COUNT; i++)
		{
			strarrFolders.Add(arrAllExcFolderInfo[i].m_strFolderPath);
			dwarrData.Add(arrAllExcFolderInfo[i].m_dwLoad);
		}
		VSSetSettings(CAT_UNINSTALLER_ALLPROGSFOLDEREXCLUDE, strarrFolders, dwarrData);
	}


	int nElems = strarrFolders.GetCount();
	for (int nIndex = 0; nIndex<nElems; nIndex++)
	{
		if (StrStrI(strFolderName, strarrFolders.GetAt(nIndex)) != NULL)
			return FALSE;
	}

	TCHAR szRestrictedFolderName[4096] = { 0 };
	// C:\Program Files\Common
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMON, NULL, 0, szRestrictedFolderName)))
	{
		if (StrStrI(strFolderName, szRestrictedFolderName))
			return FALSE;
	}

	// C:\Windows
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_WINDOWS, NULL, 0, szRestrictedFolderName)))
	{
		if (StrStrI(strFolderName, szRestrictedFolderName))
			return FALSE;
	}

	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szRestrictedFolderName)))
	{
		// C:\Program Files
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;

		CString strPath = szRestrictedFolderName;
		strPath += TEXT("\\Internet Explorer");
		if (StrStrI(strFolderName, strPath) != NULL)
			return FALSE;

		// C:\Program Files\\Windows NT
		StringCchCat(szRestrictedFolderName, 4096, _T("\\Windows NT"));
		if (StrStrI(strFolderName, szRestrictedFolderName))
			return FALSE;


	}

	// My Documents
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// Start Menu\Programs
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAMS, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}


	// <user name>\Start Menu
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_STARTMENU, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// <user name>\Desktop
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, 0, szRestrictedFolderName)))
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;

	// All Users\Desktop
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_DESKTOPDIRECTORY, NULL, 0, szRestrictedFolderName)))
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;

	// All Users\Start Menu
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_STARTMENU, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\Start Menu\Programs
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_PROGRAMS, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\Desktop
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_DESKTOPDIRECTORY, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\Start Menu
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_STARTMENU, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// <user name>\Local Settings\Applicaiton Data (non roaming)
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;

		CString strPath = szRestrictedFolderName;
		strPath += TEXT("\\Microsoft\\Windows Virtual PC\\Virtual Applications");
		if (StrStrI(strFolderName, strPath) != NULL)
			return FALSE;

		// <user name>\Local Settings\Applicaiton Data\Microsoft\\Windows Sidebar\\Gadgets
		StringCchCat(szRestrictedFolderName, 4096, _T("\\Microsoft\\Windows Sidebar\\Gadgets"));
		if (StrStrI(strFolderName, szRestrictedFolderName))
			return FALSE;
	}

	// All Users\Application Data
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// <user name>\Application Data
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\Documents
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_DOCUMENTS, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// My Computer\Control Panel
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_CONTROLS, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// USERPROFILE
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\Startup
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_STARTUP, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// Start Menu\Programs\Startup
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_STARTUP, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// "My Music" folder
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYMUSIC, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// "My Videos" folder
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYVIDEO, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// C:\Program Files\My Pictures
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYPICTURES, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\Templates
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_TEMPLATES, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\My Music
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_MUSIC, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\My Pictures
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_PICTURES, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users\My Video
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_VIDEO, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// Resource Direcotry
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_RESOURCES, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// Valid only for Windows NT systems
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_FAVORITES, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// C:\Documents and Settings\username\Favorites
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_FAVORITES, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	//Exclude Quick Launch
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szRestrictedFolderName)))
	{
		_tcscat_s(szRestrictedFolderName, 4096, _T("\\Microsoft\\Internet Explorer\\Quick Launch"));
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// All Users
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szRestrictedFolderName)))
	{
		CString strPath = szRestrictedFolderName, strFolder;

		//Extract the All Users folder from Common App Data
		GetLastFolderPath(strPath, &strFolder);
		if (StrCmpI(strFolderName, strFolder) == NULL)
			return FALSE;
	}

	// %ProgramFilesx86% (%SystemDrive%\Program Files)
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
		CString strPath = szRestrictedFolderName;
		strPath += TEXT("\\Internet Explorer");

		if (StrStrI(strFolderName, strPath) != NULL)
			return FALSE;

		// C:\Program Files(86)\\Windows NT
		StringCchCat(szRestrictedFolderName, 4096, _T("\\Windows NT"));
		if (StrStrI(strFolderName, szRestrictedFolderName))
			return FALSE;

	}

	// x86 Program Files\Common on RISC
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMONX86, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// x86 system directory on RISC
	memset(szRestrictedFolderName, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_SYSTEMX86, NULL, 0, szRestrictedFolderName)))
	{
		if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
			return FALSE;
	}

	// %ProgramFiles%\Windows Sidebar\Gadgets
	memset(szRestrictedFolderName, 0, 4096);
	ExpandEnvironmentStrings(_T("%ProgramFiles%\\Windows Sidebar\\Gadgets"), szRestrictedFolderName, 4096);
	if (StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
		return FALSE;


	return TRUE;
}

BOOL GetLastFolderName(CString* strInstLoc, CString* strFolderName)
{
	if ((strInstLoc == NULL) || (strFolderName == NULL))
		return FALSE;

	int nLen = strInstLoc->GetLength();
	if (nLen)
	{
		CString strTemp = *strInstLoc;
		CString strLast;
		strLast += strTemp.GetAt(nLen - 1);
		if (strLast.Compare(_T("\\")) == 0)
		{
			strTemp.Delete(nLen - 1);
			nLen -= 1;
		}

		int nSlash = strTemp.ReverseFind(_T('\\'));
		*strFolderName = strTemp.Right(nLen - nSlash - 1);
		return TRUE;
	}

	strFolderName->Empty();
	return FALSE;
}

BOOL GetLastFolderPath(LPCTSTR pszString, CString* strResultPath)
{
	if ((pszString == NULL) || (strResultPath == NULL))
		return FALSE;

	size_t nSourceLen = _tcslen(pszString);
	if (nSourceLen)
	{
		CString strTemp(pszString);
		int nSlash = strTemp.ReverseFind(_T('\\'));
		CString strPath = strTemp.Left(nSlash);

		int nDblDots = strPath.ReverseFind(_T(':'));
		int nLen = strPath.GetLength() - nDblDots;
		*strResultPath = strPath.Right(nLen + 1);

		return TRUE;
	}

	return FALSE;
}

void SearchAtGoogle(LPCTSTR pszToken)
{
	int nLen = _tcslen(pszToken);
	if (nLen > 0 && nLen < 3700)
	{
		//TCHAR szURL[4096] = _T("http://www.google.com/custom?domains=www.revouninstaller.com&q=");
		///lstrcat(szURL, pszToken);
		//lstrcat(szURL, _T("&sa=Google+Search&sitesearch=&client=pub-3354790367475502&forid=1&ie=ISO-8859-1&oe=ISO-8859-1&cof=GALT%3A%23008000%3BGL%3A1%3BDIV%3A%23336699%3BVLC%3A663399%3BAH%3Acenter%3BBGC%3AFFFFFF%3BLBGC%3A336699%3BALC%3A0000FF%3BLC%3A0000FF%3BT%3A000000%3BGFNT%3A0000FF%3BGIMP%3A0000FF%3BFORID%3A1&hl=en"));

		TCHAR szURL[4096] = _T("http://www.google.com/cse?cx=partner-pub-3354790367475502%3Ashyuk0-3ha1&ie=UTF-8&q=");
		lstrcat(szURL, pszToken);
		lstrcat(szURL, _T("&sa=Search"));

		SHELLEXECUTEINFO ShExecInfo;

		ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
		ShExecInfo.fMask = NULL;
		ShExecInfo.hwnd = NULL;
		ShExecInfo.lpVerb = TEXT("open");
		ShExecInfo.lpFile = szURL;
		ShExecInfo.lpParameters = NULL;
		ShExecInfo.lpDirectory = NULL;
		ShExecInfo.nShow = SW_NORMAL;
		ShExecInfo.hInstApp = NULL;

		ShellExecuteEx(&ShExecInfo);
	}

}


BOOL RemoveWhiteSpaces(CString* strSource, CString* strDest)
{
	if ((strSource) && (strDest))
	{
		*strDest = *strSource;
		int nChanges = strDest->Replace(_T(" "), _T(""));
		if (nChanges == 0)
			return FALSE;
		else
			return TRUE;
	}

	return FALSE;
}

BOOL RemoveSpecSymbols(CString* strSource, CString* strDest)
{
	if ((strSource) && (strDest))
	{
		*strDest = *strSource;
		int nChanges = strDest->Replace(_T("\r\n"), _T(""));
		if (nChanges == 0)
			return FALSE;
		else
			return TRUE;
	}

	return FALSE;
}

BOOL GetStartMenuItems(LPCTSTR pFolder, CStringArray* pArray)
{
	if (pFolder == NULL)
		return FALSE;

	if (_tcslen(pFolder) == 0)
		return FALSE;

	TCHAR szFullPathFileName[4096] = { 0 };
	TCHAR szFilename[4096] = { 0 };

	WIN32_FIND_DATA FileData = { 0 };
	BOOL bFinished = FALSE;
	DWORD dwSize = 0;

	_stprintf(szFullPathFileName, TEXT("%s\\*.*"), pFolder);
	HANDLE hSearch = FindFirstFile(szFullPathFileName, &FileData);
	if (hSearch == INVALID_HANDLE_VALUE)
		return FALSE;

	while (!bFinished)
	{
		//if(::WaitForSingleObject(g_StopScanThreads,0) == WAIT_OBJECT_0)
		//{
		//	FindClose(hSearch);
		//	return FALSE;
		//}

		_stprintf(szFilename, TEXT("%s\\%s"), pFolder, FileData.cFileName);

		if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
		{

			if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_tcscmp(FileData.cFileName, TEXT(".")) && _tcscmp(FileData.cFileName, TEXT("..")))
				{
					GetStartMenuItems(szFilename, pArray);
				}
			}
			else
			{
				//check if file is an executable
				TCHAR pszTargetBuff[4096] = { 0 };
				getShellLinkTarget(szFilename, pszTargetBuff, 4096);
				//get Common folder and exclude all executables from it
				TCHAR szProgramFilesCommon[4096] = { 0 };
				if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMON, NULL, 0, szProgramFilesCommon)))
				{
					if (StrStrI(pszTargetBuff, szProgramFilesCommon) == NULL)
					{
						//get Program Files folder and check if executable is there
						TCHAR szProgramFiles[4096] = { 0 };
						if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFiles)))
						{
							if (StrStrI(pszTargetBuff, szProgramFiles))
							{
								pArray->Add(pszTargetBuff);
							}
						}
					}
				}
			}
		}
		if (!FindNextFile(hSearch, &FileData))
		{
			//if (GetLastError() == ERROR_NO_MORE_FILES) 
			bFinished = TRUE;
		}
	}
	FindClose(hSearch);

	return TRUE;
}

//BOOL GetFilesFolders(LPCTSTR pFolder, CStringArray* pArray, BOOL fRecursive)
//{
//	CString strFolder = pFolder;
//	if(IsFolderAllowedForSearch(&strFolder) == FALSE)
//		return FALSE;
//
//	TCHAR szFullPathFileName[4096] = {0};
//	TCHAR szFilename[4096]= {0};
//
//	WIN32_FIND_DATA FileData = {0};
//	BOOL bFinished = FALSE; 
//	DWORD dwSize = 0;
//
//	_stprintf(szFullPathFileName, TEXT("%s\\*.*"), pFolder);
//	HANDLE hSearch = FindFirstFile(szFullPathFileName, &FileData); 
//	if (hSearch == INVALID_HANDLE_VALUE) 
//		return FALSE;
//
//	while (!bFinished)
//	{
//		if(::WaitForSingleObject(g_StopScanThreads,0) == WAIT_OBJECT_0)
//		{
//			FindClose(hSearch);
//			return FALSE;
//		}
//
//		_stprintf(szFilename, TEXT("%s\\%s"),pFolder,FileData.cFileName);
//		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
//		{
//			if (_tcscmp(FileData.cFileName,TEXT(".")) && _tcscmp(FileData.cFileName,TEXT("..")))
//			{
//				if(fRecursive)
//					GetFilesFolders(szFilename, pArray, TRUE);
//			}
//		}
//		else
//		{
//			pArray->Add(szFilename);
//		}
//		if (!FindNextFile(hSearch, &FileData)) 
//		{
//			if (GetLastError() == ERROR_NO_MORE_FILES) 
//				bFinished = TRUE;
//		} 
//	}
//	FindClose(hSearch);
//
//	return TRUE;
//}



//pFolder - Folder name to search in it
//pName - Name of file or folder to search for
//fFolder - true if we are searching for folder
//pArray - return path
//BOOL FindFileOrFolder(LPCTSTR pFolder, CStringArray* arrNames, BOOL fFolder, CStringArray* pArray, BOOL fRecursive)
//{
//	if(pFolder == NULL)
//		return FALSE;	
//	
//	if(_tcslen(pFolder) == 0)
//		return FALSE;
//
//	TCHAR szFullPathFileName[4096] = {0};
//	TCHAR szFilename[4096] = {0};
//
//	WIN32_FIND_DATA FileData = {0};
//	BOOL bFinished = FALSE; 
//	DWORD dwSize = 0;
//
//	_stprintf(szFullPathFileName, TEXT("%s"), pFolder);
//	::PathRemoveBackslash(szFullPathFileName);
//	_tcscat(szFullPathFileName, TEXT("\\*.*"));
//	HANDLE hSearch = FindFirstFile(szFullPathFileName, &FileData); 
//	if (hSearch == INVALID_HANDLE_VALUE) 
//		return FALSE;
//
//	while (!bFinished)
//	{
//		if(::WaitForSingleObject(g_StopScanThreads,0) == WAIT_OBJECT_0)
//		{
//			FindClose(hSearch);
//			return FALSE;
//		}
//
//		_stprintf(szFilename, TEXT("%s\\%s"),pFolder,FileData.cFileName);
//		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
//		{
//			if (_tcscmp(FileData.cFileName,TEXT(".")) && _tcscmp(FileData.cFileName,TEXT("..")))
//			{
//				if(fFolder)
//					if(IsStringInList(arrNames, FileData.cFileName))
//					{
//						pArray->Add(szFilename);
//						FindClose(hSearch);
//						return TRUE;
//					}
//
//				if(fRecursive)
//					FindFileOrFolder(szFilename, arrNames, fFolder, pArray, TRUE);
//			}
//		}
//		else
//		{
//			if(!fFolder)
//			{
//				CString strFile = FileData.cFileName;
//				::PathRemoveExtension(strFile.GetBuffer());
//				strFile.ReleaseBuffer();
//				if(IsStringInList(arrNames, strFile))
//					pArray->Add(szFilename);
//			}
//		}
//		if (!FindNextFile(hSearch, &FileData)) 
//		{
//			if (GetLastError() == ERROR_NO_MORE_FILES) 
//				bFinished = TRUE;
//		} 
//	}
//	FindClose(hSearch);
//
//	return TRUE;
//}
BOOL GetSeparatedKey(LPCTSTR pszKeysPath, int nKeyNumber, CString* strOneKeyOnly)
{

	if ((pszKeysPath == NULL) || (nKeyNumber == 0) || (strOneKeyOnly == NULL))
		return FALSE;

	CString strKeysPath = pszKeysPath;
	int curPos = 0;
	CString resToken;
	resToken = strKeysPath.Tokenize(TEXT("\\"), curPos);

	int nCount = 0;
	while (resToken != TEXT(""))
	{

		nCount++;
		if (nCount == nKeyNumber)
		{
			*strOneKeyOnly = resToken;
			return TRUE;
		}
		else
			resToken = strKeysPath.Tokenize(TEXT("\\"), curPos); // Get next token:

	}
	return FALSE;
}

void CenterWindowToWorkArea(HWND hWnd)
{
	RECT rectSys;
	int nRet = ::SystemParametersInfo(SPI_GETWORKAREA, 0, &rectSys, SPIF_UPDATEINIFILE);

	RECT rectWnd;
	::GetWindowRect(hWnd, &rectWnd);
	int nWidth = rectWnd.right - rectWnd.left;
	int nHeight = rectWnd.bottom - rectWnd.top;
	POINT ptLeftUpperPoint;
	ptLeftUpperPoint.x = (rectSys.right - nWidth) / 2;
	ptLeftUpperPoint.y = (rectSys.bottom - nHeight) / 2;

	::SetWindowPos(hWnd, HWND_TOP, ptLeftUpperPoint.x, ptLeftUpperPoint.y, nWidth, nHeight, NULL);
}
BOOL squash_guid(LPCWSTR in, LPWSTR out)
{
	DWORD i, n = 1;
	GUID guid;

	out[0] = 0;

	if (FAILED(CLSIDFromString((LPOLESTR)in, &guid)))
		return FALSE;

	for (i = 0; i<8; i++)
		out[7 - i] = in[n++];
	n++;
	for (i = 0; i<4; i++)
		out[11 - i] = in[n++];
	n++;
	for (i = 0; i<4; i++)
		out[15 - i] = in[n++];
	n++;
	for (i = 0; i<2; i++)
	{
		out[17 + i * 2] = in[n++];
		out[16 + i * 2] = in[n++];
	}
	n++;
	for (; i<8; i++)
	{
		out[17 + i * 2] = in[n++];
		out[16 + i * 2] = in[n++];
	}
	out[32] = 0;
	return TRUE;
}
BOOL unsquash_guid(LPCWSTR in, LPWSTR out)
{
	DWORD i, n = 0;

	out[n++] = '{';
	for (i = 0; i<8; i++)
		out[n++] = in[7 - i];
	out[n++] = '-';
	for (i = 0; i<4; i++)
		out[n++] = in[11 - i];
	out[n++] = '-';
	for (i = 0; i<4; i++)
		out[n++] = in[15 - i];
	out[n++] = '-';
	for (i = 0; i<2; i++)
	{
		out[n++] = in[17 + i * 2];
		out[n++] = in[16 + i * 2];
	}
	out[n++] = '-';
	for (; i<8; i++)
	{
		out[n++] = in[17 + i * 2];
		out[n++] = in[16 + i * 2];
	}
	out[n++] = '}';
	out[n] = 0;
	return TRUE;
}

BOOL IsRealString(LPCTSTR pszString)
{
	CString strNoSpaces, strChecked(pszString);

	RemoveWhiteSpaces(&strChecked, &strNoSpaces);

	if (strNoSpaces.IsEmpty())
		return FALSE;
	else
		return TRUE;
}
BOOL IsRealStringForced(LPCTSTR pszString)
{
	CString strNoSpaces, strNoEnter, strChecked(pszString);

	RemoveWhiteSpaces(&strChecked, &strNoSpaces);
	RemoveSpecSymbols(&strNoSpaces, &strNoEnter);

	if (strNoEnter.IsEmpty())
		return FALSE;
	else
		return TRUE;
}

CString GetCurrentUserSID()
{
	// Declare variables
	CString sUsername = TEXT(""), sDomainName = TEXT(""), sBuffer, sResult = TEXT("");
	PSID pSid = NULL;
	BYTE bySidBuffer[256];
	DWORD dwSidSize = sizeof(bySidBuffer), dwDomainNameSize = 1024, dwUsernameSize = 1024;
	SID_NAME_USE sidType;

	// Initialize variables
	pSid = (PSID)bySidBuffer;
	dwSidSize = sizeof(bySidBuffer);

	// Get current username
	GetUserName(sBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
	sUsername = sBuffer.GetBuffer(dwUsernameSize);
	sBuffer.ReleaseBuffer();

	// Get the domain name
	LookupAccountName(NULL, sUsername, (PSID)pSid, &dwSidSize,
		sBuffer.GetBuffer(dwDomainNameSize), &dwDomainNameSize, (PSID_NAME_USE)&sidType);
	sDomainName = sBuffer.GetBuffer(dwDomainNameSize);
	sBuffer.ReleaseBuffer();

	// Return right result
	//if (!sDomainName.IsEmpty())
	//	sResult = sDomainName + "\\";
	//sResult += sUsername;

	//TCHAR strSID[1024] = {0};
	LPTSTR pszSid = NULL;
	ConvertSidToStringSid(pSid, &pszSid);
	sResult = pszSid;

	return sResult;
}

void GetWindowsVersion(int* nMajor, int* nMinor)
{
	if (Is_Win10())
	{
		(*nMajor) = 10;
		(*nMinor) = 0;
		return;
	}

	if (Is_Win81())
	{
		(*nMajor) = 6;
		(*nMinor) = 3;
		return;
	}

	OSVERSIONINFOEX osvi;
	BOOL bOsVersionInfoEx;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	if (!(bOsVersionInfoEx = GetVersionEx((OSVERSIONINFO *)&osvi)))
	{
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		GetVersionEx((OSVERSIONINFO *)&osvi);
	}

	(*nMajor) = osvi.dwMajorVersion;
	(*nMinor) = osvi.dwMinorVersion;
}

void GetRootHKEYByName(CString strRootName, HKEY& hRootKey)
{
	if (strRootName.IsEmpty())
		return;

	if (StrCmp(strRootName, _T("HKEY_CLASSES_ROOT")) == 0)
	{
		hRootKey = HKEY_CLASSES_ROOT;
		return;
	}
	if (StrCmp(strRootName, _T("HKEY_CURRENT_USER")) == 0)
	{
		hRootKey = HKEY_CURRENT_USER;
		return;
	}
	if (StrCmp(strRootName, _T("HKEY_LOCAL_MACHINE")) == 0)
	{
		hRootKey = HKEY_LOCAL_MACHINE;
		return;
	}
	if (StrCmp(strRootName, _T("HKEY_CURRENT_CONFIG")) == 0)
	{
		hRootKey = HKEY_CURRENT_CONFIG;
		return;
	}
	if (StrCmp(strRootName, _T("HKEY_USERS")) == 0)
	{
		hRootKey = HKEY_USERS;
		return;
	}
}

BOOL CheckIfMSI_Has_I(CString strCommand)
{
	if (strCommand.Find(TEXT("/I")) > -1)
		return TRUE;
	else
		return FALSE;
}

//First parameter is the Root key - i.e. HKEY_LOCAL_MACHINE etc.
//Second parameter is the Key path - i.e. Sofwtware\\VS Revo Group etc.
BOOL TakeRegKeyOwnership(HKEY hRoot, LPCTSTR lpszOwnKey, SE_OBJECT_TYPE eObjType)
{
	BOOL bRetval = FALSE;

	HANDLE hToken = NULL;
	PSID pSIDAdmin = NULL;
	PSID pSIDEveryone = NULL;
	PACL pACL = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld =
		SECURITY_WORLD_SID_AUTHORITY;
	SID_IDENTIFIER_AUTHORITY SIDAuthNT = SECURITY_NT_AUTHORITY;
	const int NUM_ACES = 2;
	EXPLICIT_ACCESS ea[NUM_ACES];
	DWORD dwRes;

	//Create a Registry Object Name!
	TCHAR szKeyName[4096] = { 0 };
	if (hRoot == HKEY_LOCAL_MACHINE)
	{
		_tcscpy_s(szKeyName, 4096, _T("MACHINE\\"));
	}
	if (hRoot == HKEY_CURRENT_USER)
	{
		_tcscpy_s(szKeyName, 4096, _T("CURRENT_USER\\"));
	}
	if (hRoot == HKEY_CLASSES_ROOT)
	{
		_tcscpy_s(szKeyName, 4096, _T("CLASSES_ROOT\\"));
	}
	if (hRoot == HKEY_USERS)
	{
		_tcscpy_s(szKeyName, 4096, _T("USERS\\"));
	}
	_tcscat_s(szKeyName, 4096, lpszOwnKey);


	// Specify the DACL to use.
	// Create a SID for the Everyone group.
	if (!AllocateAndInitializeSid(&SIDAuthWorld, 1,
		SECURITY_WORLD_RID,
		0,
		0, 0, 0, 0, 0, 0,
		&pSIDEveryone))
	{
		printf("AllocateAndInitializeSid (Everyone) error %u\n",
			GetLastError());
		goto Cleanup;
	}

	// Create a SID for the BUILTIN\Administrators group.
	if (!AllocateAndInitializeSid(&SIDAuthNT, 2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&pSIDAdmin))
	{
		printf("AllocateAndInitializeSid (Admin) error %u\n",
			GetLastError());
		goto Cleanup;
	}

	ZeroMemory(&ea, NUM_ACES * sizeof(EXPLICIT_ACCESS));

	// Set read access for Everyone.
	ea[0].grfAccessPermissions = GENERIC_READ;
	ea[0].grfAccessMode = SET_ACCESS;
	ea[0].grfInheritance = NO_INHERITANCE;
	ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea[0].Trustee.ptstrName = (LPTSTR)pSIDEveryone;

	// Set full control for Administrators.
	ea[1].grfAccessPermissions = GENERIC_ALL;
	ea[1].grfAccessMode = SET_ACCESS;
	ea[1].grfInheritance = SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[1].Trustee.ptstrName = (LPTSTR)pSIDAdmin;

	if (ERROR_SUCCESS != SetEntriesInAcl(NUM_ACES,
		ea,
		NULL,
		&pACL))
	{
		printf("Failed SetEntriesInAcl\n");
		goto Cleanup;
	}

	// Try to modify the object's DACL.
	dwRes = SetNamedSecurityInfo(
		szKeyName,                 // name of the object
		eObjType,              // type of object
		DACL_SECURITY_INFORMATION,   // change only the object's DACL
		NULL, NULL,                  // do not change owner or group
		pACL,                        // DACL specified
		NULL);                       // do not change SACL

	if (ERROR_SUCCESS == dwRes)
	{
		printf("Successfully changed DACL\n");
		bRetval = TRUE;
		// No more processing needed.
		goto Cleanup;
	}
	if (dwRes != ERROR_ACCESS_DENIED)
	{
		printf("First SetNamedSecurityInfo call failed: %u\n",
			dwRes);
		goto Cleanup;
	}

	// If the preceding call failed because access was denied, 
	// enable the SE_TAKE_OWNERSHIP_NAME privilege, create a SID for 
	// the Administrators group, take ownership of the object, and 
	// disable the privilege. Then try again to set the object's DACL.

	// Open a handle to the access token for the calling process.
	if (!OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES,
		&hToken))
	{
		printf("OpenProcessToken failed: %u\n", GetLastError());
		goto Cleanup;
	}

	// Enable the SE_TAKE_OWNERSHIP_NAME privilege.
	if (!SetPrivilege(hToken, SE_TAKE_OWNERSHIP_NAME, TRUE))
	{
		printf("You must be logged on as Administrator.\n");
		goto Cleanup;
	}

	// Set the owner in the object's security descriptor.
	dwRes = SetNamedSecurityInfo(
		szKeyName,                 // name of the object
		eObjType,              // type of object
		OWNER_SECURITY_INFORMATION,  // change only the object's owner
		pSIDAdmin,                   // SID of Administrator group
		NULL,
		NULL,
		NULL);

	if (dwRes != ERROR_SUCCESS)
	{
		printf("Could not set owner. Error: %u\n", dwRes);
		goto Cleanup;
	}

	// Disable the SE_TAKE_OWNERSHIP_NAME privilege.
	if (!SetPrivilege(hToken, SE_TAKE_OWNERSHIP_NAME, FALSE))
	{
		printf("Failed SetPrivilege call unexpectedly.\n");
		goto Cleanup;
	}

	// Try again to modify the object's DACL,
	// now that we are the owner.
	dwRes = SetNamedSecurityInfo(
		szKeyName,                 // name of the object
		eObjType,              // type of object
		DACL_SECURITY_INFORMATION,   // change only the object's DACL
		NULL, NULL,                  // do not change owner or group
		pACL,                        // DACL specified
		NULL);                       // do not change SACL

	if (dwRes == ERROR_SUCCESS)
	{
		printf("Successfully changed DACL\n");
		bRetval = TRUE;
	}
	else
	{
		printf("Second SetNamedSecurityInfo call failed: %u\n",
			dwRes);
	}

Cleanup:

	if (pSIDAdmin)
		FreeSid(pSIDAdmin);

	if (pSIDEveryone)
		FreeSid(pSIDEveryone);

	if (pACL)
		LocalFree(pACL);

	if (hToken)
		CloseHandle(hToken);

	return bRetval;

}
BOOL VSModifyValue(HKEY hRoot, LPCTSTR pszKeyName, HKEY hParentKey, LPCTSTR pszValueName, DWORD dwDataType,
	DWORD dwSize, LPBYTE pData, REGSAM dwFlag)
{
	if (_tcslen(pszKeyName) == 0)
		return FALSE;

	if ((pData == NULL) || (dwSize == 0))
		return FALSE;

	if (RegSetValueEx(hParentKey, pszValueName, NULL, dwDataType, pData, dwSize) == ERROR_SUCCESS)
		return TRUE;
	//Access Denied
	if (RegSetValueEx(hParentKey, pszValueName, NULL, dwDataType, pData, dwSize) != ERROR_ACCESS_DENIED)
		return FALSE;

#ifdef _WIN64		
	if (dwFlag == KEY_WOW64_32KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_WOW64_32KEY);
	else//if(dwFlag == KEY_WOW64_64KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);
#else
	TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);
#endif

	DWORD dwErr = RegSetValueEx(hParentKey, pszValueName, NULL, dwDataType, pData, dwSize);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;

}
///The method return TRUE also if the key doesn't exist in this case RegDeleteKey returns 2
BOOL VSRegDeleteKeyNoSubs(HKEY hRoot, LPCTSTR pszKeyName, REGSAM samFlag)
{
	if (_tcslen(pszKeyName) == 0)
		return FALSE;
#ifdef _WIN64
	LONG lRes = RegDeleteKeyEx(hRoot, pszKeyName, samFlag, 0);

	if ((lRes == ERROR_SUCCESS) || (lRes == 2))
		return TRUE;

	//Access Denied
	if (RegDeleteKeyEx(hRoot, pszKeyName, samFlag, 0) != ERROR_ACCESS_DENIED)
		return FALSE;

	if (samFlag == KEY_WOW64_32KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_WOW64_32KEY);
	else //if(samFlag == KEY_WOW64_64KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);

	DWORD dwErr = ::RegDeleteKeyEx(hRoot, pszKeyName, samFlag, 0);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;
#else
	LONG lRes = RegDeleteKey(hRoot, pszKeyName);

	if ((lRes == ERROR_SUCCESS) || (lRes == 2))
		return TRUE;

	//Access Denied
	if (RegDeleteKey(hRoot, pszKeyName) != ERROR_ACCESS_DENIED)
		return FALSE;

	TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);

	DWORD dwErr = ::RegDeleteKey(hRoot, pszKeyName);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;
#endif
}

BOOL VSCreateRegKey(HKEY hRoot, LPCTSTR pszParentPath, HKEY hParent, LPCTSTR pszKeyName, REGSAM dwFlag)
{
	if (_tcslen(pszKeyName) == 0)
		return FALSE;

	HKEY hk;
	DWORD dwDisp;

	if (RegCreateKeyEx(hParent, pszKeyName, 0, NULL, REG_OPTION_NON_VOLATILE,
		dwFlag, NULL, &hk, &dwDisp) == ERROR_SUCCESS)
	{
		RegCloseKey(hk);
		return TRUE;
	}

	//Access Denied
	if (RegCreateKeyEx(hParent, pszKeyName, 0, NULL, REG_OPTION_NON_VOLATILE,
		dwFlag, NULL, &hk, &dwDisp) != ERROR_ACCESS_DENIED)
		return FALSE;
#ifdef	_WIN64	
	if ((dwFlag & KEY_WOW64_32KEY) == KEY_WOW64_32KEY)
		TakeRegKeyOwnership(hRoot, pszParentPath, SE_REGISTRY_WOW64_32KEY);
	else //if(dwFlag & KEY_WOW64_64KEY == KEY_WOW64_64KEY)
		TakeRegKeyOwnership(hRoot, pszParentPath, SE_REGISTRY_KEY);
#else
	TakeRegKeyOwnership(hRoot, pszParentPath, SE_REGISTRY_KEY);
#endif
	DWORD dwErr = RegCreateKeyEx(hParent, pszKeyName, 0, NULL, REG_OPTION_NON_VOLATILE,
		dwFlag, NULL, &hk, &dwDisp);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;

}
#ifdef _WIN64a

//First parameter is the Root key - i.e. HKEY_LOCAL_MACHINE etc.
//Second parameter is the Key path - i.e. Sofwtware\\VS Revo Group etc.
BOOL VSDeleteRegKey(HKEY hRoot, LPCTSTR pszKeyName, BOOL bIs64Bit)
{
	if (_tcslen(pszKeyName) == 0)
		return FALSE;

#ifdef _WIN64
	if (bIs64Bit == 0)
	{
		if (VSRegDeleteKeyExRecur(hRoot, pszKeyName, KEY_WOW64_32KEY) == ERROR_SUCCESS)
			return TRUE;

		//Access Denied
		if (VSRegDeleteKeyExRecur(hRoot, pszKeyName, KEY_WOW64_32KEY) != ERROR_ACCESS_DENIED)
			return FALSE;

		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_WOW64_32KEY);

		if (VSRegDeleteKeyExRecur(hRoot, pszKeyName, KEY_WOW64_32KEY) == ERROR_SUCCESS)
			return TRUE;
		else
			return FALSE;
	}
	else //	if(bIs64Bit == 1)
	{
		if (::SHDeleteKey(hRoot, pszKeyName) == ERROR_SUCCESS)
			return TRUE;

		//Access Denied
		if (::SHDeleteKey(hRoot, pszKeyName) != ERROR_ACCESS_DENIED)
			return FALSE;

		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);

		DWORD dwErr = ::SHDeleteKey(hRoot, pszKeyName);
		if (dwErr == ERROR_SUCCESS)
			return TRUE;
		else
			return FALSE;
	}
#endif

	if (::SHDeleteKey(hRoot, pszKeyName) == ERROR_SUCCESS)
		return TRUE;

	//Access Denied
	if (::SHDeleteKey(hRoot, pszKeyName) != ERROR_ACCESS_DENIED)
		return FALSE;

	TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);

	DWORD dwErr = ::SHDeleteKey(hRoot, pszKeyName);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;
}
DWORD VSRegDeleteKeyExRecur(HKEY hStartKey, LPCTSTR pKeyName, REGSAM samFlag)
{
	DWORD   dwRtn, dwSubKeyLength;
	LPTSTR  pSubKey = NULL;
	TCHAR   szSubKey[MAX_KEY_LENGTH]; // (256) this should be dynamic.
	HKEY    hKey;

	// Do not allow NULL or empty key name
	if (pKeyName &&  lstrlen(pKeyName))
	{
		if ((dwRtn = RegOpenKeyEx(hStartKey, pKeyName,
			0, KEY_ENUMERATE_SUB_KEYS | DELETE | samFlag, &hKey)) == ERROR_SUCCESS)
		{
			while (dwRtn == ERROR_SUCCESS)
			{
				dwSubKeyLength = MAX_KEY_LENGTH;
				dwRtn = RegEnumKeyEx(
					hKey,
					0,       // always index zero
					szSubKey,
					&dwSubKeyLength,
					NULL,
					NULL,
					NULL,
					NULL
				);

				if (dwRtn == ERROR_NO_MORE_ITEMS)
				{
					dwRtn = RegDeleteKeyEx(hStartKey, pKeyName, samFlag, 0);
					break;
				}
				else if (dwRtn == ERROR_SUCCESS)
					dwRtn = VSRegDeleteKeyExRecur(hKey, szSubKey, samFlag);
			}
			RegCloseKey(hKey);
			// Do not save return code because error
			// has already occurred
		}
	}
	else
		dwRtn = ERROR_BADKEY;

	return dwRtn;
}
#endif
BOOL SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;

	if (!LookupPrivilegeValue(
		NULL,            // lookup privilege on local system
		lpszPrivilege,   // privilege to lookup 
		&luid))        // receives LUID of privilege
	{
		printf("LookupPrivilegeValue error: %u\n", GetLastError());
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	// Enable the privilege or disable all privileges.

	if (!AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		(PTOKEN_PRIVILEGES)NULL,
		(PDWORD)NULL))
	{
		printf("AdjustTokenPrivileges error: %u\n", GetLastError());
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

	{
		printf("The token does not have the specified privilege. \n");
		return FALSE;
	}

	return TRUE;
}

//void ReplaceMSIItoXParameter(LPTSTR pszCommand)
//{
//	CString strCommand = pszCommand;
//	if(strCommand.Replace(TEXT("/I"),TEXT("/X"))!= 0)
//		StringCchCopy(pszCommand,lstrlen(pszCommand)+1,strCommand);
//}

BOOL GetRegKeysFromString(CString &strSource, CStringArray &arrReturn)
{
	CString resToken;
	int curPos = 0;

	resToken = strSource.Tokenize(_T("\\"), curPos);
	while (resToken != "")
	{
		arrReturn.Add(resToken);
		resToken = strSource.Tokenize(_T("\\"), curPos);
	}

	if (arrReturn.GetCount()>0)
		return TRUE;
	else
		return FALSE;
}


BOOL GetFileData(const LPCTSTR szFileName, _int64* nSize, CTime* timeMod)
{
	struct __stat64 fileStat;
	int err = _tstat64(szFileName, &fileStat);
	if (0 != err) return FALSE;
	CTime temp(fileStat.st_mtime);// __time64_t 
	*timeMod = temp;
	*nSize = fileStat.st_size;
	return TRUE;
}

BOOL GetFileSize(LPCTSTR szFileName, _int64* nSize)
{
	struct __stat64 fileStat;
	int err = _tstat64(szFileName, &fileStat);
	if (0 != err)
		return FALSE;
	*nSize = fileStat.st_size;
	return TRUE;
}


void GetDateAttribFromString(CString strDate, int* nDay, int* nMonth, int* nYear)
{
	if (strDate.IsEmpty())
		return;
	*nDay = _ttoi(strDate.Left(2));
	*nMonth = _ttoi(strDate.Mid(3, 4));
	*nYear = _ttoi(strDate.Right(4));
}

void GetHourAttribFromString(CString strHour, int* nHour, int* nMinutes, int* nSeconds)
{
	if (strHour.IsEmpty())
		return;
	*nHour = _ttoi(strHour.Left(2));
	*nMinutes = _ttoi(strHour.Mid(3, 4));
	*nSeconds = _ttoi(strHour.Right(2));
}

BOOL IsStringGUID(LPCTSTR strChecked)
{
	CString strData = strChecked;
	if (strData.GetAt(0) == TEXT('{'))
		return TRUE;
	else
		return FALSE;
}

BOOL VSDeleteValue(HKEY hRoot, LPCTSTR pszKeyName, HKEY hParentKey, LPCTSTR pszValueName, REGSAM dwFlag)
{
	if (_tcslen(pszKeyName) == 0)
		return FALSE;

	if (RegDeleteValue(hParentKey, pszValueName) == ERROR_SUCCESS)
		return TRUE;

	//Access Denied
	if (RegDeleteValue(hParentKey, pszValueName) != ERROR_ACCESS_DENIED)
		return FALSE;
#ifdef _WIN64
	if (dwFlag == KEY_WOW64_32KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_WOW64_32KEY);
	else// if(dwFlag == KEY_WOW64_64KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);
#else
	TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);
#endif

	DWORD dwErr = RegDeleteValue(hParentKey, pszValueName);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;
}

BOOL VSOpenRegKey(HKEY hRoot, LPCTSTR pszKeyName, PHKEY hCurrent, REGSAM dwFlag)
{
	if (_tcslen(pszKeyName) == 0)
		return FALSE;

	if (RegOpenKeyEx(hRoot, pszKeyName, NULL, dwFlag, hCurrent) == ERROR_SUCCESS)
		return TRUE;

	//Access Denied
	if (RegOpenKeyEx(hRoot, pszKeyName, NULL, dwFlag, hCurrent) != ERROR_ACCESS_DENIED)
		return FALSE;

#ifdef _WIN64		
	if ((dwFlag & KEY_WOW64_32KEY) == KEY_WOW64_32KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_WOW64_32KEY);
	else //if(dwFlag & KEY_WOW64_64KEY == KEY_WOW64_64KEY)
		TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);
#else
	TakeRegKeyOwnership(hRoot, pszKeyName, SE_REGISTRY_KEY);
#endif


	DWORD dwErr = RegOpenKeyEx(hRoot, pszKeyName, NULL, dwFlag, hCurrent);
	if (dwErr == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;
}
HKEY GetRootFromPath(CString strOurPath, CString* strRoot)
{
	int curPos = 0, nSymbolstodel = 0;

	CString resToken = strOurPath.Tokenize(_T("\\"), curPos);
	nSymbolstodel += resToken.GetLength();
	//resToken= strOurPath.Tokenize(_T("\\"),curPos);
	//nSymbolstodel += resToken.GetLength();
	if (strOurPath[0] == TEXT('\\'))
		strOurPath.Delete(0, nSymbolstodel + 2);
	else
		strOurPath.Delete(0, nSymbolstodel + 1);
	//strResultPath = strOurPath;
	*strRoot = resToken;

	if (strRoot->CompareNoCase(_T("HKEY_LOCAL_MACHINE")) == 0)
		return HKEY_LOCAL_MACHINE;
	if (strRoot->CompareNoCase(_T("HKEY_USERS")) == 0)
		return HKEY_USERS;
	if (strRoot->CompareNoCase(_T("HKEY_CURRENT_USER")) == 0)
		return HKEY_CURRENT_USER;

	return NULL;
}
HKEY GetRootFromPath(CString strOurPath)
{
	int curPos = 0, nSymbolstodel = 0;
	CString strRoot;
	CString resToken = strOurPath.Tokenize(_T("\\"), curPos);
	nSymbolstodel += resToken.GetLength();
	//resToken= strOurPath.Tokenize(_T("\\"),curPos);
	//nSymbolstodel += resToken.GetLength();
	if (strOurPath[0] == TEXT('\\'))
		strOurPath.Delete(0, nSymbolstodel + 2);
	else
		strOurPath.Delete(0, nSymbolstodel + 1);
	//strResultPath = strOurPath;
	strRoot = resToken;

	if (strRoot.CompareNoCase(_T("HKEY_LOCAL_MACHINE")) == 0)
		return HKEY_LOCAL_MACHINE;
	if (strRoot.CompareNoCase(_T("HKEY_USERS")) == 0)
		return HKEY_USERS;
	if (strRoot.CompareNoCase(_T("HKEY_CURRENT_USER")) == 0)
		return HKEY_CURRENT_USER;

	return NULL;
}

void RemoveRootFromPath(CString strOurPath, CString& strResultPath)
{
	int curPos = 0, nSymbolstodel = 0;
	CString strRoot;
	CString resToken = strOurPath.Tokenize(_T("\\"), curPos);
	nSymbolstodel += resToken.GetLength();
	//resToken= strOurPath.Tokenize(_T("\\"),curPos);
	//nSymbolstodel += resToken.GetLength();
	if (strOurPath[0] == TEXT('\\'))
		strOurPath.Delete(0, nSymbolstodel + 2);
	else
		strOurPath.Delete(0, nSymbolstodel + 1);

	strResultPath = strOurPath;
}

void ConvertByteArrayToCString(BYTE* arrBytes, int narrSize, CString* strResult)
{
	TCHAR temp[6] = { 0 };
	for (int i = 0; i<narrSize; i++)
	{
		_ultot(arrBytes[i], temp, 16);
		*strResult += temp;
		*strResult += TEXT(" ");
	}
}

int GetPathDepth(CString& strPath)
{
	if (strPath.IsEmpty())
		return 0;
	int count = 0;
	TCHAR tchPoint = TEXT('\\');
	int isymbols = strPath.GetLength();
	for (int i = 0; i<isymbols - 1; i++) //"isymbols-1" because we do not want to cound last symbol in case it is backslash
	{
		if (strPath.GetAt(i) == tchPoint)
			count++;
	}
	return count;
}

void ConvertStringListToRegPath(CStringList* strList, CString& strResult)
{
	POSITION pos = strList->GetHeadPosition();
	CString strFirstKey = strList->GetNext(pos);
	while (pos != NULL)
	{
		CString strKey = strList->GetNext(pos);
		strResult += strKey;
		if (pos != NULL)
			strResult += TEXT("\\");
	}

}

void ConvertMultiSzToArray(TCHAR* pszMultySz, DWORD dwSize, CStringArray& arrResult)
{
	DWORD dwCurrentSize = 0;
	TCHAR* pCurrPos = pszMultySz;
	while (dwCurrentSize < dwSize)
	{
		if (_tcslen(pCurrPos))
			arrResult.Add(pCurrPos);

		int nLen = (int)_tcslen(pCurrPos) + 1;
		pCurrPos = pCurrPos + nLen;
		dwCurrentSize += ((nLen + 1) * sizeof(TCHAR));
	}
}
void ConvertMultiSzToStringOld(TCHAR* pszMultySz, DWORD dwSize, CString& strResult)
{
	DWORD dwCurrentSize = 0;
	TCHAR* pCurrPos = pszMultySz;
	while (dwCurrentSize < dwSize)
	{
		//arrResult.Add(pCurrPos);
		strResult += pCurrPos;
		strResult += TEXT(" ");

		int nLen = (int)_tcslen(pCurrPos) + 1;
		pCurrPos = pCurrPos + nLen;
		dwCurrentSize += ((nLen + 1) * sizeof(TCHAR));
	}
}
void ConvertMultiSzToString(CByteArray* pByteArray, DWORD dwSize, CString& strResult)
{
	DWORD dwCurrentSize = 0;
	//TCHAR* pCurrPos = pszMultySz;
	DWORD swStringSize = dwSize / sizeof(TCHAR) + 1;
	TCHAR* pszByteArray = new TCHAR[swStringSize];
	TCHAR* pszTempPos = pszByteArray;
	memset(pszByteArray, 0, swStringSize);
	CopyMemory(pszByteArray, pByteArray->GetData(), dwSize);
	pszByteArray[dwSize / sizeof(TCHAR)] = TEXT('\0');
	while (dwCurrentSize < dwSize)
	{
		//arrResult.Add(pCurrPos);
		strResult += pszTempPos;
		strResult += TEXT(" ");

		int nLen = (int)_tcslen(pszTempPos) + 1;
		if ((dwCurrentSize + ((nLen + 1) * sizeof(TCHAR)))< dwSize)
			pszTempPos = pszTempPos + nLen;
		dwCurrentSize += ((nLen + 1) * sizeof(TCHAR));
	}
	delete[]pszByteArray;
}

//////void ConvertMultiSzToString(TCHAR* pszMultySz, DWORD dwSize, CString& strResult)
//////{
//////	DWORD dwCurrentSize = 0;
//////	TCHAR* pCurrPos = pszMultySz;
//////	while (dwCurrentSize < dwSize)
//////	{
//////		//arrResult.Add(pCurrPos);
//////		//CString strLine;
//////		CString strTemp = pCurrPos;
//////		int nNullTermPos = strTemp.Find(TEXT('\0'));
//////
//////		strResult += strTemp.Left(nNullTermPos);
//////		strResult += TEXT(" ");
//////
//////		int nLen = (int)_tcslen(pCurrPos);
//////		pCurrPos = pCurrPos + nLen;
//////		dwCurrentSize += ((nLen) * sizeof(TCHAR));
//////	}
//////}

BOOL GetLastKeyNameAndItsPath(CString& strFullPath, CString& strLastKeyName, CString& strLastKeyPath)
{
	if (strFullPath.IsEmpty())
		return FALSE;

	int nLen = strFullPath.GetLength();
	if (nLen)
	{
		CString strTemp = strFullPath;
		CString strLast;
		strLast += strTemp.GetAt(nLen - 1);
		if (strLast.Compare(_T("\\")) == 0)
		{
			strTemp.Delete(nLen - 1);
			nLen -= 1;
		}

		int nSlash = strTemp.ReverseFind(_T('\\'));
		strLastKeyName = strTemp.Right(nLen - nSlash - 1);
		strLastKeyPath = strTemp.Left(nLen - (nLen - nSlash - 1) - 1);
		return TRUE;
	}

	return FALSE;
}

_int64 GetFileSize64(LPCTSTR szFileName)
{
	struct __stat64 fileStat;
	int err = _tstat64(szFileName, &fileStat);
	if (0 != err)
		return 0;
	return fileStat.st_size;
}

BOOL Is64BitWindows()
{
#if defined(_WIN64)
	return TRUE;  // 64-bit programs run only on Win64
#elif defined(_WIN32)
	// 32-bit programs run on both 32-bit and 64-bit Windows
	// so must sniff
	BOOL f64 = FALSE;
	return IsWow64Process(GetCurrentProcess(), &f64) && f64;
#else
	return FALSE; // Win64 does not support Win16
#endif
}

BOOL GetSubKey(LPCTSTR pszKeysPath, int nTillKeyNumber, CString* strRetKeyPath)
{

	if ((pszKeysPath == NULL) || (nTillKeyNumber == 0) || (strRetKeyPath == NULL))
		return FALSE;

	CString strKeysPath = pszKeysPath;
	int curPos = 0;
	CString resToken;
	resToken = strKeysPath.Tokenize(TEXT("\\"), curPos);

	int nCount = 0;
	while (resToken != TEXT(""))
	{

		nCount++;
		if (nCount == nTillKeyNumber)
			return TRUE;
		else
		{
			*strRetKeyPath += _T('\\');
			*strRetKeyPath += resToken;
			resToken = strKeysPath.Tokenize(TEXT("\\"), curPos); // Get next token:
		}
	}
	return FALSE;
}
/*
void SortTree(CExtTreeCtrl* pTree, HTREEITEM hItem)
{
	if (pTree->ItemHasChildren(hItem) != 0)
		pTree->SortChildren(hItem);

	HTREEITEM hchild = pTree->GetNextItem(hItem, TVGN_CHILD);
	for (; hchild != NULL; )
	{
		SortTree(pTree, hchild);
		hchild = pTree->GetNextSiblingItem(hchild);
	}
}
*/

BOOL TakeFileOwnership(LPTSTR lpszOwnFile)
{

	BOOL bRetval = FALSE;

	HANDLE hToken = NULL;
	PSID pSIDAdmin = NULL;
	PSID pSIDEveryone = NULL;
	PACL pACL = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld =
		SECURITY_WORLD_SID_AUTHORITY;
	SID_IDENTIFIER_AUTHORITY SIDAuthNT = SECURITY_NT_AUTHORITY;
	const int NUM_ACES = 2;
	EXPLICIT_ACCESS ea[NUM_ACES];
	DWORD dwRes;

	// Specify the DACL to use.
	// Create a SID for the Everyone group.
	if (!AllocateAndInitializeSid(&SIDAuthWorld, 1,
		SECURITY_WORLD_RID,
		0,
		0, 0, 0, 0, 0, 0,
		&pSIDEveryone))
	{
		printf("AllocateAndInitializeSid (Everyone) error %u\n",
			GetLastError());
		goto Cleanup;
	}

	// Create a SID for the BUILTIN\Administrators group.
	if (!AllocateAndInitializeSid(&SIDAuthNT, 2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&pSIDAdmin))
	{
		printf("AllocateAndInitializeSid (Admin) error %u\n",
			GetLastError());
		goto Cleanup;
	}

	ZeroMemory(&ea, NUM_ACES * sizeof(EXPLICIT_ACCESS));

	// Set read access for Everyone.
	ea[0].grfAccessPermissions = GENERIC_READ;
	ea[0].grfAccessMode = SET_ACCESS;
	ea[0].grfInheritance = NO_INHERITANCE;
	ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea[0].Trustee.ptstrName = (LPTSTR)pSIDEveryone;

	// Set full control for Administrators.
	ea[1].grfAccessPermissions = GENERIC_ALL;
	ea[1].grfAccessMode = SET_ACCESS;
	//ea[1].grfInheritance = NO_INHERITANCE;
	ea[1].grfInheritance = SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea[1].Trustee.ptstrName = (LPTSTR)pSIDAdmin;

	if (ERROR_SUCCESS != SetEntriesInAcl(NUM_ACES,
		ea,
		NULL,
		&pACL))
	{
		printf("Failed SetEntriesInAcl\n");
		goto Cleanup;
	}

	// Try to modify the object's DACL.
	dwRes = SetNamedSecurityInfo(
		lpszOwnFile,                 // name of the object
		SE_FILE_OBJECT,              // type of object
		DACL_SECURITY_INFORMATION,   // change only the object's DACL
		NULL, NULL,                  // do not change owner or group
		pACL,                        // DACL specified
		NULL);                       // do not change SACL

	if (ERROR_SUCCESS == dwRes)
	{
		printf("Successfully changed DACL\n");
		bRetval = TRUE;
		// No more processing needed.
		goto Cleanup;
	}
	if (dwRes != ERROR_ACCESS_DENIED)
	{
		printf("First SetNamedSecurityInfo call failed: %u\n",
			dwRes);
		goto Cleanup;
	}

	// If the preceding call failed because access was denied, 
	// enable the SE_TAKE_OWNERSHIP_NAME privilege, create a SID for 
	// the Administrators group, take ownership of the object, and 
	// disable the privilege. Then try again to set the object's DACL.

	// Open a handle to the access token for the calling process.
	if (!OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES,
		&hToken))
	{
		printf("OpenProcessToken failed: %u\n", GetLastError());
		goto Cleanup;
	}

	// Enable the SE_TAKE_OWNERSHIP_NAME privilege.
	if (!SetPrivilege(hToken, SE_TAKE_OWNERSHIP_NAME, TRUE))
	{
		printf("You must be logged on as Administrator.\n");
		goto Cleanup;
	}

	// Set the owner in the object's security descriptor.
	dwRes = SetNamedSecurityInfo(
		lpszOwnFile,                 // name of the object
		SE_FILE_OBJECT,              // type of object
		OWNER_SECURITY_INFORMATION,  // change only the object's owner
		pSIDAdmin,                   // SID of Administrator group
		NULL,
		NULL,
		NULL);

	if (dwRes != ERROR_SUCCESS)
	{
		printf("Could not set owner. Error: %u\n", dwRes);
		goto Cleanup;
	}

	// Disable the SE_TAKE_OWNERSHIP_NAME privilege.
	if (!SetPrivilege(hToken, SE_TAKE_OWNERSHIP_NAME, FALSE))
	{
		printf("Failed SetPrivilege call unexpectedly.\n");
		goto Cleanup;
	}

	// Try again to modify the object's DACL,
	// now that we are the owner.
	dwRes = SetNamedSecurityInfo(
		lpszOwnFile,                 // name of the object
		SE_FILE_OBJECT,              // type of object
		DACL_SECURITY_INFORMATION,   // change only the object's DACL
		NULL, NULL,                  // do not change owner or group
		pACL,                        // DACL specified
		NULL);                       // do not change SACL

	if (dwRes == ERROR_SUCCESS)
	{
		printf("Successfully changed DACL\n");
		bRetval = TRUE;
	}
	else
	{
		printf("Second SetNamedSecurityInfo call failed: %u\n",
			dwRes);
	}

Cleanup:

	if (pSIDAdmin)
		FreeSid(pSIDAdmin);

	if (pSIDEveryone)
		FreeSid(pSIDEveryone);

	if (pACL)
		LocalFree(pACL);

	if (hToken)
		CloseHandle(hToken);

	return bRetval;

}

BOOL VSRemoveDirectory(LPTSTR pszParentPath)
{
	if (_tcslen(pszParentPath) == 0)
		return FALSE;

	if (RemoveDirectory(pszParentPath) != FALSE)
		return TRUE;
	else
	{
		DWORD dw = GetLastError();
		if (dw == ERROR_ACCESS_DENIED)
		{
			//::AfxMessageBox(TEXT("VSRemoveDirectory Access Denied so takeownership"));
			TakeFileOwnership(pszParentPath);
			if (RemoveDirectory(pszParentPath) != FALSE)
				return TRUE;
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}

	}
}


int VSSHFileOperation(LPSHFILEOPSTRUCT pSFO)
{
	if (pSFO == NULL)
		return 1;


	int nRetValue = SHFileOperation(pSFO);
	if (nRetValue == 0)
		return 0;
	else
	{
		TCHAR szRestrictedFolderName[4096] = { 0 };
		memset(szRestrictedFolderName, 0, 4096);
		SHGetFolderPath(NULL, CSIDL_WINDOWS, NULL, 0, szRestrictedFolderName);
		CString strCSIDL_WINDOWS = szRestrictedFolderName;

		if (strCSIDL_WINDOWS.IsEmpty() == FALSE)
		{
			if (StrStrI(pSFO->pFrom, strCSIDL_WINDOWS))
				return nRetValue;
		}


		if ((nRetValue == ERROR_ACCESS_DENIED) || (nRetValue == ERROR_CALL_NOT_IMPLEMENTED))
		{
			//AfxMessageBox(TEXT("VSSHFileOperation Access Denied so takeownership"));
			CString strFileName = pSFO->pFrom;
			CString strOrigName = pSFO->pFrom;
			TakeFileOwnership(strFileName.GetBuffer());
			strFileName.ReleaseBuffer();
			pSFO->pFrom = strOrigName;
			int nRetValue2 = SHFileOperation(pSFO);
			if (nRetValue2 == 0)
				return 0;
			else
				return nRetValue2;
		}
		else
			return nRetValue;
	}

}
//nVSType = 0 - Information message; nVSType = 1 - Warning message
int VSMessageBox(HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType, int nVSType)
{
	int nRetVal = -1;
	if (::IsWindow(hWnd) == FALSE)
		hWnd = ::AfxGetMainWnd()->GetSafeHwnd();

	DWORD dwVSTypeDisabled = 0;
	switch (nVSType)
	{
	case 0:
		if (VSGetSettings(CAT_GENERAL, VAL_SKIPINFO, dwVSTypeDisabled) == FALSE)
			VSSetSettings(CAT_GENERAL, VAL_SKIPINFO, dwVSTypeDisabled);
		break;
	case 1:
		if (VSGetSettings(CAT_GENERAL, VAL_SKIPWARN, dwVSTypeDisabled) == FALSE)
			VSSetSettings(CAT_GENERAL, VAL_SKIPWARN, dwVSTypeDisabled);
		break;
	}

	//if (dwVSTypeDisabled == FALSE)
//		nRetVal = ::MessageBox(hWnd, lpText, lpCaption, uType);

	return nRetVal;

}

// This code is copied from Microsoft's website (http://msdn2.microsoft.com/en-us/library/Aa387705.aspx)
///////////////////////////////////////////////////////////////////////
HRESULT ModifyPrivilege(
	IN LPCTSTR szPrivilege,
	IN BOOL fEnable)
{
	HRESULT hr = S_OK;
	TOKEN_PRIVILEGES NewState;
	LUID             luid;
	HANDLE hToken = NULL;

	// Open the process token for this process.
	if (!OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
		&hToken))
	{
		printf("Failed OpenProcessToken\n");
		return ERROR_FUNCTION_FAILED;
	}

	// Get the local unique ID for the privilege.
	if (!LookupPrivilegeValue(NULL,
		szPrivilege,
		&luid))
	{
		CloseHandle(hToken);
		printf("Failed LookupPrivilegeValue\n");
		return ERROR_FUNCTION_FAILED;
	}

	// Assign values to the TOKEN_PRIVILEGE structure.
	NewState.PrivilegeCount = 1;
	NewState.Privileges[0].Luid = luid;
	NewState.Privileges[0].Attributes = (fEnable ? SE_PRIVILEGE_ENABLED : 0);

	// Adjust the token privilege.
	if (!AdjustTokenPrivileges(hToken,
		FALSE,
		&NewState,
		0,
		NULL,
		NULL))
	{
		printf("Failed AdjustTokenPrivileges\n");
		hr = ERROR_FUNCTION_FAILED;
	}

	// Close the handle.
	::CloseHandle(hToken);

	return hr;
}

BOOL DevicePathToDosPath(const TCHAR* pszDevicePath, CString* pstrDosPath)
{
	pstrDosPath->Empty();

	TCHAR szDriveStrings[MAX_PATH] = _T("");
	if (!::GetLogicalDriveStrings(MAX_PATH, szDriveStrings))
		return FALSE;

	// Drive strings are stored as a set of null terminated strings, with an
	// extra null after the last string. Each drive string is of the form "C:\".
	// We convert it to the form "C:", which is the format expected by
	// ::QueryDosDevice().
	TCHAR szDriveColon[3] = _T(" :");
	for (const TCHAR* pszNextDriveLetter = szDriveStrings; *pszNextDriveLetter; pszNextDriveLetter += _tcslen(pszNextDriveLetter) + 1)
	{
		// Dos device of the form "C:".
		*szDriveColon = *pszNextDriveLetter;
		TCHAR szDeviceName[MAX_PATH] = _T("");

		if (!::QueryDosDevice(szDriveColon, szDeviceName, MAX_PATH))
		{
			continue;
		}

		size_t name_length = _tcslen(szDeviceName);
		if (_tcsnicmp(pszDevicePath, szDeviceName, name_length) == 0)
		{
			// Construct DOS path.
			pstrDosPath->Format(_T("%s%s"), szDriveColon, pszDevicePath + name_length);
			return TRUE;
		}
	}

	return FALSE;
}
/*
//Folder should always end with a back slash!
BOOL CreateFullRegBackup(LPCTSTR pszBackupFolderPath, CEvent* pEventNTU_SkipBU)
{
	// Acquire the SE_BACKUP_NAME privilege
	HRESULT hr = ModifyPrivilege(SE_BACKUP_NAME, TRUE);

	if (SUCCEEDED(hr))
	{
		HKEY hKey;
		LONG lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Control\\hivelist"), 0, KEY_READ, &hKey);
		if (lResult == ERROR_SUCCESS)
		{
			// Get the class name and the value count.
			DWORD cValues, cMaxValueNameLen, cMaxValueLen;
			RegQueryInfoKey(
				hKey,                    // key handle 
				NULL,                    // buffer for class name 
				NULL,					 // size of class string 
				NULL,                    // reserved 
				NULL,					 // number of subkeys 
				NULL,					 // longest subkey size 
				NULL,		             // longest class string 
				&cValues,	             // number of values for this key 
				&cMaxValueNameLen,		 // longest value name 
				&cMaxValueLen,           // longest value data 
				NULL,					 // security descriptor 
				NULL);				     // last write time 

										 // Enumerate values
			if (cValues)
			{
				//Create the BAT file
				DWORD dwWritten;

				if (::PathFileExists(pszBackupFolderPath) == FALSE)
					VSCreateDirectory(pszBackupFolderPath);

				CString strBatFile = pszBackupFolderPath;
				strBatFile.Append(_T("Restore.dat"));
				HANDLE hFile = CreateFile(strBatFile,       // name of the write
					GENERIC_WRITE,          // open for writing
					0,                      // do not share
					NULL,                   // default security
					CREATE_ALWAYS,          // overwrite existing
					FILE_ATTRIBUTE_NORMAL,  // normal file
					NULL);                  // no attr. template

				if (hFile == INVALID_HANDLE_VALUE)
					return FALSE;


				TCHAR* achValue = new TCHAR[cMaxValueNameLen + 1];
				TCHAR* szData = new TCHAR[cMaxValueLen];
				LSTATUS retCode;
				for (DWORD i = 0; i<cValues; i++)
				{
					DWORD cchValue = cMaxValueNameLen + 1, dwDataLen = cMaxValueLen, dwType;
					achValue[0] = '\0';
					szData[0] = '\0';
					retCode = RegEnumValue(hKey,
						i,
						achValue,
						&cchValue,
						NULL,
						&dwType,
						(LPBYTE)szData,
						&dwDataLen);

					if (pEventNTU_SkipBU != NULL)
					{
						if (::WaitForSingleObject(*pEventNTU_SkipBU, 0) == WAIT_OBJECT_0)
						{
							if (achValue)
								delete[] achValue;
							if (szData)
								delete[] szData;

							//Close BAT file
							::CloseHandle(hFile);
							::RegCloseKey(hKey);
							ModifyPrivilege(SE_BACKUP_NAME, FALSE);
							return FALSE;
						}
					}

					if (retCode == ERROR_SUCCESS)
					{
						if (dwDataLen < 3)
							continue;

						if (dwType == REG_SZ)
						{
							CString strValName = achValue;
							HKEY hKeySave;

							if (StrStrI(achValue, _T("\\REGISTRY\\MACHINE\\")) != NULL)
							{
								CString strMachine = _T("\\REGISTRY\\MACHINE\\");
								int nLenght = strMachine.GetLength();
								strValName.Delete(0, nLenght);

								DWORD dwRes = RegCreateKeyEx(HKEY_LOCAL_MACHINE, strValName, 0, NULL, REG_OPTION_BACKUP_RESTORE, NULL, NULL, &hKeySave, NULL);
								if (ERROR_SUCCESS != dwRes)
								{
									///*if(achValue)
									//	delete [] achValue;
									//if(szData)
									//	delete [] szData;

									////////////CString strError,strErrorDigit;
									////////////GetErrorText(dwRes,strError);
									////////////strErrorDigit.Format(TEXT("%d"),dwRes);
									////////////strError+= TEXT("-");
									////////////strError+= strValName;
									////////////strError+= TEXT("-");
									////////////strError+= strErrorDigit;
									////////////AfxMessageBox(strError);

									//Close BAT file
									//////::CloseHandle(hFile);
									//////::RegCloseKey(hKey);
									//////ModifyPrivilege(SE_BACKUP_NAME, FALSE);
									//////return FALSE;
									continue;
								}
							}

							if (StrStrI(achValue, _T("\\REGISTRY\\USER\\")) != NULL)
							{
								CString strUsers = _T("\\REGISTRY\\USER\\");
								int nLenght = strUsers.GetLength();
								strValName.Delete(0, nLenght);

								DWORD dwRes = RegCreateKeyEx(HKEY_USERS, strValName, 0, NULL, REG_OPTION_BACKUP_RESTORE, NULL, NULL, &hKeySave, NULL);
								if (ERROR_SUCCESS != dwRes)
								{
									/////*if(achValue)
									////	delete [] achValue;
									////if(szData)
									////	delete [] szData;

									//////////CString strError,strErrorDigit;
									//////////GetErrorText(dwRes,strError);
									//////////strErrorDigit.Format(TEXT("%d"),dwRes);
									//////////strError+= TEXT("-");
									//////////strError+= strValName;
									//////////strError+= TEXT("-");
									//////////strError+= strErrorDigit;
									//////////AfxMessageBox(strError);

									continue;

									//Close BAT file
									//////////::CloseHandle(hFile);
									//////////::RegCloseKey(hKey);
									//////////ModifyPrivilege(SE_BACKUP_NAME, FALSE);
									//////////return FALSE;
								}
							}

							CString strFileName = strValName;
							strFileName.Insert(0, pszBackupFolderPath);
							::DeleteFile(strFileName);

							DWORD dwErr = ::RegSaveKeyEx(hKeySave, strFileName, NULL, REG_NO_COMPRESSION);
							////////////if(ERROR_SUCCESS != dwErr)
							////////{
							//////////CString strError,strErrorDigit;
							//////////GetErrorText(dwErr,strError);
							//////////strErrorDigit.Format(TEXT("%d"),dwErr);
							//////////strError+= TEXT("-");
							//////////strError+= strFileName;
							//////////strError+= TEXT("-");
							//////////strError+= strErrorDigit;
							//////////AfxMessageBox(strError);
							/////////}
							::RegCloseKey(hKeySave);



							//Get the real hive location 
							CString strRes;
							DevicePathToDosPath(szData, &strRes);

							int nBatFileLineLen = (strRes.GetLength() * 2) + 20;
							char* pszBat = new char[nBatFileLineLen];

							//Convert to ANSI
							CT2CA szRes(strRes);
							CT2CA szValName(strValName);

							//Make a BAK file of current reg hives
							StringCchPrintfA(pszBat, nBatFileLineLen, "copy \"%s\" \"%s.bak\" \r\n", (LPSTR)szRes, (LPSTR)szRes);
							WriteFile(hFile, pszBat, strlen(pszBat), &dwWritten, NULL);

							if (pszBat)
								delete[] pszBat;

							nBatFileLineLen = (strRes.GetLength() + strValName.GetLength()) + 20;
							pszBat = new char[nBatFileLineLen];

							//Copy our backup to its original destination
							StringCchPrintfA(pszBat, nBatFileLineLen, "copy \"%s\" \"%s\" \r\n", (LPSTR)szValName, (LPSTR)szRes);
							WriteFile(hFile, pszBat, strlen(pszBat), &dwWritten, NULL);

							if (pszBat)
								delete[] pszBat;
						}
					}
				}

				if (achValue)
					delete[] achValue;

				if (szData)
					delete[] szData;

				//Close BAT file
				::CloseHandle(hFile);

			}
			::RegCloseKey(hKey);
		}

		ModifyPrivilege(SE_BACKUP_NAME, FALSE);
	}

	return TRUE;
}
*/
///The result do not have \ at the end
void GetLocalAppData(CString& strPath)
{

#ifdef PORTABLE
	CString strPortPath;
	CString strCustomDataDir;
	if (VSGetSettings(CAT_PORTABILITY, VAL_PORT_CUSTOMDATADIR, strCustomDataDir) == FALSE)
	{
		CString strResPath;
		strPortPath = GetEXEPath();

		RemoveLastItemFromPath(strPortPath, strResPath); // remove from path the x86 or x64 folder
		strPortPath = strResPath;

		strPortPath += TEXT("Data\\");
	}
	else
	{
		strPortPath = strCustomDataDir;

		if (strPortPath.IsEmpty())
		{
			CString strDataFolder, strResPath;

			strDataFolder = GetEXEPath();
			RemoveLastItemFromPath(strDataFolder, strResPath); // remove from path the x86 or x64 folder
			strDataFolder = strResPath;
			strDataFolder += TEXT("Data\\");

			strPortPath = strDataFolder;
			VSSetSettings(CAT_PORTABILITY, VAL_PORT_CUSTOMDATADIR, strPortPath);

		}
		else
		{
			if (VS_IsExistingDirectory(strPortPath) == FALSE)
			{
				if (VSCreateDirectory(strPortPath) == FALSE)
				{
					CString strDataFolder, strResPath;

					strDataFolder = GetEXEPath();
					RemoveLastItemFromPath(strDataFolder, strResPath); // remove from path the x86 or x64 folder
					strDataFolder = strResPath;
					strDataFolder += TEXT("Data\\");
					strPortPath = strDataFolder;
					VSSetSettings(CAT_PORTABILITY, VAL_PORT_CUSTOMDATADIR, strPortPath);
				}
			}
			if (strPortPath.Right(1) != TEXT("\\"))
				strPortPath += TEXT("\\");

		}


	}

	DWORD dwUserName = 4096, dwCompName = 4096;

	TCHAR szCompName[4096] = { 0 };
	memset(szCompName, 0, dwCompName);
	GetComputerName(szCompName, &dwCompName);
	strPortPath += szCompName;

	strPortPath += TEXT("_");

	TCHAR szUser[4096] = { 0 };
	memset(szUser, 0, dwUserName);
	GetUserName(szUser, &dwUserName);

	strPortPath += szUser;
	strPortPath += TEXT("_");

	CString strSID = GetCurrentUserSID();
	strSID.Delete(0, 9);
	//strSID.Replace(

	strPortPath += strSID;

	if (::PathFileExists(strPortPath) == FALSE)
		VSCreateDirectory(strPortPath);

	strPath = strPortPath;

#else
	TCHAR szAppDataPath[4096] = { 0 };
	memset(szAppDataPath, 0, 4096);
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, SHGFP_TYPE_CURRENT, szAppDataPath)))
	{
		strPath = szAppDataPath;
		strPath += TEXT("\\VS Revo Group\\Revo Uninstaller Pro");
		if (::PathFileExists(strPath) == FALSE)
			VSCreateDirectory(strPath);

	}
	else
		strPath = GetEXEPath();
#endif
}

BOOL VSCreateDirectory(LPCTSTR szPath)
{
#if 1 //change reversed for now, SHCreateDirectoryEx is limited to MAX_PATH
	DWORD dwAttr = GetFileAttributes(CString(_T("\\\\?\\")) + szPath);
	if ((dwAttr != INVALID_FILE_ATTRIBUTES) &&
		(dwAttr & FILE_ATTRIBUTE_DIRECTORY))
		return TRUE;

	CString strDir(szPath);
	for (int nStart = 3; ; )
	{
		int nSlash = strDir.Find(_T('\\'), nStart);
		if ((nSlash == -1) || (nSlash == strDir.GetLength() - 1))
			break;
		CString strSubDir(strDir.Left(nSlash + 1));
		dwAttr = GetFileAttributes(CString(_T("\\\\?\\")) + strSubDir);

		if ((dwAttr == INVALID_FILE_ATTRIBUTES) ||
			!(dwAttr & FILE_ATTRIBUTE_DIRECTORY))
			if (!CreateDirectory(CString(_T("\\\\?\\")) + strSubDir, NULL))
				return FALSE;
		nStart = nSlash + 1;
	}
	return CreateDirectory(CString(_T("\\\\?\\")) + szPath, NULL);
#else
	//change made because previous method did not allow for navigation (\..\) in path
	if (SHCreateDirectoryEx(NULL, szPath, NULL) == ERROR_SUCCESS)
		return TRUE;
	else
		return FALSE;
#endif
}

BOOL IsValidTime(LPSYSTEMTIME stUTC)
{
	if (((stUTC->wYear >= 1970 && stUTC->wYear <= 3000) &&
		(stUTC->wMonth >= 1 && stUTC->wMonth <= 12) &&
		(stUTC->wDay >= 1 && stUTC->wDay <= 31) &&
		(stUTC->wHour >= 0 && stUTC->wHour <= 23) &&
		(stUTC->wMinute >= 0 && stUTC->wMinute <= 59) &&
		(stUTC->wSecond >= 0 && stUTC->wSecond <= 59)) == FALSE)
		return FALSE;
	else
		return TRUE;
}

BOOL VS_IsExistingDirectory(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(CString(_T("\\\\?\\")) + szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}
BOOL GetRegKeyForOpen(LPCTSTR pszFullKey, CString& strKeyToOpen)
{
	CString resToken, strSource(pszFullKey), strDel;

	if (strSource.IsEmpty())
		return FALSE;

	CString strFirst;
	strFirst += strSource[0];
	if (strFirst.Compare(_T("\\")) == 0)
		strSource.Delete(0, 1);

	int curPos = 0, nCounter = 0;

	resToken = strSource.Tokenize(_T("\\"), curPos);
	while (resToken != "")
	{
		strDel += resToken + TEXT("\\");
		nCounter++;
		if (nCounter == 2)
			break;

		resToken = strSource.Tokenize(_T("\\"), curPos);
	}

	strSource.Delete(0, strDel.GetLength());
	strKeyToOpen = strSource;
	if (strKeyToOpen.IsEmpty())
		return FALSE;
	else
		return TRUE;
}

void  GetNumberofSubFolders(CString& strFolderName, int& nCounter, CString& strSubfolder)
{
	if (strFolderName.IsEmpty())
		return;

	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo = { 0 };
	DWORD64    dwSize = 0;

	CString strFileFilter = strFolderName;
	strFileFilter += TEXT("\\*.*");

	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{

			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
					{
						//Do nothing for "." and ".." folders
						continue;
					}
					else
					{
						nCounter++;
						strSubfolder = strFolderName + TEXT("\\") + fileinfo.cFileName;
					}
				}
				else
				{
				}
			}

		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);
}

BOOL RemoveLastItemFromPath(CString& strPath, CString& strResPath)
{
	if (strPath.IsEmpty())
		return FALSE;

	CString strTemp = strPath;

	int nLen = strTemp.GetLength();

	if (strTemp.GetAt(nLen - 1) == _T('\\'))
	{
		strTemp.Delete(nLen - 1);
		nLen -= 1;
	}
	int nSlash = strTemp.ReverseFind(_T('\\'));
	if (nSlash >0)
	{
		strTemp.Delete(nSlash + 1, nLen - nSlash - 1);
		strResPath = strTemp;
		return TRUE;
	}
	else
		return FALSE;
}

BOOL VSFileExists(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(CString(_T("\\\\?\\")) + szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

BOOL VSPathExists(LPCTSTR szPath)
{
	//DWORD dwAttrib = GetFileAttributes(CString( _T("\\\\?\\") ) + szPath);

	// return dwAttrib != INVALID_FILE_ATTRIBUTES; 
	if (_taccess_s(szPath, 0) == 0)
		return TRUE;
	else
		return FALSE;
}

void GetErrorText(DWORD dwErr, CString& strErrorText)
{
	LPTSTR lpMsgBuf;

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dwErr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	strErrorText = TEXT("Error - ");
	strErrorText += lpMsgBuf;
}

void GetValueTypeText(DWORD dwType, CString& strType)
{
	switch (dwType)
	{
	case 0:
		strType = TEXT("REG_NONE");
		break;
	case 1:
		strType = TEXT("REG_SZ");
		break;
	case 2:
		strType = TEXT("REG_EXPAND_SZ");
		break;
	case 3:
		strType = TEXT("REG_BINARY");
		break;
	case 4:
		strType = TEXT("REG_DWORD");
		break;
	case 5:
		strType = TEXT("REG_DWORD_BIG_ENDIAN");
		break;
	case 6:
		strType = TEXT("REG_LINK");
		break;
	case 7:
		strType = TEXT("REG_MULTI_SZ");
		break;
	case 8:
		strType = TEXT("REG_RESOURCE_LIST");
		break;
	case 9:
		strType = TEXT("REG_FULL_RESOURCE_DESCRIPTOR");
		break;
	case 10:
		strType = TEXT("REG_RESOURCE_REQUIREMENTS_LIST");
		break;
	case 11:
		strType = TEXT("REG_QWORD");
		break;
	}
}

void UpdateRegPathIfOld(CString& strNewPath)
{
	CString strLogUrl;
	if (VSGetSettings(CAT_GENERAL, VAL_LDBURL, strLogUrl) == FALSE)
	{
		strLogUrl = TEXT("http://www.revouninstallerpro.com/db/ilogs/");
		VSSetSettings(CAT_GENERAL, VAL_LDBURL, strLogUrl);
	}

	if (strLogUrl.CompareNoCase(strNewPath) != 0)
		VSSetSettings(CAT_GENERAL, VAL_LDBURL, strNewPath);

}

void FolderRemoveBadSymbols(CString& strFolder, BOOL bAllowPath)
{
	if (bAllowPath == FALSE)
		strFolder.Replace(TEXT("\\"), TEXT(""));
	strFolder.Replace(TEXT("/"), TEXT(""));
	strFolder.Replace(TEXT(":"), TEXT(""));
	strFolder.Replace(TEXT("|"), TEXT(""));
	strFolder.Replace(TEXT("?"), TEXT(""));
	strFolder.Replace(TEXT("<"), TEXT(""));
	strFolder.Replace(TEXT(">"), TEXT(""));
	strFolder.Replace(TEXT("\""), TEXT(""));
	strFolder.Replace(TEXT("*"), TEXT(""));
	strFolder.Replace(TEXT("["), TEXT(""));
	strFolder.Replace(TEXT("]"), TEXT(""));
}

BOOL Is_Win81()
{
	OSVERSIONINFOEX osvi;
	DWORDLONG dwlConditionMask = 0;
	int op = VER_EQUAL;

	// Initialize the OSVERSIONINFOEX structure.

	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	osvi.dwMajorVersion = 6;
	osvi.dwMinorVersion = 3;
	osvi.wServicePackMajor = 0;
	osvi.wServicePackMinor = 0;

	// Initialize the condition mask.

	VER_SET_CONDITION(dwlConditionMask, VER_MAJORVERSION, op);
	VER_SET_CONDITION(dwlConditionMask, VER_MINORVERSION, op);
	VER_SET_CONDITION(dwlConditionMask, VER_SERVICEPACKMAJOR, op);
	VER_SET_CONDITION(dwlConditionMask, VER_SERVICEPACKMINOR, op);

	// Perform the test.

	return VerifyVersionInfo(
		&osvi,
		VER_MAJORVERSION | VER_MINORVERSION |
		VER_SERVICEPACKMAJOR | VER_SERVICEPACKMINOR,
		dwlConditionMask);
}

BOOL GetOsVersion(RTL_OSVERSIONINFOEXW* pk_OsVer)
{
	typedef LONG(WINAPI* tRtlGetVersion)(RTL_OSVERSIONINFOEXW*);

	memset(pk_OsVer, 0, sizeof(RTL_OSVERSIONINFOEXW));
	pk_OsVer->dwOSVersionInfoSize = sizeof(RTL_OSVERSIONINFOEXW);

	HMODULE h_NtDll = GetModuleHandleW(L"ntdll.dll");
	tRtlGetVersion f_RtlGetVersion = (tRtlGetVersion)GetProcAddress(h_NtDll, "RtlGetVersion");

	if (!f_RtlGetVersion)
		return FALSE; // This will never happen (all processes load ntdll.dll)

	LONG Status = f_RtlGetVersion(pk_OsVer);
	return Status == 0; // STATUS_SUCCESS;
}

BOOL Is_Win10()
{
	RTL_OSVERSIONINFOEXW rtl_osvi;
	ZeroMemory(&rtl_osvi, sizeof(RTL_OSVERSIONINFOEXW));
	GetOsVersion(&rtl_osvi);

	if (rtl_osvi.dwMajorVersion == 10 && rtl_osvi.dwMinorVersion == 0)
		return TRUE;
	else
		return FALSE;

}

BOOL IsTextURL(CString& strText)
{
	if (strText.IsEmpty())
		return FALSE;

	CString strwww, strhttp, strhttps;
	strwww = strText.Left(4);
	strhttp = strText.Left(5);
	strhttps = strText.Left(6);

	if ((strwww.CompareNoCase(TEXT("www.")) == 0) || (strhttp.CompareNoCase(TEXT("http:")) == 0) || (strhttps.CompareNoCase(TEXT("https:")) == 0))
	{
		if (strwww.CompareNoCase(TEXT("www.")) == 0)
		{
			CString strFullURL = TEXT("http://");
			strFullURL += strText;
			strText = strFullURL;

		}
		return TRUE;
	}
	else
		return FALSE;
}

BOOL RemoveLastBrackets(CString& strText)
{
	BOOL bResult = FALSE;
	int f = -1, l = -1;
	strText.TrimRight(TEXT(" "));

	int nLength = strText.GetLength();
	f = strText.ReverseFind(TEXT('('));
	l = strText.ReverseFind(TEXT(')'));

	if ((f != -1) && (l != -1) && (l > f) && (l == nLength - 1))
	{
		strText.Delete(f, l - f + 1);
		strText.TrimRight(TEXT(" "));
		bResult = TRUE;
	}

	return bResult;
}



/**
* This method call windows Function: <b>SHGetFolderPath</b>.
*
* @result The method saves AppData folder location to the variable
* <b>appDataLocation</b>.
*
* @return TRUE (1) on success
* @return FALSE (0) on failure
*/
BOOL FindAppDataFolder(CString& p_appDataFolderLocation, ReferenceKnownFolder p_referenceKnownFolder)
{

	//KNOWNFOLDERID folderId;
	LONG folderCsidl;


	switch (p_referenceKnownFolder)
	{
	case LocalAppData:

		//	folderId = FOLDERID_LocalAppData;
		//folderCsidl = CSIDL_APPDATA;
		folderCsidl = CSIDL_LOCAL_APPDATA;

		break;

	case RoamingAppData:

		//	folderId = FOLDERID_RoamingAppData;
		//folderCsidl = CSIDL_LOCAL_APPDATA;
		folderCsidl = CSIDL_APPDATA;

		break;

	default:
		return FALSE;
		break;
	}

	//RevoUninstallerPro::Library::WindowsOperation::GetWindowsVersion(windowsMajorVersion, windowsMinorVersion);

	//if (windowsMajorVersion < 6)
	//{
	// In this case the client is using Windows XP or earlier version.

	TCHAR szPath[4096];

	if (SUCCEEDED(SHGetFolderPath(NULL, folderCsidl, NULL, 0, szPath)))
	{
		p_appDataFolderLocation = szPath;

		return TRUE;
	}
	//}
	////else
	////{
	////	// In this case the client is using Windows Vista or later version.

	////	PWSTR localAppData = (PWSTR)malloc(128);

	////	if (SUCCEEDED(SHGetKnownFolderPath(folderId, 0, NULL, &localAppData)))
	////	{
	////		p_appDataFolderLocation = localAppData;

	////		return TRUE;
	////	}
	////}

	return FALSE;
}

void FindAllFolders(CString& strFolderName, CStringArray& parrLogsHDDItems)
{
	if (strFolderName.IsEmpty())
		return;


	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo = { 0 };
	DWORD64    dwSize = 0;

	CString strFileFilter = strFolderName;
	CString	strFullPath = strFolderName;
	strFileFilter += TEXT("\\");
	strFileFilter += TEXT("*.*");


	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
					{
						continue; //Do nothing for "." and ".." folders
					}
					else
					{
						CString strTemFullPath = strFullPath;
						strTemFullPath += TEXT("\\");
						strTemFullPath += fileinfo.cFileName;

						parrLogsHDDItems.Add(strTemFullPath);
					}
				}
				else // file
				{
					// Do noting for file

					continue;
				}
			}
		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);
}

//int DeleteFolder(CString folderToDelete)
//{
//	if (folderToDelete == L"")
//		return 1;
//
//	CString removeLocation;
//
//	removeLocation = folderToDelete;
//
//	removeLocation.TrimRight(L'\\');
//
//	SHFILEOPSTRUCT shFileOp;
//	BOOL temp = -1;
//
//	shFileOp.hwnd = NULL;
//	shFileOp.wFunc = FO_DELETE;
//	folderToDelete += TEXT('\0');
//	shFileOp.pFrom = folderToDelete;
//	shFileOp.pTo = NULL;
//	shFileOp.fFlags = FOF_NOCONFIRMATION | FOF_ALLOWUNDO | FOF_SILENT;
//	shFileOp.fAnyOperationsAborted = temp;
//	// shFileOp.hNameMappings = ;
//	// shFileOp.lpszProgressTitle = ;
//
//	int nRetValue = SHFileOperation(&shFileOp);
//	if (nRetValue == 0)
//	{
//		return 0;
//	}
//	else
//	{
//		TCHAR szRestrictedFolderName[4096] = { 0 };
//		memset(szRestrictedFolderName, 0, 4096);
//		SHGetFolderPath(NULL, CSIDL_WINDOWS, NULL, 0, szRestrictedFolderName);
//		CString strCSIDL_WINDOWS = szRestrictedFolderName;
//
//		if (strCSIDL_WINDOWS.IsEmpty() == FALSE)
//		{
//			if (StrStrI(shFileOp.pFrom, strCSIDL_WINDOWS))
//				return nRetValue;
//		}
//
//		if ((nRetValue == ERROR_ACCESS_DENIED) || (nRetValue == ERROR_CALL_NOT_IMPLEMENTED))
//		{
//			//AfxMessageBox(TEXT("VSSHFileOperation Access Denied so takeownership"));
//			CString strFileName = shFileOp.pFrom;
//			CString strOrigName = shFileOp.pFrom;
//			/*RevoUninstallerPro::Library::FileOperation::*/TakeFileOwnership(strFileName.GetBuffer());
//			strFileName.ReleaseBuffer();
//			shFileOp.pFrom = strOrigName;
//			int nRetValue2 = SHFileOperation(&shFileOp);
//
//			if (nRetValue2 == 0)
//			{
//				return 0;
//			}
//			else
//			{
//				if (::MoveFileEx(strFileName, NULL, MOVEFILE_DELAY_UNTIL_REBOOT))
//				{
//					return 0;
//				}
//			}
//
//			return nRetValue2;
//		}
//		else
//		{
//			return nRetValue;
//		}
//	}
//
//}


int RevoRemoveFile(CString fileToDelete)
{
	int result;
	SHFILEOPSTRUCT shFileOp;
	BOOL temp = -1;

	shFileOp.hwnd = NULL;
	shFileOp.wFunc = FO_DELETE;
	fileToDelete += TEXT('\0');
	shFileOp.pFrom = fileToDelete;
	shFileOp.pTo = NULL;
	shFileOp.fFlags = FOF_FILESONLY | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT;
	shFileOp.fAnyOperationsAborted = temp;
	// shFileOp.hNameMappings = ;
	// shFileOp.lpszProgressTitle = ;

	result = SHFileOperation(&shFileOp);

	return result;
}

int RevoRemoveFileOwnership(CString fileToDelete)
{
	int result;
	SHFILEOPSTRUCT shFileOp;
	BOOL temp = -1;

	TakeFileOwnership(fileToDelete.GetBuffer());
	fileToDelete.ReleaseBuffer();

	shFileOp.hwnd = NULL;
	shFileOp.wFunc = FO_DELETE;
	fileToDelete += TEXT('\0');
	shFileOp.pFrom = fileToDelete;
	shFileOp.pTo = NULL;
	shFileOp.fFlags = FOF_FILESONLY | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT;
	shFileOp.fAnyOperationsAborted = temp;
	// shFileOp.hNameMappings = ;
	// shFileOp.lpszProgressTitle = ;

	result = SHFileOperation(&shFileOp);

	return result;
}

void FindAllFiles(CString& strFolderName, CStringArray& parrLogsHDDItems)
{
	if (strFolderName.IsEmpty())
		return;


	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo = { 0 };
	DWORD64    dwSize = 0;

	CString strFileFilter = strFolderName;
	CString	strFullPath = strFolderName;
	strFileFilter += TEXT("\\");
	strFileFilter += TEXT("*.*");


	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{
				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					// Do noting for Folder

					continue;
				}
				else // file
				{
					CString strTemFullPath = strFullPath;
					strTemFullPath += TEXT("\\");
					strTemFullPath += fileinfo.cFileName;

					parrLogsHDDItems.Add(strTemFullPath);
				}
			}
		} while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);
}

void WriteBOM(LPCTSTR szFileName)
{
	HANDLE hFile;         // the file handle
	DWORD dwWritten = 0;


	hFile = CreateFile(szFileName,
		GENERIC_WRITE,
		0,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		return;
	}

	_TCHAR bom = (_TCHAR)0xFEFF;
	WriteFile(hFile, &bom, sizeof(_TCHAR), &dwWritten, NULL);
	CloseHandle(hFile);
}

BOOL IsStringOnlyDigits(CString& strText)
{
	int nstrLen = strText.GetLength();
	for (int nCount = 0; nCount<nstrLen; nCount++)
	{
		if ((IsCharAlpha(strText.GetAt(nCount))) && (strText.GetAt(nCount) != TEXT(' ')))
		{
			return FALSE;
		}
	}
	return TRUE;
}

void GetStringtoFirstPoint(CString& strOrigVer, CString& strVerToPoint)
{
	if (strOrigVer.IsEmpty())
		return;

	int nPointPos = strOrigVer.Find(TEXT("."));
	if (nPointPos != -1)
		strVerToPoint = strOrigVer.Left(nPointPos);
	else
		strVerToPoint = strOrigVer;
}

void GetArrayofKeys(CString strAllKeys, CStringArray& arrAllKeys)
{
	CString resToken, strSource(strAllKeys);

	strAllKeys.Trim(TEXT(" "));

	int curPos = 0;

	resToken = strSource.Tokenize(_T(";"), curPos);
	while (resToken != "")
	{
		resToken.Trim(TEXT(" "));
		if (resToken.GetLength() >2)
			arrAllKeys.Add(resToken);
		resToken = strSource.Tokenize(_T(";"), curPos);
	}
}
using namespace std;
#include <sstream>
//Simply expand enviroment strings
void EnvExpand(fs::path& Path)
{
	wchar_t Final[4096] = { 0 };
	ExpandEnvironmentStringsW(Path.c_str(), Final, 4096);
	Path = Final;
}

vector<wstring> SplitW(const wstring& s, wchar_t delimiter)
{
	vector<wstring> tokens;
	wstring token;
	wstringstream tokenStream(s);
	while (getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
	return tokens;
}


void FilterWString(std::wstring& Str, const std::wstring& Filter)
{
	for (auto& i : Filter)
		Str.erase(remove(Str.begin(), Str.end(), i), Str.end());
}

