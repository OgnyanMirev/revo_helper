// IniData.h: interface for the CIniData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INIDATA_H__6D692BB1_3C53_42C6_AF46_79788D5AB53D__INCLUDED_)
#define AFX_INIDATA_H__6D692BB1_3C53_42C6_AF46_79788D5AB53D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Ini.h"

struct stIniItem
   {
      TCHAR* szKey;
      TCHAR* szValue;
   };


class CIniData  
{
public:
			CIniData();
	virtual ~CIniData();
	BOOL	AddItem(TCHAR* szKey, TCHAR* szValue);
	void	ReleaseMemory();
	void	GetValue(int nKey,CString* strRes) const;
	void	GetValue(LPCTSTR szKey,CString* strRes);
	BOOL	InitData(CString& sAppPath);
protected:
	BOOL LoadIniFile(TCHAR* szFileName);


private:
	CPtrList	m_listIniData;
	CIni		m_iniFile;
	CString		m_sAppPath;
	

};

#endif // !defined(AFX_INIDATA_H__6D692BB1_3C53_42C6_AF46_79788D5AB53D__INCLUDED_)
