#include "stdafx.h"
#include "VSTrackFinder.h"
#include <strsafe.h> 
#include <msi.h>
#include <intshcut.h>
#include <Sddl.h>
#include "Registry.h"
#include "Utils.h"
int gstatCall = 0;
#define DENIEDNAMES 257

static CString garrDeniedStrings[DENIEDNAMES] =
{
	_T(".net"),
	_T("account"),
	_T("accounts"),
	_T("admin"),
	_T("administrator"),
	_T("adobe"),
	_T("advanced"),
	_T("antivirus"),
	_T("anti-virus"),
	_T("appdata"),
	_T("application"),
	_T("applications"),
	_T("arpcache"),
	_T("audio"),
	_T("backup"),
	_T("backups"),
	_T("browse"),
	_T("browser"),
	_T("boot"),
	_T("button"),
	_T("buttons"),
	_T("cache"),
	_T("cached"),
	_T("center"),
	_T("class"),
	_T("classes"),
	_T("clean"),
	_T("cleaner"),
	_T("client"),
	_T("close"),
	_T("command"),
	_T("commands"),
	_T("common"),
	_T("compact"),
	_T("company"),
	_T("component"),
	_T("components"),
	_T("config"),
	_T("configuration"),
	_T("configure"),
	_T("connection"),
	_T("connections"),
	_T("console"),
	_T("contacts"),
	_T("control panel"),
	_T("control"),
	_T("controls"),
	_T("core"),
	_T("data"),
	_T("date"),
	_T("debug"),
	_T("default"),
	_T("defaults"),
	_T("design"),
	_T("designs"),
	_T("desktop"),
	_T("desktops"),
	_T("device"),
	_T("devices"),
	_T("dictionarie"),
	_T("dictionaries"),
	_T("disk"),
	_T("disks"),
	_T("document"),
	_T("documents"),
	_T("domain"),
	_T("domains"),
	_T("downloads"),
	_T("driver"),
	_T("drivers"),
	_T("drivers32"),
	_T("edit"),
	_T("edition"),
	_T("emulator"),
	_T("enterprise"),
	_T("execute"),
	_T("exit"),
	_T("explore"),
	_T("explorer"),
	_T("express"),
	_T("extension"),
	_T("extensions"),
	_T("favorites"),
	_T("file"),
	_T("files"),
	_T("firewall"),
	_T("folder"),
	_T("folders"),
	_T("font"),
	_T("fonts"),
	_T("free"),
	_T("freeware"),
	_T("full"),
	_T("game"),
	_T("games"),
	_T("gateway"),
	_T("gateways"),
	_T("group"),
	_T("hardware"),
	_T("help"),
	_T("host"),
	_T("hosting"),
	_T("hotfix"),
	_T("icon"),
	_T("icons"),
	_T("info"),
	_T("install"),
	_T("installer"),
	_T("installers"),
	_T("interactive"),
	_T("internet"),
	_T("internet explorer"),
	_T("license"),
	_T("light"),
	_T("links"),
	_T("log"),
	_T("logs"),
	_T("manager"),
	_T("media"),
	_T("menu"),
	_T("message"),
	_T("messenger"),
	_T("microsoft"),
	_T("mobile"),
	_T("module"),
	_T("modules"),
	_T("monitor"),
	_T("monitors"),
	_T("multimedia"),
	_T("my music"),
	_T("my pictures"),
	_T("my videos"),
	_T("network"),
	_T("networks"),
	_T("object"),
	_T("objects"),
	_T("office"),
	_T("open"),
	_T("option"),
	_T("options"),
	_T("panel"),
	_T("panels"),
	_T("personal"),
	_T("phone"),
	_T("phones"),
	_T("platform"),
	_T("platforms"),
	_T("player"),
	_T("players"),
	_T("plugin"),
	_T("plugins"),
	_T("plug-ins"),
	_T("point"),
	_T("policies"),
	_T("policy"),
	_T("portable"),
	_T("presentation"),
	_T("print"),
	_T("printers"),
	_T("process"),
	_T("processor"),
	_T("product"),
	_T("products"),
	_T("professional"),
	_T("profile"),
	_T("profiles"),
	_T("program"),
	_T("program files (x86)"),
	_T("program files"),
	_T("programdata"),
	_T("programs"),
	_T("properties"),
	_T("reader"),
	_T("register"),
	_T("registration"),
	_T("registry"),
	_T("reinstall"),
	_T("remote"),
	_T("remove"),
	_T("report"),
	_T("reports"),
	_T("resource"),
	_T("resources"),
	_T("restore"),
	_T("route"),
	_T("router"),
	_T("routers"),
	_T("run"),
	_T("sample"),
	_T("samples"),
	_T("saved games"),
	_T("script"),
	_T("scripts"),
	_T("searches"),
	_T("secure"),
	_T("secured"),
	_T("security"),
	_T("server"),
	_T("servers"),
	_T("service"),
	_T("services"),
	_T("setting"),
	_T("settings"),
	_T("setup"),
	_T("share"),
	_T("shared"),
	_T("shell"),
	_T("soft"),
	_T("software"),
	_T("skin"),
	_T("skins"),
	_T("sound"),
	_T("sounds"),
	_T("source"),
	_T("sources"),
	_T("storage"),
	_T("store"),
	_T("studio"),
	_T("style"),
	_T("styles"),
	_T("support"),
	_T("system"),
	_T("system32"),
	_T("systems"),
	_T("technologies"),
	_T("technology"),
	_T("template"),
	_T("templates"),
	_T("time"),
	_T("tool"),
	_T("tools"),
	_T("toolbar"),
	_T("toolbars"),
	_T("trial"),
	_T("unin"),
	_T("uninst"),
	_T("uninstall"),
	_T("uninstaller"),
	_T("update"),
	_T("updates"),
	_T("user"),
	_T("users"),
	_T("util"),
	_T("utilities"),
	_T("utility"),
	_T("value"),
	_T("values"),
	_T("version"),
	_T("versions"),
	_T("video"),
	_T("videos"),
	_T("window"),
	_T("windows"),
	_T("winlogon"),
	_T("wizard"),
	_T("wizards"),
	_T("workspace"),
};



CVSTrackFinder::CVSTrackFinder(LPCTSTR pszAppName, LPCTSTR pszInstallLocation, eUninstallMode uMode, BOOL b64bit)
	: m_pJobsDoneEvent(new CEvent(FALSE, TRUE))
	, m_pTree(new VSRegTree)
	, m_thScanThred(NULL)
	, m_bMSI(TRUE)
	, m_b64Bit(b64bit)
	, m_uninModerateMode(uMode)

{
	m_parrFUProgNames = NULL;
	m_parrFUPaths = NULL;
}

CVSTrackFinder::CVSTrackFinder()
	: m_pJobsDoneEvent(new CEvent(FALSE, TRUE))
	, m_pTree(new VSRegTree)
	, m_thScanThred(NULL)
	, m_bMSI(TRUE)
	, m_b64Bit(FALSE)
	, m_uninModerateMode(uninModerateMode)

{
	::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	InitCSIDLs();
	CoUninitialize();

	m_bSpecialProgram = FALSE;
	m_bPopularBrowser = FALSE;
	m_parrFUProgNames = NULL;
	m_parrFUPaths = NULL;
	m_bUninstallingWinApp = FALSE;
	m_bUninstallingEdgeExt = FALSE;
}


CVSTrackFinder::~CVSTrackFinder()
{
	StopScan();

	delete m_pJobsDoneEvent;

	if (m_pTree)
		delete m_pTree;

}

//Start scanning start menu for more info  
//after built-in, found missing element folders are saved for search
void CVSTrackFinder::StartMenuScan(BOOL bBeforeBuiltIn)
{
	//CDex - Open Source Digital Audio CD Extractor
	if ((m_strAppName.Find(TEXT("CDex")) != -1) || (m_strAppName.Find(TEXT("EaseUS Todo Backup Free")) != -1))
		return;

	if (bBeforeBuiltIn)
	{
		::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

		//Scan Start Menu before uninstall
		TCHAR szStartPrograms[4096] = { 0 };
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAMS, NULL, 0, szStartPrograms)))
			GetStartMenuItems(szStartPrograms, &m_arrStartBefore);

		TCHAR szAllStartPrograms[4096] = { 0 };
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_PROGRAMS, NULL, 0, szAllStartPrograms)))
			GetStartMenuItems(szAllStartPrograms, &m_arrAllStartBefore);

		CoUninitialize();
	}
	else
	{
		::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

		//Scan Start Menu after uninstall
		TCHAR szStartPrograms[4096] = { 0 };
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAMS, NULL, 0, szStartPrograms)))
			GetStartMenuItems(szStartPrograms, &m_arrStartAfter);

		TCHAR szAllStartPrograms[4096] = { 0 };
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_PROGRAMS, NULL, 0, szAllStartPrograms)))
			GetStartMenuItems(szAllStartPrograms, &m_arrAllStartAfter);

		ExtractMissingFromStartMenu();

		CoUninitialize();
	}

}

//Start scanning and wait for the returned event
CEvent* CVSTrackFinder::StartScan(eUninstallMode eMode)
{
	//Just in case for double calls
	StopScan();

	m_uninModerateMode = eMode;

	m_pJobsDoneEvent->ResetEvent();

	m_thScanThred = AfxBeginThread((AFX_THREADPROC)ScanThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_thScanThred->m_bAutoDelete = false;
	m_thScanThred->ResumeThread();

	return m_pJobsDoneEvent;
}

//Scan thread
void CVSTrackFinder::Scan()
{

	if ((m_strAppName.CompareNoCase(TEXT("Google Chrome")) == 0) || (m_strAppName.Find(TEXT("Mozilla Firefox")) != -1)
		|| (m_strAppName.Find(TEXT("Opera")) != -1))
		m_bPopularBrowser = TRUE;

	if (m_bUninstallingWinApp)
		UninstallingWinApp();
	else
	{
		switch (m_uninModerateMode)
		{
			case uninSafeMode:
				UninstallModeSafe();
				break;
			case uninModerateMode:
				UninstallModeModerate();
				break;
			case uninAdvancedMode:
				UninstallModeAdvanced();
				break;
		}
		ClearRegTreeFromApps();
	}
}

void CVSTrackFinder::StopScan()
{
	m_pJobsDoneEvent->SetEvent();

	if (m_thScanThred)
	{
		::WaitForSingleObject(m_thScanThred->m_hThread, INFINITE);
		delete m_thScanThred;
		m_thScanThred = NULL;
	}
}

//If the string contains a string from list
BOOL CVSTrackFinder::IsListInString(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase)
{
	if (arrStringList == NULL)
		return FALSE;

	if (pszString == NULL)
		return FALSE;

	for (int i = 0; i < arrStringList->GetCount(); i++)
	{
		if (bMatchCase)
		{
			if (StrCmpI(pszString, arrStringList->GetAt(i)) == 0)
				return TRUE;
		}
		else
		{
			if (StrStrI(pszString, arrStringList->GetAt(i)) != NULL)
				return TRUE;
		}
	}
	return FALSE;
}
//If the list contains the string - exactly or partially
BOOL CVSTrackFinder::IsStringInList(LPCTSTR pszString, CStringArray* arrStringList, BOOL bMatchCase)
{
	if (arrStringList == NULL)
		return FALSE;

	if (pszString == NULL)
		return FALSE;

	for (int i = 0; i < arrStringList->GetCount(); i++)
	{
		if (bMatchCase)
		{
			if (StrCmpI(pszString, arrStringList->GetAt(i)) == 0)
				return TRUE;
		}
		else
		{
			if (StrStrI(arrStringList->GetAt(i), pszString))
				return TRUE;
		}
	}
	return FALSE;
}

//nDepthLevels = -1 for all levels
//nBreakIfEqualToCount = -1 as much as it finds (no limit)
BOOL CVSTrackFinder::SearchRegistry(HKEY hKey, LPCTSTR pszKeyName, LPTSTR pszFullRegKeyName, CStringArray& arrToSearchFor, VSRegTree* pResultTree, DWORD* pdwFoundCount,
									CStringArray* parrIgnoredKeys, BOOL b64Bit, int nDepthLevels, int nBreakIfEqualToCount,
									BOOL bKeys, BOOL bValues, BOOL bData, BOOL bMatchCase)
{
	if (pResultTree == NULL)
		return FALSE;

	if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		return FALSE;

	TCHAR    achKey[MAX_PATH];   // buffer for subkey name
	DWORD    cbName;                   // size of name string 
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
	DWORD    cchClassName = MAX_PATH;  // size of class string 
	DWORD    cSubKeys = 0;               // number of subkeys 
	DWORD    cbMaxSubKey;              // longest subkey size 
	DWORD    cchMaxClass;              // longest class string 
	DWORD    cValues;              // number of values for key 
	DWORD    cchMaxValueName;      // longest value name 
	DWORD    cbMaxValueData;       // longest value data 
	DWORD    cbSecurityDescriptor; // size of security descriptor 
	FILETIME ftLastWriteTime;      // last write time 

	DWORD i, retCode;

	TCHAR* pachValue = NULL;
	DWORD  cchValue = 0;

	BYTE* pbtData = NULL;
	DWORD  dwDataType = 0;
	DWORD  dwDataSize = 0;

	REGSAM regPermissions;
	eRegWOW6432Type eKeyWOW;
	if (b64Bit)
	{
		eKeyWOW = Key64Bit;
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	}
	else
	{
		eKeyWOW = Key32Bit;
		regPermissions = KEY_READ | KEY_WOW64_32KEY;
	}

	LONG lResult = RegOpenKeyEx(hKey, pszKeyName, 0, regPermissions, &hKey);

	if (lResult != ERROR_SUCCESS)
	{
		if (lResult == ERROR_FILE_NOT_FOUND)
		{
			//printf("Key not found.\n");
			return FALSE;
		}
		else
		{
			//printf("Error opening key.\n");
			return FALSE;
		}
	}

	// Get the class name and the value count. 
	retCode = RegQueryInfoKey(
		hKey,                    // key handle 
		achClass,                // buffer for class name 
		&cchClassName,           // size of class string 
		NULL,                    // reserved 
		&cSubKeys,               // number of subkeys 
		&cbMaxSubKey,            // longest subkey size 
		&cchMaxClass,            // longest class string 
		&cValues,                // number of values for this key 
		&cchMaxValueName,            // longest value name 
		&cbMaxValueData,         // longest value data 
		&cbSecurityDescriptor,   // security descriptor 
		&ftLastWriteTime);       // last write time 

//if values or data are in interest 
	if ((bValues) || (bData))
	{
		// Enumerate the key values. 
		if (cValues)
		{
			pbtData = new BYTE[cbMaxValueData];
			pachValue = new TCHAR[cchMaxValueName + sizeof(TCHAR)];

			for (i = 0, retCode = ERROR_SUCCESS; i < cValues; i++)
			{
				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				{
					delete[] pachValue;
					delete[] pbtData;

					return FALSE;
				}

				//Found items count matches required count, so stop searching
				if (nBreakIfEqualToCount == 0)
				{
					delete[] pachValue;
					delete[] pbtData;

					return TRUE;
				}

				dwDataSize = cbMaxValueData;
				cchValue = cchMaxValueName + sizeof(TCHAR);

				retCode = RegEnumValue(hKey, i,
									   pachValue,
									   &cchValue,
									   NULL,
									   &dwDataType,
									   pbtData,
									   &dwDataSize);

				if (retCode == ERROR_SUCCESS)
				{
					//if we are interested in value names
					if (bValues)
					{
						if (IsListInString(&arrToSearchFor, pachValue, bMatchCase))
						{
							pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, pbtData, dwDataSize, 0, NULL, 0, eKeyWOW);

							//decrease count because we found a match
							nBreakIfEqualToCount--;

							//item found
							(*pdwFoundCount)++;
							continue;
						}
					}
					//if we are interested in value data
					if (bData)
					{
						TCHAR* pszData = (TCHAR*)pbtData;
						DWORD dwSzDataSize = dwDataSize;
						switch (dwDataType)
						{
							case REG_SZ:
							case REG_EXPAND_SZ:
							{
								if (pszData[(dwDataSize / sizeof(TCHAR)) - 1] != _T('\0'))
								{
									dwSzDataSize += sizeof(TCHAR);
									pszData = new TCHAR[dwSzDataSize];
									ZeroMemory(pszData, dwSzDataSize);
									CopyMemory(pszData, pbtData, dwDataSize);
								}
							}
							break;
							case REG_MULTI_SZ:
							{
								if (pszData[(dwDataSize / sizeof(TCHAR)) - 1] != _T('\0'))
								{
									//double null terminated
									dwSzDataSize += sizeof(TCHAR);
									dwSzDataSize += sizeof(TCHAR);

									pszData = new TCHAR[dwSzDataSize];
									ZeroMemory(pszData, dwSzDataSize);
									CopyMemory(pszData, pbtData, dwDataSize);
								}
							}
							break;
						}

						//if reg value type is REG_SZ
						if (dwDataType == REG_SZ)
						{
							if (IsListInString(&arrToSearchFor, pszData, bMatchCase))
							{
								pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, (BYTE*)pszData, dwSzDataSize, 0, NULL, 0, eKeyWOW);

								//decrease count because we found a match
								nBreakIfEqualToCount--;

								//item found
								(*pdwFoundCount)++;
							}
						}

						//if reg value type is REG_EXPAND_SZ
						if (dwDataType == REG_EXPAND_SZ)
						{
							TCHAR* pszBuff = new TCHAR[512];
							memset(pszBuff, 0, 512);

							DWORD dwRes = ExpandEnvironmentStrings(pszData, pszBuff, 512);
							if (dwRes > 512)
							{
								delete[] pszBuff;
								pszBuff = new TCHAR[dwRes + 2];
								memset(pszBuff, 0, dwRes + 2);
								ExpandEnvironmentStrings(pszData, pszBuff, dwRes);
							}

							if (IsListInString(&arrToSearchFor, pszBuff, bMatchCase))
							{
								pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, (BYTE*)pszData, dwSzDataSize, 0, NULL, 0, eKeyWOW);

								//decrease count because we found a match
								nBreakIfEqualToCount--;

								//item found
								(*pdwFoundCount)++;
							}
							delete[] pszBuff;
						}

						//if reg value type is REG_MULTI_SZ
						if (dwDataType == REG_MULTI_SZ)
						{
							CStringArray arrRes;
							ConvertMultySzToArray(pszData, dwSzDataSize, arrRes);
							for (int i = 0; i < arrRes.GetCount(); i++)
							{
								if (IsListInString(&arrToSearchFor, arrRes.GetAt(i), bMatchCase))
								{
									pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, (BYTE*)pszData, dwSzDataSize, 0, NULL, 0, eKeyWOW);

									//decrease count because we found a match
									nBreakIfEqualToCount--;

									//item found
									(*pdwFoundCount)++;
								}
							}
						}
					}
				}
			}

			delete[] pachValue;
			delete[] pbtData;
		}
	}

	// Enumerate the subkeys, until RegEnumKeyEx fails.   
	if (cSubKeys)
	{
		//if 0 then no more levels to go
		if (nDepthLevels != 0)
		{
			for (i = 0; i < cSubKeys; i++)
			{
				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

				//Found items count matches required count, so stop searching
				if (nBreakIfEqualToCount == 0)
					break;

				cbName = MAX_PATH;
				retCode = RegEnumKeyEx(hKey, i,
									   achKey,
									   &cbName,
									   NULL,
									   NULL,
									   NULL,
									   &ftLastWriteTime);
				if (retCode == ERROR_SUCCESS)
				{
					//ignore specific keys and their subkeys 
					if (parrIgnoredKeys)
						if (IsStringInList(achKey, parrIgnoredKeys))
							continue;

					//add current key name to full path
					StrCat(pszFullRegKeyName, _T("\\"));
					StrCat(pszFullRegKeyName, achKey);

					int nDepth = nDepthLevels;

					BOOL bSearch = TRUE;
					//if we are interested in key names
					if (bKeys)
					{
						if (IsListInString(&arrToSearchFor, achKey, bMatchCase))
						{
							//a key name matches search criteria 
							pResultTree->CreateKey(pszFullRegKeyName, eKeyWOW);
							//so stop recursive search on this node
							bSearch = FALSE;

							//decrease count because we found a match
							nBreakIfEqualToCount--;

							//item found
							(*pdwFoundCount)++;
						}
					}
					if (bSearch)
					{
						nDepthLevels--;
						SearchRegistry(hKey, achKey, pszFullRegKeyName, arrToSearchFor, pResultTree, pdwFoundCount, parrIgnoredKeys, b64Bit, nDepthLevels, nBreakIfEqualToCount, bKeys, bValues, bData, bMatchCase);
						nDepthLevels++;
					}
					//remove the last key name because we are back from recurse
					RemoveLastKeyName(pszFullRegKeyName);
				}
			}
		}
	}

	RegCloseKey(hKey);

	return TRUE;
}

//nDepthLevels = -1 for all levels
//nBreakIfEqualToCount = -1 as much as it finds (no limit)
//bIgnoreParentVals - default FALSE, if TRUE ignore the current key data and values from comparison
BOOL CVSTrackFinder::SearchRegistry(HKEY hKey, LPCTSTR pszKeyName, LPTSTR pszFullRegKeyName, CStringArray& arrToSearchFor, RegDataList* listFoundData, DWORD* pdwFoundCount,
									CStringArray* parrIgnoredKeys, BOOL b64Bit, int nDepthLevels, int nBreakIfEqualToCount,
									BOOL bKeys, BOOL bValues, BOOL bData, BOOL bMatchCase, BOOL bIgnoreParentVals)
{
	if (listFoundData == NULL)
		return FALSE;

	if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		return FALSE;

	TCHAR    achKey[MAX_PATH];   // buffer for subkey name
	DWORD    cbName;                   // size of name string 
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
	DWORD    cchClassName = MAX_PATH;  // size of class string 
	DWORD    cSubKeys = 0;               // number of subkeys 
	DWORD    cbMaxSubKey;              // longest subkey size 
	DWORD    cchMaxClass;              // longest class string 
	DWORD    cValues;              // number of values for key 
	DWORD    cchMaxValueName;      // longest value name 
	DWORD    cbMaxValueData;       // longest value data 
	DWORD    cbSecurityDescriptor; // size of security descriptor 
	FILETIME ftLastWriteTime;      // last write time 

	DWORD i, retCode;

	TCHAR* pachValue = NULL;
	DWORD  cchValue = 0;

	BYTE* pbtData = NULL;
	DWORD  dwDataType = 0;
	DWORD  dwDataSize = 0;

	REGSAM regPermissions;
	eRegWOW6432Type eKeyWOW;
	if (b64Bit)
	{
		eKeyWOW = Key64Bit;
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	}
	else
	{
		eKeyWOW = Key32Bit;
		regPermissions = KEY_READ | KEY_WOW64_32KEY;
	}

	LONG lResult = RegOpenKeyEx(hKey, pszKeyName, 0, regPermissions, &hKey);

	if (lResult != ERROR_SUCCESS)
	{
		if (lResult == ERROR_FILE_NOT_FOUND)
		{
			//printf("Key not found.\n");
			return FALSE;
		}
		else
		{
			//printf("Error opening key.\n");
			return FALSE;
		}
	}

	// Get the class name and the value count. 
	retCode = RegQueryInfoKey(
		hKey,                    // key handle 
		achClass,                // buffer for class name 
		&cchClassName,           // size of class string 
		NULL,                    // reserved 
		&cSubKeys,               // number of subkeys 
		&cbMaxSubKey,            // longest subkey size 
		&cchMaxClass,            // longest class string 
		&cValues,                // number of values for this key 
		&cchMaxValueName,            // longest value name 
		&cbMaxValueData,         // longest value data 
		&cbSecurityDescriptor,   // security descriptor 
		&ftLastWriteTime);       // last write time 

//if values or data are in interest 
	if ((bValues) || (bData))
	{
		// Enumerate the key values. 
		if (cValues)
		{
			if (cbMaxValueData)
				pbtData = new BYTE[cbMaxValueData];
			else
				pbtData = NULL;

			pachValue = new TCHAR[cchMaxValueName + sizeof(TCHAR)];
			ZeroMemory(pachValue, cchMaxValueName);

			for (i = 0, retCode = ERROR_SUCCESS; i < cValues; i++)
			{
				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				{
					delete[] pachValue;
					delete[] pbtData;

					return FALSE;
				}

				//Found items count matches required count, so stop searching
				if (nBreakIfEqualToCount == 0)
				{
					delete[] pachValue;
					delete[] pbtData;

					return TRUE;
				}

				dwDataSize = cbMaxValueData;
				cchValue = cchMaxValueName + sizeof(TCHAR);

				retCode = RegEnumValue(hKey, i,
									   pachValue,
									   &cchValue,
									   NULL,
									   &dwDataType,
									   pbtData,
									   &dwDataSize);

				if (retCode == ERROR_SUCCESS)
				{
					//if we are interested in value names
					if (bValues)
					{
						if (IsListInString(&arrToSearchFor, pachValue, bMatchCase))
						{
							//pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, pbtData, dwDataSize, 0, NULL, 0);

							FoundRegData* regData = new FoundRegData;

							regData->strKeyName = pszKeyName;
							regData->strFullKeyPath = pszFullRegKeyName;
							regData->strValueName = pachValue;
							regData->dwType = dwDataType;
							regData->pbtData = new BYTE[dwDataSize];
							CopyMemory(regData->pbtData, pbtData, dwDataSize);
							regData->dwDataSize = dwDataSize;

							listFoundData->AddTail(regData);

							//decrease count because we found a match
							nBreakIfEqualToCount--;

							//item found
							(*pdwFoundCount)++;

							if (bData)
								continue;

						}
					}
					//if we are interested in value data
					if (bData)
					{
						if (dwDataSize == 0)
							continue;

						//if reg value type is REG_SZ
						if (dwDataType == REG_SZ)
						{
							TCHAR* pszData = (TCHAR*)pbtData;
							DWORD dwDataSzLen = dwDataSize;
							if (pszData[(dwDataSize / sizeof(TCHAR)) - 1] != _T('\0'))
							{
								dwDataSzLen = dwDataSize + sizeof(TCHAR);

								pszData = new TCHAR[dwDataSzLen];
								memset(pszData, 0, dwDataSzLen);
								CopyMemory(pszData, pbtData, dwDataSize);
							}

							if (IsListInString(&arrToSearchFor, pszData, bMatchCase) && (!bIgnoreParentVals))
							{
								//pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, pbtData, dwDataSize, 0, NULL, 0);
								FoundRegData* regData = new FoundRegData;

								regData->strKeyName = pszKeyName;
								regData->strFullKeyPath = pszFullRegKeyName;
								regData->strValueName = pachValue;
								regData->dwType = dwDataType;
								regData->pbtData = new BYTE[dwDataSzLen];
								CopyMemory(regData->pbtData, pszData, dwDataSzLen);
								regData->dwDataSize = dwDataSzLen;

								listFoundData->AddTail(regData);

								//decrease count because we found a match
								nBreakIfEqualToCount--;

								//item found
								(*pdwFoundCount)++;
							}

							//We've zero terminated the string that is why the size is not equal
							if (dwDataSzLen != dwDataSize)
								delete[] pszData;
						}

						//if reg value type is REG_EXPAND_SZ
						if (dwDataType == REG_EXPAND_SZ)
						{
							TCHAR* pszData = (TCHAR*)pbtData;
							DWORD dwDataSzLen = dwDataSize;
							if (pszData[(dwDataSize / sizeof(TCHAR)) - 1] != _T('\0'))
							{
								dwDataSzLen = dwDataSize + sizeof(TCHAR);

								pszData = new TCHAR[dwDataSzLen];
								memset(pszData, 0, dwDataSzLen);
								CopyMemory(pszData, pbtData, dwDataSize);
							}

							//double the size of the buffer because it will be expanded
							TCHAR* pszBuff = new TCHAR[2 * dwDataSzLen];
							memset(pszBuff, 0, 2 * dwDataSzLen);

							DWORD dwRes = ExpandEnvironmentStrings(pszData, pszBuff, 2 * dwDataSzLen);
							if (dwRes > (2 * dwDataSzLen))
							{
								delete[] pszBuff;
								pszBuff = new TCHAR[dwRes + 2];
								memset(pszBuff, 0, dwRes + 2);
								ExpandEnvironmentStrings(pszData, pszBuff, dwRes);
							}

							if (IsListInString(&arrToSearchFor, pszBuff, bMatchCase) && (!bIgnoreParentVals))
							{
								//pResultTree->SetKeyVal(pszFullRegKeyName, pachValue, dwDataType, pbtData, dwDataSize, 0, NULL, 0);
								FoundRegData* regData = new FoundRegData;

								regData->strKeyName = pszKeyName;
								regData->strFullKeyPath = pszFullRegKeyName;
								regData->strValueName = pachValue;
								regData->dwType = dwDataType;
								regData->pbtData = new BYTE[dwDataSzLen];
								CopyMemory(regData->pbtData, pszData, dwDataSzLen);
								regData->dwDataSize = dwDataSzLen;

								listFoundData->AddTail(regData);

								//decrease count because we found a match
								nBreakIfEqualToCount--;

								//item found
								(*pdwFoundCount)++;
							}

							delete[] pszBuff;

							//We've zero terminated the string that is why the size is not equal
							if (dwDataSzLen != dwDataSize)
								delete[] pszData;
						}

						//if reg value type is REG_MULTI_SZ
						if (dwDataType == REG_MULTI_SZ)
						{

							TCHAR* pszData = (TCHAR*)pbtData;
							DWORD dwDataSzLen = dwDataSize;
							if (pszData[(dwDataSize / sizeof(TCHAR)) - 1] != _T('\0'))
							{
								//REG_MULTI_SZ strings should have two null-terminating characters
								dwDataSzLen = dwDataSize + sizeof(TCHAR) + sizeof(TCHAR);

								pszData = new TCHAR[dwDataSzLen];
								memset(pszData, 0, dwDataSzLen);
								CopyMemory(pszData, pbtData, dwDataSize);
							}

							CStringArray arrRes;
							ConvertMultySzToArray(pszData, dwDataSzLen, arrRes);
							for (int i = 0; i < arrRes.GetCount(); i++)
							{
								if (IsListInString(&arrToSearchFor, arrRes.GetAt(i), bMatchCase) && (!bIgnoreParentVals))
								{
									FoundRegData* regData = new FoundRegData;

									regData->strKeyName = pszKeyName;
									regData->strFullKeyPath = pszFullRegKeyName;
									regData->strValueName = pachValue;
									regData->dwType = dwDataType;
									regData->pbtData = new BYTE[dwDataSzLen];
									CopyMemory(regData->pbtData, pszData, dwDataSzLen);
									regData->dwDataSize = dwDataSzLen;

									listFoundData->AddTail(regData);

									//decrease count because we found a match
									nBreakIfEqualToCount--;

									//item found
									(*pdwFoundCount)++;
								}
							}

							//We've zero terminated the string that is why the size is not equal
							if (dwDataSzLen != dwDataSize)
								delete[] pszData;
						}
					}
				}
			}

			delete[] pachValue;

			if (pbtData)
				delete[] pbtData;
		}
	}

	// Enumerate the subkeys, until RegEnumKeyEx fails.   
	if (cSubKeys)
	{
		//if 0 then no more levels to go
		if (nDepthLevels != 0)
		{
			for (i = 0; i < cSubKeys; i++)
			{
				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

				//Found items count matches required count, so stop searching
				if (nBreakIfEqualToCount == 0)
					break;

				cbName = MAX_PATH;
				retCode = RegEnumKeyEx(hKey, i,
									   achKey,
									   &cbName,
									   NULL,
									   NULL,
									   NULL,
									   &ftLastWriteTime);
				if (retCode == ERROR_SUCCESS)
				{
					//ignore specific keys and their subkeys 
					if (parrIgnoredKeys)
						if (IsStringInList(achKey, parrIgnoredKeys))
							continue;

					//add current key name to full path
					StrCat(pszFullRegKeyName, _T("\\"));
					StrCat(pszFullRegKeyName, achKey);

					int nDepth = nDepthLevels;

					BOOL bSearch = TRUE;
					//if we are interested in key names
					if (bKeys)
					{
						if (IsListInString(&arrToSearchFor, achKey, bMatchCase))
						{
							//a key name matches search criteria 
							//pResultTree->CreateKey(pszFullRegKeyName);

							FoundRegData* regData = new FoundRegData;
							regData->strKeyName = pszKeyName;
							regData->strFullKeyPath = pszFullRegKeyName;
							regData->dwType = 0;
							listFoundData->AddTail(regData);

							//so stop recursive search on this node
							bSearch = FALSE;

							//decrease count because we found a match
							nBreakIfEqualToCount--;

							//item found
							(*pdwFoundCount)++;
						}
					}
					if (bSearch)
					{
						nDepthLevels--;
						SearchRegistry(hKey, achKey, pszFullRegKeyName, arrToSearchFor, listFoundData, pdwFoundCount, parrIgnoredKeys, b64Bit, nDepthLevels, nBreakIfEqualToCount, bKeys, bValues, bData, bMatchCase);
						nDepthLevels++;
					}
					//remove the last key name because we are back from recurse
					RemoveLastKeyName(pszFullRegKeyName);
				}
			}
		}
	}

	RegCloseKey(hKey);

	return TRUE;
}


//nDepthLevels = -1 for all levels
//nBreakIfEqualToCount = -1 as much as it finds (no limit)
BOOL CVSTrackFinder::SearchInRegistryPath(HKEY hKey, LPCTSTR pszKeyName, LPTSTR pszFullRegKeyName, LPTSTR pszSearchedRegPath, CStringArray& arrToSearchFor, VSRegTree* pResultTree, DWORD* pdwFoundCount, int nMatchCount,
										  CStringArray* parrIgnoredKeys, BOOL b64Bit, int nDepthLevels, int nBreakIfEqualToCount, BOOL bMatchCase)
{
	if (pResultTree == NULL)
		return FALSE;

	if (arrToSearchFor.GetCount() == 0)
		return FALSE;

	if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		return FALSE;

	TCHAR    achKey[MAX_PATH];   // buffer for subkey name
	DWORD    cbName;                   // size of name string 
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
	DWORD    cchClassName = MAX_PATH;  // size of class string 
	DWORD    cSubKeys = 0;               // number of subkeys 
	DWORD    cbMaxSubKey;              // longest subkey size 

	DWORD i, retCode;

	TCHAR* pachValue = NULL;
	DWORD  cchValue = 0;

	BYTE* pbtData = NULL;
	DWORD  dwDataType = 0;
	DWORD  dwDataSize = 0;

	REGSAM regPermissions;
	eRegWOW6432Type eKeyWOW;
	if (b64Bit)
	{
		eKeyWOW = Key64Bit;
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	}
	else
	{
		eKeyWOW = Key32Bit;
		regPermissions = KEY_READ | KEY_WOW64_32KEY;
	}

	LONG lResult = RegOpenKeyEx(hKey, pszKeyName, 0, regPermissions, &hKey);

	if (lResult != ERROR_SUCCESS)
	{
		if (lResult == ERROR_FILE_NOT_FOUND)
		{
			//printf("Key not found.\n");
			return FALSE;
		}
		else
		{
			//printf("Error opening key.\n");
			return FALSE;
		}
	}

	// Get the class name and the value count. 
	retCode = RegQueryInfoKey(
		hKey,                    // key handle 
		achClass,                // buffer for class name 
		&cchClassName,           // size of class string 
		NULL,                    // reserved 
		&cSubKeys,               // number of subkeys 
		&cbMaxSubKey,            // longest subkey size 
		NULL,		             // longest class string 
		NULL,	                 // number of values for this key 
		NULL,			         // longest value name 
		NULL,			         // longest value data 
		NULL,					 // security descriptor 
		NULL);				     // last write time 


// Enumerate the subkeys, until RegEnumKeyEx fails.   
	if (cSubKeys)
	{
		//if 0 then no more levels to go
		if (nDepthLevels != 0)
		{
			for (i = 0; i < cSubKeys; i++)
			{
				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

				//Found items count matches required count, so stop searching
				if (nBreakIfEqualToCount == 0)
					break;

				cbName = MAX_PATH;
				retCode = RegEnumKeyEx(hKey, i,
									   achKey,
									   &cbName,
									   NULL,
									   NULL,
									   NULL,
									   NULL);
				if (retCode == ERROR_SUCCESS)
				{
					//ignore specific keys and their subkeys 
					if (parrIgnoredKeys)
						if (IsStringInList(achKey, parrIgnoredKeys))
							continue;

					//add current key name to full path
					StrCat(pszSearchedRegPath, _T("\\"));
					StrCat(pszSearchedRegPath, achKey);

					//add current key name to full path
					StrCat(pszFullRegKeyName, _T("\\"));
					StrCat(pszFullRegKeyName, achKey);

					int nDepth = nDepthLevels;

					BOOL bFoundMatch = FALSE;
					//////////////////////////////////

					CStringArray arrTmpToSearchFor;
					arrTmpToSearchFor.Append(arrToSearchFor);

					int nCounter = 0;
					int curPos = 0;
					CString strSearchedRegPath = pszSearchedRegPath;

					//get each key name first
					CString strKeyName = strSearchedRegPath.Tokenize(TEXT("\\"), curPos);
					while (strKeyName != TEXT(""))
					{
						for (int i = 0; i < arrTmpToSearchFor.GetCount(); i++)
						{
							if (StrCmpI(strKeyName, arrTmpToSearchFor.GetAt(i)) == 0)
							{
								nCounter++;
								arrTmpToSearchFor.RemoveAt(i);

								//if all enough elements are found in reg path
								if (nCounter == nMatchCount)
									bFoundMatch = TRUE;

								break;
							}

						}

						if (bFoundMatch)
							break;

						strKeyName = strSearchedRegPath.Tokenize(TEXT("\\"), curPos); // Get next token:
					}

					//////////////////////////////////
					if (bFoundMatch == FALSE)
					{
						nDepthLevels--;
						SearchInRegistryPath(hKey, achKey, pszFullRegKeyName, pszSearchedRegPath, arrToSearchFor, pResultTree, pdwFoundCount, nMatchCount, parrIgnoredKeys, b64Bit, nDepthLevels, nBreakIfEqualToCount, bMatchCase);
						nDepthLevels++;
					}
					else
					{
						//a key name matches search criteria 
						pResultTree->CreateKey(pszFullRegKeyName, eKeyWOW);

						//decrease count because we found a match
						nBreakIfEqualToCount--;

						//item found
						(*pdwFoundCount)++;
					}
					//remove the last key name because we are back from recurse
					RemoveLastKeyName(pszFullRegKeyName);

					//remove the last key name because we are back from recurse
					RemoveLastKeyName(pszSearchedRegPath);
				}
			}
		}
	}

	RegCloseKey(hKey);

	return TRUE;
}

int CVSTrackFinder::AddSearchPattern(CStringArray& arrPattern)
{
	int nAddedCount = 0;
	for (int i = 0; i < arrPattern.GetCount(); i++)
		nAddedCount += AddSearchPattern(m_arrSearchPattern.GetAt(i));

	return nAddedCount;
}

int CVSTrackFinder::AddSearchPattern(LPCTSTR pszPattern)
{
	int nAddedCount = 0;
	if (IsPatternAllowedForSearch(pszPattern))
	{
		if (IsStringInList(pszPattern, &m_arrSearchPattern) == FALSE)
		{
			m_arrSearchPattern.Add(pszPattern);
			nAddedCount++;
		}
	}

	return nAddedCount;
}

BOOL CVSTrackFinder::IsPatternAllowedForSearch(LPCTSTR szPattern)
{
	if (_tcslen(szPattern) < 4)
		return FALSE;

	gstatCall++;
	TRACE(TEXT("Call :%d\n"), gstatCall);

	//////CStringArray arrDeniedStrings;
	//////_T(".net"));
	//////arrDeniedStrings.Add(_T("account"));
	//////arrDeniedStrings.Add(_T("accounts"));
	//////arrDeniedStrings.Add(_T("admin"));
	//////arrDeniedStrings.Add(_T("administrator"));
	//////arrDeniedStrings.Add(_T("advanced"));
	//////arrDeniedStrings.Add(_T("antivirus"));
	//////arrDeniedStrings.Add(_T("anti-virus"));
	//////arrDeniedStrings.Add(_T("application"));
	//////arrDeniedStrings.Add(_T("applications"));
	//////arrDeniedStrings.Add(_T("arpcache"));
	//////arrDeniedStrings.Add(_T("audio"));
	//////arrDeniedStrings.Add(_T("backup"));
	//////arrDeniedStrings.Add(_T("backups"));
	//////arrDeniedStrings.Add(_T("browse"));
	//////arrDeniedStrings.Add(_T("browser"));
	//////arrDeniedStrings.Add(_T("boot"));
	//////arrDeniedStrings.Add(_T("button"));
	//////arrDeniedStrings.Add(_T("buttons"));
	//////arrDeniedStrings.Add(_T("cache"));
	//////arrDeniedStrings.Add(_T("cached"));
	//////arrDeniedStrings.Add(_T("center"));
	//////arrDeniedStrings.Add(_T("class"));
	//////arrDeniedStrings.Add(_T("classes"));
	//////arrDeniedStrings.Add(_T("clean"));
	//////arrDeniedStrings.Add(_T("cleaner"));
	//////arrDeniedStrings.Add(_T("client"));
	//////arrDeniedStrings.Add(_T("close"));
	//////arrDeniedStrings.Add(_T("command"));
	//////arrDeniedStrings.Add(_T("commands"));
	//////arrDeniedStrings.Add(_T("common"));
	//////arrDeniedStrings.Add(_T("compact"));
	//////arrDeniedStrings.Add(_T("company"));
	//////arrDeniedStrings.Add(_T("component"));
	//////arrDeniedStrings.Add(_T("components"));
	//////arrDeniedStrings.Add(_T("config"));
	//////arrDeniedStrings.Add(_T("configuration"));
	//////arrDeniedStrings.Add(_T("configure"));
	//////arrDeniedStrings.Add(_T("connection"));
	//////arrDeniedStrings.Add(_T("connections"));
	//////arrDeniedStrings.Add(_T("console"));
	//////arrDeniedStrings.Add(_T("control panel"));
	//////arrDeniedStrings.Add(_T("control"));
	//////arrDeniedStrings.Add(_T("controls"));
	//////arrDeniedStrings.Add(_T("core"));
	//////arrDeniedStrings.Add(_T("data"));
	//////arrDeniedStrings.Add(_T("date"));
	//////arrDeniedStrings.Add(_T("debug"));
	//////arrDeniedStrings.Add(_T("default"));
	//////arrDeniedStrings.Add(_T("defaults"));
	//////arrDeniedStrings.Add(_T("design"));
	//////arrDeniedStrings.Add(_T("designs"));
	//////arrDeniedStrings.Add(_T("desktop"));
	//////arrDeniedStrings.Add(_T("desktops"));
	//////arrDeniedStrings.Add(_T("device"));
	//////arrDeniedStrings.Add(_T("devices"));
	//////arrDeniedStrings.Add(_T("dictionarie"));
	//////arrDeniedStrings.Add(_T("dictionaries"));
	//////arrDeniedStrings.Add(_T("disk"));
	//////arrDeniedStrings.Add(_T("disks"));
	//////arrDeniedStrings.Add(_T("document"));
	//////arrDeniedStrings.Add(_T("documents"));
	//////arrDeniedStrings.Add(_T("domain"));
	//////arrDeniedStrings.Add(_T("domains"));
	//////arrDeniedStrings.Add(_T("driver"));
	//////arrDeniedStrings.Add(_T("drivers"));
	//////arrDeniedStrings.Add(_T("drivers32"));
	//////arrDeniedStrings.Add(_T("edit"));
	//////arrDeniedStrings.Add(_T("edition"));
	//////arrDeniedStrings.Add(_T("emulator"));
	//////arrDeniedStrings.Add(_T("enterprise"));
	//////arrDeniedStrings.Add(_T("execute"));
	//////arrDeniedStrings.Add(_T("exit"));
	//////arrDeniedStrings.Add(_T("explore"));
	//////arrDeniedStrings.Add(_T("explorer"));
	//////arrDeniedStrings.Add(_T("express"));
	//////arrDeniedStrings.Add(_T("extension"));
	//////arrDeniedStrings.Add(_T("extensions"));
	//////arrDeniedStrings.Add(_T("file"));
	//////arrDeniedStrings.Add(_T("files"));
	//////arrDeniedStrings.Add(_T("firewall"));
	//////arrDeniedStrings.Add(_T("folder"));
	//////arrDeniedStrings.Add(_T("folders"));
	//////arrDeniedStrings.Add(_T("font"));
	//////arrDeniedStrings.Add(_T("fonts"));
	//////arrDeniedStrings.Add(_T("free"));
	//////arrDeniedStrings.Add(_T("freeware"));
	//////arrDeniedStrings.Add(_T("full"));
	//////arrDeniedStrings.Add(_T("game"));
	//////arrDeniedStrings.Add(_T("games"));
	//////arrDeniedStrings.Add(_T("gateway"));
	//////arrDeniedStrings.Add(_T("gateways"));
	//////arrDeniedStrings.Add(_T("group"));
	//////arrDeniedStrings.Add(_T("hardware"));
	//////arrDeniedStrings.Add(_T("help"));
	//////arrDeniedStrings.Add(_T("host"));
	//////arrDeniedStrings.Add(_T("hosting"));
	//////arrDeniedStrings.Add(_T("hotfix"));
	//////arrDeniedStrings.Add(_T("icon"));
	//////arrDeniedStrings.Add(_T("icons"));
	//////arrDeniedStrings.Add(_T("info"));
	//////arrDeniedStrings.Add(_T("install"));
	//////arrDeniedStrings.Add(_T("installer"));
	//////arrDeniedStrings.Add(_T("installers"));
	//////arrDeniedStrings.Add(_T("interactive"));
	//////arrDeniedStrings.Add(_T("internet"));
	//////arrDeniedStrings.Add(_T("internet explorer"));
	//////arrDeniedStrings.Add(_T("license"));
	//////arrDeniedStrings.Add(_T("light"));
	//////arrDeniedStrings.Add(_T("log"));
	//////arrDeniedStrings.Add(_T("logs"));
	//////arrDeniedStrings.Add(_T("manager"));
	//////arrDeniedStrings.Add(_T("media"));
	//////arrDeniedStrings.Add(_T("menu"));
	//////arrDeniedStrings.Add(_T("message"));
	//////arrDeniedStrings.Add(_T("messenger"));
	//////arrDeniedStrings.Add(_T("microsoft"));
	//////arrDeniedStrings.Add(_T("mobile"));
	//////arrDeniedStrings.Add(_T("module"));
	//////arrDeniedStrings.Add(_T("modules"));
	//////arrDeniedStrings.Add(_T("monitor"));
	//////arrDeniedStrings.Add(_T("monitors"));
	//////arrDeniedStrings.Add(_T("multimedia"));
	//////arrDeniedStrings.Add(_T("network"));
	//////arrDeniedStrings.Add(_T("networks"));
	//////arrDeniedStrings.Add(_T("object"));
	//////arrDeniedStrings.Add(_T("objects"));
	//////arrDeniedStrings.Add(_T("office"));
	//////arrDeniedStrings.Add(_T("open"));
	//////arrDeniedStrings.Add(_T("option"));
	//////arrDeniedStrings.Add(_T("options"));
	//////arrDeniedStrings.Add(_T("panel"));
	//////arrDeniedStrings.Add(_T("panels"));
	//////arrDeniedStrings.Add(_T("personal"));
	//////arrDeniedStrings.Add(_T("phone"));
	//////arrDeniedStrings.Add(_T("phones"));
	//////arrDeniedStrings.Add(_T("platform"));
	//////arrDeniedStrings.Add(_T("platforms"));
	//////arrDeniedStrings.Add(_T("player"));
	//////arrDeniedStrings.Add(_T("players"));
	//////arrDeniedStrings.Add(_T("plugin"));
	//////arrDeniedStrings.Add(_T("plugins"));
	//////arrDeniedStrings.Add(_T("plug-ins"));
	//////arrDeniedStrings.Add(_T("point"));
	//////arrDeniedStrings.Add(_T("policies"));
	//////arrDeniedStrings.Add(_T("policy"));
	//////arrDeniedStrings.Add(_T("portable"));
	//////arrDeniedStrings.Add(_T("presentation"));
	//////arrDeniedStrings.Add(_T("print"));
	//////arrDeniedStrings.Add(_T("printers"));
	//////arrDeniedStrings.Add(_T("process"));
	//////arrDeniedStrings.Add(_T("processor"));
	//////arrDeniedStrings.Add(_T("product"));
	//////arrDeniedStrings.Add(_T("products"));
	//////arrDeniedStrings.Add(_T("professional"));
	//////arrDeniedStrings.Add(_T("profile"));
	//////arrDeniedStrings.Add(_T("profiles"));
	//////arrDeniedStrings.Add(_T("program"));
	//////arrDeniedStrings.Add(_T("programs"));
	//////arrDeniedStrings.Add(_T("properties"));
	//////arrDeniedStrings.Add(_T("reader"));
	//////arrDeniedStrings.Add(_T("register"));
	//////arrDeniedStrings.Add(_T("registration"));
	//////arrDeniedStrings.Add(_T("registry"));
	//////arrDeniedStrings.Add(_T("reinstall"));
	//////arrDeniedStrings.Add(_T("remote"));
	//////arrDeniedStrings.Add(_T("remove"));
	//////arrDeniedStrings.Add(_T("report"));
	//////arrDeniedStrings.Add(_T("reports"));
	//////arrDeniedStrings.Add(_T("resource"));
	//////arrDeniedStrings.Add(_T("resources"));
	//////arrDeniedStrings.Add(_T("restore"));
	//////arrDeniedStrings.Add(_T("route"));
	//////arrDeniedStrings.Add(_T("router"));
	//////arrDeniedStrings.Add(_T("routers"));
	//////arrDeniedStrings.Add(_T("run"));
	//////arrDeniedStrings.Add(_T("sample"));
	//////arrDeniedStrings.Add(_T("samples"));
	//////arrDeniedStrings.Add(_T("script"));
	//////arrDeniedStrings.Add(_T("scripts"));
	//////arrDeniedStrings.Add(_T("secure"));
	//////arrDeniedStrings.Add(_T("secured"));
	//////arrDeniedStrings.Add(_T("security"));
	//////arrDeniedStrings.Add(_T("server"));
	//////arrDeniedStrings.Add(_T("servers"));
	//////arrDeniedStrings.Add(_T("service"));
	//////arrDeniedStrings.Add(_T("services"));
	//////arrDeniedStrings.Add(_T("setting"));
	//////arrDeniedStrings.Add(_T("settings"));
	//////arrDeniedStrings.Add(_T("setup"));
	//////arrDeniedStrings.Add(_T("share"));
	//////arrDeniedStrings.Add(_T("shared"));
	//////arrDeniedStrings.Add(_T("shell"));
	//////arrDeniedStrings.Add(_T("soft"));
	//////arrDeniedStrings.Add(_T("software"));
	//////arrDeniedStrings.Add(_T("skin"));
	//////arrDeniedStrings.Add(_T("skins"));
	//////arrDeniedStrings.Add(_T("sound"));
	//////arrDeniedStrings.Add(_T("sounds"));
	//////arrDeniedStrings.Add(_T("source"));
	//////arrDeniedStrings.Add(_T("sources"));
	//////arrDeniedStrings.Add(_T("storage"));
	//////arrDeniedStrings.Add(_T("store"));
	//////arrDeniedStrings.Add(_T("style"));
	//////arrDeniedStrings.Add(_T("styles"));
	//////arrDeniedStrings.Add(_T("support"));
	//////arrDeniedStrings.Add(_T("system"));
	//////arrDeniedStrings.Add(_T("system32"));
	//////arrDeniedStrings.Add(_T("systems"));
	//////arrDeniedStrings.Add(_T("technologies"));
	//////arrDeniedStrings.Add(_T("technology"));
	//////arrDeniedStrings.Add(_T("template"));
	//////arrDeniedStrings.Add(_T("templates"));
	//////arrDeniedStrings.Add(_T("time"));
	//////arrDeniedStrings.Add(_T("tool"));
	//////arrDeniedStrings.Add(_T("tools"));
	//////arrDeniedStrings.Add(_T("toolbar"));
	//////arrDeniedStrings.Add(_T("toolbars"));
	//////arrDeniedStrings.Add(_T("trial"));
	//////arrDeniedStrings.Add(_T("unin"));
	//////arrDeniedStrings.Add(_T("uninst"));
	//////arrDeniedStrings.Add(_T("uninstall"));
	//////arrDeniedStrings.Add(_T("uninstaller"));
	//////arrDeniedStrings.Add(_T("update"));
	//////arrDeniedStrings.Add(_T("updates"));
	//////arrDeniedStrings.Add(_T("user"));
	//////arrDeniedStrings.Add(_T("users"));
	//////arrDeniedStrings.Add(_T("util"));
	//////arrDeniedStrings.Add(_T("utilities"));
	//////arrDeniedStrings.Add(_T("utility"));
	//////arrDeniedStrings.Add(_T("value"));
	//////arrDeniedStrings.Add(_T("values"));
	//////arrDeniedStrings.Add(_T("version"));
	//////arrDeniedStrings.Add(_T("versions"));
	//////arrDeniedStrings.Add(_T("video"));
	//////arrDeniedStrings.Add(_T("videos"));
	//////arrDeniedStrings.Add(_T("window"));
	//////arrDeniedStrings.Add(_T("windows"));
	//////arrDeniedStrings.Add(_T("winlogon"));
	//////arrDeniedStrings.Add(_T("wizard"));
	//////arrDeniedStrings.Add(_T("wizards"));
	//////arrDeniedStrings.Add(_T("workspace"));
	for (int i = 0; i < DENIEDNAMES; i++)
	{
		if (StrCmpI(szPattern, garrDeniedStrings[i]) == 0)
			return FALSE;
	}

	return TRUE;
}

void CVSTrackFinder::ConvertMultySzToArray(TCHAR* pszMultySz, DWORD dwSize, CStringArray& arrResult)
{
	DWORD dwCurrentSize = 0;
	TCHAR* pCurrPos = pszMultySz;
	while (dwCurrentSize < dwSize)
	{
		if (_tcslen(pCurrPos))
			arrResult.Add(pCurrPos);

		int nLen = (int)_tcslen(pCurrPos) + 1;
		pCurrPos = pCurrPos + nLen;
		dwCurrentSize += ((nLen + 1) * sizeof(TCHAR));
	}

}

void CVSTrackFinder::RemoveLastKeyName(LPTSTR pszPath)
{
	CString strPath(pszPath);

	int nSlashPos = strPath.ReverseFind(_T('\\'));

	if (nSlashPos == (strPath.GetLength() - 1))
	{
		strPath.Delete(nSlashPos);
		nSlashPos = strPath.ReverseFind(_T('\\'));
	}

	StrCpy(pszPath, strPath.Left(nSlashPos));
}


void CVSTrackFinder::CheckForPinnedShortcuts(bool ExtendedSearch)
{
	if (m_strInstallLocation.GetLength() == 0) return;

	fs::path p = "%AppData%\\Microsoft\\Internet Explorer\\Quick Launch\\User Pinned\\Taskbar";
	EnvExpand(p);
	TCHAR LinkTarget[4096];
	CoInitialize(0);

	for (auto& i : fs::directory_iterator(p))
	{
		p = i.path();
		ZeroMemory(LinkTarget, 4096 * sizeof(TCHAR));
		getShellLinkTarget(p.c_str(), LinkTarget, 4096);

		std::wstring wstrLinkTarget = LinkTarget;


		if (ExtendedSearch)
		{
			for (int i = 0; i < m_arrFinalFolders.GetSize(); i++)
			{
				if (IsFolderAllowedForSearch(m_arrFinalFolders[i].GetString()))
				{
					if (wstrLinkTarget.find(m_arrFinalFolders[i].GetString()) != std::wstring::npos)
					{
						m_arrFoundFilesFolders.Add(p.c_str());
					}
				}
			}
		}
		else if (m_strInstallLocation.GetLength() && IsFolderAllowedForSearch(m_strInstallLocation))
		{
			if (wstrLinkTarget.find(m_strInstallLocation.GetString()) != std::wstring::npos)
				m_arrFoundFilesFolders.Add(p.c_str());
		}
	}
	CoUninitialize();
}



void CVSTrackFinder::UninstallModeSafe()
{
	CheckIsSpecProgram();
	//Common search patters are app name and key name
	//install location and uninstall string path are added to folders for search
	InitCommonSearchPatterns();

	//Search common registry keys like RUN, RUNONCE, Exlorer MRU etc.
	SearchCommonRegPaths();

	//Search common search pattern in SOFTWARE
	SearchCmnPtrnInRegKeySoftware();

	/////////////////////////
	//Files and folders

	CheckForPinnedShortcuts();

	if (m_parrFUPaths)
	{
		for (int i = 0; i < m_parrFUPaths->GetCount(); i++)
		{
			CStringArray arrFound;
			CString strPath = m_parrFUPaths->GetAt(i);
			if (strPath.GetLength())
			{
				if (IsFolderAllowedForSearch(strPath))
				{
					CStringArray arrNames, arrIgnore;
					FindAllDataInFolder(strPath, &arrNames, &arrFound, &arrIgnore, -1, -1, TRUE, TRUE, TRUE, TRUE);
				}
			}
			if (arrFound.GetCount() > 0)
				m_arrFoundFilesFolders.Append(arrFound);
		}
	}
	else
	{
		CStringArray arrFound;
		if (m_strInstallLocation.GetLength())
		{
			if (IsFolderAllowedForSearch(m_strInstallLocation))
			{
				CStringArray arrNames, arrIgnore;
				FindAllDataInFolder(m_strInstallLocation, &arrNames, &arrFound, &arrIgnore, -1, -1, TRUE, TRUE, TRUE, TRUE);
			}
		}
		m_arrFoundFilesFolders.Append(arrFound);
	}


}

void CVSTrackFinder::UninstallModeModerate()
{
	CheckIsSpecProgram();
	//Common search patters are app name and key name
	//install location and uninstall string path are added to folders for search
	InitCommonSearchPatterns();

	InitMSIInfo();

	//Search common registry keys like RUN, RUNONCE, Exlorer MRU etc.
	SearchCommonRegPaths();

	//Searche in Classes
	SearchInRegKeyClasses();

	//Search all MSI Components
	SearchForMSILeftovers();

	//Search common search pattern in SOFTWARE
	if (SearchCmnPtrnInRegKeySoftware() == FALSE)
	{
		//if nothing has found in HKCU/HKLM Software then
		//search for dislpay name with match in reg path
		if (SearchMatchAppNameInRegPath() == FALSE)
		{
			//if nothing has found in HKCU/HKLM Software then
			//search for key name with match in reg path
			SearchMatchKeyNameInRegPath();
		}
	}

	SearchLeftoverFilesFolders(&m_arrSearchPattern);
	CheckForPinnedShortcuts();
}

void CVSTrackFinder::UninstallModeAdvanced()
{
	eRegWOW6432Type eKeyWOW;
	if (m_b64Bit)
		eKeyWOW = Key64Bit;
	else
		eKeyWOW = Key32Bit;

	CheckIsSpecProgram();

	//Common search patters are app name and key name
	//install location and uninstall string path are added to folders for search
	InitCommonSearchPatterns();

	InitMSIInfo();

	//Scan Program Files if there is no InstallLocation supplied 
	//or any other folder is found for search
	CStringArray arrExactFolders;
	GetExactFoldersForSearch(arrExactFolders);
	if (arrExactFolders.GetCount() == 0)
	{
		//Get Program Files folder
		TCHAR szProgramFiles[4096] = { 0 };
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFiles)) && m_arrSearchPattern.GetCount())
		{
			CStringArray arrFoundFolders;
			FindFileOrFolder(szProgramFiles, &m_arrSearchPattern, TRUE, &arrFoundFolders, FALSE);

			//if nothing - try match case false
			if (arrFoundFolders.GetCount() == 0)
				FindFileOrFolder(szProgramFiles, &m_arrSearchPattern, TRUE, &arrFoundFolders, FALSE, FALSE);

			//if still nothing - try recursive search whole Program Files folder
			if (arrFoundFolders.GetCount() == 0)
				FindFileOrFolder(szProgramFiles, &m_arrSearchPattern, TRUE, &arrFoundFolders, TRUE);

			//Try very hard - match case false and recursive 
			if (arrFoundFolders.GetCount() == 0)
				FindFileOrFolder(szProgramFiles, &m_arrSearchPattern, TRUE, &arrFoundFolders, FALSE, FALSE);

			PutFoldersForSearch(arrFoundFolders);
		}
	}

	//Search common registry keys like RUN, RUNONCE, Exlorer MRU etc.
	SearchCommonRegPaths();

	//Searche in Classes
	SearchInRegKeyClasses();

	//Search all MSI Components
	SearchForMSILeftovers();

	//Search common search pattern in SOFTWARE
	if (SearchCmnPtrnInRegKeySoftware() == FALSE)
	{
		//if nothing has found in HKCU/HKLM Software then
		//search for dislpay name with match in reg path
		if (SearchMatchAppNameInRegPath() == FALSE)
		{
			//if nothing has found in HKCU/HKLM Software then
			//search for key name with match in reg path
			if (SearchMatchKeyNameInRegPath() == FALSE)
			{
				//if nothing has been found so far /all moderate mode/
				//try some advanced techniques
				BOOL bFound = FALSE;

				CStringArray arrDisplayName;
				INT_PTR nCount = 0;

				if (m_parrFUProgNames)
				{
					for (INT_PTR i = 0; i < m_parrFUProgNames->GetCount(); i++)
					{
						CStringArray arrCurNames;
						int nElems = 0;
						CString strName = m_parrFUProgNames->GetAt(i);
						nElems = ExtractPureStrings(strName, arrCurNames);
						if (nElems > 0)
							arrDisplayName.Append(arrCurNames);
					}
					nCount = arrDisplayName.GetCount();
				}
				else
					nCount = ExtractPureStrings(m_strAppName, arrDisplayName);

				if (nCount)
				{
					TCHAR* pszSearchedRegPath = new TCHAR[65536];
					pszSearchedRegPath[0] = 0;
					TCHAR  szFullRegPath[65536] = { 0 };

					CStringArray arrIgnore;
					arrIgnore.Add(_T("Classes"));
					arrIgnore.Add(_T("classes"));

					int nDepth = 3, nBreak = 1;

					StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE"));
					DWORD dwFoundCount = 0;
					SearchInRegistryPath(HKEY_CURRENT_USER, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrDisplayName, m_pTree, &dwFoundCount, (int)arrDisplayName.GetCount(), &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);

					pszSearchedRegPath[0] = 0;

					StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE"));
					SearchInRegistryPath(HKEY_LOCAL_MACHINE, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrDisplayName, m_pTree, &dwFoundCount, (int)arrDisplayName.GetCount(), &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);

					if (dwFoundCount = 0)
					{
						if (arrDisplayName.GetCount() > 2)
						{
							StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE"));
							DWORD dwFoundCount = 0;
							SearchInRegistryPath(HKEY_CURRENT_USER, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrDisplayName, m_pTree, &dwFoundCount, (int)arrDisplayName.GetCount() - 1, &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);

							pszSearchedRegPath[0] = 0;

							StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE"));
							SearchInRegistryPath(HKEY_LOCAL_MACHINE, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrDisplayName, m_pTree, &dwFoundCount, (int)arrDisplayName.GetCount() - 1, &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);

						}
					}

					delete[] pszSearchedRegPath;
				}
			}
		}
	}

	//Find data for Tasks from Task Sheduler from Win Vista up
	CStringArray arrIgnoreTasks;
	arrIgnoreTasks.Add(_T("Microsoft"));
	DWORD dwFoundCount = 0;
	TCHAR  szFullRegPath[65536] = { 0 };
	RegDataList listTasks;
	StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Tree"));
	SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Tree"), szFullRegPath, m_arrSearchPattern, &listTasks, &dwFoundCount, &arrIgnoreTasks, m_b64Bit, 2, -1, TRUE, FALSE, FALSE, FALSE);
	if (listTasks.GetCount())
	{
		POSITION pos = listTasks.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* dataTmp = (FoundRegData*)listTasks.GetNext(pos);

			CString strTempRes, strTaskTreePath;
			RemoveRootFromPath(dataTmp->strFullKeyPath, strTempRes);
			RemoveRootFromPath(strTempRes, strTaskTreePath);

			//CString strKeyName = _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Tree\\") + dataTmp->strKeyName ;
			m_pTree->CreateKey(dataTmp->strFullKeyPath, eKeyWOW);

			LONG lRes;
			HKEY hKey;

			lRes = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strTaskTreePath, NULL, KEY_READ, &hKey);
			if (lRes == ERROR_SUCCESS)
			{
				TCHAR	szTaskID[100] = { 0 };
				DWORD	dwDataSize = sizeof(szTaskID);
				if (::RegQueryValueEx(hKey, TEXT("Id"), NULL, NULL, (LPBYTE)(szTaskID), &dwDataSize) == ERROR_SUCCESS)
				{
					dwFoundCount = 0;
					CStringArray arrTaskId;
					arrTaskId.Add(szTaskID);
					StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Tasks"));
					SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Tasks"), szFullRegPath, arrTaskId, m_pTree, &dwFoundCount, NULL, m_b64Bit, 1, 1, TRUE, FALSE, FALSE, TRUE);

					dwFoundCount = 0;
					StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Plain"));
					SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Plain"), szFullRegPath, arrTaskId, m_pTree, &dwFoundCount, NULL, m_b64Bit, 1, 1, TRUE, FALSE, FALSE, TRUE);
					if (dwFoundCount == 0)
					{
						StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Logon"));
						SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Logon"), szFullRegPath, arrTaskId, m_pTree, &dwFoundCount, NULL, m_b64Bit, 1, 1, TRUE, FALSE, FALSE, TRUE);
						if (dwFoundCount == 0)
						{
							StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Boot"));
							SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Schedule\\TaskCache\\Boot"), szFullRegPath, arrTaskId, m_pTree, &dwFoundCount, NULL, m_b64Bit, 1, 1, TRUE, FALSE, FALSE, TRUE);
						}
					}
				}
				::RegCloseKey(hKey);
			}
			delete dataTmp;
		}
	}

	//Search in Control Panel reg key for CPL files
	CStringArray arrFolders;
	GetAllTypeFoldersForSearch(arrFolders);

	if (arrFolders.GetCount())
	{
		CStringArray arrIgnoreFirewall;
		arrIgnoreFirewall.Add(_T("SharedAccess"));

		DWORD dwFoundCount = 0;
		TCHAR  szFullRegPath[65536] = { 0 };
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Control Panel\\Cpls"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Control Panel\\Cpls"), szFullRegPath, arrFolders, m_pTree, &dwFoundCount, &arrIgnoreFirewall, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, FALSE);

		//Search for Services and get Enum key
		dwFoundCount = 0;
		RegDataList listServices;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services"), szFullRegPath, arrFolders, &listServices, &dwFoundCount, &arrIgnoreFirewall, m_b64Bit, 1, -1, FALSE, FALSE, TRUE, FALSE);
		if (listServices.GetCount())
		{
			POSITION pos = listServices.GetHeadPosition();
			while (pos != NULL)
			{
				FoundRegData* dataTmp = (FoundRegData*)listServices.GetNext(pos);

				CString strEnum;
				strEnum = _T("SYSTEM\\CurrentControlSet\\Services\\") + dataTmp->strKeyName + _T("\\Enum");

				CString strKeyName = _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\") + dataTmp->strKeyName;
				m_pTree->CreateKey(strKeyName, eKeyWOW);

				LONG lRes;
				HKEY hKey;

				lRes = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strEnum, NULL, KEY_READ, &hKey);
				if (lRes == ERROR_SUCCESS)
				{
					DWORD dwVal = 0;
					DWORD dwValSize = sizeof(DWORD);
					lRes = ::RegQueryValueEx(hKey, _T("Count"), NULL, NULL, (LPBYTE)(&dwVal), &dwValSize);
					if (lRes == ERROR_SUCCESS)
					{

						for (DWORD i = 0; i < dwVal; i++)
						{
							TCHAR szEnumPath[256] = { 0 };
							DWORD dwEnumPathSize = 256;
							TCHAR szCount[12] = { 0 };
							_stprintf(szCount, _T("%d"), i);
							lRes = ::RegQueryValueEx(hKey, szCount, NULL, NULL, (LPBYTE)(&szEnumPath), &dwEnumPathSize);
							if (lRes == ERROR_SUCCESS)
							{

								strKeyName = _T("SYSTEM\\CurrentControlSet\\Enum\\");

								CString strEnumPath = szEnumPath;
								int nPos = strEnumPath.Find(_T("\\"));
								if (nPos)
								{
									nPos = strEnumPath.Find(_T("\\"), nPos + 1);
									CString strLeft = strEnumPath.Left(nPos);
									strKeyName += strLeft;
								}
								else
									strKeyName += szEnumPath;

								strKeyName.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
								m_pTree->CreateKey(strKeyName, eKeyWOW);
							}
						}
					}
					::RegCloseKey(hKey);
				}
			}
		}



		//Find data in Eventlog
		dwFoundCount = 0;
		RegDataList listEventLog;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Eventlog\\Application"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\Eventlog\\Application"), szFullRegPath, arrFolders, &listServices, &dwFoundCount, NULL, m_b64Bit, 2, -1, FALSE, FALSE, TRUE, FALSE);

		POSITION pos = listServices.GetHeadPosition();
		while (pos != NULL)
		{

			FoundRegData* temp = (FoundRegData*)listServices.GetNext(pos);

			PREGNODE pNode = NULL;
			pNode = m_pTree->CreateKey(temp->strFullKeyPath, eKeyWOW);

			if (pNode)
				pNode->eOperation = RegCreated;

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return;

		dwFoundCount = 0;
		RegDataList listEventLogSys;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Eventlog\\System"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\Eventlog\\System"), szFullRegPath, arrFolders, &listEventLogSys, &dwFoundCount, NULL, m_b64Bit, 2, -1, FALSE, FALSE, TRUE, FALSE);

		pos = listEventLogSys.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listEventLogSys.GetNext(pos);

			PREGNODE pNode = NULL;
			pNode = m_pTree->CreateKey(temp->strFullKeyPath, eKeyWOW);

			if (pNode)
				pNode->eOperation = RegCreated;

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}
		///////
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return;
	}

	ClearRegTreeFromApps();
	SearchLeftoverFilesFolders(&m_arrSearchPattern);
	CheckForPinnedShortcuts(true);
}

BOOL CVSTrackFinder::GetStartMenuItems(LPCTSTR pFolder, CStringArray* pArray)
{
	if (pFolder == NULL)
		return FALSE;

	if (pArray == NULL)
		return FALSE;

	if (_tcslen(pFolder) == 0)
		return FALSE;

	TCHAR szFullPathFileName[4096] = { 0 };
	TCHAR szFilename[4096] = { 0 };

	WIN32_FIND_DATA FileData = { 0 };
	BOOL bFinished = FALSE;
	DWORD dwSize = 0;

	StringCchPrintf(szFullPathFileName, 4096, TEXT("%s\\*.*"), pFolder);
	HANDLE hSearch = FindFirstFile(szFullPathFileName, &FileData);
	if (hSearch == INVALID_HANDLE_VALUE)
		return FALSE;

	TCHAR szProgramFilesCommon[4096] = { 0 }, szProgramFiles[4096] = { 0 }, szProgramFilesCommonX86[4096] = { 0 }, szProgramFilesX86[4096] = { 0 };
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMON, NULL, 0, szProgramFilesCommon))
		&& SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMONX86, NULL, 0, szProgramFilesCommonX86)))
	{
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFiles))
			&& SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szProgramFilesX86)))
		{
			while (!bFinished)
			{
				StringCchPrintf(szFilename, 4096, TEXT("%s\\%s"), pFolder, FileData.cFileName);
				if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
				{
					if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						if (_tcscmp(FileData.cFileName, TEXT(".")) && _tcscmp(FileData.cFileName, TEXT("..")))
						{
							GetStartMenuItems(szFilename, pArray);
						}
					}
					else
					{
						//check if file is an executable
						TCHAR pszTargetBuff[4096] = { 0 };
						GetShellLinkTarget(szFilename, pszTargetBuff, 4096);
						//get Common folder and exclude all executables from it
						//TCHAR szProgramFilesCommon[4096] = {0};
						//if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMON, NULL, 0, szProgramFilesCommon))) 
						//{
						if ((StrStrI(pszTargetBuff, szProgramFilesCommon) == NULL) && (StrStrI(pszTargetBuff, szProgramFilesCommonX86) == NULL))
						{
							//get Program Files folder and check if executable is there
							//TCHAR szProgramFiles[4096] = {0};
							//if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFiles))) 
							//{
							if ((StrStrI(pszTargetBuff, szProgramFiles)) || (StrStrI(pszTargetBuff, szProgramFilesX86)))
							{
								pArray->Add(pszTargetBuff);
							}
							//}
						}
						//}
					}
				}
				if (!FindNextFile(hSearch, &FileData))
				{
					//if (GetLastError() == ERROR_NO_MORE_FILES) 
					bFinished = TRUE;
				}
			}
		}
	}
	FindClose(hSearch);

	return TRUE;
}

// Get the target of a file:
// If the file is a shortcut, return its target; handle all known cases:
// - installer shortcuts (MSI API)
// - IUniformResourceLocator shortcuts
// - IShellLink shortcuts
// If the file is a normal file, return its path unchanged
BOOL CVSTrackFinder::GetShellLinkTarget(LPCTSTR pszLinkFile, LPTSTR pszTargetPath, DWORD buffSize)
{
	*pszTargetPath = _T('\0');

	TCHAR pszProductCode[39] = { 0 };
	TCHAR pszComponentCode[39] = { 0 };
	DWORD buf = buffSize;
	if (ERROR_SUCCESS == MsiGetShortcutTarget(pszLinkFile, pszProductCode, NULL, pszComponentCode))
		MsiGetComponentPath(pszProductCode, pszComponentCode, pszTargetPath, &buf);
	if (*pszTargetPath)
		return true;

#ifndef UNICODE
	WCHAR wsz[MAX_PATH];
	MultiByteToWideChar(CP_ACP, 0, pszLinkFile, -1, wsz, nbArray(wsz));
#endif
	LPCTSTR wszLinkFile = pszLinkFile;

	// Resolve the shortcut using the IUniformResourceLocator and IPersistFile interfaces
	IUniformResourceLocator* purl;
	if (SUCCEEDED(CoCreateInstance(CLSID_InternetShortcut, NULL, CLSCTX_INPROC_SERVER,
								   IID_IUniformResourceLocator, (LPVOID*)&purl)))
	{
		IPersistFile* ppf;
		if (SUCCEEDED(purl->QueryInterface(IID_IPersistFile, (void**)&ppf)))
		{
			LPTSTR pszURL;
			HRESULT hr1 = ppf->Load(wszLinkFile, STGM_READ);
			HRESULT hr2 = purl->GetURL(&pszURL);
			if ((S_OK == hr1) && (S_OK == hr2))
				StringCchCopy(pszTargetPath, buffSize, pszURL);
			if (hr2 == S_OK)
				ShellFree(pszURL);
			ppf->Release();
		}
		purl->Release();

		if (*pszTargetPath)
		{
			return true;
		}
	}
	DWORD dwErr = GetLastError();
	// Resolve the shortcut using the IShellLink and IPersistFile interfaces
	IShellLink* psl;
	if (SUCCEEDED(CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
								   IID_IShellLink, (LPVOID*)&psl)))
	{
		IPersistFile* ppf;
		if (SUCCEEDED(psl->QueryInterface(IID_IPersistFile, (void**)&ppf)))
		{
			if (SUCCEEDED(ppf->Load(wszLinkFile, STGM_READ)) &&
				SUCCEEDED(psl->Resolve(NULL, MAKELONG(SLR_NO_UI | SLR_NOUPDATE | SLR_NOTRACK | SLR_NOSEARCH | SLR_NOLINKINFO, 1000))))
				psl->GetPath(pszTargetPath, buffSize, NULL, 0);
			ppf->Release();
		}
		psl->Release();

		if (*pszTargetPath)
		{
			return true;
		}
	}
	dwErr = GetLastError();
	//CoUninitialize();
	return false;
}

// Use IMalloc to free a memory block
void CVSTrackFinder::ShellFree(void* p)
{
	LPMALLOC pMalloc;
	if (SUCCEEDED(SHGetMalloc(&pMalloc)))
	{
		pMalloc->Free(p);
		pMalloc->Release();
	}
}

void CVSTrackFinder::ExtractMissingFromStartMenu()
{
	//Extract missing items to m_arrSMRemovedTargets
	if (m_arrStartBefore.GetCount() > m_arrStartAfter.GetCount())
	{
		for (int i = 0; i < m_arrStartBefore.GetCount(); i++)
		{
			BOOL bFound = FALSE;
			for (int k = 0; k < m_arrStartAfter.GetCount(); k++)
			{
				if (StrCmpI(m_arrStartBefore.GetAt(i), m_arrStartAfter.GetAt(k)) == 0)
				{
					bFound = TRUE;
					continue;
				}
			}
			if (bFound == FALSE)
				m_arrSMRemovedTargets.Add(m_arrStartBefore.GetAt(i));
		}
	}

	//Extract missing items to m_arrSMAllRemovedTargets
	if (m_arrAllStartBefore.GetCount() > m_arrAllStartAfter.GetCount())
	{
		for (int i = 0; i < m_arrAllStartBefore.GetCount(); i++)
		{
			BOOL bFound = FALSE;
			for (int k = 0; k < m_arrAllStartAfter.GetCount(); k++)
			{
				if (StrCmpI(m_arrAllStartBefore.GetAt(i), m_arrAllStartAfter.GetAt(k)) == 0)
				{
					bFound = TRUE;
					continue;
				}
			}
			if (bFound == FALSE)
				m_arrSMAllRemovedTargets.Add(m_arrAllStartBefore.GetAt(i));

		}
	}

	BOOL bFound = FALSE;
	for (int i = 0; i < m_arrSMRemovedTargets.GetCount(); i++)
	{
		CString strB = m_arrSMRemovedTargets.GetAt(i);

		CString strFileNameOnly;
		if (GetFileNameFromPath(strB, strFileNameOnly))
		{
			if (IsStringInList(strFileNameOnly, &m_arrStartMenuFileNames, TRUE) == FALSE)
				m_arrStartMenuFileNames.Add(strFileNameOnly);
		}

		::PathRemoveFileSpec(strB.GetBuffer());
		strB.ReleaseBuffer();

		//::PathUnquoteSpaces(strB.GetBuffer());
		//strB.ReleaseBuffer();

		PutFoldersForSearch(strB);

	}

	for (int i = 0; i < m_arrSMAllRemovedTargets.GetCount(); i++)
	{
		CString strB = m_arrSMAllRemovedTargets.GetAt(i);

		CString strFileNameOnly;
		if (GetFileNameFromPath(strB, strFileNameOnly))
		{
			if (IsStringInList(strFileNameOnly, &m_arrStartMenuFileNames, TRUE) == FALSE)
				m_arrStartMenuFileNames.Add(strFileNameOnly);
		}

		::PathRemoveFileSpec(strB.GetBuffer());
		strB.ReleaseBuffer();

		//::PathUnquoteSpaces(strB.GetBuffer());
		//strB.ReleaseBuffer();

		PutFoldersForSearch(strB);
	}
}

void CVSTrackFinder::GetExactFoldersForSearch(CStringArray& arrToPutInto)
{

	arrToPutInto.Append(m_arrFinalFolders);

}
void CVSTrackFinder::GetAllTypeFoldersForSearch(CStringArray& arrToPutInto)
{

	arrToPutInto.Append(m_arrAllKindFolders);

}

void CVSTrackFinder::PutFoldersForSearch(LPCTSTR lpszFolderPath)
{
	if (lpszFolderPath == NULL)
		return;

	DWORD nBuffLen = (DWORD)_tcslen(lpszFolderPath) * 4;

	if (nBuffLen == 0)
		return;

	TCHAR* pszUnquotaPath = new TCHAR[nBuffLen];
	StringCchCopy(pszUnquotaPath, nBuffLen, lpszFolderPath);

	::PathUnquoteSpaces(pszUnquotaPath);

	//Always should have backslash because there is no way to distingush the following:
	//C:\Program File\Ado
	//C:\Program File\Adobe
	::PathAddBackslash(pszUnquotaPath);

	if (IsFolderAllowedForSearch(pszUnquotaPath) == FALSE)
	{
		delete[] pszUnquotaPath;
		return;
	}

	//If the string contains a string from list
	if (IsListInString(&m_arrAllKindFolders, pszUnquotaPath, FALSE) == TRUE)
	{
		delete[] pszUnquotaPath;
		return;
	}

	m_arrFinalFolders.Add(pszUnquotaPath);
	//Add unquoted folder path
	m_arrAllKindFolders.Add(pszUnquotaPath);

	//Add short path
	TCHAR* pszPathShort = new TCHAR[nBuffLen];
	StringCchCopy(pszPathShort, nBuffLen, pszUnquotaPath);
	::GetShortPathName(pszUnquotaPath, pszPathShort, nBuffLen);

	//If the string does not contain the string in list
	if (IsListInString(&m_arrAllKindFolders, pszPathShort, FALSE) == FALSE)
		m_arrAllKindFolders.Add(pszPathShort);

	delete[] pszPathShort;

	//Add expanded environment path
	TCHAR* pszPathEnv = new TCHAR[nBuffLen];
	DWORD dwRet = ::ExpandEnvironmentStrings(pszUnquotaPath, pszPathEnv, nBuffLen);
	if (dwRet > nBuffLen)
	{
		delete[] pszPathEnv;
		pszPathEnv = new TCHAR[dwRet];
		::ExpandEnvironmentStrings(pszUnquotaPath, pszPathEnv, dwRet);
	}
	if (IsListInString(&m_arrAllKindFolders, pszPathEnv, FALSE) == FALSE)
		m_arrAllKindFolders.Add(pszPathEnv);
	delete[] pszPathEnv;

	//Add unexpanded environment path
	TCHAR* pszPathUnEnv = new TCHAR[nBuffLen];
	if (PathUnExpandEnvStrings(pszUnquotaPath, pszPathUnEnv, nBuffLen))
	{
		if (IsListInString(&m_arrAllKindFolders, pszPathUnEnv, FALSE) == FALSE)
			m_arrAllKindFolders.Add(pszPathUnEnv);
	}
	delete[] pszPathUnEnv;

	//Add long path name
	TCHAR* pszPathLong = new TCHAR[nBuffLen];
	DWORD dwBuffSize = ::GetLongPathName(pszUnquotaPath, pszPathLong, nBuffLen);
	if (dwBuffSize > nBuffLen)
	{
		delete[] pszPathLong;
		pszPathLong = new TCHAR[dwBuffSize];
		::GetLongPathName(pszUnquotaPath, pszPathLong, dwBuffSize);
	}
	if (dwBuffSize)
		if (IsListInString(&m_arrAllKindFolders, pszPathLong, FALSE) == FALSE)
			m_arrAllKindFolders.Add(pszPathLong);

	delete[] pszPathLong;

	delete[] pszUnquotaPath;
}

void CVSTrackFinder::PutFoldersForSearch(CStringArray& arrFolderPaths)
{
	for (INT_PTR nCount = 0; nCount < arrFolderPaths.GetCount(); nCount++)
		PutFoldersForSearch(arrFolderPaths.GetAt(nCount));
}

void CVSTrackFinder::PutFoundFilesFolders(LPCTSTR lpszFileFolderPaths, BOOL bTurnOnProtection)
{
	if (lpszFileFolderPaths == NULL)
		return;

	if (_tcslen(lpszFileFolderPaths) == 0)
		return;

	size_t nBuffLen = _tcslen(lpszFileFolderPaths) + 2;

	TCHAR* pszUnquotaPath = new TCHAR[nBuffLen];
	memset(pszUnquotaPath, 0, nBuffLen);
	StrCpy(pszUnquotaPath, lpszFileFolderPaths);

	::PathUnquoteSpaces(pszUnquotaPath);
	::PathRemoveBackslash(pszUnquotaPath);

	if (bTurnOnProtection)
	{
		if (IsFolderAllowedForSearch(pszUnquotaPath) == FALSE)
		{
			delete[] pszUnquotaPath;
			return;
		}
	}

	if (IsStringInList(pszUnquotaPath, &m_arrFoundFilesFolders) == TRUE)
	{
		delete[] pszUnquotaPath;
		return;
	}

	if (::PathFileExists(pszUnquotaPath))
		m_arrFoundFilesFolders.Add(pszUnquotaPath);

	delete[] pszUnquotaPath;
}


void CVSTrackFinder::PutFoundFilesFolders(CStringArray& arrFileFolderPaths)
{
	for (int i = 0; i < arrFileFolderPaths.GetCount(); i++)
		PutFoundFilesFolders(arrFileFolderPaths.GetAt(i));
}

DWORD CVSTrackFinder::CreateSystemRestorePoint()
{

	//If it is windows 8 tobe able  to create a restore point more than ones per 24 hours
	int nMajorVer = 0, nMinorVer = 0;
	GetWindowsVersion(&nMajorVer, &nMinorVer);
	//if(nMajorVer >= 6 && nMinorVer >= 2)
	if ((nMajorVer == 6 && nMinorVer == 2) || (Is_Win81()) || (Is_Win10()))
	{

		CRegistry regSRKey(HKEY_LOCAL_MACHINE);
		CString strPath = TEXT("Software\\Microsoft\\Windows NT\\CurrentVersion\\SystemRestore");

		if (regSRKey.ForcedOpen(strPath, KEY_ALL_ACCESS))
		{
			DWORD dwType = 0;
			if (regSRKey.GetDoubleWordValue(TEXT("SystemRestorePointCreationFrequency"), dwType) == FALSE)
			{
				regSRKey.SetDoubleWordValue(TEXT("SystemRestorePointCreationFrequency"), 0);
			}
			regSRKey.SetDoubleWordValue(TEXT("RPSessionInterval"), 0);
		}
	}


	HINSTANCE hinstLib;
	hinstLib = LoadLibrary(TEXT("SrClient.dll"));
	if (hinstLib != NULL)
	{
		typedef struct _RESTOREPTINFOW
		{
			DWORD   dwEventType;
			DWORD   dwRestorePtType;
			INT64   llSequenceNumber;
			WCHAR   szDescription[256];
		} RESTOREPOINTINFOW, * PRESTOREPOINTINFOW;

		typedef struct _SMGRSTATUS
		{
			DWORD   nStatus;            // Status returned by State Manager Process
			INT64   llSequenceNumber;   // Sequence Number for the restore point
		} STATEMGRSTATUS, * PSTATEMGRSTATUS;

		typedef BOOL(WINAPI* PFN_SRSetRestorePointW)(
			PRESTOREPOINTINFOW  pRestorePtSpec,
			PSTATEMGRSTATUS     pSMgrStatus
			);

		const PFN_SRSetRestorePointW pfnSRSetRestorePointW = (PFN_SRSetRestorePointW)GetProcAddress(hinstLib, "SRSetRestorePointW");

		// If the function address is valid, call the function.
		if (NULL != pfnSRSetRestorePointW)
		{
			RESTOREPOINTINFOW RstPt;
			STATEMGRSTATUS MgrStat;

			RstPt.dwEventType = 100; //BEGIN_SYSTEM_CHANGE
			RstPt.dwRestorePtType = 1; //APPLICATION_UNINSTALL
			StringCchCopy(RstPt.szDescription, 256, _T("Revo Uninstaller Pro's restore point - "));
			if (m_parrFUProgNames)
				StringCchCat(RstPt.szDescription, 256, m_parrFUProgNames->GetAt(0));
			else
				StringCchCat(RstPt.szDescription, 256, m_strAppName);

			if (!pfnSRSetRestorePointW(&RstPt, &MgrStat))
			{
				//printf("Unable to set restore point. Error %ld\n", MgrStat.nStatus);
				FreeLibrary(hinstLib);
				return FALSE;
			}
		}

		FreeLibrary(hinstLib);
		return TRUE;
	}

	return FALSE;
}

BOOL CVSTrackFinder::PutFoldersOfMSIAppForSearch()
{

	CStringArray arrSharedPaths, arrValidPaths;

	TCHAR szRes[50] = { 0 };
	if (SquashGUID(m_strMSIGUID, szRes))
	{
		//MSI is 64bit on 64 and 32bit on 32 Windows
		REGSAM regPermissions = KEY_READ;

		m_bMSI = TRUE;

		CString strKeyComp = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Components");

		CRegistry oComponents(HKEY_LOCAL_MACHINE);
		oComponents.Open(strKeyComp, regPermissions);

		int nKeyCount = oComponents.GetNumberOfSubkeys();
		CString strKeyName, strClassName;
		for (int nKeyIndex = 0; nKeyIndex < nKeyCount; nKeyIndex++)
		{
			oComponents.EnumerateKeys(nKeyIndex, strKeyName, strClassName);

			CString strCompFullPath = strKeyComp + _T("\\");
			strCompFullPath += strKeyName;

			CRegistry oTmpComponent(HKEY_LOCAL_MACHINE);
			oTmpComponent.Open(strCompFullPath, regPermissions);

			int nValCount = oTmpComponent.GetNumberOfValues();

			for (int nVals = 0; nVals < nValCount; nVals++)
			{
				CRegistry::KeyValueTypes value_type;
				CString strName;
				DWORD dwDataSize = oTmpComponent.GetValueDataSize();
				LPBYTE data = new BYTE[dwDataSize];
				::ZeroMemory(data, dwDataSize);
				oTmpComponent.EnumerateValues(nVals, strName, value_type, data, dwDataSize);

				if (StrCmpI(strName, szRes) == 0)
				{
					CString strData = (TCHAR*)data;
					strData.Replace(_T("?"), _T(":"));
					if (IsInSharedDlls(strData) == FALSE)
					{
						if (::PathIsDirectory(strData))
						{
							if (IsStringInList(strData, &arrSharedPaths) == FALSE)
							{
								if (nValCount == 1)//because if there are more that file/module is shared
								{
									//PutFoldersForSearch(strData);
									if (IsStringInList(strData, &arrValidPaths) == FALSE)
										arrValidPaths.Add(strData);
								}
								else
								{
									INT_PTR nCount = arrValidPaths.GetCount();
									for (INT_PTR i = 0; i < nCount; i++)
									{
										if (strData.CompareNoCase(arrValidPaths.GetAt(i)) == 0)
										{
											arrValidPaths.RemoveAt(i);
											nCount--;
											i--;
										}

									}
									arrSharedPaths.Add(strData);
								}
							}
						}
						else
						{
							::PathRemoveFileSpec(strData.GetBuffer());
							strData.ReleaseBuffer();
							if (::PathIsDirectory(strData))
							{
								if (IsStringInList(strData, &arrSharedPaths) == FALSE)
								{
									if (nValCount == 1)//because if there are more that file/module is shared
									{
										if (IsStringInList(strData, &arrValidPaths) == FALSE)
											arrValidPaths.Add(strData);
									}
									//PutFoldersForSearch(strData);
									else
									{
										INT_PTR nCount = arrValidPaths.GetCount();
										for (INT_PTR i = 0; i < nCount; i++)
										{
											if (strData.CompareNoCase(arrValidPaths.GetAt(i)) == 0)
											{
												arrValidPaths.RemoveAt(i);
												nCount--;
												i--;
											}

										}
										arrSharedPaths.Add(strData);
									}
								}
							}
						}

						m_arrMSIComponentNames.Add(strKeyName);
					}
				}
				delete[] data;
			}

		}

		CRegistry oComponentsCU(HKEY_LOCAL_MACHINE);
		CString strKeyCompCU = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\") + GetCurrentUserSID() + _T("\\Components");
		oComponentsCU.Open(strKeyCompCU, regPermissions);
		int nKeyCountCU = oComponentsCU.GetNumberOfSubkeys();
		CString strKeyNameCU, strClassNameCU;
		for (int nKeyIndex = 0; nKeyIndex < nKeyCountCU; nKeyIndex++)
		{
			oComponentsCU.EnumerateKeys(nKeyIndex, strKeyNameCU, strClassNameCU);

			CString strCompFullPath = strKeyCompCU + _T("\\");
			strCompFullPath += strKeyNameCU;

			CRegistry oTmpComponent(HKEY_LOCAL_MACHINE);
			oTmpComponent.Open(strCompFullPath, regPermissions);

			int nValCount = oTmpComponent.GetNumberOfValues();

			for (int nVals = 0; nVals < nValCount; nVals++)
			{
				CRegistry::KeyValueTypes value_type;
				CString strName;
				DWORD dwDataSize = oTmpComponent.GetValueDataSize();
				LPBYTE data = new BYTE[dwDataSize];
				::ZeroMemory(data, dwDataSize);
				oTmpComponent.EnumerateValues(nVals, strName, value_type, data, dwDataSize);

				if (StrCmpI(strName, szRes) == 0)
				{
					CString strData = (TCHAR*)data;
					strData.Replace(_T("?"), _T(":"));
					if ((IsInSharedDlls(strData) == FALSE) && (IsStringInList(strData, &arrSharedPaths) == FALSE))
					{
						if (::PathIsDirectory(strData))
						{
							if (IsStringInList(strData, &arrSharedPaths) == FALSE)
							{
								if (nValCount == 1)//because if there are more that file/module is shared
								{
									//PutFoldersForSearch(strData);
									if (IsStringInList(strData, &arrValidPaths) == FALSE)
										arrValidPaths.Add(strData);
								}
								else
								{
									INT_PTR nCount = arrValidPaths.GetCount();
									for (INT_PTR i = 0; i < nCount; i++)
									{
										if (strData.CompareNoCase(arrValidPaths.GetAt(i)) == 0)
										{
											arrValidPaths.RemoveAt(i);
											nCount--;
											i--;
										}

									}
									arrSharedPaths.Add(strData);
								}
							}
						}
						else
						{
							::PathRemoveFileSpec(strData.GetBuffer());
							strData.ReleaseBuffer();
							if (::PathIsDirectory(strData))
							{
								if (IsStringInList(strData, &arrSharedPaths) == FALSE)
								{
									if (nValCount == 1)//because if there are more that file/module is shared
									{
										if (IsStringInList(strData, &arrValidPaths) == FALSE)
											arrValidPaths.Add(strData);
										//PutFoldersForSearch(strData);
									}

									else
									{
										INT_PTR nCount = arrValidPaths.GetCount();
										for (INT_PTR i = 0; i < nCount; i++)
										{
											if (strData.CompareNoCase(arrValidPaths.GetAt(i)) == 0)
											{
												arrValidPaths.RemoveAt(i);
												nCount--;
												i--;
											}

										}
										arrSharedPaths.Add(strData);
									}
								}
							}
						}

						m_arrMSIComponentNames.Add(strKeyNameCU);
					}
				}
				delete[] data;
			}
		}
		PutFoldersForSearch(arrValidPaths);
		return TRUE;
	}
	return FALSE;
}

BOOL CVSTrackFinder::SquashGUID(LPCWSTR in, LPWSTR out)
{
	DWORD i, n = 1;
	GUID guid;

	out[0] = 0;

	if (FAILED(CLSIDFromString((LPOLESTR)in, &guid)))
		return FALSE;

	for (i = 0; i < 8; i++)
		out[7 - i] = in[n++];
	n++;
	for (i = 0; i < 4; i++)
		out[11 - i] = in[n++];
	n++;
	for (i = 0; i < 4; i++)
		out[15 - i] = in[n++];
	n++;
	for (i = 0; i < 2; i++)
	{
		out[17 + i * 2] = in[n++];
		out[16 + i * 2] = in[n++];
	}
	n++;
	for (; i < 8; i++)
	{
		out[17 + i * 2] = in[n++];
		out[16 + i * 2] = in[n++];
	}
	out[32] = 0;
	return TRUE;
}

BOOL CVSTrackFinder::UnsquashGUID(LPCWSTR in, LPWSTR out)
{
	DWORD i, n = 0;

	out[n++] = '{';
	for (i = 0; i < 8; i++)
		out[n++] = in[7 - i];
	out[n++] = '-';
	for (i = 0; i < 4; i++)
		out[n++] = in[11 - i];
	out[n++] = '-';
	for (i = 0; i < 4; i++)
		out[n++] = in[15 - i];
	out[n++] = '-';
	for (i = 0; i < 2; i++)
	{
		out[n++] = in[17 + i * 2];
		out[n++] = in[16 + i * 2];
	}
	out[n++] = '-';
	for (; i < 8; i++)
	{
		out[n++] = in[17 + i * 2];
		out[n++] = in[16 + i * 2];
	}
	out[n++] = '}';
	out[n] = 0;
	return TRUE;
}

BOOL CVSTrackFinder::IsInSharedDlls(LPCTSTR pszPath)
{
	if (pszPath == NULL)
		return FALSE;

	LONG lRes;
	HKEY hKey;

	REGSAM regPermissions;
	if (m_b64Bit)
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	else
		regPermissions = KEY_READ | KEY_WOW64_32KEY;

	lRes = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SharedDlls"), NULL, regPermissions, &hKey);
	if (lRes == ERROR_SUCCESS)
	{
		DWORD dwShareCount = 0;
		DWORD dwShareCountSize = sizeof(DWORD);
		lRes = ::RegQueryValueEx(hKey, pszPath, NULL, NULL, (LPBYTE)(&dwShareCount), &dwShareCountSize);
		if (lRes == ERROR_SUCCESS)
		{
			if (dwShareCount > 1)
			{
				::RegCloseKey(hKey);
				return TRUE;
			}
		}
		::RegCloseKey(hKey);
	}

	return FALSE;
}

CString CVSTrackFinder::GetCurrentUserSID()
{
	// Declare variables
	CString sUsername = TEXT(""), sDomainName = TEXT(""), sBuffer, sResult = TEXT("");
	PSID pSid = NULL;
	BYTE bySidBuffer[256];
	DWORD dwSidSize = sizeof(bySidBuffer), dwDomainNameSize = 1024, dwUsernameSize = 1024;
	SID_NAME_USE sidType;

	// Initialize variables
	pSid = (PSID)bySidBuffer;
	dwSidSize = sizeof(bySidBuffer);

	// Get current username
	GetUserName(sBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
	sUsername = sBuffer.GetBuffer(dwUsernameSize);
	sBuffer.ReleaseBuffer();

	// Get the domain name
	LookupAccountName(NULL, sUsername, (PSID)pSid, &dwSidSize,
					  sBuffer.GetBuffer(dwDomainNameSize), &dwDomainNameSize, (PSID_NAME_USE)&sidType);
	sDomainName = sBuffer.GetBuffer(dwDomainNameSize);
	sBuffer.ReleaseBuffer();

	LPTSTR pszSid = NULL;
	ConvertSidToStringSid(pSid, &pszSid);
	sResult = pszSid;

	return sResult;
}

void CVSTrackFinder::GetAllPathsFromString(LPCTSTR pszStringPaths, CStringArray* arrPaths)
{
	CString strPaths = pszStringPaths;
	CString resToken, strCurrentPath;
	int curPos = 0;
	BOOL bIsFisrt = TRUE;

	resToken = strPaths.Tokenize(_T(":"), curPos);
	while (resToken != "")
	{
		if (bIsFisrt == FALSE)
		{
			int nSlashPos = resToken.ReverseFind(TEXT('\\'));
			CString strLastSlash = resToken.Left(nSlashPos);
			strCurrentPath += strLastSlash;
			arrPaths->Add(strCurrentPath);
			strCurrentPath.Empty();
		}

		strCurrentPath = resToken.Right(1);
		strCurrentPath += _T(":");

		resToken = strPaths.Tokenize(_T(":"), curPos);
		bIsFisrt = FALSE;
	}
}
BOOL CVSTrackFinder::IsStringMatchToListDrive(CStringArray* arrStringList, LPCTSTR pszString, int nMatchCounter)
{
	if ((arrStringList == NULL) || (nMatchCounter == 0) || (pszString == NULL))
		return FALSE;

	CStringArray arrSearchStrings;
	arrSearchStrings.Append(*arrStringList);

	int nCounter = 0;
	int curPos = 0;

	CString strPath = pszString;

	CString resToken = strPath.Tokenize(TEXT("\\"), curPos);
	while (resToken != TEXT(""))
	{
		int curPosition = 0;
		CString subToken;
		for (int i = 0; i < arrSearchStrings.GetCount(); i++)
		{
			if (StrCmpI(resToken, arrSearchStrings.GetAt(i)) == 0)
			{
				nCounter++;
				arrSearchStrings.RemoveAt(i);

				if (nCounter == nMatchCounter)
					return TRUE;

				break;
			}
		}
		resToken = strPath.Tokenize(TEXT("\\"), curPos); // Get next token:
	}

	return FALSE;
}

void CVSTrackFinder::MatchInDrivePath(LPCTSTR pFolder, CStringArray* arrNames, CStringArray* pResultArray,
									  CStringArray* arrIgnoreList, int nMatchCounter, int nDepthLevel, int nBreakCount)
{
	if (pFolder == NULL)
		return;

	if (_tcslen(pFolder) == 0)
		return;

	if (nDepthLevel == 0)
		return;

	if (nBreakCount != -1)
		if (pResultArray->GetCount() == nBreakCount)
			return;

	TCHAR* pszFullPathFileName = new TCHAR[4096];
	TCHAR* pszFilename = new TCHAR[4096];

	WIN32_FIND_DATA FileData = { 0 };
	DWORD dwSize = 0;

	StringCchPrintf(pszFullPathFileName, 4096, TEXT("%s\\*.*"), pFolder);

	HANDLE hSearch = FindFirstFile(pszFullPathFileName, &FileData);
	if (hSearch == INVALID_HANDLE_VALUE)
	{
		delete[] pszFullPathFileName;
		delete[] pszFilename;
		return;
	}

	do
	{
		if (nBreakCount != -1)
			if (pResultArray->GetCount() == nBreakCount)
				break;

		StringCchPrintf(pszFilename, 4096, TEXT("%s\\%s"), pFolder, FileData.cFileName);

		if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
		{

			if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{

				if (_tcscmp(FileData.cFileName, TEXT(".")) && _tcscmp(FileData.cFileName, TEXT("..")))
				{

					if (IsStringInList(FileData.cFileName, arrIgnoreList))
						continue;

					if (IsStringMatchToListDrive(arrNames, pszFilename, nMatchCounter))
					{
						pResultArray->Add(pszFilename);
						if (nBreakCount != -1)
							if (pResultArray->GetCount() == nBreakCount)
								break;

						continue;
					}

					if (nDepthLevel > 0)
					{
						MatchInDrivePath(pszFilename, arrNames, pResultArray, arrIgnoreList, nMatchCounter, nDepthLevel - 1, nBreakCount);
					}
					else
					{
						if (nDepthLevel < 0)
						{
							MatchInDrivePath(pszFilename, arrNames, pResultArray, arrIgnoreList, nMatchCounter, nDepthLevel, nBreakCount);
						}
					}
				}
			}
		}


	}
	while (FindNextFile(hSearch, &FileData));

	FindClose(hSearch);

	delete[] pszFullPathFileName;
	delete[] pszFilename;

	return;
}

/** Iterate a hierarchy of folders and files under a give n folder
 *
 * \param pFolder - parent folder,which child objects will be iterated
 * \param arrNames - array of strings that will be searched for
  * \param pResultArray  - array of all found paths
 * \param arrIgnoreList  - array of string that will be ignored while iterating
 * \param nDepthLevel  - depth for folder levels of scanning in the folder
 * \param nBreakCount - function break if results are equal to taht argument
 * \param bNoCompare - flag if while iterating the arrNames array will be checked or anything will be
 *						added to the result array (used to add anything under a matched folder)
 * \param bFolders - flag whether to found folders
 * \param bFiles  - flag whether to found files
 * \return void
 */
void CVSTrackFinder::FindAllDataInFolder(LPCTSTR pFolder, CStringArray* arrNames, CStringArray* pResultArray,
										 CStringArray* arrIgnoreList, int nDepthLevel, int nBreakCount,
										 BOOL bNoCompare, BOOL bMatchCase, BOOL bFolders, BOOL bFiles)
{
	if (pFolder == NULL)
		return;

	if (_tcslen(pFolder) == 0)
		return;

	if ((nDepthLevel == 0) && (bNoCompare == FALSE))/// if bNoCompare is TRUE DepthLevel no matter we want all data
		return;

	if (nBreakCount != -1)
		if (pResultArray->GetCount() == nBreakCount)
			return;

	TCHAR* pszFullPathFileName = new TCHAR[4096];
	TCHAR* pszSearchPath = new TCHAR[4096];
	TCHAR* pszFilename = new TCHAR[4096];

	WIN32_FIND_DATA FileData = { 0 };
	DWORD dwSize = 0;

	StringCchPrintf(pszFullPathFileName, 4096, TEXT("%s"), pFolder);
	::PathRemoveBackslash(pszFullPathFileName);

	StringCchCopy(pszSearchPath, 4096, pszFullPathFileName);
	StringCchCat(pszSearchPath, 4096, TEXT("\\*.*"));

	HANDLE hSearch = FindFirstFile(pszSearchPath, &FileData);
	if (hSearch == INVALID_HANDLE_VALUE)
	{
		delete[] pszFullPathFileName;
		delete[] pszSearchPath;
		delete[] pszFilename;
		return;
	}

	do
	{
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		{
			delete[] pszFullPathFileName;
			delete[] pszSearchPath;
			delete[] pszFilename;
			return;
		}

		if (nBreakCount != -1)
			if (pResultArray->GetCount() == nBreakCount)
				break;

		StringCchPrintf(pszFilename, 4096, TEXT("%s\\%s"), pszFullPathFileName, FileData.cFileName);

		if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
		{

			if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_tcscmp(FileData.cFileName, TEXT(".")) && _tcscmp(FileData.cFileName, TEXT("..")))
				{
					if (bNoCompare == FALSE) /// if bNoCompare is TRUE ingnore array no matter we want all data
						if (IsStringInList(FileData.cFileName, arrIgnoreList))
							continue;

					BOOL bNewSetNoCompare = bNoCompare;
					if (bFolders)
					{
						if ((bNoCompare) || (IsStringInList(FileData.cFileName, arrNames, bMatchCase)))
						{
							pResultArray->Add(pszFilename);
							if (nBreakCount != -1)
								if (pResultArray->GetCount() == nBreakCount)
									break;
							bNewSetNoCompare = TRUE; /// if it found a folder we want all into that folder
						}
					}
					if ((nDepthLevel > 0) || (bNewSetNoCompare))
						FindAllDataInFolder(pszFilename, arrNames, pResultArray, arrIgnoreList, nDepthLevel - 1,
											nBreakCount, bNewSetNoCompare, bMatchCase, bFolders, bFiles);
					else
						if (nDepthLevel < 0)
							FindAllDataInFolder(pszFilename, arrNames, pResultArray, arrIgnoreList, nDepthLevel,
												nBreakCount, bNewSetNoCompare, bMatchCase, bFolders, bFiles);
				}
			}
			else
			{
				if (bFiles)
				{
					CString strFile = FileData.cFileName;
					::PathRemoveExtension(strFile.GetBuffer());
					strFile.ReleaseBuffer();

					if ((bNoCompare) || (IsStringInList(strFile, arrNames, bMatchCase)))
						pResultArray->Add(pszFilename);

					if (nBreakCount != -1)
					{
						if (pResultArray->GetCount() == nBreakCount)
						{
							FindClose(hSearch);
							delete[] pszFullPathFileName;
							delete[] pszSearchPath;
							delete[] pszFilename;
							return;
						}
					}
				}
			}
		}


	}
	while (FindNextFile(hSearch, &FileData));
	FindClose(hSearch);

	delete[] pszFullPathFileName;
	delete[] pszSearchPath;
	delete[] pszFilename;

	return;
}

BOOL CVSTrackFinder::SearchCommonRegPaths()
{
	CStringArray arrFolders;
	GetAllTypeFoldersForSearch(arrFolders);

	if (m_arrSearchPattern.GetCount())
	{
		TCHAR  szFullRegPath[65536] = { 0 };

		int nDepth = 2, nBreak = 1;
		DWORD dwFoundCount = 0;

		RegDataList listFoundKeys;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), szFullRegPath, m_arrSearchPattern, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, TRUE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), szFullRegPath, m_arrSearchPattern, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, TRUE, TRUE);

		if (m_strMSIGUID.IsEmpty() == FALSE)
		{
			CStringArray arrGUID;
			arrGUID.Add(m_strMSIGUID);
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"));
			SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), szFullRegPath, arrGUID, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), szFullRegPath, arrGUID, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);
		}


		//Search also for folders if no uninstall key is found
		//this is not tested in free version!
		if (listFoundKeys.GetCount() == 0)
		{
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"));
			SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), szFullRegPath, arrFolders, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE, TRUE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), szFullRegPath, arrFolders, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE, TRUE);
		}
		////////////////////////////

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\MenuOrder\\Start Menu2\\Programs"));
		SearchRegistry(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\MenuOrder\\Start Menu2\\Programs"), szFullRegPath, m_arrSearchPattern, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, nBreak, TRUE, FALSE, FALSE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Management\\ARPCache"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Management\\ARPCache"), szFullRegPath, m_arrSearchPattern, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, nBreak, TRUE, FALSE, FALSE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\RegisteredApplications"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\RegisteredApplications"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Clients"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Clients"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);


		if (m_uninModerateMode == uninAdvancedMode)
		{
			BOOL bWinBits = Is64BitWindows();
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Shared Tools\\MSConfig\\startupreg"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Shared Tools\\MSConfig\\startupreg"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, bWinBits, 1, -1, TRUE, FALSE, FALSE, FALSE);
		}

		if (m_arrStartMenuFileNames.GetCount() > 0)
		{
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\Software\\Microsoft\\Tracing"));
			SearchRegistry(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Tracing"), szFullRegPath, m_arrStartMenuFileNames, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Tracing"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("Software\\Microsoft\\Tracing"), szFullRegPath, m_arrStartMenuFileNames, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);

		}
		else
		{
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\Software\\Microsoft\\Tracing"));
			SearchRegistry(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Tracing"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Tracing"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("Software\\Microsoft\\Tracing"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);

		}

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_APP_PATHS_KEY);
		SearchRegistry(HKEY_LOCAL_MACHINE, R_APP_PATHS_KEY, szFullRegPath, arrFolders, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);

		POSITION pos = listFoundKeys.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listFoundKeys.GetNext(pos);

			if (StrCmpI(temp->strFullKeyPath, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_APP_PATHS_KEY) != 0)
			{
				PREGNODE pNode = NULL;
				if (m_b64Bit)
					pNode = m_pTree->CreateKey(temp->strFullKeyPath, Key64Bit);
				else
					pNode = m_pTree->CreateKey(temp->strFullKeyPath, Key32Bit);

				if (pNode)
					pNode->eOperation = RegCreated;
			}

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_RUN_KEY);
		SearchRegistry(HKEY_CURRENT_USER, R_RUN_KEY, szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, FALSE, FALSE);
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_RUN_KEY);
		SearchRegistry(HKEY_LOCAL_MACHINE, R_RUN_KEY, szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, FALSE, FALSE);

		CStringArray arrTemp;
		arrTemp.Add(m_strAppName);
		arrTemp.Add(m_strKeyName);
		arrTemp.Add(m_strAppNameAlpha);
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Compatibility Assistant\\Persisted"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Compatibility Assistant\\Persisted"),
					   szFullRegPath, arrTemp, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, FALSE, FALSE);



		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;
	}

	//Search for removed folders in registry
	if (arrFolders.GetCount())
	{
		try
		{
			TCHAR  szFullRegPath[65536] = { 0 };

			int nDepth = 2;
			DWORD dwFoundCount = 0;

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_RUN_KEY);
			SearchRegistry(HKEY_CURRENT_USER, R_RUN_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_RUN_KEY);
			SearchRegistry(HKEY_LOCAL_MACHINE, R_RUN_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_WIN_FIREWALL);
			SearchRegistry(HKEY_LOCAL_MACHINE, R_WIN_FIREWALL, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_RUNSERVICES_KEY);
			SearchRegistry(HKEY_CURRENT_USER, R_RUNSERVICES_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_RUNSERVICES_KEY);
			SearchRegistry(HKEY_LOCAL_MACHINE, R_RUNSERVICES_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_RUNSERVICESONCE_KEY);
			SearchRegistry(HKEY_CURRENT_USER, R_RUNSERVICESONCE_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_RUNSERVICESONCE_KEY);
			SearchRegistry(HKEY_LOCAL_MACHINE, R_RUNSERVICESONCE_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_EXPLORER_RUN_KEY);
			SearchRegistry(HKEY_CURRENT_USER, R_EXPLORER_RUN_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\")R_EXPLORER_RUN_KEY);
			SearchRegistry(HKEY_LOCAL_MACHINE, R_EXPLORER_RUN_KEY, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_MUI_CACHE);
			SearchRegistry(HKEY_CURRENT_USER, R_MUI_CACHE, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\")R_HELP_KEYS);
			SearchRegistry(HKEY_CURRENT_USER, R_HELP_KEYS, szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, TRUE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags"));
			SearchRegistry(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags"), szFullRegPath, arrFolders, m_pTree,
						   &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers"), szFullRegPath, arrFolders, m_pTree,
						   &dwFoundCount, NULL, m_b64Bit, 1, -1, FALSE, TRUE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Compatibility Assistant\\Persisted"));
			SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Compatibility Assistant\\Persisted"),
						   szFullRegPath, arrFolders, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, TRUE, FALSE, FALSE);

			if (m_uninModerateMode == uninAdvancedMode)
			{
				BOOL bWinBits = Is64BitWindows();
				RegDataList listFoundStartUpKeys;
				dwFoundCount = 0;
				StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Shared Tools\\MSConfig\\startupreg"));
				SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Shared Tools\\MSConfig\\startupreg"), szFullRegPath, arrFolders, &listFoundStartUpKeys, &dwFoundCount, NULL, bWinBits, 2, -1, FALSE, FALSE, TRUE, FALSE);


				POSITION pos = listFoundStartUpKeys.GetHeadPosition();
				while (pos != NULL)
				{
					FoundRegData* temp = (FoundRegData*)listFoundStartUpKeys.GetNext(pos);

					PREGNODE pNode = NULL;
					if (bWinBits)
						pNode = m_pTree->CreateKey(temp->strFullKeyPath, Key64Bit);
					else
						pNode = m_pTree->CreateKey(temp->strFullKeyPath, Key32Bit);

					if (pNode)
						pNode->eOperation = RegCreated;

					if (temp->dwDataSize)
						delete[] temp->pbtData;

					delete temp;
				}
			}


			CStringArray arrExactFolders;
			GetExactFoldersForSearch(arrExactFolders);

			REGSAM regPermissions;
			eRegWOW6432Type eKeyWOW;
			if (m_b64Bit)
			{
				eKeyWOW = Key64Bit;
				regPermissions = KEY_READ | KEY_WOW64_64KEY;
			}
			else
			{
				eKeyWOW = Key32Bit;
				regPermissions = KEY_READ | KEY_WOW64_32KEY;
			}


			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SharedDLLs"));
			//SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\Folders"), szFullRegPath, arrExactFolders, m_pTree, &dwFoundCount, NULL, b64bit, -1, -1, FALSE, TRUE, FALSE, FALSE);

			CString strSharedDllsPath = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SharedDLLs");
			CRegistry oSharedDlls(HKEY_LOCAL_MACHINE);
			if (oSharedDlls.Open(strSharedDllsPath, regPermissions))
			{

				//if there are more values then the component is shared
				int nValCount = oSharedDlls.GetNumberOfValues();
				for (int nVals = 0; nVals < nValCount; nVals++)
				{
					CRegistry::KeyValueTypes value_type;
					CString strName;
					//DWORD  dwDataType = 0; 

					DWORD dwDataSize = oSharedDlls.GetValueDataSize();
					LPBYTE data = new BYTE[dwDataSize];
					::ZeroMemory(data, dwDataSize);
					oSharedDlls.EnumerateValues(nVals, strName, value_type, data, dwDataSize);

					if (IsListInString(&arrExactFolders, strName, FALSE))
					{
						DWORD dwUsedBy = DWORD(*data);
						if (dwUsedBy == 1)
							m_pTree->SetKeyVal(szFullRegPath, strName, DWORD(value_type), data, dwDataSize, 0, NULL, 0, eKeyWOW);
					}

					delete[] data;
				}
			}

			return TRUE;

		}
		catch (...)
		{
			return FALSE;
		}
	}

	return FALSE;
}

BOOL CVSTrackFinder::SearchCmnPtrnInRegKeySoftware()
{
	BOOL bFoundInSoftware = FALSE;
	//Search for pattern - Key and Display names with and w/o spaces
	if (m_arrSearchPattern.GetCount())
	{
		CStringArray arrIgnore;
		arrIgnore.Add(_T("Classes"));
		arrIgnore.Add(_T("WOW6432Node"));

		int nDepth = 2, nBreak = 1;

		TCHAR  szFullRegPath[65536] = { 0 };
		szFullRegPath[0] = 0;

		DWORD dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE, FALSE, FALSE);
		if (dwFoundCount)
		{
			bFoundInSoftware = TRUE;
			CString strFullPath = TEXT("\\");
			strFullPath += szFullRegPath;

			MarkFoundParentKeyAsCreated(strFullPath, &arrIgnore);
		}

		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE"), szFullRegPath, m_arrSearchPattern, m_pTree, &dwFoundCount, &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE, FALSE, FALSE);
		if (dwFoundCount)
		{
			bFoundInSoftware = TRUE;
			CString strFullPath = TEXT("\\");
			strFullPath += szFullRegPath;

			MarkFoundParentKeyAsCreated(strFullPath, &arrIgnore);
		}

	}

	return bFoundInSoftware;
}

void CVSTrackFinder::InitCommonSearchPatterns()
{
	if (m_parrFUProgNames)
	{
		for (int i = 0; i < m_parrFUProgNames->GetCount(); i++)
		{
			CString strName = m_parrFUProgNames->GetAt(i);
			AddSearchPattern(strName);

			//Remove whitespaces
			CString strAppNameNoWhiteSpaces(strName);
			strAppNameNoWhiteSpaces.Remove(_T(' '));
			AddSearchPattern(strAppNameNoWhiteSpaces);
		}
	}
	else
	{
		//Check if Display name is not forbidden and add it for search
		AddSearchPattern(m_strAppName);

		//Remove whitespaces
		CString strAppNameNoWhiteSpaces(m_strAppName);
		strAppNameNoWhiteSpaces.Remove(_T(' '));
		AddSearchPattern(strAppNameNoWhiteSpaces);
	}

	CString strSource = m_strAppName;
	for (int nCount = 0; nCount < strSource.GetLength(); nCount++)
	{
		if ((IsCharAlpha(strSource.GetAt(nCount)) == FALSE) && (strSource.GetAt(nCount) != TEXT(' ')))
		{
			strSource.Delete(nCount);
			nCount--;
		}
	}
	strSource.Trim(TEXT(" "));
	m_strAppNameAlpha = strSource;

	if (m_uninModerateMode != uninSafeMode)
	{
		CString strAppNameNoBrackets(m_strAppName);
		if (RemoveLastBrackets(strAppNameNoBrackets))
			AddSearchPattern(strAppNameNoBrackets);
	}

	if (m_uninModerateMode == uninAdvancedMode)
		AddSearchPattern(m_strDispIconFileName);

	//if(m_uninModerateMode == uninAdvancedMode)
	//	AddSearchPattern(m_strAppNameAlpha);

	/*if(m_uninModerateMode == uninAdvancedMode)
	{
		CStringArray arrCurNames;
		ExtractPureStrings(m_strAppName, arrCurNames);
		for(int nIndex = 0; nIndex<arrCurNames.GetCount(); nIndex++)
		{
			if(arrCurNames.GetAt(nIndex).GetLength()>3)
				AddSearchPattern(arrCurNames.GetAt(nIndex));
		}
	}*/

	//Check if Uninstall Key name is not forbidden and add it for search
	AddSearchPattern(m_strKeyName);

	//Remove whitespaces
	CString strKeyNameNoWhiteSpaces(m_strKeyName);
	strKeyNameNoWhiteSpaces.Remove(_T(' '));
	AddSearchPattern(strKeyNameNoWhiteSpaces);


	if (m_parrFUPaths)
	{
		for (int i = 0; i < m_parrFUPaths->GetCount(); i++)
		{
			CString strPath = m_parrFUPaths->GetAt(i);
			CString strLastFolder;
			GetLastFolderName(&strPath, &strLastFolder);
			if (strLastFolder.CompareNoCase(TEXT("Adobe")) == 0)
				m_parrFUPaths->SetAt(i, TEXT(""));
			//Add last folder name of install location as pattern for search
			if (IsFolderAllowedForSearch(strPath))
			{
				CString strPattern;
				GetLastFolderName(&strPath, &strPattern);
				if (IsStringOnlyDigits(strPattern) == FALSE)
				{
					AddSearchPattern(strPattern);
					//Remove whitespaces and add last folder name
					strPattern.Remove(_T(' '));
					AddSearchPattern(strPattern);
				}

				if (m_uninModerateMode == uninAdvancedMode)
					UsePublisherDataIfSingleApp(m_strInstallLocation);
			}

			//Put install location for search
			PutFoldersForSearch(strPath);
			AddParentFolderAsFound(strPath);
		}
	}
	else
	{
		CString strLastFolder;
		GetLastFolderName(&m_strInstallLocation, &strLastFolder);
		if (strLastFolder.CompareNoCase(TEXT("Adobe")) == 0)
			m_strInstallLocation.Empty();
		//Add last folder name of install location as pattern for search
		if (IsFolderAllowedForSearch(m_strInstallLocation))
		{
			CString strPattern;
			GetLastFolderName(&m_strInstallLocation, &strPattern);
			if (IsStringOnlyDigits(strPattern) == FALSE)
			{
				AddSearchPattern(strPattern);
				//Remove whitespaces and add last folder name
				strPattern.Remove(_T(' '));
				AddSearchPattern(strPattern);
			}

			if (m_uninModerateMode == uninAdvancedMode)
				UsePublisherDataIfSingleApp(m_strInstallLocation);
		}

		//Put install location for search
		PutFoldersForSearch(m_strInstallLocation);
		AddParentInstallLocAsFound(m_strInstallLocation);
	}

	//Put all paths in uninstall string for search. 
	//Because it may cause false positives,as in the case with ADobe Creative Cloud, we could use it only if there is no Install Location
	if (m_strInstallLocation.IsEmpty())
	{
		CStringArray arrPaths;
		GetAllPathsFromString(m_strUninstString, &arrPaths);
		PutFoldersForSearch(arrPaths);
	}

	if (m_strKeyName.Find(TEXT("Steam")) == -1) // use icon folder only if it is not a steam app/game
	{
		if (m_strDisplayIconPath.IsEmpty() == FALSE)
			PutFoldersForSearch(m_strDisplayIconPath);
	}

}

void CVSTrackFinder::InitMSIInfo()
{
	if (m_strMSIGUID.GetLength() == 0)
	{
		//Try to find MSI GUID
		CStringArray arrFolders;
		GetAllTypeFoldersForSearch(arrFolders);
		if (arrFolders.GetCount() || (m_strAppName.IsEmpty() == FALSE) || (m_parrFUProgNames != NULL))
		{
			if (m_parrFUProgNames)
			{
				for (int i = 0; i < m_parrFUProgNames->GetCount(); i++)
				{
					CString strName = m_parrFUProgNames->GetAt(i);
					if (FindMSIGUIDFromProductsKey(arrFolders, strName) == FALSE)
						FindMSIGUIDFromComponentsKey(arrFolders);
					else
						break;
				}
			}
			else
			{
				if (FindMSIGUIDFromProductsKey(arrFolders, m_strAppName) == FALSE)
					FindMSIGUIDFromComponentsKey(arrFolders);
			}
		}
	}

	if (m_strMSIGUID.GetLength() != 0)
	{
		UninstallInfo oInfo;
		if (FindMSIUInfoFromUninstallKey(m_strMSIGUID, &oInfo) == FALSE)
			return;

		if (m_strAppName.GetLength() == 0)
			m_strAppName = oInfo.m_strAppName;
		AddSearchPattern(oInfo.m_strAppName);

		if (oInfo.m_strInstallLocation.GetLength())
			PutFoldersForSearch(oInfo.m_strInstallLocation);
	}

	PutFoldersOfMSIAppForSearch();
}

BOOL CVSTrackFinder::SearchMatchAppNameInRegPath()
{
	BOOL bFound = FALSE;

	CStringArray arrSearch;
	CString resToken;


	CStringArray arrIgnore;
	arrIgnore.Add(_T("Classes"));
	arrIgnore.Add(_T("WOW6432Node"));

	if (m_parrFUProgNames)
	{
		for (int i = 0; i < m_parrFUProgNames->GetCount(); i++)
		{
			int curPos = 0;
			CString strName = m_parrFUProgNames->GetAt(i);
			resToken = strName.Tokenize(_T(" "), curPos);
			while (resToken != "")
			{
				arrSearch.Add(resToken);
				resToken = strName.Tokenize(_T(" "), curPos);
			}
		}
	}
	else
	{
		int curPos = 0;
		resToken = m_strAppName.Tokenize(_T(" "), curPos);
		while (resToken != "")
		{
			arrSearch.Add(resToken);
			resToken = m_strAppName.Tokenize(_T(" "), curPos);
		}
	}

	if (arrSearch.GetCount())
	{
		TCHAR* pszSearchedRegPath = new TCHAR[65536];
		pszSearchedRegPath[0] = 0;
		TCHAR  szFullRegPath[65536] = { 0 };

		int nDepth = 3, nBreak = 1;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE"));
		DWORD dwFoundCount = 0;
		SearchInRegistryPath(HKEY_CURRENT_USER, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrSearch, m_pTree, &dwFoundCount, (int)arrSearch.GetCount(), &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);
		if (dwFoundCount)
		{
			bFound = TRUE;
			CString strFullPath = TEXT("\\");
			strFullPath += szFullRegPath;

			MarkFoundParentKeyAsCreated(strFullPath, &arrIgnore);
		}


		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE"));

		dwFoundCount = 0;
		pszSearchedRegPath[0] = 0;

		SearchInRegistryPath(HKEY_LOCAL_MACHINE, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrSearch, m_pTree, &dwFoundCount, (int)arrSearch.GetCount(), &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);
		if (dwFoundCount)
		{
			bFound = TRUE;
			CString strFullPath = TEXT("\\");
			strFullPath += szFullRegPath;

			MarkFoundParentKeyAsCreated(strFullPath, &arrIgnore);
		}

		delete[] pszSearchedRegPath;
	}

	return bFound;
}

BOOL CVSTrackFinder::SearchMatchKeyNameInRegPath()
{
	BOOL bFound = FALSE;

	CStringArray arrSearch;
	CString resToken;
	int curPos = 0;

	CStringArray arrIgnore;
	arrIgnore.Add(_T("Classes"));
	arrIgnore.Add(_T("WOW6432Node"));

	resToken = m_strKeyName.Tokenize(_T(" "), curPos);
	while (resToken != "")
	{
		arrSearch.Add(resToken);
		resToken = m_strKeyName.Tokenize(_T(" "), curPos);
	}

	if (arrSearch.GetCount())
	{

		TCHAR* pszSearchedRegPath = new TCHAR[65536];
		pszSearchedRegPath[0] = 0;
		TCHAR  szFullRegPath[65536] = { 0 };

		int nDepth = 3, nBreak = 1;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE"));
		DWORD dwFoundCount = 0;
		SearchInRegistryPath(HKEY_CURRENT_USER, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrSearch, m_pTree, &dwFoundCount, (int)arrSearch.GetCount(), &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);
		if (dwFoundCount)
		{
			bFound = TRUE;
			CString strFullPath = TEXT("\\");
			strFullPath += szFullRegPath;

			MarkFoundParentKeyAsCreated(strFullPath, &arrIgnore);
		}

		pszSearchedRegPath[0] = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE"));
		dwFoundCount = 0;

		SearchInRegistryPath(HKEY_LOCAL_MACHINE, _T("SOFTWARE"), szFullRegPath, pszSearchedRegPath, arrSearch, m_pTree, &dwFoundCount, (int)arrSearch.GetCount(), &arrIgnore, m_b64Bit, nDepth, nBreak, TRUE);
		if (dwFoundCount)
		{
			bFound = TRUE;
			CString strFullPath = TEXT("\\");
			strFullPath += szFullRegPath;

			MarkFoundParentKeyAsCreated(strFullPath, &arrIgnore);
		}

		delete[] pszSearchedRegPath;
	}

	return bFound;
}

BOOL CVSTrackFinder::SearchInRegKeyClasses()
{
	REGSAM regPermissions;
	eRegWOW6432Type eKeyWOW;
	if (m_b64Bit)
	{
		eKeyWOW = Key64Bit;
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	}
	else
	{
		eKeyWOW = Key32Bit;
		regPermissions = KEY_READ | KEY_WOW64_32KEY;
	}

	CStringArray arrFolders;
	GetAllTypeFoldersForSearch(arrFolders);

	//Search for removed folders in registry
	if (arrFolders.GetCount())
	{
		//Search for file extension associations
		CStringArray arrIgnoreROOT;
		arrIgnoreROOT.Add(_T("WOW6432Node"));
		arrIgnoreROOT.Add(_T("CLSID"));
		arrIgnoreROOT.Add(_T("Interface"));
		arrIgnoreROOT.Add(_T("Applications"));
		arrIgnoreROOT.Add(_T("TypeLib"));
		arrIgnoreROOT.Add(_T("AppID"));
		arrIgnoreROOT.Add(_T("Installer"));
		arrIgnoreROOT.Add(_T("Mime"));
		arrIgnoreROOT.Add(_T("SystemFileAssociations"));
		arrIgnoreROOT.Add(_T("Record"));
		arrIgnoreROOT.Add(_T("Media Type"));

		TCHAR szFullRegPath[65536] = { 0 };

		RegDataList listRegDataExt;
		DWORD dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes"), szFullRegPath, arrFolders, &listRegDataExt, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, FALSE);

		if (m_arrSearchPattern.GetCount())
		{
			dwFoundCount = 0;
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes"), szFullRegPath, m_arrSearchPattern, &listRegDataExt, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, TRUE);
		}

		/////
		RegDataList listRegDataClassessApp;
		CStringArray arrIgnore;
		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Applications"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\Applications"), szFullRegPath, arrFolders, &listRegDataClassessApp, &dwFoundCount, &arrIgnore, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, FALSE);

		POSITION pos = listRegDataClassessApp.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listRegDataClassessApp.GetNext(pos);
			CString strRet;
			GetSeparatedKey(temp->strFullKeyPath, 7, &strRet);
			if (StrCmpI(strRet, _T("Shell")) == 0)
			{
				GetSeparatedKey(temp->strFullKeyPath, 9, &strRet);
				if (StrCmpI(strRet, _T("Command")) == 0)
				{
					//if the extension is associated to open the file
					//then add for deleting the wholse ext key
					CString strOpenCmd;
					GetSeparatedKey(temp->strFullKeyPath, 8, &strOpenCmd);
					if (StrCmpI(strOpenCmd, _T("Open")) == 0)
					{
						//get till 6th sub key ... it must be the appname.exe
						//it is REGISTRY\(HKLM|HKCU)\SOFTWARE\Classes\Application...\shell\open\command
						CString strFullKeyPath;
						GetSubKey(temp->strFullKeyPath, 7, &strFullKeyPath);
						PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
						if (pNode)
							pNode->eOperation = RegCreated;
					}
					else
						m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);
				}
			}
			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}
		////////////////////////////


		//SearchInHKCUExplorerFileExt(&listRegDataExt, eKeyWOW, regPermissions);		

		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes"), szFullRegPath, arrFolders, &listRegDataExt, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, FALSE);

		if (m_arrSearchPattern.GetCount())
		{
			dwFoundCount = 0;
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes"));
			SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes"), szFullRegPath, m_arrSearchPattern, &listRegDataExt, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, TRUE);
		}


		SearchInHKCUExplorerFileExt(&listRegDataExt, eKeyWOW, regPermissions);


		//list these keys for deleting
		RegDataList listSpecRegData;
		CStringArray arrFileTypes;
		pos = listRegDataExt.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listRegDataExt.GetNext(pos);
			CString strRet;
			GetSeparatedKey(temp->strFullKeyPath, 6, &strRet);
			if (StrCmpI(strRet, _T("Shell")) == 0)
			{
				GetSeparatedKey(temp->strFullKeyPath, 8, &strRet);
				if (StrCmpI(strRet, _T("Command")) == 0)
				{
					//if the extension is associated to open the file
					//then add for deleting the wholse ext key
					CString strOpenCmd;
					GetSeparatedKey(temp->strFullKeyPath, 7, &strOpenCmd);
					if (StrCmpI(strOpenCmd, _T("Open")) == 0)
					{
						//get till 6th sub key ...
						//it is REGISTRY\(HKLM|HKCU)\SOFTWARE\Classes\...\shell\open\command
						CString strFullKeyPath;
						GetSubKey(temp->strFullKeyPath, 6, &strFullKeyPath);
						PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
						if (pNode)
							pNode->eOperation = RegCreated;
						//add the parent key name for listing later
						//and to get more info for searching later
						GetSeparatedKey(temp->strFullKeyPath, 5, &strRet);
						if (IsStringInList(strRet, &arrFileTypes) == FALSE)
							arrFileTypes.Add(strRet);
					}
					else
					{
						//get till 8th sub key ...
						//it is (REGISTRY\HKLM|HKCU)\SOFTWARE\Classes\...\shell\EditInPicassa\command
						CString strFullKeyPath;
						GetSubKey(temp->strFullKeyPath, 8, &strFullKeyPath);
						PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
						if (pNode)
							pNode->eOperation = RegCreated;
					}
				}
				else
					//Add exact key for listing later
					m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);
			}
			else
			{
				if (StrCmpI(strRet, _T("defaultIcon")) == 0)
				{
					GetSeparatedKey(temp->strFullKeyPath, 5, &strRet);
					//add the parent key name for listing
					//later and to get more info for searching later
					if (IsStringInList(strRet, &arrFileTypes) == FALSE)
						arrFileTypes.Add(strRet);

					//get till 6th sub key ...
					//it is REGISTRY\(HKLM|HKCU)\SOFTWARE\Classes\...\defaultIcon
					CString strFullKeyPath;
					GetSubKey(temp->strFullKeyPath, 6, &strFullKeyPath);
					PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
					if (pNode)
						pNode->eOperation = RegCreated;
				}
				else
				{
					//if((IsStringInList(temp->strKeyName,m_arrSearchPattern,TRUE)||(IsStringInList(temp->strValueName,m_arrSearchPattern,TRUE)||(IsStringInList(temp->strKeyName,m_arrSearchPattern,TRUE))
					//{
					FoundRegData* regData = new FoundRegData;
					regData->strKeyName = temp->strKeyName;
					regData->strFullKeyPath = temp->strFullKeyPath;
					regData->strValueName = temp->strValueName;
					regData->dwType = temp->dwType;
					regData->pbtData = new BYTE[temp->dwDataSize];
					CopyMemory(regData->pbtData, temp->pbtData, temp->dwDataSize);
					regData->dwDataSize = temp->dwDataSize;

					listSpecRegData.AddTail(regData);
					//}
				}
			}

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}

		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;

		//Find all extensions
		//RegDataList listFoundExtensionKeys;
		CRegistry oRegExtCU(HKEY_CURRENT_USER);
		oRegExtCU.Open(_T("SOFTWARE\\Classes"), regPermissions);
		int nKeyCount = oRegExtCU.GetNumberOfSubkeys();
		CString strKeyName, strClassName;
		for (int nKeyIndex = 0; nKeyIndex < nKeyCount; nKeyIndex++)
		{
			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			oRegExtCU.EnumerateKeys(nKeyIndex, strKeyName, strClassName);
			if (StrCmpI(strKeyName.Left(1), _T(".")) == 0)
			{
				CString strDefault, strPureKeyExt = strKeyName;
				CRegistry oFile(HKEY_CURRENT_USER);
				strKeyName.Insert(0, _T("SOFTWARE\\Classes\\"));
				oFile.Open(strKeyName, regPermissions);
				oFile.GetValue(NULL, strDefault);
				if (IsStringInList(strDefault, &arrFileTypes))
				{
					CString strFullKeyPath(_T("REGISTRY\\HKEY_CURRENT_USER\\"));
					strFullKeyPath += strKeyName;
					PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
					if (pNode)
						pNode->eOperation = RegCreated;


					if (m_uninModerateMode == uninAdvancedMode)
					{
						CString strValContentType;
						if (oFile.GetValue(TEXT("Content Type"), strValContentType))
						{
							CRegistry oMIME(HKEY_CURRENT_USER);
							CString strFullMIMEKey = TEXT("SOFTWARE\\Classes\\MIME\\Database\\Content Type\\");
							strFullMIMEKey += strValContentType;
							if (oMIME.Open(strFullMIMEKey, regPermissions))
							{
								CString strFullKeyPath(_T("REGISTRY\\HKEY_CURRENT_USER\\"));
								strFullKeyPath += strFullMIMEKey;
								PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
								if (pNode)
									pNode->eOperation = RegCreated;
							}
						}

					}


				}
			}
		}

		CRegistry oRegExtLM(HKEY_LOCAL_MACHINE);
		oRegExtLM.Open(_T("SOFTWARE\\Classes"), regPermissions);
		nKeyCount = oRegExtLM.GetNumberOfSubkeys();
		for (int nKeyIndex = 0; nKeyIndex < nKeyCount; nKeyIndex++)
		{
			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			oRegExtLM.EnumerateKeys(nKeyIndex, strKeyName, strClassName);
			if (StrCmpI(strKeyName.Left(1), _T(".")) == 0)
			{
				CString strDefault;
				CRegistry oFile(HKEY_LOCAL_MACHINE);
				strKeyName.Insert(0, _T("SOFTWARE\\Classes\\"));
				oFile.Open(strKeyName, regPermissions);
				oFile.GetValue(NULL, strDefault);
				if (IsStringInList(strDefault, &arrFileTypes))
				{
					CString strFullKeyPath(_T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
					strFullKeyPath += strKeyName;
					PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
					if (pNode)
						pNode->eOperation = RegCreated;

					if (m_uninModerateMode == uninAdvancedMode)
					{
						CString strValContentType;
						if (oFile.GetValue(TEXT("Content Type"), strValContentType))
						{
							CRegistry oMIME(HKEY_LOCAL_MACHINE);
							CString strFullMIMEKey = TEXT("SOFTWARE\\Classes\\MIME\\Database\\Content Type\\");
							strFullMIMEKey += strValContentType;
							if (oMIME.Open(strFullMIMEKey, regPermissions))
							{
								CString strFullKeyPath(_T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
								strFullKeyPath += strFullMIMEKey;
								PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
								if (pNode)
									pNode->eOperation = RegCreated;
							}
						}


					}

				}//if(IsStringInList(strDefault, &arrFileTypes))
				else //as, the default value of the extension is not as ours found, we search for values with that extension in OpenWithProgIds subkey of the extenison
				{

					CString strSubekeyOpenWithProgIds = strKeyName + TEXT("\\OpenWithProgIds");
					CString strFullRegPath = TEXT("REGISTRY\\HKEY_LOCAL_MACHINE\\") + strKeyName;
					CString strFullRegPathOpenIds = strFullRegPath + TEXT("\\OpenWithProgIds");
					CRegistry oClasses(HKEY_LOCAL_MACHINE);
					if (oClasses.Open(strSubekeyOpenWithProgIds, regPermissions))
					{

						//if there are more values then the component is shared
						int nValCount = oClasses.GetNumberOfValues();
						for (int nVals = 0; nVals < nValCount; nVals++)
						{
							CRegistry::KeyValueTypes value_type;
							CString strName;
							//DWORD  dwDataType = 0; 

							DWORD dwDataSize = oClasses.GetValueDataSize();
							LPBYTE data = new BYTE[dwDataSize];
							::ZeroMemory(data, dwDataSize);
							oClasses.EnumerateValues(nVals, strName, value_type, data, dwDataSize);

							if (IsStringInList(strName, &arrFileTypes))
							{
								if (nValCount == 1 && oFile.GetNumberOfSubkeys() == 1)
								{
									//CString strFullKeyPath(_T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
									//strFullKeyPath += strFullMIMEKey;
									PREGNODE pNode = m_pTree->CreateKey(strFullRegPath, eKeyWOW);
									if (pNode)
										pNode->eOperation = RegCreated;
								}
								else
									m_pTree->SetKeyVal(strFullRegPathOpenIds, strName, DWORD(value_type), data, dwDataSize, 0, NULL, 0, eKeyWOW);
							}

							delete[] data;
						}
					}
				}
			}
		}

		//Add to the tree specific values in classess that ar eno extensions or empty keys extensions that should be deleted entirely 
		pos = listSpecRegData.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listRegDataExt.GetNext(pos);

			CString strKeyToTest = TEXT("\\"), strRet, strCurFullKeyPath, strKeyToOpen;
			GetSeparatedKey(temp->strFullKeyPath, 5, &strRet);
			strKeyToTest += temp->strFullKeyPath;
			PREGNODE pKey = m_pTree->GetKey(strKeyToTest);

			CRegistry oRegExplorerExt(HKEY_CURRENT_USER);
			GetSubKey(temp->strFullKeyPath, 6, &strCurFullKeyPath);
			GetRegKeyForOpen(strCurFullKeyPath, strKeyToOpen);
			if (oRegExplorerExt.Open(strKeyToOpen, regPermissions))
			{

				if ((StrCmpI(strRet.Left(1), _T(".")) == 0) && (oRegExplorerExt.GetNumberOfSubkeys() == 0))
				{
					if (pKey)
					{
						if (pKey->eOperation != RegCreated)
							pKey->eOperation = RegCreated;
					}
					else
					{
						CString strFullKeyPath;
						GetSubKey(temp->strFullKeyPath, 6, &strFullKeyPath);
						PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
						if (pNode)
							pNode->eOperation = RegCreated;
					}

				}
				else
				{
					if (pKey == NULL)
						m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);

				}
				oRegExplorerExt.Close();
			}
			else
			{
				if (pKey == NULL)
					m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);

			}


			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}
		////////////////////////////////////////////////////////////////////

		CStringArray arrCLSIDs, arrAppIDs, arrProgIDs, arrTypeLibs, arrVerIndProgID;
		RegDataList listCLSID;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\CLSID"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\CLSID"), szFullRegPath, arrFolders, &listCLSID, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, FALSE);

		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\CLSID"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\CLSID"), szFullRegPath, arrFolders, &listCLSID, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, FALSE);

		pos = listCLSID.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listCLSID.GetNext(pos);
			CString strRet;
			GetSeparatedKey(temp->strFullKeyPath, 7, &strRet);
			if ((StrCmpI(strRet, _T("InprocServer32")) == 0)
				|| (StrCmpI(strRet, _T("InprocServer")) == 0)
				|| (StrCmpI(strRet, _T("LocalServer")) == 0)
				|| (StrCmpI(strRet, _T("InprocHandler32")) == 0)
				|| (StrCmpI(strRet, _T("LocalServer32")) == 0))
			{
				//get till 7th sub key ...
				//it is REGISTRY\(HKLM|HKCU)\SOFTWARE\Classes\CLSID\....
				CString strFullKeyPath;
				GetSubKey(temp->strFullKeyPath, 7, &strFullKeyPath);
				PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				//strRet contains a CLSID like {1E54D648-B804-468d-BC78-4AFFED8E262F}
				GetSeparatedKey(temp->strFullKeyPath, 6, &strRet);
				arrCLSIDs.Add(strRet);
				strRet.Insert(0, _T("Software\\Classes\\CLSID\\"));

				CString strRoot;
				GetSeparatedKey(temp->strFullKeyPath, 2, &strRoot);

				HKEY hRoot;
				if (StrCmpI(strRoot, _T("HKEY_LOCAL_MACHINE")) == 0)
					hRoot = HKEY_LOCAL_MACHINE;
				else
					hRoot = HKEY_CURRENT_USER;

				HKEY hKey;
				if (::RegOpenKeyEx(hRoot, strRet, NULL, regPermissions, &hKey) == ERROR_SUCCESS)
				{
					TCHAR	szAppID[100] = { 0 };
					DWORD	dwDataSize = sizeof(szAppID);
					if (::RegQueryValueEx(hKey, TEXT("AppID"), NULL, NULL, (LPBYTE)(szAppID), &dwDataSize) == ERROR_SUCCESS)
					{
						if (_tcslen(szAppID) > 0)
						{
							arrAppIDs.Add(szAppID);

							CString strAppID = _T("Software\\Classes\\AppID\\");
							strAppID += szAppID;
							HKEY hSubKey;
							if (::RegOpenKeyEx(hRoot, strAppID, NULL, regPermissions, &hSubKey) == ERROR_SUCCESS)
							{
								strAppID.Insert(0, _T('\\'));
								strAppID.Insert(0, strRoot);
								strAppID.Insert(0, _T("REGISTRY\\"));
								PREGNODE pNode = m_pTree->CreateKey(strAppID, eKeyWOW);
								if (pNode)
									pNode->eOperation = RegCreated;

								RegCloseKey(hSubKey);
							}
						}
					}

					RegCloseKey(hKey);
				}

				CString strProgID(strRet);
				strProgID += _T("\\ProgID");
				if (::RegOpenKeyEx(hRoot, strProgID, NULL, regPermissions, &hKey) == ERROR_SUCCESS)
				{
					TCHAR	szProgID[100] = { 0 };
					DWORD	dwDataSize = sizeof(szProgID);
					if (::RegQueryValueEx(hKey, NULL, NULL, NULL, (LPBYTE)(szProgID), &dwDataSize) == ERROR_SUCCESS)
					{
						if (_tcslen(szProgID) > 0)
						{
							arrProgIDs.Add(szProgID);

							strProgID = _T("Software\\Classes\\");
							strProgID += szProgID;
							HKEY hSubKey;
							if (::RegOpenKeyEx(hRoot, strProgID, NULL, regPermissions, &hSubKey) == ERROR_SUCCESS)
							{
								strProgID.Insert(0, _T('\\'));
								strProgID.Insert(0, strRoot);
								strProgID.Insert(0, _T("REGISTRY\\"));
								PREGNODE pNode = m_pTree->CreateKey(strProgID, eKeyWOW);
								if (pNode)
									pNode->eOperation = RegCreated;

								RegCloseKey(hSubKey);
							}
						}
					}

					RegCloseKey(hKey);
				}

				CString strVerIndProgID(strRet);
				strVerIndProgID += _T("\\VersionIndependentProgID");
				if (::RegOpenKeyEx(hRoot, strVerIndProgID, NULL, regPermissions, &hKey) == ERROR_SUCCESS)
				{
					TCHAR	szProgID[100] = { 0 };
					DWORD	dwDataSize = sizeof(szProgID);
					if (::RegQueryValueEx(hKey, NULL, NULL, NULL, (LPBYTE)(szProgID), &dwDataSize) == ERROR_SUCCESS)
					{
						arrVerIndProgID.Add(szProgID);

						strVerIndProgID = _T("Software\\Classes\\");
						strVerIndProgID += szProgID;
						HKEY hSubKey;
						if (::RegOpenKeyEx(hRoot, strVerIndProgID, NULL, regPermissions, &hSubKey) == ERROR_SUCCESS)
						{
							strVerIndProgID.Insert(0, _T('\\'));
							strVerIndProgID.Insert(0, strRoot);
							strVerIndProgID.Insert(0, _T("REGISTRY\\"));
							PREGNODE pNode = m_pTree->CreateKey(strVerIndProgID, eKeyWOW);
							if (pNode)
								pNode->eOperation = RegCreated;

							RegCloseKey(hSubKey);
						}
					}

					RegCloseKey(hKey);
				}

				CString strTypeLib(strRet);
				strTypeLib += _T("\\TypeLib");
				if (::RegOpenKeyEx(hRoot, strTypeLib, NULL, regPermissions, &hKey) == ERROR_SUCCESS)
				{
					TCHAR	szTypeLib[100] = { 0 };
					DWORD	dwDataSize = sizeof(szTypeLib);
					if (::RegQueryValueEx(hKey, NULL, NULL, NULL, (LPBYTE)(szTypeLib), &dwDataSize) == ERROR_SUCCESS)
					{
						arrTypeLibs.Add(szTypeLib);

						strTypeLib = _T("Software\\Classes\\TypeLib\\");
						strTypeLib += szTypeLib;
						HKEY hSubKey;
						if (::RegOpenKeyEx(hRoot, strTypeLib, NULL, regPermissions, &hSubKey) == ERROR_SUCCESS)
						{
							strTypeLib.Insert(0, _T('\\'));
							strTypeLib.Insert(0, strRoot);
							strTypeLib.Insert(0, _T("REGISTRY\\"));
							PREGNODE pNode = m_pTree->CreateKey(strTypeLib, eKeyWOW);
							if (pNode)
								pNode->eOperation = RegCreated;

							RegCloseKey(hSubKey);
						}
					}
					RegCloseKey(hKey);
				}
			}
			else
			{
				if (StrCmpI(strRet, _T("Shell")) == 0)
				{
					CString strCommand;
					GetSeparatedKey(temp->strFullKeyPath, 9, &strCommand);
					if (StrCmpI(strCommand, _T("Command")) == 0)
					{
						CString strFullKeyPath;
						GetSubKey(temp->strFullKeyPath, 9, &strFullKeyPath);
						PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
						if (pNode)
							pNode->eOperation = RegCreated;
					}
				}
				if (StrCmpI(strRet, _T("DefaultIcon")) == 0)
					m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);


			}
			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;

		}
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;

		//Search for more TypeLibs
		RegDataList listAddTypelibs;
		CStringArray arrModFolders;
		arrModFolders.Copy(arrFolders);
		AddBackSlashToPaths(arrModFolders);


		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\TypeLib"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\TypeLib"), szFullRegPath, arrModFolders, &listAddTypelibs, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\TypeLib"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\TypeLib"), szFullRegPath, arrModFolders, &listAddTypelibs, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, FALSE);

		pos = listAddTypelibs.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listAddTypelibs.GetNext(pos);
			CString strTypeLib, strFullTypeLib;
			GetSeparatedKey(temp->strFullKeyPath, 6, &strTypeLib);

			if (NotInTheArray(arrTypeLibs, strTypeLib))
			{
				arrTypeLibs.Add(strTypeLib);

				//strTypeLib = _T("Software\\Classes\\TypeLib\\");
				//strTypeLib += szTypeLib;
				//strTypeLib.Insert(0, _T('\\'));
				//strTypeLib.Insert(0, strRoot);
				//strTypeLib.Insert(0, _T("REGISTRY\\"));
				GetSubKey(temp->strFullKeyPath, 7, &strFullTypeLib);

				PREGNODE pNode = m_pTree->CreateKey(strFullTypeLib, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;
			}

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}

		//Search for found CLSIDs 
		RegDataList listFoundCLSID;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Ext"));
		SearchRegistry(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Ext"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Ext"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Ext"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, &arrIgnoreROOT, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Internet Explorer\\Extensions"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Internet Explorer\\Extensions"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Internet Explorer\\Extensions"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Internet Explorer\\Extensions"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Internet Explorer\\Explorer Bars"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Internet Explorer\\Explorer Bars"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Internet Explorer\\Explorer Bars"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Internet Explorer\\Explorer Bars"), szFullRegPath, arrCLSIDs, &listFoundCLSID, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, FALSE);

		pos = listFoundCLSID.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listFoundCLSID.GetNext(pos);
			PREGNODE pNode = m_pTree->CreateKey(temp->strFullKeyPath, eKeyWOW);
			if (pNode)
				pNode->eOperation = RegCreated;

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell Extensions"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell Extensions"), szFullRegPath, arrCLSIDs, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell Extensions"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell Extensions"), szFullRegPath, arrCLSIDs, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Internet Explorer\\Toolbar"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Internet Explorer\\Toolbar"), szFullRegPath, arrCLSIDs, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, TRUE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Internet Explorer\\Toolbar"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Internet Explorer\\Toolbar"), szFullRegPath, arrCLSIDs, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, TRUE, TRUE, TRUE);

		///////////////////////////////////////////////////////////////////////
		//Find the rest of AppIDs

		RegDataList listAppID;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\AppID"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\AppID"), szFullRegPath, arrAppIDs, &listAppID, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, TRUE);

		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\AppID"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\AppID"), szFullRegPath, arrAppIDs, &listAppID, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, TRUE);

		pos = listAppID.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listAppID.GetNext(pos);
			PREGNODE pNode = m_pTree->CreateKey(temp->strFullKeyPath, eKeyWOW);
			if (pNode)
				pNode->eOperation = RegCreated;

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;

		///////////////////////////////////////////////////////////////////////
		//Find Interfaces

		RegDataList listInterFace;

		arrTypeLibs.Append(arrCLSIDs);

		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Interface"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\Interface"), szFullRegPath, arrTypeLibs, &listInterFace, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, TRUE);

		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Interface"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\Interface"), szFullRegPath, arrTypeLibs, &listInterFace, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, TRUE);

		pos = listInterFace.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listInterFace.GetNext(pos);
			CString strFullKeyPath;
			GetSubKey(temp->strFullKeyPath, 7, &strFullKeyPath);
			PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
			if (pNode)
				pNode->eOperation = RegCreated;

			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}



		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;
	}

	return TRUE;
}

void CVSTrackFinder::SearchForMSILeftovers()
{
	TCHAR szRes[50] = { 0 };
	if (SquashGUID(m_strMSIGUID, szRes))
	{
#ifdef _WIN64								
		eRegWOW6432Type eKeyWOW = Key64Bit;
		BOOL b64bit = TRUE;
#else
		eRegWOW6432Type eKeyWOW = Key32Bit;
		BOOL b64bit = FALSE;
#endif

		//MSI is 64bit on 64 and 32bit on 32 Windows
		REGSAM regPermissions = KEY_READ;

		////////////////////	
		//New Algo
		for (int nBitsType = 0; nBitsType < 2; nBitsType++)
		{
			if (nBitsType == 0)
			{
				eKeyWOW = Key64Bit;
				b64bit = TRUE;
				regPermissions = KEY_READ | KEY_WOW64_64KEY;
			}
			else
			{
				b64bit = FALSE;
				eKeyWOW = Key32Bit;
				regPermissions = KEY_READ | KEY_WOW64_32KEY;
			}


			HKEY hKey;
			CString strFullPath, strKeyProd;
			LONG lResult = 0;
			//We have default profile S-1-5-18 and USER SID
			for (int nProfiles = 0; nProfiles < 2; nProfiles++)
			{
				if (nProfiles == 0)
					strKeyProd = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Products\\");
				else
					strKeyProd = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\") + GetCurrentUserSID() + _T("\\Products\\");

				strKeyProd += szRes;
				lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strKeyProd, 0, regPermissions, &hKey);
				if (lResult == ERROR_SUCCESS)
				{
					strFullPath = strKeyProd;
					strFullPath.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
					PREGNODE pNode = m_pTree->CreateKey(strFullPath, eKeyWOW);
					if (pNode)
						pNode->eOperation = RegCreated;

					//try to open Patches key to find if there any
					strKeyProd += _T("\\Patches");
					HKEY hSubKey;
					lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strKeyProd, 0, regPermissions, &hSubKey);
					if (lResult == ERROR_SUCCESS)
					{
						//query all patches for a product
						TCHAR	szPatches[2048] = { 0 };
						DWORD	dwDataSize = sizeof(szPatches);
						lResult = ::RegQueryValueEx(hSubKey, _T("AllPatches"), NULL, NULL, (LPBYTE)(szPatches), &dwDataSize);
						if (lResult == ERROR_SUCCESS)
						{
							CStringArray arrPathces;
							ConvertMultySzToArray(szPatches, 2048, arrPathces);
							for (int i = 0; i < arrPathces.GetCount(); i++)
							{
								//check if patches are existing
								CString strPatchKey = strKeyProd;
								strPatchKey += arrPathces.GetAt(i);
								HKEY hKeyPatches;
								lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strPatchKey, 0, regPermissions, &hKeyPatches);
								if (lResult == ERROR_SUCCESS)
								{
									strPatchKey.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
									PREGNODE pNode = m_pTree->CreateKey(strPatchKey, eKeyWOW);
									if (pNode)
										pNode->eOperation = RegCreated;

									::RegCloseKey(hKeyPatches);
								}

								strPatchKey = _T("SOFTWARE\\Classes\\Installer\\Patches\\");
								strPatchKey += arrPathces.GetAt(i);
								lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strPatchKey, 0, regPermissions, &hKeyPatches);
								if (lResult == ERROR_SUCCESS)
								{
									strPatchKey.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
									PREGNODE pNode = m_pTree->CreateKey(strPatchKey, eKeyWOW);
									if (pNode)
										pNode->eOperation = RegCreated;

									::RegCloseKey(hKeyPatches);
								}
							}
						}
						::RegCloseKey(hSubKey);
					}
					::RegCloseKey(hKey);
				}
			}

			//check if a product (Product GUID squished) exists - HKEY_LOCAL_MACHINE
			strKeyProd = _T("SOFTWARE\\Classes\\Installer\\Products\\");
			strKeyProd += szRes;
			lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strKeyProd, 0, regPermissions, &hKey);
			if (lResult == ERROR_SUCCESS)
			{
				strKeyProd.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
				PREGNODE pNode = m_pTree->CreateKey(strKeyProd, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				::RegCloseKey(hKey);
			}

			//check if a product (Product GUID squished) exists - HKEY_CURRENT_USER
			strKeyProd = _T("Software\\Microsoft\\Installer\\Products\\");
			strKeyProd += szRes;
			lResult = ::RegOpenKeyEx(HKEY_CURRENT_USER, strKeyProd, 0, regPermissions, &hKey);
			if (lResult == ERROR_SUCCESS)
			{
				strKeyProd.Insert(0, _T("REGISTRY\\HKEY_CURRENT_USER\\"));
				PREGNODE pNode = m_pTree->CreateKey(strKeyProd, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				::RegCloseKey(hKey);
			}

			//check if a feature (Product GUID squished) exists - HKEY_LOCAL_MACHINE
			strKeyProd = _T("SOFTWARE\\Classes\\Installer\\Features\\");
			strKeyProd += szRes;
			lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strKeyProd, 0, regPermissions, &hKey);
			if (lResult == ERROR_SUCCESS)
			{
				strKeyProd.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
				PREGNODE pNode = m_pTree->CreateKey(strKeyProd, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				::RegCloseKey(hKey);
			}

			//check if a product (Product GUID squished) exists - HKEY_CURRENT_USER
			strKeyProd = _T("Software\\Microsoft\\Installer\\Features\\");
			strKeyProd += szRes;
			lResult = ::RegOpenKeyEx(HKEY_CURRENT_USER, strKeyProd, 0, regPermissions, &hKey);
			if (lResult == ERROR_SUCCESS)
			{
				strKeyProd.Insert(0, _T("REGISTRY\\HKEY_CURRENT_USER\\"));
				PREGNODE pNode = m_pTree->CreateKey(strKeyProd, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				::RegCloseKey(hKey);
			}

			//we already know component names so just check which one still exists
			for (int i = 0; i < m_arrMSIComponentNames.GetCount(); i++)
			{
				//We have default profile S-1-5-18 and USER SID
				for (int nProfiles = 0; nProfiles < 2; nProfiles++)
				{
					if (nProfiles == 0)
						strKeyProd = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Components\\");
					else
						strKeyProd = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\") + GetCurrentUserSID() + _T("\\Components\\");

					strKeyProd += m_arrMSIComponentNames.GetAt(i);
					lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strKeyProd, 0, regPermissions, &hKey);
					if (lResult == ERROR_SUCCESS)
					{
						DWORD dwValueCount = 0;
						lResult = ::RegQueryInfoKey(hKey, NULL, NULL, NULL, NULL, NULL, NULL, &dwValueCount, NULL, NULL, NULL, NULL);
						if (lResult == ERROR_SUCCESS)
						{
							//if there is only one product associated with that component
							//add the whole key
							if (dwValueCount == 1)
							{
								strKeyProd.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
								PREGNODE pNode = m_pTree->CreateKey(strKeyProd, eKeyWOW);
								if (pNode)
									pNode->eOperation = RegCreated;
							}
							else
							{
								//add only the product key value and data
								DWORD dwType;
								TCHAR szData[2048] = { 0 };
								DWORD dwDataSize = sizeof(szData);
								SHQueryValueEx(hKey, szRes, NULL, &dwType, (LPBYTE)(szData), &dwDataSize);
								strKeyProd.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
								m_pTree->SetKeyVal(strKeyProd, szRes, dwType, (BYTE*)szData, dwDataSize, 0, NULL, 0, eKeyWOW);
							}
						}
						::RegCloseKey(hKey);
					}
				}
			}

			CStringArray arrProductCode;
			arrProductCode.Add(szRes);

			DWORD dwFoundCount = 0;
			TCHAR szFullRegPath[65536] = { 0 };

			RegDataList listFoundMSIRegKeys;

			CStringArray arrGUID;
			arrGUID.Add(m_strMSIGUID);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Installer\\UpgradeCodes"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\Installer\\UpgradeCodes"), szFullRegPath, arrProductCode, &listFoundMSIRegKeys, &dwFoundCount, NULL, b64bit, -1, -1, FALSE, TRUE, FALSE, TRUE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UpgradeCodes"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UpgradeCodes"), szFullRegPath, arrProductCode, &listFoundMSIRegKeys, &dwFoundCount, NULL, b64bit, -1, -1, FALSE, TRUE, FALSE, TRUE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Installer\\UpgradeCodes"));
			SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Installer\\UpgradeCodes"), szFullRegPath, arrProductCode, &listFoundMSIRegKeys, &dwFoundCount, NULL, b64bit, -1, -1, FALSE, TRUE, FALSE, TRUE);

			POSITION pos = listFoundMSIRegKeys.GetHeadPosition();
			while (pos != NULL)
			{
				FoundRegData* temp = (FoundRegData*)listFoundMSIRegKeys.GetNext(pos);
				PREGNODE pNode = m_pTree->CreateKey(temp->strFullKeyPath, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				if (temp->dwDataSize)
					delete[] temp->pbtData;

				delete temp;
			}

			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return;

			strKeyProd = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\") + GetCurrentUserSID() + _T("\\Products\\");
			strKeyProd += szRes;
			lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, strKeyProd, 0, regPermissions, &hKey);
			if (lResult == ERROR_SUCCESS)
			{
				strKeyProd.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\"));
				PREGNODE pNode = m_pTree->CreateKey(strKeyProd, eKeyWOW);
				if (pNode)
					pNode->eOperation = RegCreated;

				::RegCloseKey(hKey);
			}

			//Search for installer folders
			CStringArray arrExactFolders;
			GetExactFoldersForSearch(arrExactFolders);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\Folders"));
			//SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\Folders"), szFullRegPath, arrExactFolders, m_pTree, &dwFoundCount, NULL, b64bit, -1, -1, FALSE, TRUE, FALSE, FALSE);

			CString strMSIFoldersPath = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\Folders");
			CRegistry oMSIFolders(HKEY_LOCAL_MACHINE);
			if (oMSIFolders.Open(strMSIFoldersPath, regPermissions))
			{

				//if there are more values then the component is shared
				int nValCount = oMSIFolders.GetNumberOfValues();
				for (int nVals = 0; nVals < nValCount; nVals++)
				{
					CRegistry::KeyValueTypes value_type;
					CString strName;
					//DWORD  dwDataType = 0; 

					DWORD dwDataSize = oMSIFolders.GetValueDataSize();
					LPBYTE data = new BYTE[dwDataSize];
					::ZeroMemory(data, dwDataSize);
					oMSIFolders.EnumerateValues(nVals, strName, value_type, data, dwDataSize);

					if (IsListInString(&arrExactFolders, strName, FALSE))
					{
						m_pTree->SetKeyVal(szFullRegPath, strName, DWORD(value_type), data, dwDataSize, 0, NULL, 0, eKeyWOW);
						continue;
					}

					for (int nIndex = 0; nIndex < m_arrSearchPattern.GetCount(); nIndex++)
					{
						int nPos = strName.Find(m_arrSearchPattern[nIndex]);

						if (nPos != -1)
						{
							int nEndPos = nPos + m_arrSearchPattern[nIndex].GetLength();
							if ((strName.GetAt(nPos - 1) == TEXT('\\')) && (strName.GetAt(nEndPos) == TEXT('\\')))
								m_pTree->SetKeyVal(szFullRegPath, strName, DWORD(value_type), data, dwDataSize, 0, NULL, 0, eKeyWOW);
						}
					}

					//if(::PathIsDirectory(strName) != FALSE)
					//{
					//	PutFoldersForSearch(strName);
					//}
					delete[] data;
				}
			}


			//Search Assemblies
			CRegistry oAssemblies(HKEY_LOCAL_MACHINE);
			oAssemblies.Open(_T("SOFTWARE\\Classes\\Installer\\Assemblies"), regPermissions);

			CString strAssemKeyName, strAssemClassName, strAssemKeyNameOriginal;

			int nAssemKeyCount = oAssemblies.GetNumberOfSubkeys();
			for (int nIndex = 0; nIndex < nAssemKeyCount; nIndex++)
			{
				oAssemblies.EnumerateKeys(nIndex, strAssemKeyNameOriginal, strAssemClassName);

				strAssemKeyName = strAssemKeyNameOriginal;
				strAssemKeyName.Replace(_T("|"), _T("\\"));

				if (IsListInString(&arrExactFolders, strAssemKeyName, FALSE))
				{
					strAssemKeyNameOriginal.Insert(0, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Installer\\Assemblies\\"));
					PREGNODE pNode = m_pTree->CreateKey(strAssemKeyNameOriginal, eKeyWOW);
					if (pNode)
						pNode->eOperation = RegCreated;

				}
			}

		}
	}
}

BOOL CVSTrackFinder::SearchLeftoverFilesFolders(CStringArray* arrSearchPattern, BOOL fMatchInPath, CStringArray* arrMatch)
{
	BOOL bRes = FALSE;

	CStringArray arrFoundFileFolders;

	CStringArray arrNames, arrExactFolders, arrIgnore;
	GetExactFoldersForSearch(arrExactFolders);

	if (arrExactFolders.GetCount())
	{
		//Search folders previously found from InstallLocation, UninstallCommand, Start Menu etc.
		for (int nFoldersCnt = 0; nFoldersCnt < arrExactFolders.GetCount(); nFoldersCnt++)
		{
			FindAllDataInFolder(arrExactFolders.GetAt(nFoldersCnt), &arrNames, &arrFoundFileFolders, &arrIgnore, -1, -1, TRUE, TRUE, TRUE, TRUE);
			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;
		}

		//Add exact folders to list if they exist
		PutFoundFilesFolders(arrExactFolders);
	}

	int nDepth = -1;
	if (m_bPopularBrowser)
		nDepth = 2;

	if (arrSearchPattern->GetCount())
	{
		//get CommonFiles folder
		TCHAR szProgramFilesCommon[4096] = { 0 };
		SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMON, NULL, 0, szProgramFilesCommon);

		//get CommonFiles(x86) folder
		TCHAR szProgramFilesCommonX86[4096] = { 0 };
		SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMONX86, NULL, 0, szProgramFilesCommonX86);

		//get Program Files folder
		TCHAR szProgramFiles[4096] = { 0 };
		SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFiles);

		//get Program Files folder
		TCHAR szProgramFilesX86[4096] = { 0 };
		SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szProgramFilesX86);

		//Scan All Users\Start Menu\Programs
		TCHAR szAppData[4096] = { 0 };
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_PROGRAMS, NULL, 0, szAppData)))
		{
			CStringArray arrFoundFolders;
			FindFileOrFolder(szAppData, arrSearchPattern, TRUE, &arrFoundFolders, TRUE, TRUE, nDepth);
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrFoundFolders);
			PutFoundFilesFolders(arrFoundFolders);

			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			for (int i = 0; i < arrFoundFolders.GetCount(); i++)
			{
				FindAllDataInFolder(arrFoundFolders.GetAt(i), NULL, &arrFoundFileFolders, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);

				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

			}
			if (arrFoundFolders.GetCount() == 0)
			{
				::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

				FindStartMenuLeftovers(szAppData, arrExactFolders, szProgramFilesCommon, szProgramFiles,
									   szProgramFilesCommonX86, szProgramFilesX86, arrFoundFileFolders);

				::CoUninitialize();
			}
		}

		//Scan Start Menu\Programs
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAMS, NULL, 0, szAppData)))
		{
			CStringArray arrFoundFolders;
			FindFileOrFolder(szAppData, arrSearchPattern, TRUE, &arrFoundFolders);
			PutFoundFilesFolders(arrFoundFolders);

			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			for (int i = 0; i < arrFoundFolders.GetCount(); i++)
			{
				FindAllDataInFolder(arrFoundFolders.GetAt(i), NULL, &arrFoundFileFolders, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);

				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;
			}
			if (arrFoundFolders.GetCount() == 0)
			{
				::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

				FindStartMenuLeftovers(szAppData, arrExactFolders, szProgramFilesCommon, szProgramFiles,
									   szProgramFilesCommonX86, szProgramFilesX86, arrFoundFileFolders);

				::CoUninitialize();
			}
		}

		//Scan <user name>\Application Data
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szAppData)))
		{
			CStringArray arrFoundFolders;
			FindFileOrFolder(szAppData, arrSearchPattern, TRUE, &arrFoundFolders, TRUE, TRUE, nDepth);
			PutFoundFilesFolders(arrFoundFolders);

			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			for (int i = 0; i < arrFoundFolders.GetCount(); i++)
			{
				CStringArray arrAllFound;
				FindAllDataInFolder(arrFoundFolders.GetAt(i), NULL, &arrAllFound, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);
				PutFoundFilesFolders(arrAllFound);

				CString strFolderPath = arrFoundFolders.GetAt(i);
				AddParentFolderAsFound(strFolderPath);

				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

			}
			if (fMatchInPath)
			{
				CStringArray arrAllFoundFolders;
				MatchInDrivePath(szAppData, arrMatch, &arrAllFoundFolders, NULL, (int)arrMatch->GetCount(), 3, -1);
				for (int i = 0; i < arrAllFoundFolders.GetCount(); i++)
				{
					if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
						return FALSE;

					CStringArray arrAllFound;
					FindAllDataInFolder(arrAllFoundFolders.GetAt(i), NULL, &arrAllFound, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);
					PutFoundFilesFolders(arrAllFound);
					CString strFolderPath = arrAllFoundFolders.GetAt(i);
					AddParentFolderAsFound(strFolderPath);
				}
			}
		}
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;

		//Scan <user name>\Local Settings\Applicaiton Data (non roaming)
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, 0, szAppData)))
		{
			CStringArray arrFoundFolders, arrSerchPatternLocal;
			arrSerchPatternLocal.Append(*arrSearchPattern);
			arrSerchPatternLocal.Add(m_strAppNameAlpha);
			FindFileOrFolder(szAppData, &arrSerchPatternLocal, TRUE, &arrFoundFolders, TRUE, TRUE, nDepth);
			PutFoundFilesFolders(arrFoundFolders);

			/*CStringArray arrFoundVirtStoreFolders;
			CStringArray arrSerchPatternLocal;
			arrSerchPatternLocal.Append(*arrSearchPattern);
			arrSerchPatternLocal.Add(m_strAppNameAlpha);*/
			//CString strVirtualStorePath(szAppData);
			//strVirtualStorePath += TEXT("\\VirtualStore\\Program Files (x86)");
			//FindFileOrFolder(strVirtualStorePath, &arrSerchPatternLocal, TRUE, &arrFoundVirtStoreFolders);
			//PutFoundFilesFolders(arrFoundVirtStoreFolders);

			//arrFoundFolders.Append(arrFoundVirtStoreFolders);

			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			for (int i = 0; i < arrFoundFolders.GetCount(); i++)
			{
				CStringArray arrAllFound;
				FindAllDataInFolder(arrFoundFolders.GetAt(i), NULL, &arrAllFound, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);
				PutFoundFilesFolders(arrAllFound);
				CString strFolderPath = arrFoundFolders.GetAt(i);
				AddParentFolderAsFound(strFolderPath);

				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

			}
			if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
				return FALSE;

			if (fMatchInPath)
			{
				CStringArray arrAllFoundFolders;
				MatchInDrivePath(szAppData, arrMatch, &arrAllFoundFolders, NULL, (int)arrMatch->GetCount(), 3, -1);
				for (int i = 0; i < arrAllFoundFolders.GetCount(); i++)
				{
					CStringArray arrAllFound;
					FindAllDataInFolder(arrAllFoundFolders.GetAt(i), NULL, &arrAllFound, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);
					PutFoundFilesFolders(arrAllFound);
					CString strFolderPath = arrAllFoundFolders.GetAt(i);
					AddParentFolderAsFound(strFolderPath);
				}
			}

			//Check and LocalLow folder
			CString strLocalLowFolder;
			GetLastFolderPath(szAppData, &strLocalLowFolder);
			strLocalLowFolder += TEXT("\\LocalLow");
			CStringArray arrFoundFoldersLL, arrSerchPatternLocalLow;
			arrSerchPatternLocalLow.Append(*arrSearchPattern);
			FindFileOrFolder(strLocalLowFolder, &arrSerchPatternLocalLow, TRUE, &arrFoundFoldersLL, TRUE, TRUE, nDepth);
			PutFoundFilesFolders(arrFoundFoldersLL);
		}

		//Scan All Users\Application Data
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szAppData)))
		{
			CStringArray arrFoundFolders;
			FindFileOrFolder(szAppData, arrSearchPattern, TRUE, &arrFoundFolders, TRUE, TRUE, nDepth);
			PutFoundFilesFolders(arrFoundFolders);

			for (int i = 0; i < arrFoundFolders.GetCount(); i++)
			{
				CStringArray arrAllFound;
				FindAllDataInFolder(arrFoundFolders.GetAt(i), NULL, &arrAllFound, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);
				PutFoundFilesFolders(arrAllFound);
				CString strFolderPath = arrFoundFolders.GetAt(i);
				AddParentFolderAsFound(strFolderPath);

				if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
					return FALSE;

			}
			if (fMatchInPath)
			{
				CStringArray arrAllFoundFolders;
				MatchInDrivePath(szAppData, arrMatch, &arrAllFoundFolders, NULL, (int)arrMatch->GetCount(), 3, -1);
				for (int i = 0; i < arrAllFoundFolders.GetCount(); i++)
				{
					if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
						return FALSE;

					CStringArray arrAllFound;
					FindAllDataInFolder(arrAllFoundFolders.GetAt(i), NULL, &arrAllFound, NULL, -1, -1, TRUE, TRUE, TRUE, TRUE);
					PutFoundFilesFolders(arrAllFound);
					CString strFolderPath = arrAllFoundFolders.GetAt(i);
					AddParentFolderAsFound(strFolderPath);
				}
			}
		}

		//search for files
		CStringArray arrRet;


		//Scan <user name>
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, 0, szAppData)))
		{
			CStringArray arrtempSearchPattern;
			arrtempSearchPattern.Append(*arrSearchPattern);

			if (IsPatternAllowedForSearch(m_strAppNameAlpha))
			{
				if (IsStringInList(m_strAppNameAlpha, &arrtempSearchPattern) == FALSE)
					arrtempSearchPattern.Add(m_strAppNameAlpha);
			}

			FindFileOrFolder(szAppData, &arrtempSearchPattern, TRUE, &arrRet, FALSE, FALSE);

			StringCchCat(szAppData, 4096, _T("\\.config"));
			FindFileOrFolder(szAppData, &arrtempSearchPattern, TRUE, &arrRet, FALSE, FALSE);
		}

		//Scan <user name>\Desktop
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, 0, szAppData)))
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE);

		//Scan All Users\Desktop
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_DESKTOPDIRECTORY, NULL, 0, szAppData)))
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE);

		//Scan <user name>\SendTo
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_SENDTO, NULL, 0, szAppData)))
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE);

		//Scan non localized startup
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_ALTSTARTUP, NULL, 0, szAppData)))
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE);

		//Scan non localized common startup
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_ALTSTARTUP, NULL, 0, szAppData)))
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE);

		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_RECENT, NULL, 0, szAppData)))
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE, FALSE);

		//Scan Quick Launch
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szAppData)))
		{
			StringCchCat(szAppData, 4096, _T("\\Microsoft\\Internet Explorer\\Quick Launch"));
			FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrRet, FALSE);
		}

		//Scan Prefetch
		CStringArray arrPrefetch;
		CString strPrefetchFolder = m_strCSIDL_WINDOWS;
		strPrefetchFolder += TEXT("\\Prefetch");
		FindFileOrFolder(strPrefetchFolder, arrSearchPattern, FALSE, &arrPrefetch, FALSE, FALSE);
		INT_PTR nPrefetchCount = arrPrefetch.GetCount();
		if (nPrefetchCount > 0)
		{
			for (INT_PTR i = 0; i < nPrefetchCount; i++)
			{
				PutFoundFilesFolders(arrPrefetch.GetAt(i), FALSE);
			}
		}

		//add found files
		PutFoundFilesFolders(arrRet);

		if (m_uninModerateMode == uninAdvancedMode)
		{
			CStringArray arrTasks;
			if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_WINDOWS, NULL, 0, szAppData)))
			{
				StringCchCat(szAppData, 4096, _T("\\Tasks"));
				FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrTasks, FALSE, FALSE);
			}

			if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_SYSTEM, NULL, 0, szAppData)))
			{
				StringCchCat(szAppData, 4096, _T("\\Tasks"));
				FindFileOrFolder(szAppData, arrSearchPattern, FALSE, &arrTasks, FALSE, FALSE);
			}

			for (int i = 0; i < arrTasks.GetCount(); i++)
				PutFoundFilesFolders(arrTasks.GetAt(i), FALSE);
		}
	}

	PutFoundFilesFolders(arrFoundFileFolders);

	return bRes;
}
BOOL CVSTrackFinder::FindStartMenuLeftovers(LPCTSTR pSearchFolder, CStringArray& arrTargetPath, LPCTSTR pCommonFilesFolder, LPCTSTR pProgramFilesFolder, LPCTSTR pCommonFilesFolderX86, LPCTSTR pProgramFilesFolderX86, CStringArray& arrResult)
{
	if (pSearchFolder == NULL)
		return FALSE;

	//if(_tcslen(pSearchFolder) == 0)
	//	return FALSE;

	TCHAR* pszFullPathFileName = new TCHAR[4096];
	TCHAR* pszFilename = new TCHAR[4096];

	WIN32_FIND_DATA FileData = { 0 };
	BOOL bFinished = FALSE;
	DWORD dwSize = 0;

	StringCchPrintf(pszFullPathFileName, 4096, TEXT("%s\\*.*"), pSearchFolder);
	HANDLE hSearch = FindFirstFile(pszFullPathFileName, &FileData);
	if (hSearch == INVALID_HANDLE_VALUE)
	{
		delete[] pszFullPathFileName;
		delete[] pszFilename;
		return FALSE;
	}

	while (!bFinished)
	{
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		{
			delete[] pszFullPathFileName;
			delete[] pszFilename;
			FindClose(hSearch);

			return FALSE;
		}

		StringCchPrintf(pszFilename, 4096, _T("%s\\%s"), pSearchFolder, FileData.cFileName);
		if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
		{
			if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_tcscmp(FileData.cFileName, TEXT(".")) && _tcscmp(FileData.cFileName, TEXT("..")))
				{
					FindStartMenuLeftovers(pszFilename, arrTargetPath, pCommonFilesFolder, pProgramFilesFolder, pCommonFilesFolderX86, pProgramFilesFolderX86, arrResult);
				}
			}
			else
			{
				//check if file is an executable
				TCHAR pszTargetBuff[4096] = { 0 };
				getShellLinkTarget(pszFilename, pszTargetBuff, 4096);

				if (_tcslen(pszTargetBuff) > 0)
				{
					if ((StrStrI(pszTargetBuff, pCommonFilesFolder) == NULL) && (StrStrI(pszTargetBuff, pCommonFilesFolderX86) == NULL))
					{
						if ((StrStrI(pszTargetBuff, pProgramFilesFolder)) || (StrStrI(pszTargetBuff, pProgramFilesFolderX86)))
						{
							::PathRemoveFileSpec(pszTargetBuff);
							if (IsListInString(&arrTargetPath, pszTargetBuff, FALSE))
							{
								arrResult.Add(pszFilename);
							}
						}
					}
				}
			}
		}
		if (!FindNextFile(hSearch, &FileData))
		{
			//if (GetLastError() == ERROR_NO_MORE_FILES) 
			bFinished = TRUE;
		}
	}
	FindClose(hSearch);

	delete[] pszFullPathFileName;
	delete[] pszFilename;

	return TRUE;
}

//pFolder - Folder name to search in it
//pName - Name of file or folder to search for
//fFolder - true if we are searching for folder
//pArray - return path
BOOL CVSTrackFinder::FindFileOrFolder(LPCTSTR pFolder, CStringArray* arrNames, BOOL fFolder, CStringArray* pArray, BOOL fRecursive, BOOL bMatchCase, int nDepthLevels)
{
	if (pFolder == NULL)
		return FALSE;

	if (_tcslen(pFolder) == 0)
		return FALSE;

	TCHAR* pszFullPathFileName = new TCHAR[4096];
	TCHAR* pszFilename = new TCHAR[4096];

	WIN32_FIND_DATA FileData = { 0 };
	BOOL bFinished = FALSE;
	DWORD dwSize = 0;

	StringCchPrintf(pszFullPathFileName, 4096, _T("%s"), pFolder);
	::PathRemoveBackslash(pszFullPathFileName);
	StringCchCat(pszFullPathFileName, 4096, _T("\\*.*"));

	HANDLE hSearch = FindFirstFile(pszFullPathFileName, &FileData);
	if (hSearch == INVALID_HANDLE_VALUE)
	{
		delete[] pszFullPathFileName;
		delete[] pszFilename;

		return FALSE;
	}

	while (!bFinished)
	{
		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		{
			delete[] pszFullPathFileName;
			delete[] pszFilename;

			FindClose(hSearch);
			return FALSE;
		}

		StringCchPrintf(pszFilename, 4096, _T("%s\\%s"), pFolder, FileData.cFileName);

		if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
		{
			if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_tcscmp(FileData.cFileName, TEXT(".")) && _tcscmp(FileData.cFileName, TEXT("..")))
				{
					if (fFolder)
						if (IsListInString(arrNames, FileData.cFileName, bMatchCase))
						{
							pArray->Add(pszFilename);

							delete[] pszFullPathFileName;
							delete[] pszFilename;

							FindClose(hSearch);

							return TRUE;
						}

					if (fRecursive)
					{
						if (nDepthLevels > 0)
							FindFileOrFolder(pszFilename, arrNames, fFolder, pArray, TRUE, bMatchCase, nDepthLevels - 1);
						else
						{
							if (nDepthLevels < 0)
								FindFileOrFolder(pszFilename, arrNames, fFolder, pArray, TRUE, bMatchCase, nDepthLevels);
						}
					}
				}
			}
			else
			{
				if (!fFolder)
				{
					CString strFile = FileData.cFileName;
					::PathRemoveExtension(strFile.GetBuffer());
					strFile.ReleaseBuffer();

					if (IsListInString(arrNames, strFile, bMatchCase))
						pArray->Add(pszFilename);
				}
			}
		}

		if (!FindNextFile(hSearch, &FileData))
		{
			if (GetLastError() == ERROR_NO_MORE_FILES)
				bFinished = TRUE;
		}
	}
	FindClose(hSearch);

	delete[] pszFullPathFileName;
	delete[] pszFilename;

	return TRUE;
}

int CVSTrackFinder::ExtractPureStrings(LPCTSTR pszSource, CStringArray& arrResult)
{
	//Extract only alpha chars from string
	CStringArray arrSpaces;
	CString resToken, strSource(pszSource);

	int curPos = 0;

	for (int nCount = 0; nCount < strSource.GetLength(); nCount++)
	{
		if ((IsCharAlpha(strSource.GetAt(nCount)) == FALSE) && (strSource.GetAt(nCount) != TEXT(' ')))
		{
			strSource.Delete(nCount);
			nCount--;
		}
	}

	int nCount = 0;
	resToken = strSource.Tokenize(_T(" "), curPos);
	while (resToken != "")
	{
		arrResult.Add(resToken);
		nCount++;

		resToken = strSource.Tokenize(_T(" "), curPos);
	}

	return nCount;
}

BOOL CVSTrackFinder::FindMSIGUIDFromProductsKey(CStringArray& arrFolders, LPCTSTR pszAppName)
{
	REGSAM regPermissions = KEY_READ;
	for (int nBitsType = 0; nBitsType < 2; nBitsType++)
	{

		if (nBitsType == 0)
			regPermissions = KEY_READ | KEY_WOW64_64KEY;
		else
			regPermissions = KEY_READ | KEY_WOW64_32KEY;



		if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
			return FALSE;

		//MSI is 64bit on 64 and 32bit on 32 Windows
		//REGSAM regPermissions = KEY_READ;

		TCHAR    achKey[MAX_PATH];   // buffer for subkey name
		DWORD    cbName;                   // size of name string 
		TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
		DWORD    cchClassName = MAX_PATH;  // size of class string 
		DWORD    cSubKeys = 0;               // number of subkeys 
		DWORD    cbMaxSubKey;              // longest subkey size 

		DWORD i, retCode;

		TCHAR* pachValue = NULL;
		DWORD  cchValue = 0;

		BYTE* pbtData = NULL;
		DWORD  dwDataType = 0;
		DWORD  dwDataSize = 0;

		m_strMSIGUID.Empty();

		TCHAR szKeyPath[1024] = { _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Products") };

		HKEY hKey;
		LONG lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, szKeyPath, 0, regPermissions, &hKey);
		if (lResult == ERROR_SUCCESS)
		{
			// Get the class name and the value count. 
			retCode = RegQueryInfoKey(
				hKey,                    // key handle 
				achClass,                // buffer for class name 
				&cchClassName,           // size of class string 
				NULL,                    // reserved 
				&cSubKeys,               // number of subkeys 
				&cbMaxSubKey,            // longest subkey size 
				NULL,		             // longest class string 
				NULL,	                 // number of values for this key 
				NULL,			         // longest value name 
				NULL,			         // longest value data 
				NULL,					 // security descriptor 
				NULL);				     // last write time 


	// Enumerate the subkeys, until RegEnumKeyEx fails.   
			if (cSubKeys)
			{
				for (i = 0; i < cSubKeys; i++)
				{
					cbName = MAX_PATH;
					retCode = RegEnumKeyEx(hKey, i,
										   achKey,
										   &cbName,
										   NULL,
										   NULL,
										   NULL,
										   NULL);
					if (retCode == ERROR_SUCCESS)
					{
						StringCchCat(szKeyPath, 1024, _T("\\"));
						StringCchCat(szKeyPath, 1024, achKey);
						StringCchCat(szKeyPath, 1024, _T("\\InstallProperties"));

						HKEY hSubKey;
						lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, szKeyPath, 0, regPermissions, &hSubKey);
						if (lResult == ERROR_SUCCESS)
						{
							//If we have an app name then search for it
							if (m_strAppName.GetLength())
							{
								TCHAR	szDisplayName[1024] = { 0 };
								DWORD	dwDataSize = sizeof(szDisplayName);
								if (::RegQueryValueEx(hSubKey, TEXT("DisplayName"), NULL, NULL, (LPBYTE)(szDisplayName), &dwDataSize) == ERROR_SUCCESS)
								{
									//Get DisplayName of MSI apps and compare it to the searched app name
									if (pszAppName)
									{
										if (StrCmpI(szDisplayName, pszAppName) == 0)
										{
											TCHAR szConverted[128] = { 0 };
											UnsquashGUID(achKey, szConverted);
											m_strMSIGUID = szConverted;
											::RegCloseKey(hSubKey);
											break;
										}
									}
								}
							}

							TCHAR	szInstallLocation[1024] = { 0 };
							dwDataSize = sizeof(szInstallLocation);
							if (::RegQueryValueEx(hSubKey, TEXT("InstallLocation"), NULL, NULL, (LPBYTE)(szInstallLocation), &dwDataSize) == ERROR_SUCCESS)
							{
								if (IsFolderAllowedForSearch(szInstallLocation))
								{
									if (IsStringInList(szInstallLocation, &arrFolders, FALSE))
									{
										TCHAR szConverted[128] = { 0 };
										UnsquashGUID(achKey, szConverted);
										m_strMSIGUID = szConverted;
										::RegCloseKey(hSubKey);
										break;
									}
								}
							}

							::RegCloseKey(hSubKey);
						}

						RemoveLastKeyName(szKeyPath);
						RemoveLastKeyName(szKeyPath);
					}
				}
			}

			RegCloseKey(hKey);
		}

		//if it is not found in All Users then check Current User
		if (m_strMSIGUID.GetLength() == 0)
		{
			StringCchCopy(szKeyPath, 1024, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\"));
			StringCchCat(szKeyPath, 1024, GetCurrentUserSID());
			StringCchCat(szKeyPath, 1024, _T("\\Products"));

			LONG lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, szKeyPath, 0, regPermissions, &hKey);
			if (lResult == ERROR_SUCCESS)
			{

				// Get the class name and the value count. 
				retCode = RegQueryInfoKey(
					hKey,                    // key handle 
					achClass,                // buffer for class name 
					&cchClassName,           // size of class string 
					NULL,                    // reserved 
					&cSubKeys,               // number of subkeys 
					&cbMaxSubKey,            // longest subkey size 
					NULL,		             // longest class string 
					NULL,	                 // number of values for this key 
					NULL,			         // longest value name 
					NULL,			         // longest value data 
					NULL,					 // security descriptor 
					NULL);				     // last write time 


		// Enumerate the subkeys, until RegEnumKeyEx fails.   
				if (cSubKeys)
				{
					for (i = 0; i < cSubKeys; i++)
					{
						cbName = MAX_PATH;
						retCode = RegEnumKeyEx(hKey, i,
											   achKey,
											   &cbName,
											   NULL,
											   NULL,
											   NULL,
											   NULL);
						if (retCode == ERROR_SUCCESS)
						{
							StringCchCat(szKeyPath, 1024, _T("\\"));
							StringCchCat(szKeyPath, 1024, achKey);
							StringCchCat(szKeyPath, 1024, _T("\\InstallProperties"));

							HKEY hSubKey;
							lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, szKeyPath, 0, regPermissions, &hSubKey);
							if (lResult == ERROR_SUCCESS)
							{
								TCHAR	szDisplayName[1024] = { 0 };
								DWORD	dwDataSize = sizeof(szDisplayName);
								if (::RegQueryValueEx(hSubKey, TEXT("DisplayName"), NULL, NULL, (LPBYTE)(szDisplayName), &dwDataSize) == ERROR_SUCCESS)
								{
									//Get DisplayName of MSI apps and compare it to the searched app name
									if (pszAppName)
									{
										if (StrCmpI(szDisplayName, pszAppName) == 0)
										{
											TCHAR szConverted[128] = { 0 };
											UnsquashGUID(achKey, szConverted);
											m_strMSIGUID = szConverted;
											::RegCloseKey(hSubKey);
											break;
										}
									}
								}
								TCHAR	szInstallLocation[1024] = { 0 };
								dwDataSize = sizeof(szInstallLocation);
								if (::RegQueryValueEx(hSubKey, TEXT("InstallLocation"), NULL, NULL, (LPBYTE)(szInstallLocation), &dwDataSize) == ERROR_SUCCESS)
								{
									//Get DisplayName of MSI apps and compare it to the searched app name
									if (IsStringInList(szInstallLocation, &arrFolders, FALSE))
									{
										TCHAR szConverted[128] = { 0 };
										UnsquashGUID(achKey, szConverted);
										m_strMSIGUID = szConverted;
										::RegCloseKey(hSubKey);
										break;
									}
								}
								::RegCloseKey(hSubKey);
							}
							RemoveLastKeyName(szKeyPath);
							RemoveLastKeyName(szKeyPath);
						}
					}
				}

				RegCloseKey(hKey);
			}
		}
		if (m_strMSIGUID.GetLength())
			break;
	}
	if (m_strMSIGUID.GetLength())
		return TRUE;
	else
		return FALSE;
}

BOOL CVSTrackFinder::FindMSIGUIDFromComponentsKey(CStringArray& arrFolders)
{
	if (arrFolders.GetCount() == 0)
		return FALSE;

	//MSI is 64bit on 64 and 32bit on 32 Windows
	REGSAM regPermissions = KEY_READ;
	for (int nBitsType = 0; nBitsType < 2; nBitsType++)
	{
		if (nBitsType == 0)
			regPermissions = KEY_READ | KEY_WOW64_64KEY;
		else
			regPermissions = KEY_READ | KEY_WOW64_32KEY;

		CString strKeyComp = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\") + GetCurrentUserSID() + _T("\\Components");

		for (int i = 0; i < 2; i++)
		{
			if (i == 1)
				strKeyComp = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Components");

			CRegistry oComponents(HKEY_LOCAL_MACHINE);
			oComponents.Open(strKeyComp, regPermissions);

			int nKeyCount = oComponents.GetNumberOfSubkeys();
			CString strKeyName, strClassName;
			for (int nKeyIndex = 0; nKeyIndex < nKeyCount; nKeyIndex++)
			{
				oComponents.EnumerateKeys(nKeyIndex, strKeyName, strClassName);

				CString strCompFullPath = strKeyComp + _T("\\");
				strCompFullPath += strKeyName;

				CRegistry oTmpComponent(HKEY_LOCAL_MACHINE);
				oTmpComponent.Open(strCompFullPath, regPermissions);

				//if there are more values then the component is shared
				int nValCount = oTmpComponent.GetNumberOfValues();
				if (nValCount == 1)
				{
					CRegistry::KeyValueTypes value_type;
					CString strName;
					DWORD dwDataSize = oTmpComponent.GetValueDataSize();
					LPBYTE data = new BYTE[dwDataSize];
					::ZeroMemory(data, dwDataSize);
					oTmpComponent.EnumerateValues(0, strName, value_type, data, dwDataSize);

					CString strData = (TCHAR*)data;
					strData.Replace(_T("?"), _T(":"));
					if (IsListInString(&arrFolders, strData, FALSE))
					{
						if (CheckIfMSIGUIDIsExistProduct(strName))
						{
							TCHAR szConverted[128] = { 0 };
							UnsquashGUID(strName, szConverted);
							m_strMSIGUID = szConverted;
							delete[] data;
							return TRUE;
						}
					}
					delete[] data;
				}
			}
		}
	}

	return FALSE;
}

//Caller should delete values in pBuiltInList if any
//Check if pszInstallPath is allowed for search before calling this function!
BOOL CVSTrackFinder::FindBuiltInUninstallers(LPCTSTR pszInstallPath, LPCTSTR pszAppName, CPtrList* pBuiltInList)
{
	TCHAR	 szKeyPath[1024] = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
	REGSAM	 regPermissions = KEY_READ;

	HKEY	 hKeyRoot = HKEY_LOCAL_MACHINE;
	HKEY	 hKey;
	TCHAR    achKey[MAX_PATH];   // buffer for subkey name
	DWORD    cbName;                   // size of name string 
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
	DWORD    cchClassName = MAX_PATH;  // size of class string 
	DWORD    cSubKeys = 0;               // number of subkeys 
	DWORD    cbMaxSubKey;              // longest subkey size 

	DWORD	 i, retCode;

	TCHAR* pachValue = NULL;
	DWORD	 cchValue = 0;

	BYTE* pbtData = NULL;
	DWORD	 dwDataType = 0;
	DWORD	 dwDataSize = 0;

	BOOL	 b64bit = FALSE;

#ifdef _WIN64
	for (int nUsers = 0; nUsers < 4; nUsers++)
#else
	for (int nUsers = 0; nUsers < 2; nUsers++)
#endif
	{
		if (nUsers == 0)
		{
			hKeyRoot = HKEY_LOCAL_MACHINE;
			regPermissions = KEY_READ | KEY_WOW64_32KEY;
			b64bit = FALSE;
		}

		if (nUsers == 1)
		{
			hKeyRoot = HKEY_CURRENT_USER;
			regPermissions = KEY_READ | KEY_WOW64_32KEY;
			b64bit = FALSE;
		}

		if (nUsers == 2)
		{
			hKeyRoot = HKEY_LOCAL_MACHINE;
			regPermissions = KEY_READ | KEY_WOW64_64KEY;
			b64bit = TRUE;
		}

		if (nUsers == 3)
		{
			hKeyRoot = HKEY_CURRENT_USER;
			regPermissions = KEY_READ | KEY_WOW64_64KEY;
			b64bit = TRUE;
		}

		LONG lResult = ::RegOpenKeyEx(hKeyRoot, szKeyPath, 0, regPermissions, &hKey);
		if (lResult == ERROR_SUCCESS)
		{
			// Get the class name and the value count. 
			retCode = RegQueryInfoKey(
				hKey,                    // key handle 
				achClass,                // buffer for class name 
				&cchClassName,           // size of class string 
				NULL,                    // reserved 
				&cSubKeys,               // number of subkeys 
				&cbMaxSubKey,            // longest subkey size 
				NULL,		             // longest class string 
				NULL,	                 // number of values for this key 
				NULL,			         // longest value name 
				NULL,			         // longest value data 
				NULL,					 // security descriptor 
				NULL);				     // last write time 


	// Enumerate the subkeys
			if (cSubKeys)
			{
				for (i = 0; i < cSubKeys; i++)
				{
					cbName = MAX_PATH;
					retCode = RegEnumKeyEx(hKey, i,
										   achKey,
										   &cbName,
										   NULL,
										   NULL,
										   NULL,
										   NULL);
					if (retCode == ERROR_SUCCESS)
					{
						StringCchCat(szKeyPath, 1024, _T("\\"));
						StringCchCat(szKeyPath, 1024, achKey);

						HKEY hSubKey;
						lResult = ::RegOpenKeyEx(hKeyRoot, szKeyPath, 0, regPermissions, &hSubKey);
						if (lResult == ERROR_SUCCESS)
						{
							TCHAR	szUninstallString[1024] = { 0 };
							dwDataSize = sizeof(szUninstallString);
							if (::RegQueryValueEx(hSubKey, TEXT("UninstallString"), NULL, NULL, (LPBYTE)(szUninstallString), &dwDataSize) == ERROR_SUCCESS)
							{
								TCHAR	szDisplayName[1024] = { 0 };
								DWORD	dwDataSize = sizeof(szDisplayName);
								if (::RegQueryValueEx(hSubKey, TEXT("DisplayName"), NULL, NULL, (LPBYTE)(szDisplayName), &dwDataSize) == ERROR_SUCCESS)
								{
									if (IsRealString(szDisplayName))
									{
										//if we have an uninstall string and real display name 
										//start checking for match
										UninstallInfo* pUI = new UninstallInfo;
										pUI->m_strFullKeyPath = szKeyPath;
										pUI->m_strKeyName = achKey;
										pUI->m_b64Bit = b64bit;

										DWORD	dwMSI = 0;
										dwDataSize = sizeof(dwMSI);
										if (::RegQueryValueEx(hSubKey, TEXT("WindowsInstaller"), NULL, NULL, (LPBYTE)(&dwMSI), &dwDataSize) == ERROR_SUCCESS)
										{
											if (dwMSI)
												pUI->m_bMSI = TRUE;
											else
												pUI->m_bMSI = FALSE;
										}

										//If we have an app name for search then search for it
										if (pszAppName)
										{
											if (StrStrI(szDisplayName, pszAppName) != NULL)
											{
												//if display name matches our criteria
												pUI->m_strAppName = szDisplayName;
												pUI->m_strUninstString = szUninstallString;
											}
										}

										if (StrStrI(szUninstallString, pszInstallPath) != NULL)
										{
											//if uninstall string matches our criteria
											pUI->m_strAppName = szDisplayName;
											pUI->m_strUninstString = szUninstallString;
										}

										TCHAR	szInstallLocation[1024] = { 0 };
										dwDataSize = sizeof(szInstallLocation);
										if (::RegQueryValueEx(hSubKey, TEXT("InstallLocation"), NULL, NULL, (LPBYTE)(szInstallLocation), &dwDataSize) == ERROR_SUCCESS)
										{
											if (IsFolderAllowedForSearch(szInstallLocation))
											{
												pUI->m_strInstallLocation = szInstallLocation;

												::PathAddBackslash(szInstallLocation);
												if (StrStrI(pszInstallPath, szInstallLocation) != NULL)
												{
													//if install location matches our criteria
													pUI->m_strAppName = szDisplayName;
													pUI->m_strUninstString = szUninstallString;
												}
											}
										}

										//if we have found display name and uninstall string 
										//add them to list of built-in uninstallers
										if ((pUI->m_strAppName.GetLength()) && (pUI->m_strUninstString.GetLength()))
											pBuiltInList->AddTail(pUI);
										else
											delete pUI;
									}
								}
							}

							::RegCloseKey(hSubKey);
						}

						RemoveLastKeyName(szKeyPath);
					}
				}
			}

			RegCloseKey(hKey);
		}
	}

	if (pBuiltInList->GetCount())
		return TRUE;

	//try to find built-in uninstaller from ARP Cache
	HKEY		hkArpRoot, hkArpRootCopy, hkAppKey;
	DWORD		lpcKey;//,lpcKey2,lpcKey3;//,lpcInstaller,lpcInstallerData;
	FILETIME	ftLastWriteTime;

	LONG lRes = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Management\\ARPCache"), NULL, KEY_READ, &hkArpRoot);
	if (lRes == ERROR_SUCCESS)
	{
		for (i = 0, retCode = ERROR_SUCCESS; retCode == ERROR_SUCCESS; i++)
		{

			TCHAR	achKey[4096] = { 0 }, szArpCachePath[4096] = { 0 }, achKey2[4096] = { 0 };

			lpcKey = sizeof(achKey) / sizeof(TCHAR);
			retCode = ::RegEnumKeyEx(hkArpRoot,
									 i,
									 achKey,
									 &lpcKey,
									 NULL,
									 NULL,
									 NULL,
									 &ftLastWriteTime);
			if (retCode == (DWORD)ERROR_SUCCESS)
			{

				hkArpRootCopy = hkArpRoot;
				lRes = ::RegOpenKeyEx(hkArpRootCopy, achKey, NULL, KEY_READ, &hkAppKey);
				if (lRes == ERROR_SUCCESS)
				{

					lpcKey = sizeof(szArpCachePath);

					if (::RegQueryValueEx(hkAppKey, TEXT("SlowInfoCache"), NULL, NULL, (LPBYTE)(szArpCachePath), &lpcKey) == ERROR_SUCCESS)
					{
						TCHAR* temp = szArpCachePath + 14;
						if (temp)
						{
							::PathRemoveFileSpec(temp);
							::PathRemoveBackslash(temp);
							if (StrStrI(pszInstallPath, temp) != NULL)
							{
								TCHAR pszRegPath[1024] = { 0 };
								StringCchCopy(pszRegPath, 1024, szKeyPath);
								StringCchCat(pszRegPath, 1024, _T("\\"));
								StringCchCat(pszRegPath, 1024, achKey);

								HKEY hUninstall;
								retCode = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, pszRegPath, 0, regPermissions, &hUninstall);
								if (retCode != ERROR_SUCCESS)
									retCode = ::RegOpenKeyEx(HKEY_CURRENT_USER, pszRegPath, 0, regPermissions, &hUninstall);

								if (retCode == ERROR_SUCCESS)
								{
									TCHAR	szUninstallString[1024] = { 0 };
									dwDataSize = sizeof(szUninstallString);
									if (::RegQueryValueEx(hUninstall, TEXT("UninstallString"), NULL, NULL, (LPBYTE)(szUninstallString), &dwDataSize) == ERROR_SUCCESS)
									{
										TCHAR	szDisplayName[1024] = { 0 };
										DWORD	dwDataSize = sizeof(szDisplayName);
										if (::RegQueryValueEx(hUninstall, TEXT("DisplayName"), NULL, NULL, (LPBYTE)(szDisplayName), &dwDataSize) == ERROR_SUCCESS)
										{
											//if we have an uninstall string and real display name 
											//start checking for match
											UninstallInfo* pUI = new UninstallInfo;
											pUI->m_strFullKeyPath = pszRegPath;
											pUI->m_strKeyName = achKey;
											pUI->m_b64Bit = b64bit;
											pUI->m_strAppName = szDisplayName;
											pUI->m_strUninstString = szUninstallString;

											DWORD	dwMSI = 0;
											dwDataSize = sizeof(dwMSI);
											if (::RegQueryValueEx(hUninstall, TEXT("WindowsInstaller"), NULL, NULL, (LPBYTE)(&dwMSI), &dwDataSize) == ERROR_SUCCESS)
											{
												if (dwMSI)
													pUI->m_bMSI = TRUE;
												else
													pUI->m_bMSI = FALSE;
											}

											TCHAR	szInstallLocation[1024] = { 0 };
											dwDataSize = sizeof(szInstallLocation);
											if (::RegQueryValueEx(hUninstall, TEXT("InstallLocation"), NULL, NULL, (LPBYTE)(szInstallLocation), &dwDataSize) == ERROR_SUCCESS)
											{
												if (IsFolderAllowedForSearch(szInstallLocation))
												{
													if (StrStrI(szInstallLocation, pszInstallPath) != NULL)
													{
														//if install location matches our criteria
														pUI->m_strInstallLocation = szInstallLocation;
													}
												}
											}

											pBuiltInList->AddTail(pUI);
										}
									}
									::RegCloseKey(hUninstall);
								}
							}
						}
					}
				}
				::RegCloseKey(hkAppKey);
			}
		}

		::RegCloseKey(hkArpRoot);
	}

	if (pBuiltInList->GetCount() == 0)
	{
		CStringArray arrFolder;
		arrFolder.Add(pszInstallPath);
		if (FindMSIGUIDFromComponentsKey(arrFolder) == FALSE)
			FindMSIGUIDFromProductsKey(arrFolder, pszAppName);

		if (m_strMSIGUID.GetLength())
		{
			UninstallInfo* pInfo = new UninstallInfo;

#ifdef _WIN64
			//Save the state because we will not use it by purpose
			BOOL bOld64Bit = m_b64Bit;
			//Try to find 32bit first
			m_b64Bit = FALSE;
			if (FindMSIUInfoFromUninstallKey(m_strMSIGUID, pInfo) == FALSE)
			{
				//Nothing found so try 64bit section
				//we assume that MSI GUIDs of same app, but for different 
				//platforms, are different, if not then we will find 32bit only, 
				//when both versions are installed
				m_b64Bit = TRUE;
				if (FindMSIUInfoFromUninstallKey(m_strMSIGUID, pInfo) == FALSE)
				{
					//Nothing found so return 
					m_b64Bit = bOld64Bit;
					delete pInfo;
					return FALSE;
				}
			}
			//restore the state of the member variable
			m_b64Bit = bOld64Bit;
#else
			if (FindMSIUInfoFromUninstallKey(m_strMSIGUID, pInfo) == FALSE)
			{
				delete pInfo;
				return FALSE;
			}
#endif
			pBuiltInList->AddTail(pInfo);

			return TRUE;
		}
		return FALSE;
	}
	else
		return TRUE;
}

BOOL CVSTrackFinder::FindMSIUInfoFromUninstallKey(LPCTSTR pszMSI_GUID, UninstallInfo* pInfo)
{
	TCHAR	 szKeyPath[1024] = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
	REGSAM	 regPermissions = KEY_READ;

	HKEY	 hKeyRoot = HKEY_LOCAL_MACHINE;
	HKEY	 hKey;
	TCHAR    achKey[MAX_PATH];   // buffer for subkey name
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
	DWORD    cchClassName = MAX_PATH;  // size of class string 
	DWORD    cSubKeys = 0;               // number of subkeys 

	TCHAR* pachValue = NULL;
	DWORD    cchValue = 0;

	BYTE* pbtData = NULL;
	DWORD	 dwDataType = 0;
	DWORD    dwDataSize = 0;

	if (m_b64Bit)
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	else
		regPermissions = KEY_READ | KEY_WOW64_32KEY;

	for (int nUsers = 0; nUsers < 2; nUsers++)
	{
		if (nUsers == 0)
			hKeyRoot = HKEY_LOCAL_MACHINE;
		else
			hKeyRoot = HKEY_CURRENT_USER;

		StringCchCat(szKeyPath, 1024, _T("\\"));
		StringCchCat(szKeyPath, 1024, pszMSI_GUID);
		LONG lResult = ::RegOpenKeyEx(hKeyRoot, szKeyPath, 0, regPermissions, &hKey);
		if (lResult == ERROR_SUCCESS)
		{
			TCHAR	szUninstallString[1024] = { 0 };
			dwDataSize = sizeof(szUninstallString);
			if (::RegQueryValueEx(hKey, TEXT("UninstallString"), NULL, NULL, (LPBYTE)(szUninstallString), &dwDataSize) == ERROR_SUCCESS)
			{
				TCHAR	szDisplayName[1024] = { 0 };
				DWORD	dwDataSize = sizeof(szDisplayName);
				if (::RegQueryValueEx(hKey, TEXT("DisplayName"), NULL, NULL, (LPBYTE)(szDisplayName), &dwDataSize) == ERROR_SUCCESS)
				{
					if (IsRealString(szDisplayName))
					{
						//if we have an uninstall string and real display name 
						//start checking for match
						pInfo->m_strFullKeyPath = szKeyPath;
						pInfo->m_strKeyName = achKey;
						pInfo->m_b64Bit = m_b64Bit;
						pInfo->m_bMSI = TRUE;
						pInfo->m_strAppName = szDisplayName;
						pInfo->m_strUninstString = szUninstallString;

						TCHAR	szInstallLocation[1024] = { 0 };
						dwDataSize = sizeof(szInstallLocation);
						if (::RegQueryValueEx(hKey, TEXT("InstallLocation"), NULL, NULL, (LPBYTE)(szInstallLocation), &dwDataSize) == ERROR_SUCCESS)
						{
							if (IsFolderAllowedForSearch(szInstallLocation))
								pInfo->m_strInstallLocation = szInstallLocation;
						}
						RegCloseKey(hKey);
						return TRUE;
					}
				}
			}

			RegCloseKey(hKey);
		}
	}

	return FALSE;
}

BOOL CVSTrackFinder::CheckIfMSIGUIDIsExistProduct(CString& strMSIData)
{
	if (::WaitForSingleObject(*m_pJobsDoneEvent, 0) == WAIT_OBJECT_0)
		return FALSE;

	//MSI is 64bit on 64 and 32bit on 32 Windows
	REGSAM regPermissions = KEY_READ;

	TCHAR    achKey[MAX_PATH];   // buffer for subkey name
	DWORD    cbName;                   // size of name string 
	TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
	DWORD    cchClassName = MAX_PATH;  // size of class string 
	DWORD    cSubKeys = 0;               // number of subkeys 
	DWORD    cbMaxSubKey;              // longest subkey size 

	DWORD i, retCode;

	TCHAR* pachValue = NULL;
	DWORD  cchValue = 0;

	BYTE* pbtData = NULL;
	DWORD  dwDataType = 0;
	DWORD  dwDataSize = 0;

	TCHAR szKeyPath[1024] = { _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Products") };

	HKEY hKey;
	LONG lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, szKeyPath, 0, regPermissions, &hKey);
	if (lResult == ERROR_SUCCESS)
	{
		// Get the class name and the value count. 
		retCode = RegQueryInfoKey(
			hKey,                    // key handle 
			achClass,                // buffer for class name 
			&cchClassName,           // size of class string 
			NULL,                    // reserved 
			&cSubKeys,               // number of subkeys 
			&cbMaxSubKey,            // longest subkey size 
			NULL,		             // longest class string 
			NULL,	                 // number of values for this key 
			NULL,			         // longest value name 
			NULL,			         // longest value data 
			NULL,					 // security descriptor 
			NULL);				     // last write time 


// Enumerate the subkeys, until RegEnumKeyEx fails.   
		if (cSubKeys)
		{
			for (i = 0; i < cSubKeys; i++)
			{
				cbName = MAX_PATH;
				retCode = RegEnumKeyEx(hKey, i,
									   achKey,
									   &cbName,
									   NULL,
									   NULL,
									   NULL,
									   NULL);
				if (retCode == ERROR_SUCCESS)
				{
					if (strMSIData.CompareNoCase(achKey) == 0)
						return FALSE;
				}
			}
		}

		RegCloseKey(hKey);
	}

	//if it is not found in All Users then check Current User

	StringCchCopy(szKeyPath, 1024, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\"));
	StringCchCat(szKeyPath, 1024, GetCurrentUserSID());
	StringCchCat(szKeyPath, 1024, _T("\\Products"));

	lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, szKeyPath, 0, regPermissions, &hKey);
	if (lResult == ERROR_SUCCESS)
	{

		// Get the class name and the value count. 
		retCode = RegQueryInfoKey(
			hKey,                    // key handle 
			achClass,                // buffer for class name 
			&cchClassName,           // size of class string 
			NULL,                    // reserved 
			&cSubKeys,               // number of subkeys 
			&cbMaxSubKey,            // longest subkey size 
			NULL,		             // longest class string 
			NULL,	                 // number of values for this key 
			NULL,			         // longest value name 
			NULL,			         // longest value data 
			NULL,					 // security descriptor 
			NULL);				     // last write time 


// Enumerate the subkeys, until RegEnumKeyEx fails.   
		if (cSubKeys)
		{
			for (i = 0; i < cSubKeys; i++)
			{
				cbName = MAX_PATH;
				retCode = RegEnumKeyEx(hKey, i,
									   achKey,
									   &cbName,
									   NULL,
									   NULL,
									   NULL,
									   NULL);
				if (retCode == ERROR_SUCCESS)
				{
					if (strMSIData.CompareNoCase(achKey) == 0)
						return FALSE;
				}
			}
		}

		RegCloseKey(hKey);
	}


	return TRUE;
}

BOOL CVSTrackFinder::IsFolderAllowedForSearch(LPCTSTR pszFolderName)
{

	if (pszFolderName == NULL)
		return FALSE;

	if (_tcslen(pszFolderName) == 0)
		return FALSE;

	CString strFolderName = pszFolderName;
	::PathUnquoteSpaces(strFolderName.GetBuffer());
	strFolderName.ReleaseBuffer();

	if (strFolderName.IsEmpty())
		return FALSE;

	if (strFolderName.GetAt(1) != TEXT(':'))
		return FALSE;

	::PathRemoveBackslash(strFolderName.GetBuffer());
	strFolderName.ReleaseBuffer();

	int nLen = strFolderName.GetLength() * 2;
	TCHAR* pszTmp = new TCHAR[nLen];
	pszTmp[0] = 0;
	::ExpandEnvironmentStrings(strFolderName, pszTmp, nLen);
	strFolderName = pszTmp;
	delete[] pszTmp;

	if (PathFileExists(strFolderName) != FALSE)
	{
		pszTmp = new TCHAR[nLen];
		pszTmp[0] = 0;
		::GetLongPathName(strFolderName, pszTmp, nLen);
		strFolderName = pszTmp;
		delete[] pszTmp;
	}

	if (::PathIsRoot(strFolderName))
		return FALSE;

	//Read excluded folders from the Registry to strarrFolders
	CStringArray strarrFolders;
	CUIntArray dwarrData;

	if (VSGetSettings(CAT_UNINSTALLER_ALLPROGSFOLDEREXCLUDE, strarrFolders, dwarrData) == FALSE)
	{
		FolderExcludeInfo arrAllExcFolderInfo[ALL_EXC_FOLDERS_COUNT];

		CString strLogsFolder;
		GetLocalAppData(strLogsFolder);
		strLogsFolder += TEXT("\\Logs\\");

		CString strLogsDir;
		if (VSGetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsDir) == FALSE)
		{
			VSSetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsFolder);
			strLogsDir = strLogsFolder;
		}

		if (strLogsDir.Right(1) == TEXT("\\"))
			strLogsDir.Delete(strLogsDir.GetLength() - 1);


		arrAllExcFolderInfo[0].m_strFolderPath = strLogsDir;
		arrAllExcFolderInfo[0].m_dwLoad = 1;

		for (int i = 0; i < ALL_EXC_FOLDERS_COUNT; i++)
		{
			strarrFolders.Add(arrAllExcFolderInfo[i].m_strFolderPath);
			dwarrData.Add(arrAllExcFolderInfo[i].m_dwLoad);
		}
		VSSetSettings(CAT_UNINSTALLER_ALLPROGSFOLDEREXCLUDE, strarrFolders, dwarrData);
	}

	//if(m_strAppName.CompareNoCase(TEXT("Steam")) == 0)
	//{
	//	strarrFolders.Add(TEXT("steamapps"));
	//	dwarrData.Add(1);
	//}



	INT_PTR nElems = strarrFolders.GetCount();
	for (INT_PTR nIndex = 0; nIndex < nElems; nIndex++)
	{
		if (StrStrI(strFolderName, strarrFolders.GetAt(nIndex)) != NULL)
			return FALSE;
	}

	//TCHAR szRestrictedFolderName[4096] = {0};
	// C:\Program Files\Common
	if (m_strCSIDL_PROGRAM_FILES_COMMON.IsEmpty() == FALSE)
	{
		if (StrStrI(strFolderName, m_strCSIDL_PROGRAM_FILES_COMMON))
			return FALSE;

		CString strMSShared = m_strCSIDL_PROGRAM_FILES_COMMON;
		strMSShared += TEXT("\\Microsoft Shared");
		if (StrCmpI(strFolderName, strMSShared) == NULL)
			return FALSE;
	}

	// C:\Windows
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_WINDOWS.IsEmpty() == FALSE)
	{
		if (m_bSpecialProgram == FALSE)
		{
			if (StrStrI(strFolderName, m_strCSIDL_WINDOWS))
				return FALSE;
		}
	}

	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_PROGRAM_FILES.IsEmpty() == FALSE)
	{
		// C:\Program Files
		if (StrCmpI(strFolderName, m_strCSIDL_PROGRAM_FILES) == NULL)
			return FALSE;

		CString strPath = m_strCSIDL_PROGRAM_FILES;
		strPath += TEXT("\\Internet Explorer");
		if (StrStrI(strFolderName, strPath) != NULL)
			return FALSE;

		// C:\Program Files\\Windows NT
		//StringCchCat(szRestrictedFolderName, 4096, _T("\\Windows NT"));
		CString strNT = m_strCSIDL_PROGRAM_FILES;
		strNT += _T("\\Windows NT");
		if (StrStrI(strFolderName, strNT))
			return FALSE;

		CString strGadgets = m_strCSIDL_PROGRAM_FILES;
		strGadgets += TEXT("\\Windows Sidebar\\Gadgets");
		if (StrCmpI(strFolderName, strGadgets) == NULL)
			return FALSE;

		if (!m_bUninstallingWinApp && !m_bUninstallingEdgeExt)
		{
			CString strGadgets = m_strCSIDL_PROGRAM_FILES;
			strGadgets += TEXT("\\WindowsApps");
			if (StrCmpI(strFolderName, strGadgets) == NULL)
				return FALSE;
		}

	}

	// My Documents
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_PERSONAL.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_PERSONAL) == NULL)
			return FALSE;
	}

	// Start Menu\Programs
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_PROGRAMS.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_PROGRAMS) == NULL)
			return FALSE;
	}


	// <user name>\Start Menu
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_STARTMENU.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_STARTMENU) == NULL)
			return FALSE;
	}

	// <user name>\Desktop
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_DESKTOPDIRECTORY.IsEmpty() == FALSE)
		if (StrCmpI(strFolderName, m_strCSIDL_DESKTOPDIRECTORY) == NULL)
			return FALSE;

	// All Users\Desktop
	//memset(szRestrictedFolderName, 0, 4096);
	/*if(m_strCSIDL_COMMON_DESKTOPDIRECTORY.IsEmpty() == FALSE)
		if(StrCmpI(strFolderName, m_strCSIDL_COMMON_DESKTOPDIRECTORY) == NULL)
			return FALSE;*/

			// All Users\Start Menu
			//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_STARTMENU.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_STARTMENU) == NULL)
			return FALSE;
	}

	// All Users\Start Menu\Programs
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_PROGRAMS.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_PROGRAMS) == NULL)
			return FALSE;
	}

	// All Users\Desktop
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_DESKTOPDIRECTORY.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_DESKTOPDIRECTORY) == NULL)
			return FALSE;
	}

	// All Users\Start Menu
	//memset(szRestrictedFolderName, 0, 4096);
	////if(m_strCSIDL_COMMON_STARTMENU.IsEmpty() == FALSE) 
	////{
	////	if(StrCmpI(strFolderName, m_strCSIDL_COMMON_STARTMENU) == NULL)
	////		return FALSE;
	////}

	// <user name>\Local Settings\Applicaiton Data (non roaming)
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_LOCAL_APPDATA.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_LOCAL_APPDATA) == NULL)
			return FALSE;

		CString strPath = m_strCSIDL_LOCAL_APPDATA;
		strPath += TEXT("\\Microsoft\\Windows Virtual PC\\Virtual Applications");
		if (StrStrI(strFolderName, strPath) != NULL)
			return FALSE;

		// <user name>\Local Settings\Applicaiton Data\Microsoft\\Windows Sidebar\\Gadgets
		CString strGadgets = m_strCSIDL_LOCAL_APPDATA;
		strGadgets += _T("\\Microsoft\\Windows Sidebar\\Gadgets");
		if (StrStrI(strFolderName, strGadgets))
			return FALSE;

		CString strVS = m_strCSIDL_LOCAL_APPDATA;
		strVS += _T("\\VirtualStore\\Program Files (x86)");
		if (StrCmpI(strFolderName, strVS) == NULL)
			return FALSE;

		CString strQL = m_strCSIDL_LOCAL_APPDATA;
		strQL += _T("\\Microsoft\\Internet Explorer\\Quick Launch");
		//_tcscat_s(szRestrictedFolderName, 4096, _T("\\Microsoft\\Internet Explorer\\Quick Launch"));
		if (StrCmpI(strFolderName, strQL) == NULL)
			return FALSE;
	}

	// All Users\Application Data
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_APPDATA.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_APPDATA) == NULL)
			return FALSE;

		CString strPath = m_strCSIDL_COMMON_APPDATA, strFolder;

		//Extract the All Users folder from Common App Data
		GetLastFolderPath(strPath, &strFolder);
		if (StrCmpI(strFolderName, strFolder) == NULL)
			return FALSE;
	}

	// <user name>\Application Data
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_APPDATA.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_APPDATA) == NULL)
			return FALSE;
	}

	// All Users\Documents
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_DOCUMENTS.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_DOCUMENTS) == NULL)
			return FALSE;
	}

	// My Computer\Control Panel
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_CONTROLS.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_CONTROLS) == NULL)
			return FALSE;
	}

	// USERPROFILE
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_PROFILE.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_PROFILE) == NULL)
			return FALSE;
	}

	// All Users\Startup
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_STARTUP.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_STARTUP) == NULL)
			return FALSE;
	}

	// Start Menu\Programs\Startup
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_STARTUP.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_STARTUP) == NULL)
			return FALSE;
	}

	// "My Music" folder
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_MYMUSIC.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_MYMUSIC) == NULL)
			return FALSE;
	}

	// "My Videos" folder
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_MYVIDEO.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_MYVIDEO) == NULL)
			return FALSE;
	}

	// C:\Program Files\My Pictures
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_MYPICTURES.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_MYPICTURES) == NULL)
			return FALSE;
	}

	// All Users\Templates
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_TEMPLATES.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_TEMPLATES) == NULL)
			return FALSE;
	}

	// All Users\My Music
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_MUSIC.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_MUSIC) == NULL)
			return FALSE;
	}

	// All Users\My Pictures
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_PICTURES.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_PICTURES) == NULL)
			return FALSE;
	}

	// All Users\My Video
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_VIDEO.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_VIDEO) == NULL)
			return FALSE;
	}

	// Resource Direcotry
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_RESOURCES.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_RESOURCES) == NULL)
			return FALSE;
	}

	// Valid only for Windows NT systems
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_COMMON_FAVORITES.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_COMMON_FAVORITES) == NULL)
			return FALSE;
	}

	// C:\Documents and Settings\username\Favorites
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_FAVORITES.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_FAVORITES) == NULL)
			return FALSE;
	}

	//Exclude Quick Launch
	///*memset(szRestrictedFolderName, 0, 4096);
	//if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szRestrictedFolderName))) 
	//{
	//	_tcscat_s(szRestrictedFolderName, 4096, _T("\\Microsoft\\Internet Explorer\\Quick Launch"));
	//	if(StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
	//		return FALSE;
	//}*/

	// All Users
	////memset(szRestrictedFolderName, 0, 4096);
	////if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szRestrictedFolderName))) 
	////{
	////	CString strPath = szRestrictedFolderName, strFolder;
	////
	////	//Extract the All Users folder from Common App Data
	////	GetLastFolderPath(strPath, &strFolder);
	////	if(StrCmpI(strFolderName, strFolder) == NULL)
	////		return FALSE;
	////}

	// %ProgramFilesx86% (%SystemDrive%\Program Files)
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_PROGRAM_FILESX86.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_PROGRAM_FILESX86) == NULL)
			return FALSE;

		CString strPath = m_strCSIDL_PROGRAM_FILESX86;
		strPath += TEXT("\\Internet Explorer");
		if (StrStrI(strFolderName, strPath) != NULL)
			return FALSE;

		// C:\Program Files(86)\\Windows NT
		CString strNT = m_strCSIDL_PROGRAM_FILESX86;
		strNT += _T("\\Windows NT");
		if (StrStrI(strFolderName, strNT))
			return FALSE;

	}

	// x86 Program Files\Common on RISC
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_PROGRAM_FILES_COMMONX86.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_PROGRAM_FILES_COMMONX86) == NULL)
			return FALSE;

		CString strMSShared = m_strCSIDL_PROGRAM_FILES_COMMONX86;
		strMSShared += TEXT("\\Microsoft Shared");
		if (StrCmpI(strFolderName, strMSShared) == NULL)
			return FALSE;
	}

	// x86 system directory on RISC
	//memset(szRestrictedFolderName, 0, 4096);
	if (m_strCSIDL_SYSTEMX86.IsEmpty() == FALSE)
	{
		if (StrCmpI(strFolderName, m_strCSIDL_SYSTEMX86) == NULL)
			return FALSE;
	}

	// %ProgramFiles%\Windows Sidebar\Gadgets
	////memset(szRestrictedFolderName, 0, 4096);
	////ExpandEnvironmentStrings(_T("%ProgramFiles%\\Windows Sidebar\\Gadgets"), szRestrictedFolderName, 4096);
	////if(StrCmpI(strFolderName, szRestrictedFolderName) == NULL)
	////	return FALSE;


	return TRUE;
}
void CVSTrackFinder::InitCSIDLs()
{
	TCHAR szRestrictedFolderName[4096] = { 0 };
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMON, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PROGRAM_FILES_COMMON = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_WINDOWS, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_WINDOWS = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PROGRAM_FILES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PERSONAL = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_PROGRAMS, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PROGRAMS = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_STARTMENU, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_STARTMENU = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_DESKTOPDIRECTORY = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_DESKTOPDIRECTORY, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_DESKTOPDIRECTORY = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_STARTMENU, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_STARTMENU = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_PROGRAMS, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_PROGRAMS = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_DESKTOPDIRECTORY, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_DESKTOPDIRECTORY = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_LOCAL_APPDATA = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_APPDATA = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_APPDATA = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_DOCUMENTS, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_DOCUMENTS = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_CONTROLS, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_CONTROLS = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PROFILE = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_STARTUP, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_STARTUP = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_STARTUP, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_STARTUP = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_MYMUSIC, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_MYMUSIC = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_MYVIDEO, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_MYVIDEO = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_MYPICTURES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_MYPICTURES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_TEMPLATES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_TEMPLATES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_MUSIC, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_MUSIC = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_PICTURES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_PICTURES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_VIDEO, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_VIDEO = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_RESOURCES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_RESOURCES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_COMMON_FAVORITES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_COMMON_FAVORITES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_FAVORITES, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_FAVORITES = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PROGRAM_FILESX86 = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES_COMMONX86, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_PROGRAM_FILES_COMMONX86 = szRestrictedFolderName;

	memset(szRestrictedFolderName, 0, 4096);
	SHGetFolderPath(NULL, CSIDL_SYSTEMX86, NULL, 0, szRestrictedFolderName);
	m_strCSIDL_SYSTEMX86 = szRestrictedFolderName;

}

void CVSTrackFinder::SearchInHKCUExplorerFileExt(RegDataList* pExtDatalist, eRegWOW6432Type eKeyWOW, REGSAM regPermissions)
{
	CStringArray arrIgnore, arrExt;
	CString strRet;

	POSITION pos = pExtDatalist->GetHeadPosition();
	while (pos != NULL)
	{
		FoundRegData* temp = (FoundRegData*)pExtDatalist->GetNext(pos);

		CString strRet;
		GetSeparatedKey(temp->strFullKeyPath, 6, &strRet);
		if (StrCmpI(strRet, _T("Shell")) == 0)
		{
			GetSeparatedKey(temp->strFullKeyPath, 8, &strRet);
			if (StrCmpI(strRet, _T("Command")) == 0)
			{
				//if the extension is associated to open the file
				//then add for deleting the wholse ext key
				CString strOpenCmd;
				GetSeparatedKey(temp->strFullKeyPath, 7, &strOpenCmd);
				if (StrCmpI(strOpenCmd, _T("Open")) == 0)
				{
					GetSeparatedKey(temp->strFullKeyPath, 5, &strRet);
					if (StrCmpI(strRet.Left(1), _T(".")) != 0)
						arrExt.Add(strRet);
				}

			}

		}
		else
		{
			if (StrCmpI(strRet, _T("defaultIcon")) == 0)
			{
				GetSeparatedKey(temp->strFullKeyPath, 5, &strRet);
				if (StrCmpI(strRet.Left(1), _T(".")) != 0)
					arrExt.Add(strRet);
			}

		}

	}
	//arrExt.Append(m_arrSearchPattern);

	TCHAR szFullRegPath[65536] = { 0 };
	RegDataList listRegDataExplExt;
	DWORD dwFoundCount = 0;
	if (arrExt.GetCount() > 0)
	{
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts"), szFullRegPath, arrExt, &listRegDataExplExt, &dwFoundCount, &arrIgnore, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, FALSE);
	}

	if (m_arrSearchPattern.GetCount() > 0)
	{
		dwFoundCount = 0;
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts"), szFullRegPath, m_arrSearchPattern, &listRegDataExplExt, &dwFoundCount, &arrIgnore, m_b64Bit, -1, -1, FALSE, TRUE, TRUE, TRUE);
	}


	pos = listRegDataExplExt.GetHeadPosition();

	while (pos != NULL)
	{
		CString strKeyToOpen, strCurFullKeyPath;
		FoundRegData* temp = (FoundRegData*)listRegDataExplExt.GetNext(pos);
		CRegistry oRegExplorerExt(HKEY_CURRENT_USER);
		GetSubKey(temp->strFullKeyPath, 10, &strCurFullKeyPath);
		GetRegKeyForOpen(strCurFullKeyPath, strKeyToOpen);
		if (oRegExplorerExt.Open(strKeyToOpen, regPermissions))
		{
			if (temp->strKeyName.CompareNoCase(TEXT("OpenWithProgids")) == 0)
			{
				if (oRegExplorerExt.GetNumberOfSubkeys() == 1)
				{
					CString strFullKeyPath(_T("\\REGISTRY\\HKEY_CURRENT_USER\\"));
					strFullKeyPath += strKeyToOpen;
					PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
					if (pNode)
						pNode->eOperation = RegCreated;
				}
				else
				{
					//CString strFullKeyPath = TEXT("\\");
					//strFullKeyPath += temp->strFullKeyPath;
					//PREGNODE pNode = m_pTree->CreateKey(strFullKeyPath, eKeyWOW);
					//if(pNode)
					//	pNode->eOperation = RegCreated;
					m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);
				}
			}
			else
				m_pTree->SetKeyVal(temp->strFullKeyPath, temp->strValueName, temp->dwType, temp->pbtData, temp->dwDataSize, 0, NULL, 0, eKeyWOW);

			oRegExplorerExt.Close();
		}
		if (temp->dwDataSize)
			delete[] temp->pbtData;

		delete temp;

	}
}

BOOL CVSTrackFinder::MarkFoundParentKeyAsCreated(CString& strFullPath, CStringArray* parrIgnore)
{
	if (strFullPath.IsEmpty())
		return FALSE;

	REGSAM regPermissions;
	eRegWOW6432Type eKeyWOW;
	if (m_b64Bit)
	{
		eKeyWOW = Key64Bit;
		regPermissions = KEY_READ | KEY_WOW64_64KEY;
	}
	else
	{
		eKeyWOW = Key32Bit;
		regPermissions = KEY_READ | KEY_WOW64_32KEY;
	}

	PREGNODE pMainKey = m_pTree->GetKey(strFullPath);
	if (pMainKey)
	{
		PREGNODE pMainKeyFoundChild = pMainKey->pChild;
		while (pMainKeyFoundChild)
		{
			if (parrIgnore)
			{
				if (IsStringInList(pMainKeyFoundChild->pszRegKeyName, parrIgnore))
				{
					pMainKeyFoundChild = pMainKeyFoundChild->pBrother;
					continue;
				}
			}
			if (pMainKeyFoundChild->eOperation != RegCreated)
			{
				if (pMainKeyFoundChild->pChild)
				{
					if (pMainKeyFoundChild->pChild->eOperation == RegCreated)
					{
						CString strRoot, strKeyToOpen, strNewFullPath = strFullPath;
						strNewFullPath += TEXT("\\");
						strNewFullPath += pMainKeyFoundChild->pszRegKeyName;
						GetSeparatedKey(strNewFullPath, 2, &strRoot);

						HKEY hRoot;
						if (StrCmpI(strRoot, _T("HKEY_LOCAL_MACHINE")) == 0)
							hRoot = HKEY_LOCAL_MACHINE;
						else
							hRoot = HKEY_CURRENT_USER;

						CRegistry oKey(hRoot);
						GetRegKeyForOpen(strNewFullPath, strKeyToOpen);
						if (oKey.Open(strKeyToOpen, regPermissions))
						{
							if (oKey.GetNumberOfSubkeys() == 1)
							{
								pMainKeyFoundChild->eOperation = RegCreated;
								if (m_b64Bit)
									pMainKeyFoundChild->eWOW6432Type = Key64Bit;
								else
									pMainKeyFoundChild->eWOW6432Type = Key32Bit;
							}

						}
					}
				}
			}

			pMainKeyFoundChild = pMainKeyFoundChild->pBrother;
		}
	}
	return TRUE;
}

BOOL CVSTrackFinder::GetFileNameFromPath(CString& strPath, CString& strRes)
{
	CStringArray arrIgnore;
	arrIgnore.Add(TEXT("Uninstall"));
	arrIgnore.Add(TEXT("unins000"));
	arrIgnore.Add(TEXT("setup"));
	arrIgnore.Add(TEXT("uninst"));
	arrIgnore.Add(TEXT("uninstaller"));

	int nLen = strPath.GetLength();
	if (nLen)
	{
		CString strTemp = strPath, strFileName;
		int nSlash = strTemp.ReverseFind(_T('\\'));
		strFileName = strTemp.Right(nLen - nSlash - 1);
		if (strFileName.Right(4).CompareNoCase(TEXT(".exe")) == 0)
		{
			int nFileNameLen = strFileName.GetLength();
			strFileName.Delete(nFileNameLen - 4, 4);
		}
		else
			return FALSE;


		if (IsStringInList(strFileName, &arrIgnore, TRUE))
			return FALSE;
		else
			strRes = strFileName;

		return TRUE;
	}

	return FALSE;
}

BOOL CVSTrackFinder::AddParentFolderAsFound(CString& strFolderName)
{
	if (strFolderName.IsEmpty())
		return FALSE;

	if (strFolderName.Right(1) == TEXT("\\"))
		strFolderName.Delete(strFolderName.GetLength() - 1);

	CString strParentFolder;
	if (GetLastFolderPath(strFolderName, &strParentFolder))
	{
		if (IsFolderAllowedForSearch(strParentFolder))
		{
			int nSubFolderCount = 0;
			CString strSubFolder; // not used here
			GetNumberofSubFolders(strParentFolder, nSubFolderCount, strSubFolder);
			if (nSubFolderCount == 1)
			{
				PutFoundFilesFolders(strParentFolder);
				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CVSTrackFinder::AddParentInstallLocAsFound(CString& strFolderName)
{
	if (strFolderName.IsEmpty())
		return FALSE;

	if (strFolderName.Right(1) == TEXT("\\"))
		strFolderName.Delete(strFolderName.GetLength() - 1);

	CString strParentFolder;
	if (GetLastFolderPath(strFolderName, &strParentFolder))
	{
		if (IsFolderAllowedForSearch(strParentFolder))
		{
			CString strSubFolder;
			int nSubFolderCount = 0;
			GetNumberofSubFolders(strParentFolder, nSubFolderCount, strSubFolder);
			if (nSubFolderCount <= 1)
			{
				if (nSubFolderCount == 0)
					PutFoundFilesFolders(strParentFolder);
				else
				{
					if (strFolderName.CompareNoCase(strSubFolder) == 0)
						PutFoundFilesFolders(strParentFolder);
				}
				return TRUE;
			}
		}
	}

	return FALSE;
}

void CVSTrackFinder::AddBackSlashToPaths(CStringArray& arrModFolders)
{
	for (int i = 0; i < arrModFolders.GetCount(); i++)
	{
		TCHAR	szPath[2048] = { 0 };
		StringCchCopy(szPath, 2048, arrModFolders.GetAt(i));
		::PathAddBackslash(szPath);
		arrModFolders.SetAt(i, szPath);
	}
}
///return TRUE if the String is not found in the Array
BOOL  CVSTrackFinder::NotInTheArray(CStringArray& arrTypeLibs, CString& strTypeLib)
{
	INT_PTR nCount = arrTypeLibs.GetCount();
	for (INT_PTR i = 0; i < nCount; i++)
	{
		if (strTypeLib.CompareNoCase(arrTypeLibs.GetAt(i)) == 0)
			return FALSE;
	}
	return TRUE;
}

void CVSTrackFinder::CheckIsSpecProgram()
{

	if (m_parrFUProgNames)
	{
		for (int i = 0; i < m_parrFUProgNames->GetCount(); i++)
		{
			CString strName = m_parrFUProgNames->GetAt(i);
			if (strName.Find(TEXT("Adobe Flash Player")) != -1)
			{
				AddAdobeFlashData();
				return;
			}
			else if (strName.Find(TEXT("Adobe Dreamweaver")) != -1)
			{
				AddSearchPattern(TEXT("Dreamweaver.exe"));
				AddSearchPattern(TEXT("Dreamweaver"));
				CString strPath = m_strInstallLocation + TEXT("\\") + m_strAppName;
				PutFoldersForSearch(strPath);
				return;
			}
			if (m_uninModerateMode == uninAdvancedMode)
			{
				if ((strName.Find(TEXT("Java")) != -1) && (strName.Find(TEXT("Update")) != -1))
				{
					AddJavaUpdateData();
					return;
				}
			}

		}
	}
	else
	{
		if (m_strAppName.Find(TEXT("Adobe Flash Player")) != -1)
			AddAdobeFlashData();
		else if (m_strAppName.Find(TEXT("Adobe Dreamweaver")) != -1)
		{
			CString strPath = m_strInstallLocation + TEXT("\\") + m_strAppName;
			PutFoldersForSearch(strPath);
			AddSearchPattern(TEXT("Dreamweaver.exe"));
			AddSearchPattern(TEXT("Dreamweaver"));
			return;
		}
		if (m_uninModerateMode == uninAdvancedMode)
		{
			if ((m_strAppName.Find(TEXT("Java")) != -1) && (m_strAppName.Find(TEXT("Update")) != -1))
			{
				AddJavaUpdateData();
				return;
			}
		}
	}

	if ((m_strKeyName.Find(TEXT("Steam")) != -1) && (m_strAppName.CompareNoCase(TEXT("Steam")) != 0)) // it is a steam app/game
	{
		m_strUninstString.Empty();
	}
}
void CVSTrackFinder::AddAdobeFlashData()
{
	CString strFolderPath;
	m_bSpecialProgram = TRUE;

	strFolderPath = m_strCSIDL_LOCAL_APPDATA;
	strFolderPath += TEXT("\\");
	strFolderPath += TEXT("Macromedia");
	if (VS_IsExistingDirectory(strFolderPath))
		PutFoldersForSearch(strFolderPath);

	strFolderPath = m_strCSIDL_APPDATA;
	strFolderPath += TEXT("\\");
	strFolderPath += TEXT("Macromedia");
	if (VS_IsExistingDirectory(strFolderPath))
		PutFoldersForSearch(strFolderPath);

	strFolderPath = m_strCSIDL_WINDOWS;
	strFolderPath += TEXT("\\System32\\");
	strFolderPath += TEXT("Macromed");
	if (VS_IsExistingDirectory(strFolderPath))
		PutFoldersForSearch(strFolderPath);

}

void CVSTrackFinder::AddJavaUpdateData()
{
	CString strFolderPath, strLocalLowPath;

	strFolderPath = m_strCSIDL_APPDATA;
	strFolderPath += TEXT("\\");
	strFolderPath += TEXT("Sun\\Java");
	if (VS_IsExistingDirectory(strFolderPath))
		PutFoldersForSearch(strFolderPath);

	strFolderPath = m_strCSIDL_LOCAL_APPDATA;
	strFolderPath += TEXT("Low\\");
	strLocalLowPath = strFolderPath;
	strFolderPath += TEXT("Sun\\Java");
	if (VS_IsExistingDirectory(strFolderPath))
		PutFoldersForSearch(strFolderPath);

	strLocalLowPath += TEXT("Oracle\\Java");
	if (VS_IsExistingDirectory(strLocalLowPath))
		PutFoldersForSearch(strLocalLowPath);

	strFolderPath = m_strCSIDL_COMMON_APPDATA;
	strFolderPath += TEXT("\\");
	strFolderPath += TEXT("Oracle\\Java");
	if (VS_IsExistingDirectory(strFolderPath))
		PutFoldersForSearch(strFolderPath);

}

void CVSTrackFinder::UninstallingWinApp()
{
	AddSearchPattern(m_strKeyName);
	AddSearchPattern(m_strInternalName);
	CString strIntNameNoDot = m_strInternalName;
	strIntNameNoDot.Remove(TEXT('.'));
	AddSearchPattern(strIntNameNoDot);

	BOOL bIsMicrosoftApp = FALSE;
	if (m_strKeyName.Left(9).CompareNoCase(TEXT("Microsoft")) == 0)
		bIsMicrosoftApp = TRUE;

	//if (IsFolderAllowedForSearch(m_strInstallLocation))
	//	PutFoldersForSearch(m_strInstallLocation);


	///RegistrySearch(m_packageId, L"Software\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer\\Storage\\", HKEY_CURRENT_USER, regsam, m_registryLocations);
	//Computer\HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\SettingSync\00016E80FCD731D1\Registrar\Data\Registered
	////RegistrySearch(m_packageId, L"Software\\Microsoft\\Windows\\CurrentVersion\\SettingSync\\", HKEY_CURRENT_USER, regsam, m_registryLocations);



	if (m_arrSearchPattern.GetCount())
	{
		TCHAR  szFullRegPath[65536] = { 0 };

		int nDepth = 1;
		DWORD dwFoundCount = 0;

		//RegDataList listFoundKeys;

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Local Settings\\MrtCache"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\Local Settings\\MrtCache"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, FALSE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\Repository\\Packages"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\Repository\\Packages"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, FALSE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\PackageRepository\\Packages"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\PackageRepository\\Packages"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, FALSE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\Deployment\\Package\\*"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\Deployment\\Package\\*"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, FALSE, FALSE, TRUE, FALSE);

		if (m_bUninstallingEdgeExt)
		{
			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\SystemAppData\\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\\PersistedStorageItemTable\\ManagedByApp"));
			SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\SystemAppData\\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\\PersistedStorageItemTable\\ManagedByApp"), szFullRegPath,
						   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, FALSE, FALSE);

			StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\Software\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\SystemAppData\\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\\PersistedStorageItemTable\\ManagedByApp"));
			SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\SystemAppData\\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\\PersistedStorageItemTable\\ManagedByApp"), szFullRegPath,
						   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, nDepth, -1, TRUE, FALSE, FALSE, FALSE);
		}

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\InstallService\\State\\CategoryCache"));
		SearchRegistry(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\InstallService\\State\\CategoryCache"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, TRUE, FALSE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer\\Storage"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer\\Storage\\"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SettingSync"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SettingSync"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, -1, -1, TRUE, FALSE, FALSE, FALSE);
		//Computer\HKEY_CURRENT_USER\Software\Microsoft\UserData\UninstallTimes

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\UserData\\UninstallTimes"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\UserData\\UninstallTimes"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, 1, 1, FALSE, TRUE, FALSE, FALSE);

		//[HKEY_USERS\S - 1 - 5 - 21 - 630932988 - 2882308458 - 1933330123 - 1001\Software\Microsoft\Windows\CurrentVersion\SettingSync\Telemetry\SaveKnowledgeLastSuccess]

		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SettingSync\\Telemetry\\SaveKnowledgeLastSuccess"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SettingSync\\Telemetry\\SaveKnowledgeLastSuccess"), szFullRegPath,
					   m_arrSearchPattern, m_pTree, &dwFoundCount, NULL, m_b64Bit, 1, 1, FALSE, TRUE, FALSE, FALSE);

		//Computer\HKEY_CURRENT_USER\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\EdgeExtensions\Configuration\EdgeExtensions\ConfigurationStore\State
		//Computer\HKEY_CURRENT_USER\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Protected - It is a violation of Windows Policy to modify. See aka.ms/browserpolicy\Extensions

		//int nBreak = 1;
		//nDepth = 2;
		dwFoundCount = 0;

		RegDataList listFoundKeys;
		//Computer\HKEY_CURRENT_USER\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppModel\SystemAppData\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\PersistedStorageItemTable\ManagedByApp\EdgeExtension_37237honestbleepsRedditEnhancementSuite_jzcrwe0958h6m
		StringCchCopy(szFullRegPath, 65536, _T("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\SystemAppData"));
		SearchRegistry(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel\\SystemAppData"),
					   szFullRegPath, m_arrSearchPattern, &listFoundKeys, &dwFoundCount, NULL, m_b64Bit, -1, -1, FALSE, FALSE, TRUE, TRUE);

		POSITION pos = listFoundKeys.GetHeadPosition();
		while (pos != NULL)
		{
			FoundRegData* temp = (FoundRegData*)listFoundKeys.GetNext(pos);
			CString strLastKey, strLastKeyPath;
			GetLastKeyNameAndItsPath(temp->strFullKeyPath, strLastKey, strLastKeyPath);

			PREGNODE pNode = NULL;
			if (m_b64Bit)
				pNode = m_pTree->CreateKey(strLastKeyPath, Key64Bit);
			else
				pNode = m_pTree->CreateKey(strLastKeyPath, Key32Bit);

			if (pNode)
				pNode->eOperation = RegCreated;


			if (temp->dwDataSize)
				delete[] temp->pbtData;

			delete temp;
		}


		//////m_arrFoundFilesFolders.Append(arrFound);
	}
	if (bIsMicrosoftApp == FALSE)
	{
		PutFoundFilesFolders(m_strInstallLocation);
		CStringArray arrFound, arrNames, arrIgnore;
		FindAllDataInFolder(m_strInstallLocation, &arrNames, &arrFound, &arrIgnore, -1, -1, TRUE, TRUE, TRUE, TRUE);
		m_arrFoundFilesFolders.Append(arrFound);
	}
}

/// That method use the Install location to get the Publisher name and search for it, but first tried to check if there is only one application
/// from that publisher and only then uses the publisher name and search for it as a patter and also search in the CommonFilres folders
/// 
/// </summary>
/// <param name="strInstallLocation">The install location ofthe program</param>
void CVSTrackFinder::UsePublisherDataIfSingleApp(CString& strInstallLocation)
{
	BOOL bAddPublisherForSearch = FALSE;

	if (strInstallLocation.Right(1) == TEXT("\\"))
		strInstallLocation.Delete(strInstallLocation.GetLength() - 1);

	CString strParentFolder;
	if (GetLastFolderPath(strInstallLocation, &strParentFolder))
	{
		//if (IsFolderAllowedForSearch(strParentFolder))
		//{
		CString strSubFolder;
		int nSubFolderCount = 0;
		GetNumberofSubFolders(strParentFolder, nSubFolderCount, strSubFolder);
		// if there is a brother folder to the one that should be the publisher e return
		//if (nSubFolderCount != 1)
		//	return;

		if (nSubFolderCount <= 1)
		{
			if (nSubFolderCount == 0)
				bAddPublisherForSearch = TRUE;
			else
			{
				if (strInstallLocation.CompareNoCase(strSubFolder) == 0)
					bAddPublisherForSearch = TRUE;
			}
		}
		else
			return;
	}


	//If the path is not in the format C:\ProgramFiles\Publisher\Program Name we return, otherwise get the "Publisher" folder
	CString strFolder;
	if (GetPathDepth(strInstallLocation) != 3)
		return;
	else
		GetSeparatedKey(strInstallLocation, 3, &strFolder);

	//check if in the other Program FIles is a folder with the same Publisher, if so we return as maybe there is another program from the same publisher
	//just different bits
	BOOL nNotinProgramFiles = TRUE;
	if (strInstallLocation.Find(m_strCSIDL_PROGRAM_FILESX86) != -1)
	{
		nNotinProgramFiles = FALSE;
		if (SameNameinOtherProgramFiles(m_strCSIDL_PROGRAM_FILES, strFolder))
			return;
	}
	else if (strInstallLocation.Find(m_strCSIDL_PROGRAM_FILES) != -1)
	{
		nNotinProgramFiles = FALSE;
		if (SameNameinOtherProgramFiles(m_strCSIDL_PROGRAM_FILESX86, strFolder))
			return;
	}

	if (nNotinProgramFiles || !bAddPublisherForSearch)
		return;

	//add publisher name for search
	AddSearchPattern(strFolder);

	//Search for the publisher name in the CommonFiles folders
	CStringArray arrFound, arrNames, arrIgnore;
	arrNames.Add(strFolder);
	FindAllDataInFolder(m_strCSIDL_PROGRAM_FILES_COMMON, &arrNames, &arrFound, &arrIgnore, 1, 1, FALSE, FALSE, TRUE, FALSE);
	//m_arrFoundFilesFolders.Append(arrFound);
	PutFoldersForSearch(arrFound);

	CStringArray arrFoundX86;
	FindAllDataInFolder(m_strCSIDL_PROGRAM_FILES_COMMONX86, &arrNames, &arrFoundX86, &arrIgnore, 1, 1, FALSE, FALSE, TRUE, FALSE);
	m_arrFoundFilesFolders.Append(arrFoundX86);
	PutFoldersForSearch(arrFoundX86);


}
/// Check if the same Publisher does have a folder in the other Program Files folder not in the one the Install Location is
/// We do this in order to understood if the publisher has only one program or more installed.We do not search in the APpData although
/// some programs installs there.
/// <param name="strOtherProgramFilesDir"> One of th both Program Files Folders</param>
/// <param name="strPublisherName">Publisher name we got from the Install Location</param>
/// <returns>TRUE if we have to abort the usage of the Publisher,FALSE to use it</returns>
BOOL CVSTrackFinder::SameNameinOtherProgramFiles(CString& strOtherProgramFilesDir, CString& strPublisherName)
{
	if (strOtherProgramFilesDir.IsEmpty() || strPublisherName.IsEmpty())
		return TRUE;

	HANDLE hFind = NULL;
	WIN32_FIND_DATA fileinfo = { 0 };
	DWORD64    dwSize = 0;

	CString strFileFilter = strOtherProgramFilesDir;
	strFileFilter += TEXT("\\*.*");

	hFind = ::FindFirstFile(strFileFilter, &fileinfo);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{

			if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == FALSE)
			{

				if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
					{
						//Do nothing for "." and ".." folders
						continue;
					}
					else
					{
						//nCounter++;
						//strSubfolder = strFolderName + TEXT("\\") + fileinfo.cFileName;
						if (StrCmpI(strPublisherName, fileinfo.cFileName) == 0)
							return TRUE;
					}
				}
				else
				{
				}
			}

		}
		while (::FindNextFile(hFind, &fileinfo));
	}
	::FindClose(hFind);

	return FALSE;
}

void CVSTrackFinder::ClearRegTreeFromApps()
{
	CStringArray arrKeystoExclude;
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_CURRENT_USER\\Software\\Classes"));
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_CURRENT_USER\\Software\\Classes\\Extensions"));
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_CURRENT_USER\\Software\\Classes\\Local Settings\\MrtCache"));
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_CURRENT_USER\\Software\\Microsoft\\Tracing\\WPPMediaPerApp"));
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel"));
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_CURRENT_USER\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer"));
	arrKeystoExclude.Add(TEXT("REGISTRY\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppModel"));
	int nCount = arrKeystoExclude.GetCount();

	for (int nIndex = 0; nIndex < nCount; nIndex++)
	{
		CString strKey = arrKeystoExclude.GetAt(nIndex);
		PREGNODE pProgID = m_pTree->GetKey(strKey);
		if (pProgID)
		{
			if (nIndex == 0)
			{
				PREGNODE pClassesChildKey = pProgID->pChild, strTempNode;
				while (pClassesChildKey != NULL)
				{
					strTempNode = pClassesChildKey->pBrother;
					CString strLastKey = pClassesChildKey->pszRegKeyName;

					if (strLastKey.Find(TEXT("App")) == 0)
					{
						m_pTree->RemoveNodeFromTree(pClassesChildKey);
					}
					pClassesChildKey = strTempNode;
				}
			}
			else
				m_pTree->RemoveNodeFromTree(pProgID);
		}
	}
}