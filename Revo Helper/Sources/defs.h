#pragma once

#define MAX_KEY_LENGTH 256
#define MAX_VALUE_NAME 16383

#define R_HKCU_RUN_NAME					_T("Registry: HKCU Run")
#define R_HKLM_RUN_NAME					_T("Registry: HKLM Run")
#define R_HKCU_RUN_NAME64				_T("Registry: HKCU 32bit Run")
#define R_HKLM_RUN_NAME64				_T("Registry: HKLM 32bit Run")
#define R_RUN_KEY						_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run")
#define R_RUN_KEY64						_T("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run")

#define R_HKCU_RUNSERVICES_NAME			_T("Registry: HKCU RunServices")
#define R_HKLM_RUNSERVICES_NAME			_T("Registry: HKLM RunServices")
#define R_RUNSERVICES_KEY				_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices")

#define R_HKCU_RUNSERVICESONCE_NAME		_T("Registry: HKCU RunServicesOnce")
#define R_HKLM_RUNSERVICESONCE_NAME		_T("Registry: HKLM RunServicesOnce")
#define R_RUNSERVICESONCE_KEY			_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce")

#define R_HKCU_RUNONCE_NAME				_T("Registry: HKCU RunOnce")
#define R_HKLM_RUNONCE_NAME				_T("Registry: HKLM RunOnce")
#define R_HKCU_RUNONCE_NAME64			_T("Registry: HKCU 32bit RunOnce")
#define R_HKLM_RUNONCE_NAME64			_T("Registry: HKLM 32bit RunOnce")
#define R_RUNONCE_KEY					_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce")
#define R_RUNONCE_KEY64					_T("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce")

#define R_HKCU_EXPLORER_RUN_NAME		_T("Registry: HKCU Explorer Run")
#define R_HKLM_EXPLORER_RUN_NAME		_T("Registry: HKLM Explorer Run")
#define R_EXPLORER_RUN_KEY				_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run")

#define R_HKLM_RUNONCEEX_NAME			_T("Registry: HKLM RunOnceEx")
#define R_HKLM_RUNONCEEX_NAME64			_T("Registry: HKLM 32bit RunOnceEx")
#define R_RUNONCEEX_KEY					_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx")
#define R_RUNONCEEX_KEY64				_T("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx")

#define R_BACKUP_HKCU_RUN				_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU\\Run")
#define R_BACKUP_HKLM_RUN				_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM\\Run")
#define R_BACKUP_HKCU_RUN64				_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU32\\Run")
#define R_BACKUP_HKLM_RUN64				_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM32\\Run")

#define R_BACKUP_HKCU_RUNSERVICES		_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU\\RunServices")
#define R_BACKUP_HKLM_RUNSERVICES		_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM\\RunServices")

#define R_BACKUP_HKCU_RUNSERVICESONCE	_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU\\RunServicesOnce")
#define R_BACKUP_HKLM_RUNSERVICESONCE	_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM\\RunServicesOnce")

#define R_BACKUP_HKCU_RUNONCE			_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU\\RunOnce")
#define R_BACKUP_HKLM_RUNONCE			_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM\\RunOnce")
#define R_BACKUP_HKCU_RUNONCE64			_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU32\\RunOnce")
#define R_BACKUP_HKLM_RUNONCE64			_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM32\\RunOnce")

#define R_BACKUP_HKLM_RUNONCEEX			_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM\\RunOnceEx")
#define R_BACKUP_HKLM_RUNONCEEX64		_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM32\\RunOnceEx")

#define R_BACKUP_HKCU_EXPLORER_RUN		_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKCU\\ExplorerRun")
#define R_BACKUP_HKLM_EXPLORER_RUN		_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\Autoruns\\HKLM\\ExplorerRun")


#define HUNTER_MODE_HELP_URL			_T("http://www.revouninstaller.com/hunter_mode_help_page.html")

#define HOME_PAGE						_T("http://www.revouninstaller.com")
#define UNINSTALLER						_T("UNINSTALLERURL")
#define JFC								_T("JFCURL")
#define WINDOWSTOOLS					_T("WINDOWSTOOLSURL")
#define AUTORUN							_T("AUTORUNURL")
#define TCBROWSERS						_T("TCBROWSERSURL")
#define TCOFFICE						_T("TCOFFICEURL")
#define TCWINDOWS						_T("TCWINDOWSURL")
#define TCER							_T("TCERURL")
#define TCUD							_T("TCUDURL")

#define TOOLBAR_DRAW_BUTTON_WIDTH 32

#ifdef PORTABLE
#define AUTORUN_DISABLED_CU_FILES		_T("\\ADCU")
#define AUTORUN_DISABLED_AU_FILES		_T("\\ADAU")
#else
#define AUTORUN_DISABLED_CU_FILES		_T("\\VS Revo Group\\Revo Uninstaller Pro\\ADCU")
#define AUTORUN_DISABLED_AU_FILES		_T("\\VS Revo Group\\Revo Uninstaller Pro\\ADAU")
#endif

#define R_APP_PATHS_KEY					_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths")
#define R_MUI_CACHE						_T("Software\\Microsoft\\Windows\\ShellNoRoam\\MUICache")
#define R_WIN_FIREWALL					_T("SYSTEM\\CurrentControlSet\\Services\\SharedAccess\\Parameters\\FirewallPolicy")
#define R_HELP_KEYS						_T("SOFTWARE\\Microsoft\\Windows\\HTML Help")
