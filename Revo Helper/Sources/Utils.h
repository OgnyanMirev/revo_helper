#pragma once
#include "stdafx.h"
#include "language.h"
#include "dirutils.h"
#include <Psapi.h>
#include <TlHelp32.h>

struct ReadFileToBuffer
{
	char* b = nullptr;
	size_t s;

	ReadFileToBuffer(const fs::path& Path, size_t max_size = -1)
	{
		ifstream ifs(Path, ios::ate | ios::binary);
		if (!ifs.good()) return;

		s = ifs.tellg();
		if (s > max_size) return;
		b = new char[s + 1];
		b[s] = 0;
		ifs.seekg(ios::beg);
		ifs.read(b, s);
	}
	~ReadFileToBuffer()
	{
		if (b) delete[] b;
	}
};


struct UninstallInfo
{
	CString				m_strAppName;
	CString				m_strUninstString;
	CString				m_strKeyName;
	CString				m_strFullKeyPath;
	CString				m_strInstallLocation;
	CString				m_strMSIGUID;
	CString				m_strDispIcon;
	BOOL				m_bMSI;
	BOOL				m_b64Bit;
	//only used when we unisntall Win App
	CString				m_strInternalName;
};

void SetToNow(const wchar_t* RegistryValueName);

struct UpdateInfo
{
	wstring NewVersion, CurrentVersion;
#ifdef PRO_ENABLED
	wstring Date;
#endif
	wchar_t* SetToNowKey;
};


struct Advertisement
{
	//LinkURL indicates the URL to which the user will be directed if they click the image.
	string ImageURL, LinkURL;
	const wchar_t* SetToNowKey;
	time_t execution_target;
};
shared_ptr<UpdateInfo> CheckUpdate();


BOOL DWReadRestartDataFile(const fs::path &strFilePath, int& nWizardMode, DWORD& dwLogUninstallMethod, CStringArray* parrMatchedLogPaths
	, UninstallInfo* pUninstallerData, int& nUninstallMode);



enum AppStatus
{
	only_in_cache = 0,
	existing_everywhere = 1,
	new_app = 2
};

class CInstalledAppData
{
public:
	CInstalledAppData();

	operator LPARAM () const
	{
		return reinterpret_cast < LPARAM > (this);
	}
	static CInstalledAppData &
		FromLPARAM(
			LPARAM lParam
		)
	{
		CInstalledAppData * pData =
			reinterpret_cast < CInstalledAppData * > (lParam);
		ASSERT(pData != NULL);
		return *pData;
	}



public:
	CString		strRoot;
	CString		strKeyName;
	CString		strFullKeyName;
	CString		strAppName;
	CString		strUninstString;
	CString		strIconPath;
	int			nIconIndex;
	CString		strInstallLocation;
	__int64		nSize;
	CString		strOurSize; // needed for the CListCttrl sorting
	CString		strVersion;
	CString		strDate;
	CString		strPublisher;
	CString		strHelpLink;
	CString		strComment;
	FILETIME	ftLastWriteTime;
	BOOL		bHasParentKeyName;
	BOOL		bIsSystemComponent;
	AppStatus	statMatchtoReg;
	int			nAddedtoGroup; // 0 - New 100000 - Other
	BOOL		b64Bit;
	BOOL		bIsMSI;
	fs::path UninstallStringPath;
	wstring UninstallStringArguments;
};

struct AppList : public vector<shared_ptr<CInstalledAppData> >
{
	AppList();
	virtual void push_back(shared_ptr<CInstalledAppData>);
};

// 
AppList* GetCache();

fs::path GetProcFilename(const DWORD& PID);

UninstallInfo CIAD2UninstallInfo(CInstalledAppData* pElem);

template<typename T>
struct IPCBuffer : public vector<T>
{
	bool bValid = false;
	IPCBuffer(DWORD PID, size_t size, PVOID Memory) : vector<T>(size)
	{
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID);
		if (hProcess == INVALID_HANDLE_VALUE) return;
		bValid = ReadProcessMemory(hProcess, Memory, &(*this)[0], size, &size);
		CloseHandle(hProcess);
	}
};

bool DetectedUninstallerFromRevo(CString const& strAppName);

bool IsSubDirectory(const wstring& ToTest, const wstring& Parent);


bool WriteIAppDataToFile(fs::path const& filename, shared_ptr<CInstalledAppData>& AppData);
fs::path GetTemporaryFilename();

shared_ptr<Advertisement> GetAdvert();

void check_set_autorun();

bool InvokeRevo(wstring const& args, HANDLE* phProcessHandle = NULL);

bool ShouldRun();


struct activation_state
{
	bool Trial, Licensed, Free;
	DWORD ActivationDays;

	activation_state(bool Trial, bool Licensed, bool Free, DWORD ActivationDays);
};

const activation_state& GetActivationState();

bool GlobalActivated();

#define to_lower(str) for (auto& i : str) i = tolower(i); //transform(str.begin(), str.end(), str.begin(), tolower)
#define erase_char(str, c) str.erase(std::remove(str.begin(), str.end(), c), str.end())


void ErrorMsg(const wchar_t* str);
void ErrorMsg(wstring&& str);
