#pragma once
#include <string>

enum strid
{
	new_version_title = 30000,
	unfinished_uninstall_title,
	untraced_install_title,
	uninstall_leftovers_title,
	update_button,
	continue_button,
	trace_button,
	clean_button,
	current_version_str,
	new_version_str,
	release_date_str,
	leftover_files_str ,
	registry_items_str,
	program_str,
	tool_tip_str,
	open_revo_str,
	settings_str,
	exit_str,
	never_show_again_str,
	subscription_expired_title,
	update_subscription_button,

	would_you_like_to_trace_str,
	save_log_as_str,
	save_button,
	log_name_cannot_be_empty_str,
	error_str,

	confirm_str,
	confirm_abort_trace_str,
	confirm_discard_log_data_str,
	confirm_stop_trace_str,
	stop_button,
	tracing_str,
	later_button,
	never_button,
	already_running_str,
	expired_on_str,
	log_name_contained_invalid_chars_str,
	trial_expired_title,
	buy_now_str
};
std::string cvt2utf8(std::wstring const& ws);

std::wstring LangString(strid id);
std::string LangStringUTF8(strid id);

// Returns true if a language change occured
bool LangRefresh();

