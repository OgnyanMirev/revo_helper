#include "AppBarStdafx.h"
#include "DriverClient.h"
#include <psapi.h>
#include <shlwapi.h>
#include <strsafe.h>
#include <tlhelp32.h>
#include "utils.h"
#include "gui.h"
#include "TraceLogs_utils.h"
#include "Version.h"

#ifdef _DEBUG
#include <iostream>
#endif

#pragma comment(lib, "FltLib.lib")

#define MINISPY_NAME            L"RevoFlt"
#define POLL_INTERVAL			50     // 200 milliseconds
#define TIME_BUFFER_LENGTH		20
#define TIME_ERROR				"time error"

/* File event status */
#define FILE_SUPERSEDED                 0x00000000
#define FILE_OPENED                     0x00000001
#define FILE_CREATED                    0x00000002
#define FILE_OVERWRITTEN                0x00000003
#define FILE_EXISTS                     0x00000004
#define FILE_DOES_NOT_EXIST             0x00000005
#define REGISTRY_EVENT		            0x00000006


#define MessageBox MessageBoxW


std::string cvt2utf8(PVOID p)
{
	return cvt2utf8(wstring((wchar_t*)p));
}


#ifdef _DEBUG

//unordered_map<DWORD, shared_ptr<ProcessInfo>> RecordedProcesses;

DWORD pfr_pid = 0;

unsigned trace_n = 0, log_n = 0;

unordered_map<DWORD, string> ProcessNames;
//ofstream ofs("C:\\Temp\\HelperDump.txt", ios::trunc);

string& ResolveProcName(DWORD PID)
{
	if (!ProcessNames[PID].size())
	{
		char Name[4096] = { 0 };
		HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID);
		GetModuleFileNameExA(hProc, 0, Name, 4096);
		CloseHandle(hProc);

		ProcessNames[PID] = Name;
		return ProcessNames[PID];
	}
	return ProcessNames[PID];
}

void ProcFile(VSFileLog* log, DWORD PID)
{
	string Pname = ResolveProcName(PID);

	cout << "File Log: " << PID << ' ' << Pname << ' ' << log->m_eOperation << ' ' << cvt2utf8(log->m_strFileName.GetBuffer()) << endl;
}

void ProcReg(PREG_RECORD record)
{
	string Pname = ResolveProcName((DWORD)record->ProcessId);
	cout << "Reg Log: " << record->ProcessId << ' ' << Pname << ' ' << cvt2utf8(record->RegPath) << ' ' << cvt2utf8(record->RegNewData) << endl;
}
#endif



CDriverClient::CDriverClient(): m_pStopThreadsEvent(new CEvent(FALSE, TRUE))
{	
	m_thGetLogRecordsThread		= NULL;
	m_thGetRegRecordsThread		= NULL;
	m_thProcessRegRecordsThread	= NULL;
	m_thGetPIDRecordsThread		= NULL;
	//m_dwarrPIDs					= NULL;
	m_pRegTree					= NULL;

	//m_dwPIDsCount				= 0;

	m_hPort						= INVALID_HANDLE_VALUE;
		
}
CDriverClient::~CDriverClient()
{
	StopLogging();

	POSITION pos = m_listRegistryLog.GetHeadPosition();
	while(pos!=NULL)
	{
        
		PREG_RECORD temp = (PREG_RECORD)m_listRegistryLog.GetNext(pos);
		
		if(temp->RegNewData)
			delete [] temp->RegNewData;

		if(temp->RegOldData)
			delete [] temp->RegOldData;

		if(temp->RegPath)
			delete [] temp->RegPath;

		if(temp->RegValueName)
			delete [] temp->RegValueName;

		delete  temp;
	}
	m_listRegistryLog.RemoveAll();
	
	pos = m_listFileLog.GetHeadPosition();
	while(pos!=NULL)
	{
        
		VSFileLog* temp = (VSFileLog*)m_listFileLog.GetNext(pos);
		delete temp;
	}
	m_listFileLog.RemoveAll();

	delete m_pStopThreadsEvent;

 

	if(m_pRegTree)
		delete m_pRegTree;
	
	m_arrShellExecPaths.RemoveAll();

}
BOOL CDriverClient::IsStringInList(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase)
{
	for(int i=0; i<arrStringList->GetCount(); i++)
	{	
		CString str = arrStringList->GetAt(i);
		if(bMatchCase)
		{
			if(StrCmpI(pszString, arrStringList->GetAt(i)) == 0)
				return TRUE;
		}
		else
		{
			if(StrStrI(pszString, arrStringList->GetAt(i)))
				return TRUE;
		}
	}
	return FALSE;
}

BOOLEAN CDriverClient::TranslateFileTag(__in PLOG_FILE_RECORD logRecord)
/*++

Routine Description:

    If this is a mount point reparse point, move the given name string to the
    correct position in the log record structure so it will be displayed
    by the common routines.

Arguments:

    logRecord - The log record to update

Return Value:

    TRUE - if this is a mount point reparse point
    FALSE - otherwise

--*/
{
    //PFLT_TAG_DATA_BUFFER TagData;
    //ULONG Length;

    //
    // The reparse data structure starts in the NAME field, point to it.
    //

    //TagData = (PFLT_TAG_DATA_BUFFER) &logRecord->Name[0];

    ////
    ////  See if MOUNT POINT tag
    ////

    //if (TagData->FileTag == IO_REPARSE_TAG_MOUNT_POINT) {

    //    //
    //    //  calculate how much to copy
    //    //

    //    Length = min( MAX_NAME_SPACE - sizeof(UNICODE_NULL), TagData->MountPointReparseBuffer.SubstituteNameLength );

    //    //
    //    //  Position the reparse name at the proper position in the buffer.
    //    //  Note that we are doing an overlapped copy
    //    //

    //    MoveMemory( &logRecord->Name[0],
    //                TagData->MountPointReparseBuffer.PathBuffer,
    //                Length );

    //    logRecord->Name[Length/sizeof(WCHAR)] = UNICODE_NULL;
    //    return TRUE;
    //}

    return FALSE;
}


BOOL CDriverClient::EnableDebugPrivilege()
{
	HANDLE           hToken;
	LUID             sedebugnameValue;
	LUID             seloaddriverValue;
	TOKEN_PRIVILEGES tp;


	if ( !::OpenProcessToken( GetCurrentProcess(), 	TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken ))
		return FALSE;
	//
	// Given a privilege's name SeDebugPrivilege, we should locate its local LUID mapping.
	//
	if ( !::LookupPrivilegeValue( NULL, SE_DEBUG_NAME, &sedebugnameValue ) )
	{
		::CloseHandle( hToken );
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = sedebugnameValue;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if ( !::AdjustTokenPrivileges( hToken, FALSE, &tp, sizeof(tp), NULL, NULL ) )
	{
		::CloseHandle( hToken );
		return FALSE;
	}
//////////////////////////////////////
	if ( !::LookupPrivilegeValue( NULL, SE_LOAD_DRIVER_NAME, &seloaddriverValue ) )
	{
		::CloseHandle( hToken );
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = seloaddriverValue;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if ( !::AdjustTokenPrivileges( hToken, FALSE, &tp, sizeof(tp), NULL, NULL ) )
	{
		::CloseHandle( hToken );
		return FALSE;
	}
///////////////////////////////////////
	::CloseHandle( hToken );
	return TRUE;
}

DWORD* CDriverClient::GetRunningPrcesses(DWORD& m_dwPIDsCount)
{
	CStringArray strarrProcIncNames;
	CUIntArray dwProcIncData;
	if(VSGetSettings(CAT_UNINSTALLER_TRACEDPROCINCLUDE, strarrProcIncNames, dwProcIncData) == FALSE)
	{
		strarrProcIncNames.Add(TEXT("%windir%\\system32\\msiexec.exe"));
		dwProcIncData.Add(1);

		strarrProcIncNames.Add(TEXT("%windir%\\system32\\services.exe"));
		dwProcIncData.Add(1);

		strarrProcIncNames.Add(TEXT("%windir%\\system32\\spoolsv.exe"));
		dwProcIncData.Add(1);

		strarrProcIncNames.Add(TEXT("%windir%\\system32\\rundll32.exe"));
		dwProcIncData.Add(1);

		strarrProcIncNames.Add(TEXT("%windir%\\system32\\svchost.exe"));
		dwProcIncData.Add(1);

		VSSetSettings(CAT_UNINSTALLER_TRACEDPROCINCLUDE, strarrProcIncNames, dwProcIncData);
	}

	TCHAR szExcludePath[2048] = {0};
	for(int i=0; i<strarrProcIncNames.GetCount(); i++)
	{
		if(dwProcIncData.GetAt(i))
		{
			::ExpandEnvironmentStrings(strarrProcIncNames.GetAt(i), szExcludePath, 2048);
			m_arrLogPaths.Add(szExcludePath);
		}
	}


/////////////////////////////////////////////////////
	DWORD dwPIDs[1024];
	int nCount = 0;

	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	//DWORD dwPriorityClass;

	TCHAR szProcessName[1024] = {0};

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if( hProcessSnap == INVALID_HANDLE_VALUE )
	{
		//printError( "CreateToolhelp32Snapshot (of processes)" );
		return nullptr;
	}

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof( PROCESSENTRY32 );

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if( !Process32First( hProcessSnap, &pe32 ) )
	{
		//printError( "Process32First" ); // Show cause of failure
		CloseHandle( hProcessSnap );    // Must clean up the
										//   snapshot object!
		return nullptr;
	}

	// Now walk the snapshot of processes
	do
	{
		hProcess = OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ, FALSE, pe32.th32ProcessID );
		if( hProcess != NULL )
		{	
			::GetModuleFileNameEx(hProcess, NULL, szProcessName, 1024);
			if(IsStringInList(&m_arrLogPaths, szProcessName))
			{
				TRACE("Not Included PID:%d %s\n", pe32.th32ProcessID, szProcessName);
			}
			else
			{
				TRACE("Included PID:%d %s\n", pe32.th32ProcessID, szProcessName);
				dwPIDs[nCount] = pe32.th32ProcessID;
				nCount++;
			}
			CloseHandle( hProcess );
		}
		else
		{
			TRACE("OpenProcess Failed, PID:%d, %s, Err:%d\n", pe32.th32ProcessID, pe32.szExeFile, GetLastError());
		}
		TRACE( "parent process ID = %d\n", pe32.th32ParentProcessID );

	} while( Process32Next( hProcessSnap, &pe32 ) );

	CloseHandle( hProcessSnap );

	CSingleLock singleLock(&m_CritSecPIDs);
	singleLock.Lock();

	auto m_dwarrPIDs = new DWORD[nCount];

	for (int i = 0; i < nCount; i++ )
		m_dwarrPIDs[i] = dwPIDs[i];

	m_dwPIDsCount = nCount;

	singleLock.Unlock();

	return m_dwarrPIDs;
}

void CDriverClient::SetExcludedProcesses()
{
	CStringArray strarrProcExcNames;
	CUIntArray dwProcExcData;
	if(VSGetSettings(CAT_UNINSTALLER_TRACEDPROCEXCLUDE, strarrProcExcNames, dwProcExcData) == FALSE)
	{
		//VSSetSettings(CAT_UNINSTALLER_TRACEDPROCEXCLUDE, strarrProcIncNames, dwProcIncData);
	}

	TCHAR szExcludePath[2048] = {0};
	for(int i=0; i<strarrProcExcNames.GetCount(); i++)
	{
		if(dwProcExcData.GetAt(i))
		{
			::ExpandEnvironmentStrings(strarrProcExcNames.GetAt(i), szExcludePath, 2048);
			m_arrDoNotLogPaths.Add(szExcludePath);
		}
	}
}
BOOL CDriverClient::Initialize()
{
	//StopLogging();

	if (EnableDebugPrivilege() == FALSE)
		return FALSE;


	GetFileAssociations();

	SetExcludedProcesses();

	SYSTEMTIME st;
	GetSystemTime(&st);              // gets current time
	SystemTimeToFileTime(&st, &m_ftStartTime);  // converts to file time format

	HRESULT hResult = FilterLoad(MINISPY_NAME);
	if (HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND) == hResult)
	{
		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));

		CString strPath;
		strPath.Format(_T("%s\\revoflt.inf"), GetWorkingDirectory());
		if (::PathFileExists(strPath) == FALSE)
		{
			MessageBox(0, _T("Cannot find revoflt.inf!"), _T("Revo Uninstaller Pro"), MB_OK | MB_ICONERROR);

			return FALSE;
		}

		fs::path wd = GetWorkingDirectory();

		strPath.Format(_T("RUNDLL32.EXE SETUPAPI.DLL,InstallHinfSection DefaultInstall 132 %s\\revoflt.inf"), wd);
		if (!CreateProcess(NULL,   // No module name (use command line)
						   strPath.GetBuffer(),        // Command line
						   NULL,           // Process handle not inheritable
						   NULL,           // Thread handle not inheritable
						   FALSE,          // Set handle inheritance to FALSE
						   0,              // No creation flags
						   NULL,           // Use parent's environment block
						   NULL,           // Use parent's starting directory 
						   &si,            // Pointer to STARTUPINFO structure
						   &pi)           // Pointer to PROCESS_INFORMATION structure
			)
		{
			MessageBox(0, _T("Cannot start installing of revoflt.inf!"), _T("Revo Uninstaller Pro"), MB_OK | MB_ICONERROR);
			return FALSE;
		}

		// Wait until child process exits.
		WaitForSingleObject(pi.hProcess, INFINITE);


		// Close process and thread handles. 
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);

		hResult = FilterLoad(MINISPY_NAME);
	}

	if (HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND) == hResult)
	{
		MessageBox(0, _T("Cannot load revoflt.sys - File Not Found!"), _T("Revo Uninstaller Pro"), MB_OK | MB_ICONERROR);
		return FALSE;
	}

	if (HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS) == hResult)
	{
		//MessageBox(0,_T("Could not load filter - Already Exists"), _T("ERR"),MB_OK );
		FilterUnload(MINISPY_NAME);
		hResult = FilterLoad(MINISPY_NAME);
	}

	if (HRESULT_FROM_WIN32(ERROR_SERVICE_ALREADY_RUNNING) == hResult)
	{
		//MessageBox(0,_T("Could not load filter - Already Running"), _T("ERR"),MB_OK );
		FilterUnload(MINISPY_NAME);
		hResult = FilterLoad(MINISPY_NAME);
	}

	if (S_OK == hResult)
	{
		hResult = FilterConnectCommunicationPort(MINISPY_PORT_NAME, 0, NULL, 0, NULL, &m_hPort);
		if (IS_ERROR(hResult))
		{
			CString strResult;
			strResult.Format(_T("Cannot connect to revoflt.sys! Err Code: %d"), hResult);

			MessageBox(0, strResult, _T("ERR"), MB_OK);

			if (INVALID_HANDLE_VALUE != m_hPort)
			{
				if (CloseHandle(m_hPort))
					m_hPort = INVALID_HANDLE_VALUE;
			}
			return FALSE;
		}

		//Send start capturing command to the driver
		//actually send PIDs running so it can filter
		if (SendRunningPids() == FALSE)
			return FALSE;

		return TRUE;
	}
	else
	{
		CString strResult;
		strResult.Format(_T("Cannot load revoflt.sys! Error Code: %d"), hResult);
		MessageBox(0, strResult, _T("Revo Uninstaller Pro"), MB_OK | MB_ICONERROR);
		return FALSE;
	}
}

void CDriverClient::Deinitialize()
{
	if (CloseHandle(m_hPort))
		m_hPort = INVALID_HANDLE_VALUE;

	//unloading the driver will stop capture!
	FilterUnload(MINISPY_NAME);
	DeinstallDriver();
}

bool CDriverClient::AbortTracingIfUncategorizedProgram()
{
	if (this->detected_installer) return false;

	tracing_done_flag = true;

	return true;
}


bool CDriverClient::StartLogging(shared_ptr<ProcessInfo> Process, bool already_detected_installer)
{
#ifdef _DEBUG
	//already_detected_installer = false;
	cout << "Logging for " << Process->Executable.u8string() << endl;
#endif
	StopLogging();

	m_pRegTree = new VSRegTree;
	m_pRegTree->AddExludeKey(_T("REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Cryptography\\RNG"));
	m_pRegTree->AddExludeKey(_T("REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"));


	TracedProcess = Process;
	m_pStopThreadsEvent->ResetEvent();
	bIsCapturing = true;
	tracing_done_flag = false;
	detected_installer = already_detected_installer;
	initial_check_done = false;

	m_thGetLogRecordsThread = AfxBeginThread((AFX_THREADPROC)GetLogRecordsThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_thGetLogRecordsThread->m_bAutoDelete = false;
	m_thGetLogRecordsThread->ResumeThread();

	m_thGetRegRecordsThread = AfxBeginThread((AFX_THREADPROC)GetRegRecordsThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_thGetRegRecordsThread->m_bAutoDelete = false;
	m_thGetRegRecordsThread->ResumeThread();

	m_thProcessRegRecordsThread = AfxBeginThread((AFX_THREADPROC)ProcessRegRecordsThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_thProcessRegRecordsThread->m_bAutoDelete = false;
	m_thProcessRegRecordsThread->ResumeThread();


	auto initial_check = [&]()
	{
		for (int i = 0; i < 3; i++)
		{
			Sleep(1000);
			if (TracedProcess->LiveBranch())
				return true;
		}
		return false;
	};

	if (!initial_check())
	{
		StopLogging();
		ClearAllData();
		return false;
	}
	initial_check_done = true;


	if (already_detected_installer)
		HandleTracingDialogs();
	else ID_MonitorTracing();

	StopLogging();

	//m_thGetPIDRecordsThread = AfxBeginThread((AFX_THREADPROC)GetPIDRecordsThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	//m_thGetPIDRecordsThread->m_bAutoDelete = false;
	//m_thGetPIDRecordsThread->ResumeThread();

	return true;
}


void CDriverClient::ClearAllData()
{
	m_listFileLog.RemoveAll();
	m_listRegistryLog.RemoveAll();
	if (this->m_pRegTree)
	{
		delete m_pRegTree;
		m_pRegTree = NULL;
	}
}


void CDriverClient::HandleTracingDialogs()
{
	Sleep(500);

#ifdef _DEBUG
	TracedProcess->PrintChildren();
#endif

	tracing_dialog_data data{ tracing_done_flag };

	//wstring appname = TracedProcess->Executable.filename().wstring();
	
	CVersion ver(TracedProcess->Executable.c_str());
	ver.Init();
	CString Cappname;
	ver.GetProductName(Cappname);
	wstring product_name = Cappname.GetBuffer();


	while (product_name.size() && product_name.back() == L' ') product_name.pop_back();

	if (!product_name.size()) product_name = TracedProcess->Executable.filename().replace_extension();


	create_tracing_query_dialog(product_name, TracedProcess->Filename, data);

	StopLogging();
	if (data.abort)
	{
		VSSetSettings(CAT_HELPER_REJECTED_INSTALLERS, (this->TracedProcess->Executable.wstring() + this->TracedProcess->Args).c_str(), 1);
		ClearAllData();
	}
	else
	{
		static fs::path Temp = GetSysConstant(C_LOCALAPPDATA) / "VS Revo Group\\Revo Uninstaller Pro";
		static CString strIniFile = (Temp / "RUPLogsData.ini").c_str();


		CString logName = product_name.c_str(),
			logPath = (Temp / "Logs" / product_name).c_str();
		

		if (AddNewLog(strIniFile, logName, logPath))
		{
			SaveFileRecords(logPath + L"\\filelogs.dat");
			SaveRegRecords(logPath + L"\\reglogs.dat");

			//ofstream ofs("C:\\Temp\\Recorded Processes.txt");
			//for (auto& i : RecordedProcesses)
			//{
			//	ofs << i.second->PID << ' ' << i.second->Executable << ' ' << cvt2utf8(i.second->Args) << endl;
			//}
		}
		else ErrorMsg(L"Unable to create log.");
	}
}
void CDriverClient::ID_CheckFileRecord(VSFileLog* log)
{
	static const vector<fs::path> disallowed =
	{
		GetSysConstant(C_PROGRAMFILES) / "windowsapps"
	};

	for (auto& i : disallowed)
	{
		if (IsSubDirectory(log->m_strFileName.GetString(), i))
		{
			delete m_listFileLog.RemoveTail();
			break;
		}
	}

	if (detected_installer) return;
	//ProcFile(log, pfr_pid);

	static auto ProgFiles = GetSysConstant(C_PROGRAMFILES);
	static auto ProgFilesx86 = GetSysConstant(C_PROGRAMFILESX86);

	if (log->m_eOperation == eFileOp::FileCreate)
	{
		fs::path p = log->m_strFileName.GetBuffer();
		p = p.parent_path();

		if (fs::equivalent(p, ProgFiles) || fs::equivalent(p, ProgFilesx86))
		{
			this->detected_installer = true;

#ifdef _DEBUG
			auto pname = ResolveProcName(pfr_pid);
			cout << "Found write to Program Files." << endl;
			cout << cvt2utf8(log->m_strFileName.GetBuffer()) << " by " << pname << endl << endl;
#endif
		}
	}
}
void CDriverClient::ID_CheckRegRecord(PREG_RECORD reg)
{
	if (detected_installer) return;
	//ProcReg(reg)\;

	string str = cvt2utf8(reg->RegPath);

	const char* UninstallKey = "\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
		* UninstallKeyWOW64 = "\\REGISTRY\\MACHINE\\SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall";

#ifdef _DEBUG

	//cout << "Reg Key: " << reg->eventType << "  " << str << endl;

#endif

	if((reg->eventType == REG_NOTIFY_CLASS::RegNtSetValueKey)
	   && (str.find(UninstallKey) == 0 || str.find(UninstallKeyWOW64) == 0))
	{
		this->detected_installer = true;

#ifdef _DEBUG
		auto pname = ResolveProcName(pfr_pid);
		cout << "Found write to uninstall key." << endl;
		cout << str << " by " << pname << endl << endl;
#endif
	}
}

void CDriverClient::ID_MonitorTracing()
{
	unsigned SecondCounter = 0;
	while (!detected_installer)
	{
		Sleep(1000);
		if ((++SecondCounter > INLINE_TRACER_MAXIMUM_UNCERTAINTY_PERIOD) || tracing_done_flag)
		{
			StopLogging();
			ClearAllData();
			return;
		}
	}

	HandleTracingDialogs();
}

void CDriverClient::GetPIDRecords()
{
    DWORD bytesReturned = 0;
    DWORD used;
    
    HRESULT hResult;
    PPID_RECORD pPIDRecord;
    COMMAND_MESSAGE commandMessage;

	PVOID buffer = (PVOID)new BYTE[BUFFER_SIZE/sizeof( PVOID )];

	while(1)
	{
		if (!bIsCapturing)
		{
			Sleep(1000);
			continue;
		}

		::ZeroMemory(buffer,sizeof(buffer));

		if(::WaitForSingleObject(m_pStopThreadsEvent->m_hObject, 0) == WAIT_OBJECT_0)
		{
			delete [] buffer;
			return;
		}

		commandMessage.Command = GetMiniSpyPIDLog;

		hResult = FilterSendMessage( m_hPort,
									 &commandMessage,
									 sizeof( COMMAND_MESSAGE ),
									 buffer,
									 BUFFER_SIZE/sizeof( PVOID ),
									 &bytesReturned );

		if (IS_ERROR( hResult )) {

			if (HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE ) == hResult)
			{
				//printf( "The kernel component of minispy has unloaded. Exiting\n" );
				delete [] buffer;
				return;
			} 
			else
			{
				if (hResult != HRESULT_FROM_WIN32( ERROR_NO_MORE_ITEMS )) 
				{

					//printf( "UNEXPECTED ERROR received: %x\n", hResult );
				}

				Sleep( POLL_INTERVAL );
			}

			continue;
		}
		
        pPIDRecord = (PPID_RECORD) buffer;
        used = 0;

        //
        //  Logic to write record to screen and/or file
        //

        for (;;) {

			if(::WaitForSingleObject(m_pStopThreadsEvent->m_hObject,0) == WAIT_OBJECT_0)
			{
				delete [] buffer;
				return;
			}

			used += sizeof(PID_RECORD);
            
			if (used > bytesReturned) 
               break;
			
			//Send PID to skip list or remove it from that list
			ProcessPID(pPIDRecord);

            //
            // Move to next LOG_RECORD
            //
			pPIDRecord = (PPID_RECORD)Add2Ptr(pPIDRecord, sizeof(PID_RECORD));

        }

        //
        //  If we didn't get any data, pause for 1/2 second
        //

        if (bytesReturned == 0) 
            Sleep( POLL_INTERVAL );
	}
	delete [] buffer;

}
BOOL CDriverClient::DevicePathToDosPath(const TCHAR* pszDevicePath, CString* pstrDosPath)
{
  pstrDosPath->Empty();

  TCHAR szDriveStrings[MAX_PATH] = _T("");
  if (!::GetLogicalDriveStrings(MAX_PATH, szDriveStrings))
    return FALSE;

  // Drive strings are stored as a set of null terminated strings, with an
  // extra null after the last string. Each drive string is of the form "C:\".
  // We convert it to the form "C:", which is the format expected by
  // ::QueryDosDevice().
  TCHAR szDriveColon[3] = _T(" :");
  for (const TCHAR* pszNextDriveLetter = szDriveStrings; *pszNextDriveLetter; pszNextDriveLetter += _tcslen(pszNextDriveLetter) + 1) 
  {
    // Dos device of the form "C:".
    *szDriveColon = *pszNextDriveLetter;
    TCHAR szDeviceName[MAX_PATH] = _T("");

	if (!::QueryDosDevice(szDriveColon, szDeviceName, MAX_PATH))
	{
      continue;
    }

    size_t name_length = _tcslen(szDeviceName);
    if (_tcsnicmp(pszDevicePath, szDeviceName, name_length) == 0)
	{
      // Construct DOS path.
      pstrDosPath->Format(_T("%s%s"), szDriveColon, pszDevicePath + name_length);
      return TRUE;
    }
  }

  return FALSE;
}
BOOL CDriverClient::ProcessPID(PPID_RECORD pPIDRec)
{
	return true;
	//if(pPIDRec == NULL)
	//	return FALSE;

	////if new process
	//if(pPIDRec->bCreate)
	//{
	//	// Get a handle to the process.
	//	HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION/* | PROCESS_VM_READ*/, FALSE, (DWORD)pPIDRec->ProcessId);
	//	// Get the process name.
	//	if (hProcess != NULL)
	//	{
	//		TCHAR szProcessName[4096] = {0};
	//		CString strProcessName;
	//		//DWORD dwErr = GetModuleFileNameEx( hProcess, NULL, szProcessName, 4096);
	//		DWORD dwErr = GetProcessImageFileName ( hProcess, szProcessName, 4096);
	//		DWORD dwErr2 = GetLastError();
	//		//CString strRes;
	//		DevicePathToDosPath(szProcessName, &strProcessName);
	//		//strProcessName = szProcessName;

	//		TCHAR szPrefix[10] = {0};
	//		CopyMemory(szPrefix, szProcessName, 8);
	//		if(StrCmp(szPrefix, _T("\\??\\")) == 0)
	//			strProcessName.Delete(0, 4);

	//		if(IsStringInList(&m_arrLogPaths, strProcessName) == FALSE)
	//		{
	//			if((IsStringInList(&m_arrDoNotLogPaths, strProcessName))
	//			|| (IsStringInList(&m_arrShellExecPaths, strProcessName)))
	//			{
	//				//lock shared between threads PIDs array
	//				CSingleLock singleLock(&m_CritSecPIDs);
	//				singleLock.Lock();

	//				DWORD* pdwPIDs = new DWORD[m_dwPIDsCount+1];
	//				for(DWORD i=0; i<m_dwPIDsCount; i++)
	//				{
	//					pdwPIDs[i] = m_dwarrPIDs[i];
	//				}
	//				pdwPIDs[m_dwPIDsCount] = (DWORD) pPIDRec->ProcessId;

	//				if(m_dwarrPIDs)
	//					delete [] m_dwarrPIDs;

	//				m_dwarrPIDs = pdwPIDs;
	//				m_dwPIDsCount = m_dwPIDsCount+1;

	//				singleLock.Unlock();
	//			}			
	//		}

	//		CloseHandle(hProcess);
	//		return TRUE;
	//	}
	//}
	//else
	//{
	//	//lock shared between threads PIDs array
	//	CSingleLock singleLock(&m_CritSecPIDs);
	//	singleLock.Lock();
	//	for(DWORD i=0; i<m_dwPIDsCount; i++)
	//	{
	//		if(pPIDRec->ProcessId == m_dwarrPIDs[i])
	//		{
	//			m_dwarrPIDs[i] = 0;
	//			break;
	//		}
	//	}
	//	singleLock.Unlock();
	//}
	//return FALSE;
}


BOOL CDriverClient::SendRunningPids()
{
	//Send currently running PIDs to skip
	DWORD m_dwPIDsCount;
	auto m_dwarrPIDs = GetRunningPrcesses(m_dwPIDsCount);
	if (!m_dwarrPIDs) return false;

	//int nPIDsBufferLenght = (int) ROUND_TO_SIZE( m_dwPIDsCount*sizeof(DWORD), sizeof( PVOID ));
	DWORD nPIDsBufferLenght =  m_dwPIDsCount*sizeof(DWORD);
	//PBYTE PIDsBuffer = new BYTE[nPIDsBufferLenght];
	//
	//::ZeroMemory(PIDsBuffer, nPIDsBufferLenght);
	//memcpy( PIDsBuffer, m_dwarrPIDs, nPIDsBufferLenght);
		
    DWORD bytesReturned = 0;
    COMMAND_MESSAGE commandMessage;
	commandMessage.Command = StartCapturing;

	HRESULT hResult = FilterSendMessage( m_hPort,
										 &commandMessage,
										 sizeof( COMMAND_MESSAGE ),
										 //PIDsBuffer,
										 m_dwarrPIDs,
										 nPIDsBufferLenght,
										 &bytesReturned );
//	delete [] PIDsBuffer;

	if (IS_ERROR( hResult )) 
	{
		CString strResult;
		strResult.Format(_T("Could not send pids to filter! hResult: %d"), hResult);
        MessageBox(0, strResult, _T("ERR"),MB_OK );

		return FALSE;
	}


	this->ServicesBase = ProcessInfo::GetServicesProcessTree();

	return TRUE;
}
BOOL CDriverClient::StopLogging()
{

#ifdef _DEBUG
	trace_n++;
	log_n = 0;
#endif

	m_pStopThreadsEvent->SetEvent();

	if(m_thGetLogRecordsThread)
	{
		::WaitForSingleObject(m_thGetLogRecordsThread->m_hThread, INFINITE);
		delete m_thGetLogRecordsThread;
		m_thGetLogRecordsThread = NULL;
	}
	
	if(m_thGetRegRecordsThread)
	{
		::WaitForSingleObject(m_thGetRegRecordsThread->m_hThread, INFINITE);
		delete m_thGetRegRecordsThread;
		m_thGetRegRecordsThread = NULL;
	}

	if(m_thProcessRegRecordsThread)
	{
		::WaitForSingleObject(m_thProcessRegRecordsThread->m_hThread, INFINITE);
		delete m_thProcessRegRecordsThread;
		m_thProcessRegRecordsThread = NULL;
	}

	//if (hOverseerThread != INVALID_HANDLE_VALUE)
	//{
	//	TerminateThread(hOverseerThread, 0);
	//	hOverseerThread = INVALID_HANDLE_VALUE;
	//}
	//if(m_thGetPIDRecordsThread)
	//{
	//	::WaitForSingleObject(m_thGetPIDRecordsThread->m_hThread, INFINITE);
	//	delete m_thGetPIDRecordsThread;
	//	m_thGetPIDRecordsThread = NULL;
	//}



	return TRUE;
}

void CDriverClient::GetLogRecords()
{
    DWORD bytesReturned = 0;
    DWORD used;
    
    HRESULT hResult;
    PLOG_FILE_RECORD pLogRecord;
    COMMAND_MESSAGE commandMessage;

	PVOID buffer = (PVOID)new BYTE[BUFFER_SIZE/sizeof( PVOID )];

	bytesReturned = BUFFER_SIZE / sizeof(PVOID);
	while(1)
	{
		if (!bIsCapturing)
		{
			Sleep(1000);
			continue;
		}

		::ZeroMemory(buffer, bytesReturned);

		if(::WaitForSingleObject(m_pStopThreadsEvent->m_hObject, 0) == WAIT_OBJECT_0)
		{
			delete [] buffer;
			return;
		}

		commandMessage.Command = GetMiniSpyLog;


		//ofs << "Filter send message";

		hResult = FilterSendMessage( m_hPort,
									 &commandMessage,
									 sizeof( COMMAND_MESSAGE ),
									 buffer,
									 BUFFER_SIZE/sizeof( PVOID ),
									 &bytesReturned );


		if (IS_ERROR( hResult )) {

			if (HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE ) == hResult)
			{
				//printf( "The kernel component of minispy has unloaded. Exiting\n" );
				delete [] buffer;
				return;
			} 
			else
			{
				if (hResult != HRESULT_FROM_WIN32( ERROR_NO_MORE_ITEMS )) 
				{

					//printf( "UNEXPECTED ERROR received: %x\n", hResult );
				}

				Sleep( POLL_INTERVAL );
			}

			continue;
		}

        //
        //  If we didn't get any data, pause for 50 mili second
        //
        if (bytesReturned == 0) 
		{
			Sleep( POLL_INTERVAL );
			continue;
		}
		
        pLogRecord = (PLOG_FILE_RECORD) buffer;
        used = 0;

        //
        //  Logic to write record to screen and/or file
        //
		//ofs << " no errors" << endl;

        for (;;) {

			//ofs << "Log record address " << (uint64_t)pLogRecord << endl;
			if(::WaitForSingleObject(m_pStopThreadsEvent->m_hObject,0) == WAIT_OBJECT_0)
			{
				delete [] buffer;
				return;
			}
			if(pLogRecord->Length == 0)
				break;

			if (pLogRecord->Length < (sizeof(LOG_FILE_RECORD))) 
                break;

			used += pLogRecord->Length;

             if (used > bytesReturned)
				break;

            //
            //  See if a reparse point entry
            //

            if (FlagOn(pLogRecord->RecordType,RECORD_TYPE_FILETAG)) {

                if (!TranslateFileTag( pLogRecord )){

                    //
                    // If this is a reparse point that can't be interpreted, move on.
                    //

					pLogRecord = (PLOG_FILE_RECORD)Add2Ptr(pLogRecord,pLogRecord->Length);

                    continue;
                }
            }

			if(IsPIDAllowed((DWORD)pLogRecord->ProcessId))
			{
#ifdef _DEBUG
				pfr_pid = (DWORD)pLogRecord->ProcessId;
#endif
				if(pLogRecord->CallbackMajorId == IRP_MJ_CREATE)
				{

					if( pLogRecord->Information == FILE_CREATED )
					{
						VSFileLog* pFileLog = new VSFileLog;
						pFileLog->m_eOperation = FileCreate;
						pFileLog->m_strFileName = (LPWSTR)pLogRecord->OldName;
						pFileLog->m_nIsDirectory = pLogRecord->IsDirectory;
						m_listFileLog.AddTail(pFileLog);
						ID_CheckFileRecord(pFileLog);
					} 
					if( pLogRecord->Information == FILE_OVERWRITTEN )
					{
						VSFileLog* pFileLog = new VSFileLog;
						pFileLog->m_eOperation = FileOverwritten;
						pFileLog->m_strFileName = (LPWSTR)pLogRecord->OldName;
						pFileLog->m_nIsDirectory = pLogRecord->IsDirectory;
						m_listFileLog.AddTail(pFileLog);
						ID_CheckFileRecord(pFileLog);
					}
				}
				if(pLogRecord->CallbackMajorId == IRP_MJ_SET_INFORMATION)
				{
					if(pLogRecord->CallbackMinorId == 8)
					{
						VSFileLog* pFileLog = new VSFileLog;
						pFileLog->m_eOperation = FileDelete;
						pFileLog->m_strFileName = (LPWSTR)pLogRecord->OldName;
						pFileLog->m_nIsDirectory = pLogRecord->IsDirectory;								
						m_listFileLog.AddTail(pFileLog);
						ID_CheckFileRecord(pFileLog);
					}
					if(pLogRecord->CallbackMinorId == 7)
					{
						VSFileLog* pFileLog = new VSFileLog;
						pFileLog->m_eOperation = FileRename;
						pFileLog->m_strFileName = (LPWSTR)pLogRecord->OldName;
						pFileLog->m_strRenameName = (LPWSTR)pLogRecord->NewName;
						pFileLog->m_nIsDirectory = pLogRecord->IsDirectory;
						m_listFileLog.AddTail(pFileLog);
						ID_CheckFileRecord(pFileLog);
					}
				}
			}
			
            //
            // Move to next LOG_RECORD
            //

			pLogRecord = (PLOG_FILE_RECORD)Add2Ptr(pLogRecord,pLogRecord->Length);
        }
	}
	delete [] buffer;
}

void CDriverClient::GetRegRecords()
{
	DWORD bytesReturned = 0;
    DWORD used, dwBuffSize;
    
    HRESULT hResult;
    PREG_RECORD pRegRecord;
    COMMAND_MESSAGE commandMessage;

	try 
	{		
		dwBuffSize = (BUFFER_SIZE/sizeof( PVOID ));
		PVOID buffer = (PVOID)new BYTE[dwBuffSize];

		while(1)
		{
			if (!bIsCapturing)
			{
				Sleep(1000);
				continue;
			}

			if(::WaitForSingleObject(m_pStopThreadsEvent->m_hObject,0) == WAIT_OBJECT_0)
			{
				delete [] buffer;
				return;
			}
					
			::ZeroMemory(buffer, dwBuffSize);

			commandMessage.Command = GetMiniSpyRegLog;

			hResult = FilterSendMessage( m_hPort,
										 &commandMessage,
										 sizeof( COMMAND_MESSAGE ),
										 buffer,
										 dwBuffSize,
										 &bytesReturned );

			if (IS_ERROR( hResult )) 
			{

				if (HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE ) == hResult)
				{
					//printf( "The kernel component of minispy has unloaded. Exiting\n" );
					delete [] buffer;
					MessageBox(0, _T("ERROR:revoflt unloaded!"), _T("Revo Uninstaller App Bar"), MB_OK | MB_ICONEXCLAMATION);
					return;
				} 
				else
				{
					if (hResult != HRESULT_FROM_WIN32( ERROR_NO_MORE_ITEMS )) 
					{

						//printf( "UNEXPECTED ERROR received: %x\n", hResult );
					}
					//Mapping NTSTATUS code STATUS_BUFFER_TOO_SMALL to WIN32 ERROR_INSUFFICIENT_BUFFER
					if(hResult == HRESULT_FROM_WIN32(ERROR_INSUFFICIENT_BUFFER))
					{
						
						//if needed memory exceeds 4MB stop capturing because
						//2MB is the recommended maximum data length of a registry value
						if(bytesReturned > (64*65536))
						{
							commandMessage.Command = StopCapturing;
							hResult = FilterSendMessage( m_hPort,
												 &commandMessage,
												 sizeof( COMMAND_MESSAGE ),
												 NULL,
												 0,
												 &bytesReturned );
							delete [] buffer;
							return;
						}
						//else
						//{

						//	CString strOut;
						//	strOut.Format(_T("No room:%d"), bytesReturned);
						//	OutputDebugStr(strOut);

						//	//allocate more memory
						///*	buffer = new BYTE[bytesReturned];
						//	dwBuffSize = bytesReturned;*/
						//}
					}
					Sleep( POLL_INTERVAL );
				}

				continue;
			}
			
			if (bytesReturned == 0)
			{
				Sleep( POLL_INTERVAL );	
				continue;
			}

			pRegRecord = (PREG_RECORD) buffer;
			used = 0;

			//
			//  Logic to write record to screen and/or file
			//
			for (;;) 
			{		
				if(pRegRecord->Length == 0)
					break;

				used += pRegRecord->Length;
				if (used > bytesReturned) 
				{
					//printf( "UNEXPECTED LOG_RECORD size: used=%d bytesReturned=%d\n",  used, bytesReturned);
					break;
				}
				if(IsPIDAllowed((DWORD)pRegRecord->ProcessId))
				{
					PREG_RECORD pRecord = new REG_RECORD;
					pRecord->eventType = pRegRecord->eventType;
					pRecord->ProcessId = pRegRecord->ProcessId;
					pRecord->RecordType = pRegRecord->RecordType;

					pRecord->RegNewData = new BYTE[pRegRecord->RegNewDataLen];
					CopyMemory(pRecord->RegNewData, pRegRecord->RegNewData, pRegRecord->RegNewDataLen);
					pRecord->RegNewDataLen = pRegRecord->RegNewDataLen;
					pRecord->RegNewDataType = pRegRecord->RegNewDataType;

					pRecord->RegOldData = new BYTE[pRegRecord->RegOldDataLen];
					CopyMemory(pRecord->RegOldData, pRegRecord->RegOldData, pRegRecord->RegOldDataLen);
					pRecord->RegOldDataLen = pRegRecord->RegOldDataLen;
					pRecord->RegOldDataType = pRegRecord->RegOldDataType;

					pRecord->RegPath = new BYTE[pRegRecord->RegPathLen+sizeof(TCHAR)];
					ZeroMemory(pRecord->RegPath, pRegRecord->RegPathLen+sizeof(TCHAR));
					CopyMemory(pRecord->RegPath, pRegRecord->RegPath, pRegRecord->RegPathLen);
					pRecord->RegPathLen = pRegRecord->RegPathLen;
					
					pRecord->RegValueName = new BYTE[pRegRecord->RegValueNameLen+sizeof(TCHAR)];
					ZeroMemory(pRecord->RegValueName, pRegRecord->RegValueNameLen+sizeof(TCHAR));
					CopyMemory(pRecord->RegValueName, pRegRecord->RegValueName, pRegRecord->RegValueNameLen);
					pRecord->RegValueNameLen = pRegRecord->RegValueNameLen;

					CSingleLock singleLock(&m_CritSecReg);
					singleLock.Lock();
					m_listRegistryLog.AddTail(pRecord);
					ID_CheckRegRecord(pRecord);
					singleLock.Unlock();
				}
				
				pRegRecord = (PREG_RECORD)Add2Ptr(pRegRecord, pRegRecord->Length);

			}
		}
		delete [] buffer;
	}
	catch (...)
	{
	}
}

void CDriverClient::ProcessRegRecords()
{
	while(1)
	{
		if (!bIsCapturing)
		{
			Sleep(1000);
			continue;
		}

		PREG_RECORD	pRegRecord = NULL;
		POSITION pos;

		CSingleLock singleLock(&m_CritSecReg);
		singleLock.Lock();		
		pos = m_listRegistryLog.GetHeadPosition();
		if(pos)
		{
			pRegRecord = (PREG_RECORD)m_listRegistryLog.GetAt(pos);
			m_listRegistryLog.RemoveAt(pos);
			singleLock.Unlock();
			if(pRegRecord)
			{
				TCHAR* pszPath = new TCHAR[pRegRecord->RegPathLen + sizeof(TCHAR)];
				ZeroMemory(pszPath, pRegRecord->RegPathLen + sizeof(TCHAR));
				StringCchCopy(pszPath, pRegRecord->RegPathLen + sizeof(TCHAR), (LPWSTR)pRegRecord->RegPath);

				LVITEM lvi;
				ZeroMemory(&lvi, sizeof(LVITEM));
				lvi.mask = LVIF_TEXT;
				switch (pRegRecord->eventType)
				{
				case RegNtPostCreateKey:
					{						
						m_pRegTree->CreateKey(pszPath);
					}
					break;
				case RegNtPreDeleteKey:
					{
						m_pRegTree->DeleteKey(pszPath);
					}
					break;
				case RegNtPreSetValueKey:
					{
						TCHAR* pszValName = new TCHAR[pRegRecord->RegValueNameLen + sizeof(TCHAR)];
						if(pszValName)
						{
							ZeroMemory(pszValName, pRegRecord->RegValueNameLen + sizeof(TCHAR));
							StringCchCopyN(pszValName, pRegRecord->RegValueNameLen, (LPWSTR) pRegRecord->RegValueName, pRegRecord->RegValueNameLen);
							m_pRegTree->SetKeyVal(pszPath, pszValName,
												pRegRecord->RegNewDataType, (BYTE*)pRegRecord->RegNewData, pRegRecord->RegNewDataLen,
												pRegRecord->RegOldDataType, (BYTE*)pRegRecord->RegOldData, pRegRecord->RegOldDataLen);
							delete [] pszValName;
						}
					}
					break;
				case RegNtPreDeleteValueKey:
					{
						TCHAR* pszValName = new TCHAR[pRegRecord->RegValueNameLen + sizeof(TCHAR)];
						if(pszValName)
						{
							ZeroMemory(pszValName, pRegRecord->RegValueNameLen + sizeof(TCHAR));
							StringCchCopyN(pszValName, pRegRecord->RegValueNameLen, (LPWSTR) pRegRecord->RegValueName, pRegRecord->RegValueNameLen);
							m_pRegTree->DeleteKeyVal(pszPath, pszValName, pRegRecord->RegOldDataType, (BYTE*)pRegRecord->RegOldData, pRegRecord->RegOldDataLen);					
	
							delete [] pszValName;
						}
					}
					break;
				case RegNtPostCreateKeyEx:
					{
						m_pRegTree->CreateKey(pszPath);
					}
					break;
				}			
				delete [] pszPath;

				if(pRegRecord->RegNewData)
					delete [] pRegRecord->RegNewData;

				if(pRegRecord->RegOldData)
					delete [] pRegRecord->RegOldData;

				if(pRegRecord->RegPath)
					delete [] pRegRecord->RegPath;

				if(pRegRecord->RegValueName)
					delete [] pRegRecord->RegValueName;
		
				delete pRegRecord;
			}
		}
		else
		{
			singleLock.Unlock();
			if(::WaitForSingleObject(m_pStopThreadsEvent->m_hObject,0) == WAIT_OBJECT_0)
				return;

			Sleep(50);
		}
	}
}


bool IsPIDAllowed(DWORD& dwPID, shared_ptr<ProcessInfo>& Proc)
{
	if (dwPID == Proc->PID) 
		return true;

	for (auto& i : Proc->Children)
	{
		if (IsPIDAllowed(dwPID, i.second)) 
			return true;
	}
	return false;
}

BOOL CDriverClient::IsPIDAllowed(DWORD dwPID)
{
	//TracedProcess->PrintChildren();
	//TracedProcess->LiveBranch();
	/*if (!TracedProcess->LiveBranch())
	{
		tracing_done_flag = true;
		return false;
	}*/

	if (tracing_done_flag) return false;

	if (initial_check_done)
	{
		if (!TracedProcess->LiveBranch())
		{
			tracing_done_flag = true;
			return false;
		}
	}

	//TracedProcess->PrintChildren();

	if (TracedProcess->ContainsPID(dwPID))
	{
		//RecordedProcesses[dwPID] = ProcessInfo::Processes[dwPID];
		return true;
	}

	// Implements tracing of services.exe children.
	if (ServicesBase->ContainsPID(dwPID)) return true;

	return false;
}

BOOL CDriverClient::AddStringInList(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase)
{
	if(arrStringList == NULL)
		return FALSE;
	
	if(pszString == NULL)
		return FALSE;

	INT_PTR nCount = arrStringList->GetCount();
	if(bMatchCase)
	{
		for(INT_PTR i=0; i<nCount; i++)
		{	
			if(StrCmpI(pszString, arrStringList->GetAt(i)) == 0)						
				return FALSE;
		}
	}
	else
	{
		for(int i=0; i<nCount; i++)
		{	
			if(StrStrI(pszString, arrStringList->GetAt(i)))
				return FALSE;
		}
	}	
	arrStringList->Add(pszString);
	return TRUE;
}

int CDriverClient::GetPointCount(TCHAR* szString)
{
	if(szString == NULL)
		return 0;
	int count = 0;
	TCHAR point = TEXT('.');
	int isymbols = lstrlen(szString);
	for(int i = 0;i<isymbols;i++)
	{
		if(szString[i] == point)
			count++;
	}
	return count;
}


TCHAR* CDriverClient::GetPointPos(TCHAR* szString,int nPos)
{
	if(szString == NULL)
		return NULL;

	int count = 0;
	int isymbols = lstrlen(szString);
	TCHAR point = TEXT('.');
	for(int i = 0;i<isymbols;i++)
	{
		if(szString[i] == point)
		{
			count++;
			if(count == nPos)
				return _tcsninc(szString,i);
		}
	}

	return NULL;
}
TCHAR* CDriverClient::GetPathFromString(TCHAR* szString)
{
	if(szString == NULL)
		return NULL;

	size_t tLen = _tcslen(szString);
	if( tLen == 0)
		return NULL;
	
	//because the new string will could be much bigger
	tLen = tLen*4;
	
	//Expand Environment variables
	TCHAR* pszExpString = new TCHAR[tLen];
	ZeroMemory(pszExpString, tLen);
	ExpandEnvironmentStrings(szString, pszExpString, (DWORD)tLen);

	TCHAR twopoints = TEXT(':');

	TCHAR * szPointPos, *sz2PointsPos, *szExtension;
	TCHAR *szResultString = NULL;

	szExtension = new TCHAR[4];
	_tcsnset(szExtension,0,4);

	sz2PointsPos = _tcschr(pszExpString, twopoints);
	if(sz2PointsPos == NULL)
	{
		delete [] szExtension;
		delete [] pszExpString;
		return NULL;
	}

	int nPointsCount = GetPointCount(pszExpString);
    for(int nindex = 1; nindex<nPointsCount+1; nindex++)
	{
		if(szResultString != NULL)
			delete [] szResultString;
		szResultString = new TCHAR[4096];
		_tcsnset(szResultString,0,4096);

		szPointPos = GetPointPos(pszExpString, nindex);//_tcschr(szString,point);
		if(szPointPos == NULL)
			break;
			
		if((szPointPos != NULL) && (sz2PointsPos != NULL))
		{
				if(szPointPos<sz2PointsPos)
					continue;

				_tcsncpy(szExtension,szPointPos+1,3);
				INT_PTR npos = szPointPos - (sz2PointsPos-1) + 4;

				_tcsncpy(szResultString,(sz2PointsPos-1),npos);
				if(::PathIsDirectory(szResultString))
					continue;
				if(::PathFileExists(szResultString))
				{
					delete [] szExtension;
					delete [] pszExpString;
					return szResultString;
				}
		}

	}
	delete [] pszExpString;
	delete [] szExtension;
	delete [] szResultString;
	return NULL;
}

BOOL CDriverClient::GetFileAssociations()
{
	m_arrShellExecPaths.RemoveAll();

	HKEY hKey;
	if(::RegOpenKeyEx(HKEY_CLASSES_ROOT, NULL, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
	{		
		m_arrShellExecPaths.RemoveAll();

		TCHAR    achKey[255];   // buffer for subkey name
		DWORD    cbName;                   // size of name string 
		TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
		DWORD    cchClassName = MAX_PATH;  // size of class string 
		DWORD    cSubKeys=0;               // number of subkeys 
		DWORD    cbMaxSubKey;              // longest subkey size 
		DWORD    cchMaxClass;              // longest class string 
		DWORD    cValues;              // number of values for key 
		DWORD    cchMaxValue;          // longest value name 
		DWORD    cbMaxValueData;       // longest value data 
		DWORD    cbSecurityDescriptor; // size of security descriptor 
		FILETIME ftLastWriteTime;      // last write time 
	 
		DWORD i, retCode; 
	 
		// Get the class name and the value count. 
		retCode = RegQueryInfoKey(
			hKey,                    // key handle 
			achClass,                // buffer for class name 
			&cchClassName,           // size of class string 
			NULL,                    // reserved 
			&cSubKeys,               // number of subkeys 
			&cbMaxSubKey,            // longest subkey size 
			&cchMaxClass,            // longest class string 
			&cValues,                // number of values for this key 
			&cchMaxValue,            // longest value name 
			&cbMaxValueData,         // longest value data 
			&cbSecurityDescriptor,   // security descriptor 
			&ftLastWriteTime);       // last write time 
	 
		// Enumerate the subkeys, until RegEnumKeyEx fails.
	    
		if (cSubKeys)
		{
			for (i=0; i<cSubKeys; i++) 
			{ 
				cbName = 255;
				retCode = RegEnumKeyEx(hKey, i,
						 achKey, 
						 &cbName, 
						 NULL, 
						 NULL, 
						 NULL, 
						 &ftLastWriteTime); 
				if (retCode == ERROR_SUCCESS) 
				{
					//we are interested only in keys begining with '.' so if else break
					if(IsCharAlphaNumeric(achKey[0]))
						break;
					
					//skip msiexec because it shouldn't be ignored during the capture
					if(StrCmpI(achKey, _T(".msi")) == 0)
						break;

					//get the default value to get the class name
					HKEY hSubKey;
					if(::RegOpenKeyEx(HKEY_CLASSES_ROOT, achKey, 0, KEY_READ, &hSubKey) == ERROR_SUCCESS)
					{
						TCHAR szValue[512] = {0};
						DWORD dwType = REG_SZ;
						DWORD dwBuffSize = sizeof(szValue);
						if(::RegQueryValueEx(hSubKey, NULL, NULL, &dwType, (LPBYTE)szValue, &dwBuffSize) == ERROR_SUCCESS)
						{
							HKEY hClassKey;
							CString strKey(szValue);
							//May be we should add "shell\\play\\command" aslo 
							strKey += _T("\\shell\\open\\command");
							if(::RegOpenKeyEx(HKEY_CLASSES_ROOT, strKey, 0, KEY_READ, &hClassKey) == ERROR_SUCCESS)
							{	
								TCHAR szVal[2048] = {0};
								dwBuffSize = sizeof(szVal);
								if(::RegQueryValueEx(hClassKey, NULL, NULL, &dwType, (LPBYTE)szVal, &dwBuffSize) == ERROR_SUCCESS)
								{
									TCHAR* pszPath = GetPathFromString(szVal);
									if(pszPath)
									{
										TCHAR szLongPath[4096] = {0};
										GetLongPathName(pszPath, szLongPath, 4096);
										AddStringInList(&m_arrShellExecPaths, szLongPath, TRUE);
										delete pszPath;
									}
								}
								RegCloseKey(hClassKey);
							}
						}
						RegCloseKey(hSubKey);
					}
				}
			}
		} 
		RegCloseKey(hKey);
	}

	return TRUE;
}

void CDriverClient::SaveRegRecords(LPCTSTR pszFullPath)
{
	if(m_pRegTree)
	{
		m_pRegTree->SaveRegTreeLog(pszFullPath);
		
		delete m_pRegTree;
		m_pRegTree = NULL;
	}
}

void CDriverClient::SaveFileRecords(LPCTSTR pszFullPath)
{
	//SaveFileLogs
	CFile fileData;
	CFileException e;

	BOOL bRes = fileData.Open(pszFullPath, CFile::modeWrite|CFile::modeCreate,&e);
	if(bRes == TRUE)
	{
		CArchive ar(&fileData,CArchive::store);

		m_listFileLog.Serialize(ar);

		POSITION pos = m_listFileLog.GetHeadPosition();
		while(pos!=NULL)
		{
	        
			VSFileLog* temp = (VSFileLog*)m_listFileLog.GetNext(pos);
			delete temp;
		}
		m_listFileLog.RemoveAll();

		ar.Close();
		fileData.Close();
	}
}

BOOL CDriverClient::ConvertNtNametoDosName(CString& strName)
{
	return TRUE;
	int nPos = 0, nCount = 0; 
	while(nCount<3)
	{
		nPos = strName.Find(_T("\\"), nPos);
		if(nPos == -1)
			break;
		nCount++;
		nPos++;
	}

	CString strRight = strName.Left(nPos);

	TCHAR szTarget[256] = {0};
	if(GetVolumePathName(strRight, szTarget, 256))
	{
		strName.Replace(strRight, szTarget);
		return TRUE;
	}

	return FALSE;
}

void CDriverClient::DeinstallDriver()
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	CString strPath;
	strPath.Format(_T("RUNDLL32.EXE SETUPAPI.DLL,InstallHinfSection DefaultUninstall 132 %s\\revoflt.inf"), GetWorkingDirectory());
	if( !CreateProcess( NULL,   // No module name (use command line)
		strPath.GetBuffer(),        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi )           // Pointer to PROCESS_INFORMATION structure
	) 
	{
		MessageBox(0,_T("Cannot remove revoflt.inf!"), _T("Revo Uninstaller Pro"), MB_OK | MB_ICONERROR);
		return;
	}

	// Wait until child process exits.
	WaitForSingleObject( pi.hProcess, INFINITE );
	
	
	// Close process and thread handles. 
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
}

void CDriverClient::ToggleCapturing(bool bCapFlag)
{
	bIsCapturing = bCapFlag;
}
