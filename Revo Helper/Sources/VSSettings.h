#pragma once
#include "VSSettingsDefs.h"

#define MAX_CATEGORY_NAME_LENGTH						1024
#define MAX_LOCATION_PATH_LENGTH						8192

//Main location
#ifdef PRO_ENABLED
#define MAIN_SETTINGS_LOCATION							_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller Pro\\")		
#else
#define MAIN_SETTINGS_LOCATION							_T("SOFTWARE\\VS Revo Group\\Revo Uninstaller\\")		
#endif
#define SETTINGS_FILE									_T("\\settings.ini");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//IMPORTANT!!! Always initalize your variables with their default values before any get setting request!!!
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL VSGetSettings(LPCTSTR psztCategoryName, CStringArray &retArrayValueNames, CStringArray &retArrayData, HKEY hKey = HKEY_CURRENT_USER);
BOOL VSGetSettings(LPCTSTR psztCategoryName, CStringArray &retArrayValueNames, CUIntArray &retArrayData, HKEY hKey = HKEY_CURRENT_USER);

BOOL VSSetSettings(LPCTSTR psztCategoryName, CStringArray &setArrayValueNames, CStringArray &setArrayData, HKEY hKey = HKEY_CURRENT_USER);
BOOL VSSetSettings(LPCTSTR psztCategoryName, CStringArray &setArrayValueNames, CUIntArray &setArrayData, HKEY hKey = HKEY_CURRENT_USER);


BOOL VSGetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, CString &retData, HKEY hKey = HKEY_CURRENT_USER);
BOOL VSGetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, DWORD &retData, HKEY hKey = HKEY_CURRENT_USER);

BOOL VSSetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, LPCTSTR setData, HKEY hKey = HKEY_CURRENT_USER);
BOOL VSSetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, DWORD setData, HKEY hKey = HKEY_CURRENT_USER);

BOOL VSGetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, QWORD& getData, HKEY hKey = HKEY_CURRENT_USER);
BOOL VSSetSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames, QWORD& setData, HKEY hKey = HKEY_CURRENT_USER);


//If psztValueNames is NULL then all values will be deleted!!!
//if you want to delete whole category set psztValueNames to NULL 
//and bRemoveWholeCategory to TRUE
BOOL VSDeleteSettings(LPCTSTR psztCategoryName, LPCTSTR psztValueNames = NULL, BOOL bRemoveWholeCategory = FALSE, HKEY hKey = HKEY_CURRENT_USER);

