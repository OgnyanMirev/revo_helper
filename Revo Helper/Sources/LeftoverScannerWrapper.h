#pragma once
#include "Utils.h"
#include "VSTrackFinder.h"
#include "Registry.h"


// Wrapper interface
struct LeftoverScanner
{
	void Scan();
	bool HasLeftovers(unsigned& files_n, unsigned& total_size, unsigned& registry_n);
	void CleanLeftovers();
	LeftoverScanner(shared_ptr<CInstalledAppData> appdata);

	shared_ptr<CInstalledAppData> m_appData;
	unique_ptr<CVSTrackFinder> m_trackFinder;
};

