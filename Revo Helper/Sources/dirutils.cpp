#include "stdafx.h"
#include <Windows.h>
#include <algorithm>
#include <vector>
using namespace std;
void EnvExpand(fs::path& Path);
// Allows a constant such as %UserProfile% to be evaluated at run time only once

struct static_fs_path : public fs::path
{
	using fs::path::operator=;

	static_fs_path(const char* CONSTANT, bool Lower = true)
	{
		*this = CONSTANT;
		EnvExpand(*this);
		std::wstring w = this->wstring();
		for (auto& i : w) i = tolower(i);
		*this = w;
	}
};

const fs::path& GetSysConstant(unsigned ID)
{
	static vector<static_fs_path> v = { "%UserProfile%", "%ProgramFiles%", "%ProgramFiles(x86)%",
		"%SystemRoot%", "%AppData%", "%LocalAppdata%", "%ProgramFiles%\\VS Revo Group\\Revo Uninstaller Pro\\",
		RUEXE
	};
	return v[ID];
}

const fs::path& GetWorkingDirectory()
{
	static struct _
	{
		fs::path p;
		_()
		{
			wchar_t fn[4096] = { 0 };
			GetModuleFileName(0, fn, 4096);
			p = fn;
			p = p.parent_path();
		}
	} p;
	return p.p;
}