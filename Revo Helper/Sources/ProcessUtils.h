#pragma once
#include "Utils.h"




/*
    This structure provides a convenient way to access process attributes as well as the process tree of a process.
    It also provides some helper functions for dealing with the process tree.
*/
struct ProcessInfo
{
    wstring Args, Filename;
    bool bValid = false, bStillAlive = true;
    fs::path Executable;
    DWORD PID, ParentPID;

    unordered_map<DWORD, shared_ptr<ProcessInfo> > Children;
    shared_ptr<ProcessInfo> Parent = nullptr;

    // All children share the type of their parent, unless that type is PROCESS_IRRELEVANT.
    int ProcessType = PROCESS_IRRELEVANT;


    // PID -> ProcessInfo mapping
    static unordered_map<DWORD, shared_ptr<ProcessInfo>> Processes;


    // Checks if the process is still alive
    bool Alive();
    
#ifdef _DEBUG
    // Debug function
    void PrintChildren();
#endif

    // Returns the handle of a process, also does caching of process handles
    HANDLE GetHandle(DWORD dwDesiredAccess = PROCESS_ALL_ACCESS);
    
    // Deprecated, I think
    //bool operator==(ProcessInfo const& o);

    // Sets process type, also sets the process type for all children, grandchildren, etc.
    void SetProcessType(int NewType);

    // Checks if there is a live process on this branch, meaning either this process, a sibling or a child, grandchild, etc.
    bool LiveBranch();

    // Checks if the process' branch contains a certain PID
    bool ContainsPID(DWORD const& PID);



    ~ProcessInfo();


    static shared_ptr<ProcessInfo> GetServicesProcessTree();
    
    /*
        This function calls the constructor initially but also sets up global references concerning this 
        process including parent-child and sibling relationships.

        This is the de-facto constructor for this struct.
    */
    static shared_ptr<ProcessInfo> Create(wstring const& ExePath, wstring& FullCommandLine, DWORD PID, DWORD ParentPID, HANDLE hProcess = INVALID_HANDLE_VALUE);

    /*
        Basically prevents a memory leak.
        There's no way to tell for how long the ProcessInfo structures will be needed during process tracing.
        Data may be referenced even after the process has exited.

        Therefore ONLY call this function whenever nothing else is happening to clear out old data.
    */
    static void ClearState();
private:

    /*
        Sets up the ProcessInfo member data.
    */
    ProcessInfo(wstring const& ExePath, wstring& FullCommandLine, DWORD PID, DWORD ParentPID, HANDLE hProcess = INVALID_HANDLE_VALUE);

    bool LiveBranchInternal();
    bool ContainsPIDInternal(DWORD const& PID);
    //void DeleteRoot();
    //void FlushBranchRoot();

    
    vector<ProcessInfo*>* Siblings = nullptr;
    HANDLE hProcess;
    DWORD RootPID = 0;


    /*
        A bit of a complicated structure, but ultimatelly it's meant to group together twin processes.
        Twin processes in this context are processes with the same parent and the same filename.

        The first DWORD is the ParentPID, it points to a second map which contains its twin children. 
        Note: they don't have to be 2, I just don't know of a generic word for twins/triplets/etc

        So the wstring is the filename and it points to a vector of processes whose filename is the same and whose parent has the PID from the first DWORD.

        It's only used in LiveBranch and ContainsPID and populated in Create as of this moment.
    */
    static unordered_map<DWORD, unordered_map<wstring, vector<ProcessInfo*>>> SiblingMap;
    //static unordered_map<DWORD, shared_ptr<ProcessInfo>> Roots;
};

/*
	This is the main function which is supposed to tell the difference between
	0. PROCESS_TYPE_IRRELEVANT
	1. PROCESS_TYPE_INSTALLER
	2. PROCESS_TYPE_UNINSTALLER

	More documentation inside.
*/
void GetProcessType(const shared_ptr<ProcessInfo>& proc, shared_ptr<CInstalledAppData>& Data);

/*
	This function is responsible for identifying new processes (which get added to the "info" variable)
	and setting the bStillAlive for old processes which are still running.
*/
//void GetProcessInfos(vector<shared_ptr<ProcessInfo> >& NewInfos, vector<shared_ptr<ProcessInfo>>* OldInfos);




/*
    All new proceses are pushed onto a queue which can be accessed with this function.
    Internally the processes also populate the static members of the ProcessInfo struct,
    this is done by an internal thread and does not need the function to be called.
*/
shared_ptr<ProcessInfo> ListenForProcess();

