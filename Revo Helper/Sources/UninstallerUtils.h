#pragma once
#include "IniData.h"
#include "Utils.h"
#if !defined(AFX_UNINSTALLUTILS_H__A81B52FF_F965_41F0_823F_2492186F76C7__INCLUDED_)
#define AFX_UNINSTALLUTILS_H__A81B52FF_F965_41F0_823F_2492186F76C7__INCLUDED_

#include "registry.h"


struct LogUnSecMainData
{
	CString m_strKeyName;
	CString	m_strAppName;
	CString	m_strVersion;
	CString	m_strUninstallString;
};



void GetMSIAppData(AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags, CTime* arrCacheTimes, BOOL bForcedRefresh);
void ReadMSISections(CString& strSubKey, AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags, CTime* arrCacheTimes, BOOL bAllUserSection, BOOL bForcedRefresh);
BOOL SearchForMSIIconPath(HKEY hRoot, LPCTSTR strFullKey, CString* strRetIconPath, int* nIconIndex, REGSAM flagsOpen);
void GetArpCachePath(LPCTSTR pszAppKeyName, CString& srtPathExe, REGSAM flagsOpen);
BOOL GetMSIAppIcon(CString& strAppKey, CString* strRetIconPath, int* nIconIndex, REGSAM flagsOpen);
void CheckForNewApps(AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, CTime* arrCacheTimes, BOOL bForcedRefresh);
BOOL ReadCacheDataFromFile(AppList& listCachedApps, CTime* timeLastMod, CTime* arrCacheTimes, CIniData* pIniData);
BOOL WriteCacheDataToFile(AppList& listCachedApps, CTime* arrCacheTimes, CIniData* pIniData, BOOL bClosing = FALSE);
BOOL ReadAppProps(HKEY hkeyRoot, LPCTSTR strFullKey, LPCTSTR strConvertedKeyName, LPCTSTR strOriginKeyName, BOOL IsMSI, AppList& listCachedApps, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags);

shared_ptr<CInstalledAppData> FoundKeyInList(LPCTSTR szKeyName, AppList& listCachedApps, BOOL bIsProg64Bit, HKEY hkeyRoot);
BOOL IsProgramInList(LPCTSTR szAppName, AppList& listCachedApps, LPCTSTR szVersion, BOOL bSysComp, BOOL bIsProg64Bit, BOOL bReadingMSISection);
BOOL IsMSIProgramInList(LPCTSTR szAppName, AppList& listCachedApps, LPCTSTR szVersion);

BOOL GetUninstallSectionAppData(HKEY hRoot, AppList& listCachedApps, CTime modTime, BOOL bCheckingForNewApps, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, DWORD dwFlags, CTime* arrCacheTimes, BOOL bForcedRefresh);
BOOL GetProgramDirSize(CRegistry& regAppKey, LPCTSTR pszAppKey, CString& pszProgramDir, CString& strAltInstallLoc, DWORD64* dxFolderSize, REGSAM flagsOpen);
BOOL GetArpCacheSize(LPCTSTR pszAppKeyName, INT64* nSize, REGSAM flagsOpen);
DWORD64 GetFolderSizeUninstaller(LPCTSTR szPath, DWORD *dwFiles = NULL, DWORD *dwFolders = NULL);

BOOL BuildCache(AppList& listCachedApps, BOOL bCheckingForNewApps, CTime* arrCacheTimes, CIniData* pIniData);

int GetItemsToWrite(AppList& listCachedApps);
int GetItemsToShow(AppList& listCachedApps);
//void MarkCacheItemsAsExisting(AppList& listCachedApps,CString strRoot,CString strUninstallKey);
void UpdateItemStatus(AppList& listCachedApps, CString strRoot, CString strUninstallKey, FILETIME ftCalcTimeToCompare, BOOL bOldNewByDays, BOOL bIs64Bit);
void CheckGroupingMode(FILETIME* ftCalcTimeToCompare, BOOL* bOldNewByDays);
BOOL IsDefaultGroup(int nGroupID);

BOOL VerifyEmbeddedSignature();

void ItterateProgramFilesFolder(CString& strFolderPath, CString& strBiggestExe, LONGLONG& llBiggestSize);
void ItterateStartMenuFolder(CString& strFolderPath, CString& strBiggestExe, LONGLONG& llBiggestSize);
void SearchFolderForAppName(CStringArray* strAppNames, CString& strFolderPath, BOOL bGoneOneLevelDeep, BOOL bScanFiles, CString& strFoundFullPath, BOOL& bUseAsInstallLoc);
BOOL RemoveAppVersion(CString& strAppName, CString& strNewAppName);
void AltSearchForInstallLoc(CString& strAppName, CString& strIconFile, CString& strFolder, BOOL& bUseAsInstallLoc);

BOOL IsLogAppropriate(CString& strLogSection);
BOOL CheckForLogExistence(CStringList& strFoundLogName, LogUnSecMainData*stUnProgData, BOOL* bDataMatched, BOOL* bDataPartialMatched);
BOOL SearchDatabaseForGoodLog(LogUnSecMainData* pUninstalledAppdata, CString& strLogName, CString& strLogPath, CIniData* pIniData);

BOOL VerifyProgramBits(CString& strInstallLoc, BOOL& bIsProg64Bit);

BOOL CheckSubKeysModDates(HKEY hRoot, CTime& tParentKeyTime, CString strUninstallKey, DWORD dwBitsFlags, CTime& timeNewReaded);

#ifdef PORTABLE
BOOL CheckForLogExistencePort(CStringList& strFoundLogName, LogUnSecMainData*stUnProgData, BOOL* bDataMatched, BOOL* bDataPartialMatched);
//If we have a log, which path is now invalid, because the portable version is now on other portition, try to update it to the current partition.
void UpdatePathIfNecessary(CString& strReadLogPath);
#endif

void RemoveVersionFromString(CString& strOrigString, CString& strNoVer);

#endif // !defined(AFX_UNINSTALLUTILS_H__A81B52FF_F965_41F0_823F_2492186F76C7__INCLUDED_)