// IniData.cpp: implementation of the CIniData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "VSProject.h"
#include "IniData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIniData::CIniData()
{

}

CIniData::~CIniData()
{
	ReleaseMemory();
}

BOOL CIniData::AddItem(TCHAR* szKey, TCHAR* szValue)
{
	int iKeySize = lstrlen(szKey)+1;
	int iValueSize = lstrlen(szValue)+1;
	
	stIniItem* iniItem = new stIniItem;
	iniItem->szKey = new TCHAR[iKeySize];
	iniItem->szValue = new TCHAR[iValueSize];

	_tcsnset(iniItem->szKey,0,iKeySize);
	_tcsnset(iniItem->szValue,0,iValueSize);

	_tcscpy_s(iniItem->szKey,iKeySize,szKey);
	_tcscpy_s(iniItem->szValue,iValueSize,szValue);

	m_listIniData.AddTail(iniItem);


	return TRUE;
}

void CIniData::ReleaseMemory()
{
	POSITION pos = m_listIniData.GetHeadPosition();
	while(pos!=NULL)
	{
       
		stIniItem* temp = (stIniItem*)m_listIniData.GetNext(pos);
	
		delete [] temp->szKey;
		delete [] temp->szValue;
		delete temp;
	}
	 m_listIniData.RemoveAll();
}

void CIniData::GetValue(int nKey,CString* strRes) const
{
	TCHAR szBuffer[1024] = {0};
	POSITION pos = m_listIniData.GetHeadPosition();
	while(pos!=NULL)
	{
		stIniItem* temp = (stIniItem*)m_listIniData.GetNext(pos);
		if(nKey == _ttoi(temp->szKey))
		{

			//TCHAR* fRes = NULL;
			//int len = lstrlen(temp->szValue)+1;
			//fRes = new TCHAR[len];
			//_tcscpy_s(fRes,len,temp->szValue);
			*strRes = temp->szValue;

			//if(_tcsstr(strRes,TEXT("\\n")))/// check if the read string has a symbol for new line
			if(strRes->Find(TEXT("\\n"))> -1)
			{
				///if so it must be replaced to be presented as it must be not just as "/n"
				CString strData = *strRes ; ///create temporary CString object used to replace the needed parts
				strData.Replace(TEXT("\\n"),TEXT("\n"));
				//_tcsnset(fRes,0,len);
				//lstrcpy(fRes,strData.GetBuffer()); ///copy the result in our return value
				//strData.ReleaseBuffer();
				*strRes = TEXT("");
				*strRes = strData;
			}

			return; 
		}
	}
	::LoadString(0,nKey,szBuffer,sizeof(szBuffer)/sizeof(TCHAR)); 
	
	//TCHAR* fRes = NULL;
	//int len = lstrlen(szBuffer)+1;
	//fRes = new TCHAR[len];
	//_tcscpy_s(fRes,len,szBuffer);
	*strRes = szBuffer;

	//return;
}

void CIniData::GetValue(LPCTSTR szKey,CString* strRes)
{
	TCHAR szBuffer[1024] = {0};
	POSITION pos = m_listIniData.GetHeadPosition();
	while(pos!=NULL)
	{
        
		stIniItem* temp = (stIniItem*)m_listIniData.GetNext(pos);
		if(!(lstrcmp(szKey,temp->szKey)))
		{
			//TCHAR* fRes = NULL;
			//int len = lstrlen(temp->szValue)+1;
			//fRes = new TCHAR[len];
			//_tcscpy_s(fRes,len,temp->szValue);
			*strRes = temp->szValue;

			//if(_tcsstr(fRes,TEXT("\\n")))
			if(strRes->Find(TEXT("\\n"))> -1)
			{
				
				CString strData = *strRes ;
				strData.Replace(TEXT("\\n"),TEXT("\n"));
				//_tcsnset(fRes,0,len);
				//lstrcpy(fRes,strData.GetBuffer());
				//strData.ReleaseBuffer();
				*strRes = TEXT("");
				*strRes = strData;
			}

			return; 
		}
	}
	
	int nIDS =  _ttoi(szKey);
	if(nIDS == 0)
		return;
	::LoadString(AfxGetInstanceHandle(),nIDS,szBuffer,sizeof(szBuffer)/sizeof(TCHAR)); 
	
	//TCHAR* fRes = NULL;
	//int len = lstrlen(szBuffer)+1;
	//fRes = new TCHAR[len];
	//_tcscpy_s(fRes,len,szBuffer);
	*strRes = szBuffer;
	//return fRes;
}

BOOL CIniData::LoadIniFile(TCHAR* szFileName)
{
	if (szFileName != NULL)
		m_iniFile.SetPathName(szFileName);

	CStringArray arrSections,arrKeys;
	m_iniFile.GetSectionNames(&arrSections);
	for (int i = 0; i < arrSections.GetSize(); i++)
	{
		arrKeys.RemoveAll();
		m_iniFile.GetKeyNames(arrSections[i],&arrKeys);

		for (int j = 0; j < arrKeys.GetSize(); j++)
		{
			CString strValue =  m_iniFile.GetString((LPCTSTR )arrSections[i],arrKeys[j]);
			AddItem((TCHAR*)(LPCTSTR)arrKeys[j],(TCHAR*)(LPCTSTR)strValue);
		}
	}
	return TRUE;
}

BOOL CIniData::InitData(CString& sAppPath)
{
		ReleaseMemory();
		m_sAppPath = sAppPath;	
		//m_sAppPath += _T("\\lang\\bulgarian.ini");
		m_iniFile.SetPathName(m_sAppPath);
		TCHAR szFileName[4096];

		lstrcpy(szFileName, m_sAppPath);

		LoadIniFile(szFileName);
		return TRUE;
}