#include "stdafx.h"
//#include "Registry.h"
#include "ini.h"
//#include "utils.h"
#include "TraceLogs_utils.h"

#include <Softpub.h>
#include <wincrypt.h>
#include <wintrust.h>

// Link with the Wintrust.lib file.
#pragma comment (lib, "wintrust")

SystemFolderKey arrSysDirs[SYSFOLDERCOUNT] =
{
 {TEXT("ADMINTOOLS"),CSIDL_ADMINTOOLS},
 {TEXT("ALTSTARTUP"),CSIDL_ALTSTARTUP},
 {TEXT("APPDATA"),CSIDL_APPDATA},
 {TEXT("COMMON_ADMINTOOLS"),CSIDL_COMMON_ADMINTOOLS},
 {TEXT("COMMON_ALTSTARTUP"),CSIDL_COMMON_ALTSTARTUP},
 {TEXT("COMMON_APPDATA"),CSIDL_COMMON_APPDATA},
 {TEXT("COMMON_DESKTOPDIRECTORY"),CSIDL_COMMON_DESKTOPDIRECTORY},
 {TEXT("COMMON_DOCUMENTS"),CSIDL_COMMON_DOCUMENTS},
 {TEXT("COMMON_FAVORITES"),CSIDL_COMMON_FAVORITES},
 {TEXT("COMMON_MUSIC"),CSIDL_COMMON_MUSIC},
 {TEXT("COMMON_OEM_LINKS"),CSIDL_COMMON_OEM_LINKS},
 {TEXT("COMMON_PICTURES"),CSIDL_COMMON_PICTURES},
 {TEXT("COMMON_PROGRAMS"),CSIDL_COMMON_PROGRAMS},
 {TEXT("COMMON_STARTMENU"),CSIDL_COMMON_STARTMENU},
 {TEXT("COMMON_STARTUP"),CSIDL_COMMON_STARTUP},
 {TEXT("COMMON_TEMPLATES"),CSIDL_COMMON_TEMPLATES},
 {TEXT("COMMON_VIDEO"),CSIDL_COMMON_VIDEO},
 {TEXT("COMPUTERSNEARME"),CSIDL_COMPUTERSNEARME},
 {TEXT("COOKIES"),CSIDL_COOKIES},
 {TEXT("DESKTOPDIRECTORY"),CSIDL_DESKTOPDIRECTORY},
 {TEXT("FAVORITES"),CSIDL_FAVORITES},
 {TEXT("FONTS"),CSIDL_FONTS},
 {TEXT("HISTORY"),CSIDL_HISTORY},
 {TEXT("INTERNET_CACHE"),CSIDL_INTERNET_CACHE},
 {TEXT("LOCAL_APPDATA"),CSIDL_LOCAL_APPDATA},
 {TEXT("MYMUSIC"),CSIDL_MYMUSIC},
 {TEXT("MYPICTURES"),CSIDL_MYPICTURES},
 {TEXT("MYVIDEO"),CSIDL_MYVIDEO},
 {TEXT("NETHOOD"),CSIDL_NETHOOD},
 {TEXT("PERSONAL"),CSIDL_PERSONAL},
 {TEXT("PRINTHOOD"),CSIDL_PRINTHOOD},
 {TEXT("PROGRAM_FILES"),CSIDL_PROGRAM_FILES},
 {TEXT("PROGRAM_FILESX86"),CSIDL_PROGRAM_FILESX86},
 {TEXT("PROGRAM_FILES_COMMON"),CSIDL_PROGRAM_FILES_COMMON},
 {TEXT("PROGRAM_FILES_COMMONX86"),CSIDL_PROGRAM_FILES_COMMONX86},
 {TEXT("PROGRAMS"),CSIDL_PROGRAMS},
 {TEXT("RECENT"),CSIDL_RECENT},
 {TEXT("RESOURCES"),CSIDL_RESOURCES},
 {TEXT("RESOURCES_LOCALIZED"),CSIDL_RESOURCES_LOCALIZED},
 {TEXT("SENDTO"),CSIDL_SENDTO},
 {TEXT("STARTMENU"),CSIDL_STARTMENU},
 {TEXT("STARTUP"),CSIDL_STARTUP},
 {TEXT("SYSTEM"),CSIDL_SYSTEM},
 {TEXT("SYSTEMX86"),CSIDL_SYSTEMX86},
 {TEXT("TEMPLATES"),CSIDL_TEMPLATES},
 {TEXT("WINDOWS"),CSIDL_WINDOWS},
};



BOOL CreateLogsInitData(CString& strIniFullPath)
{
    CString strExeFolder, strLogsFolder, strLogsFolderReaded, strDefaultLogFolder;
    GetLocalAppData(strExeFolder);
    strIniFullPath = strExeFolder;
    strLogsFolder = strExeFolder;
    strLogsFolder += TEXT("\\Logs\\");
    strDefaultLogFolder = strLogsFolder;

    if (VSGetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsFolderReaded) == FALSE)
        VSSetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsFolder);
    else
        strLogsFolder = strLogsFolderReaded;


    if (!PathIsDirectory(strLogsFolder))
    {
        if (SHCreateDirectory(NULL, strLogsFolder) != ERROR_SUCCESS)
            return FALSE;
    }
    strIniFullPath += TEXT("\\RUPLogsData.ini");
    if (PathFileExists(strIniFullPath) == TRUE)
        return TRUE;

    HANDLE hFile;         // the file handle
    DWORD dwWritten = 0;


    // Create the test file. Open it "Create Always" to overwrite any
    // existing file. The data is re-created below
    hFile = CreateFile(strIniFullPath,
                       GENERIC_WRITE,
                       0,
                       NULL,
                       OPEN_ALWAYS,
                       FILE_ATTRIBUTE_NORMAL,
                       NULL);

    if (hFile == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }


    _TCHAR bom = (_TCHAR)0xFEFF;
    WriteFile(hFile, &bom, sizeof(_TCHAR), &dwWritten, NULL);

    CString strComment = TEXT(";Do not edit that information");
    WriteFile(hFile, strComment, strComment.GetLength() * sizeof(TCHAR), &dwWritten, NULL);

    CloseHandle(hFile);

    CIni iniFile;
    iniFile.SetPathName(strIniFullPath);

    iniFile.WriteUInt(TEXT("General Info"), TEXT("Logs Count"), 0);

    ///if the ini file doesn't exists but the logs directory is not empty
    if (::PathIsDirectoryEmpty(strLogsFolder) == FALSE)
        TryToRestoreIniFile(strIniFullPath, strLogsFolder);
    if (::PathIsDirectoryEmpty(strDefaultLogFolder) == FALSE)
        TryToRestoreIniFile(strIniFullPath, strDefaultLogFolder);

    //iniFile.WriteUInt(TEXT("General Info"),TEXT("Total Size"),0);
    return TRUE;
}

BOOL TryToRestoreIniFile(CString& strIniFullPath, CString& strLogsFolder)
{
    CIni iniFile;
    BOOL bAnythingWritten = FALSE;
    iniFile.SetPathName(strIniFullPath);

    if (strIniFullPath.IsEmpty() || strLogsFolder.IsEmpty())
        return FALSE;

    //TCHAR szFileFilter[4096];
    //TCHAR szFilePath[4096];
    HANDLE hFind = NULL;
    WIN32_FIND_DATA fileinfo;
    CString strFolderPath = strLogsFolder;
    //strFolderPath += TEXT("\\");
    CString strFileFilter = strFolderPath;
    strFileFilter += TEXT("*.*");
    //StringCchCopy(szFilePath,sizeof(szFilePath)/sizeof(TCHAR),strLogsFolder);
    //StringCchCat(szFilePath,sizeof(szFilePath)/sizeof(TCHAR),TEXT("\\"));
    //StringCchCopy(szFileFilter,sizeof(szFileFilter)/sizeof(TCHAR),szFilePath);
    //StringCchCat(szFileFilter,sizeof(szFileFilter)/sizeof(TCHAR),TEXT("*.*"));

    hFind = ::FindFirstFile(strFileFilter, &fileinfo);
    if (hFind != INVALID_HANDLE_VALUE)
    {
        do
        {
            if (fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                if (!_tcscmp(fileinfo.cFileName, TEXT(".")) || !_tcscmp(fileinfo.cFileName, TEXT("..")))
                {
                    continue;//Do nothing for "." and ".." folders
                }
                else
                {

                    CString strSectionName = fileinfo.cFileName;
                    CString strCurrentFullPath = strFolderPath;
                    strCurrentFullPath += fileinfo.cFileName;
                    CString strRegFile, strHDDFile;
                    strRegFile = strCurrentFullPath + TEXT("\\reglogs.dat");
                    strHDDFile = strCurrentFullPath + TEXT("\\filelogs.dat");

                    /// if the current folder is empty nothing is stored or it is not created by us
                    if ((::PathIsDirectoryEmpty(strCurrentFullPath) == TRUE)
                        || ((::PathFileExists(strRegFile) == FALSE) && (::PathFileExists(strHDDFile) == FALSE)))
                        continue;

                    CTime timeCreation(fileinfo.ftCreationTime);
                    iniFile.IncreaseUInt(TEXT("General Info"), TEXT("Logs Count"));
                    strCurrentFullPath += TEXT("\\");

                    iniFile.WriteString(strSectionName, TEXT("Path"), strCurrentFullPath);

                    CString strHour;
                    strHour.Format(TEXT("%02d:%02d:%02d"), timeCreation.GetHour(), timeCreation.GetMinute(), timeCreation.GetSecond());
                    iniFile.WriteString(strSectionName, TEXT("Hour"), strHour);

                    CString strDate;
                    strDate.Format(TEXT("%02d-%02d-%d"), timeCreation.GetDay(), timeCreation.GetMonth(), timeCreation.GetYear());
                    iniFile.WriteString(strSectionName, TEXT("Date"), strDate);

                    iniFile.WriteString(strSectionName, TEXT("Name"), strSectionName);
                    iniFile.WriteString(strSectionName, TEXT("Comment"), TEXT(""));
                    iniFile.WriteInt(strSectionName, TEXT("Imported"), 0);

                    iniFile.WriteInt(strSectionName, TEXT("Parsed"), 0);

                    CreateHelperInifile(strCurrentFullPath, strSectionName, strDate, strHour);

                    bAnythingWritten = TRUE;
                }
            }
            else // file
            {

            }

        }
        while (::FindNextFile(hFind, &fileinfo));
    }
    ::FindClose(hFind);

    if (bAnythingWritten)
        return TRUE;

    return FALSE;
}

BOOL GetWindowsNameType(CString& strFullOSName, int* nIs64Bit)
{
    OSVERSIONINFOEX osvi;
    SYSTEM_INFO si;
    PGNSI pGNSI;
    PGPI pGPI;
    BOOL bOsVersionInfoEx;
    DWORD dwType;

    LPTSTR pszOS = new TCHAR[BUFSIZE];

    ZeroMemory(&si, sizeof(SYSTEM_INFO));
    ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    bOsVersionInfoEx = GetVersionEx((OSVERSIONINFO*)&osvi);

    if (bOsVersionInfoEx == NULL)
    {
        delete[] pszOS;
        return FALSE;
    }

    BOOL bWin81 = FALSE;
    if (Is_Win81())
        bWin81 = TRUE;

    BOOL bWin10 = FALSE;
    if (Is_Win10())
        bWin10 = TRUE;

    // Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

    pGNSI = (PGNSI)GetProcAddress(
        GetModuleHandle(TEXT("kernel32.dll")),
        "GetNativeSystemInfo");
    if (NULL != pGNSI)
        pGNSI(&si);
    else GetSystemInfo(&si);

    if (VER_PLATFORM_WIN32_NT == osvi.dwPlatformId &&
        osvi.dwMajorVersion > 4)
    {
        StringCchCopy(pszOS, BUFSIZE, TEXT("Microsoft "));


        // Test for the specific product.

        if (osvi.dwMajorVersion == 6)
        {
            if (bWin81)
            {
                if (osvi.wProductType == VER_NT_WORKSTATION)
                {
                    StringCchCopy(pszOS, BUFSIZE, TEXT("Windows 8.1 "));
                    // AfxMessageBox(TEXT("GetWindowsNameType 8.1 work"));
                }
                else StringCchCopy(pszOS, BUFSIZE, TEXT("Windows Server 2012 R2 "));
            }
            else if (bWin10)
            {
                StringCchCopy(pszOS, BUFSIZE, TEXT("Windows 10"));
            }
            else
            {
                if (osvi.dwMinorVersion == 0)
                {
                    if (osvi.wProductType == VER_NT_WORKSTATION)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Windows Vista "));
                    else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2008 "));
                }

                if (osvi.dwMinorVersion == 1)
                {
                    if (osvi.wProductType == VER_NT_WORKSTATION)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Windows 7 "));
                    else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2008 R2 "));
                }

                if (osvi.dwMinorVersion == 2)
                {
                    if (osvi.wProductType == VER_NT_WORKSTATION)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Windows 8 "));
                    else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2012 "));
                }
            }

            StringCchCat(pszOS, BUFSIZE, TEXT(" - "));

            pGPI = (PGPI)GetProcAddress(
                GetModuleHandle(TEXT("kernel32.dll")),
                "GetProductInfo");

            pGPI(osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

            switch (dwType)
            {
                case PRODUCT_ULTIMATE:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Ultimate Edition"));
                    break;
                case PRODUCT_PROFESSIONAL:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Professional"));
                    break;
                case PRODUCT_HOME_PREMIUM:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Home Premium Edition"));
                    break;
                case PRODUCT_HOME_BASIC:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Home Basic Edition"));
                    break;
                case PRODUCT_ENTERPRISE:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition"));
                    break;
                case PRODUCT_BUSINESS:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Business Edition"));
                    break;
                case PRODUCT_STARTER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Starter Edition"));
                    break;
                case PRODUCT_CLUSTER_SERVER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Cluster Server Edition"));
                    break;
                case PRODUCT_DATACENTER_SERVER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition"));
                    break;
                case PRODUCT_DATACENTER_SERVER_CORE:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition (core installation)"));
                    break;
                case PRODUCT_ENTERPRISE_SERVER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition"));
                    break;
                case PRODUCT_ENTERPRISE_SERVER_CORE:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition (core installation)"));
                    break;
                case PRODUCT_ENTERPRISE_SERVER_IA64:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition for Itanium-based Systems"));
                    break;
                case PRODUCT_SMALLBUSINESS_SERVER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Small Business Server"));
                    break;
                case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Small Business Server Premium Edition"));
                    break;
                case PRODUCT_STANDARD_SERVER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition"));
                    break;
                case PRODUCT_STANDARD_SERVER_CORE:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition (core installation)"));
                    break;
                case PRODUCT_WEB_SERVER:
                    StringCchCat(pszOS, BUFSIZE, TEXT("Web Server Edition"));
                    break;
            }
        }

        if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2)
        {
            if (GetSystemMetrics(SM_SERVERR2))
                StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2003 R2, "));
            else if (osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER)
                StringCchCat(pszOS, BUFSIZE, TEXT("Windows Storage Server 2003"));
            else if (osvi.wSuiteMask & VER_SUITE_WH_SERVER)
                StringCchCat(pszOS, BUFSIZE, TEXT("Windows Home Server"));
            else if (osvi.wProductType == VER_NT_WORKSTATION &&
                     si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
            {
                StringCchCat(pszOS, BUFSIZE, TEXT("Windows XP Professional x64 Edition"));
                *nIs64Bit = 1;
            }
            else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2003, "));

            StringCchCat(pszOS, BUFSIZE, TEXT(" - "));

            // Test for the server type.
            if (osvi.wProductType != VER_NT_WORKSTATION)
            {
                if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64)
                {
                    if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition for Itanium-based Systems"));
                    else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition for Itanium-based Systems"));
                }

                else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
                {
                    if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter x64 Edition"));
                    else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise x64 Edition"));
                    else StringCchCat(pszOS, BUFSIZE, TEXT("Standard x64 Edition"));
                }

                else
                {
                    if (osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Compute Cluster Edition"));
                    else if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition"));
                    else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition"));
                    else if (osvi.wSuiteMask & VER_SUITE_BLADE)
                        StringCchCat(pszOS, BUFSIZE, TEXT("Web Edition"));
                    else StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition"));
                }
            }
        }

        if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1)
        {
            StringCchCat(pszOS, BUFSIZE, TEXT("Windows XP "));
            StringCchCat(pszOS, BUFSIZE, TEXT(" - "));
            if (osvi.wSuiteMask & VER_SUITE_PERSONAL)
                StringCchCat(pszOS, BUFSIZE, TEXT("Home Edition"));
            else StringCchCat(pszOS, BUFSIZE, TEXT("Professional"));
        }

        if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0)
        {
            StringCchCat(pszOS, BUFSIZE, TEXT("Windows 2000 "));
            StringCchCat(pszOS, BUFSIZE, TEXT(" - "));

            if (osvi.wProductType == VER_NT_WORKSTATION)
            {
                StringCchCat(pszOS, BUFSIZE, TEXT("Professional"));
            }
            else
            {
                if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
                    StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Server"));
                else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
                    StringCchCat(pszOS, BUFSIZE, TEXT("Advanced Server"));
                else StringCchCat(pszOS, BUFSIZE, TEXT("Server"));
            }
        }

        // Include service pack (if any) and build number.

        if (_tcslen(osvi.szCSDVersion) > 0)
        {
            StringCchCat(pszOS, BUFSIZE, TEXT(" "));
            StringCchCat(pszOS, BUFSIZE, osvi.szCSDVersion);
        }

        //   TCHAR buf[80];

         //  StringCchPrintf( buf, 80, TEXT(" (build %d)"), osvi.dwBuildNumber);
         //  StringCchCat(pszOS, BUFSIZE, buf);

        if (osvi.dwMajorVersion >= 6)
        {
            if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
                //StringCchCat(pszOS, BUFSIZE, TEXT( ", 64-bit" ));
                *nIs64Bit = 1;
            else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_INTEL)
                //StringCchCat(pszOS, BUFSIZE, TEXT(", 32-bit"));
                *nIs64Bit = 0;
        }

        strFullOSName = pszOS;
        delete[] pszOS;
        return TRUE;
    }

    else
    {
        // printf( "This sample does not support this version of Windows.\n");
        delete[] pszOS;
        return FALSE;
    }
}

void WriteSystemFolders(CIni* iniHelperFile)
{

    TCHAR szRestrictedFolderName[4096] = { 0 };

    for (int nIndex = 0; nIndex < 46; nIndex++)
    {
        if (SUCCEEDED(SHGetFolderPath(NULL, arrSysDirs[nIndex].iCSIDL, NULL, 0, szRestrictedFolderName)))
            iniHelperFile->WriteString(TEXT("CSIDL"), arrSysDirs[nIndex].strIniKey, szRestrictedFolderName);
        _tcsnset(szRestrictedFolderName, 0, 4096);
    }

    CString strTemp, strTmp;
    strTemp.GetEnvironmentVariable(TEXT("TEMP"));
    iniHelperFile->WriteString(TEXT("ADDDIRS"), TEXT("TEMP"), strTemp);

    strTmp.GetEnvironmentVariable(TEXT("TMP"));
    iniHelperFile->WriteString(TEXT("ADDDIRS"), TEXT("TMP"), strTmp);

    TCHAR szUserPath[4096] = { 0 };
    ExpandEnvironmentStrings(_T("%USERPROFILE%"), szUserPath, 4096);
    iniHelperFile->WriteString(TEXT("ADDDIRS"), TEXT("CURUSER"), szUserPath);
}
BOOL CreateHelperInifile(CString& strLogPath, CString& strLogName, CString& strLogDate, CString& strLogHour)
{
    CString strHelperIniPath = strLogPath;
    strHelperIniPath += TEXT("data.lhi");

    HANDLE hFile;         // the file handle
    DWORD dwWritten = 0;


    hFile = CreateFile(strHelperIniPath,
                       GENERIC_WRITE,
                       0,
                       NULL,
                       OPEN_ALWAYS,
                       FILE_ATTRIBUTE_NORMAL,
                       NULL);

    if (hFile == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }


    _TCHAR bom = (_TCHAR)0xFEFF;
    WriteFile(hFile, &bom, sizeof(_TCHAR), &dwWritten, NULL);

    CString strComment = TEXT(";Do not edit that information");
    WriteFile(hFile, strComment, strComment.GetLength() * sizeof(TCHAR), &dwWritten, NULL);

    CloseHandle(hFile);

    CIni iniHelperFile;
    iniHelperFile.SetPathName(strHelperIniPath);

    iniHelperFile.WriteString(TEXT("General"), TEXT("Name"), strLogName); // Name of the log
    CString strWindowsVersion;
    int n64Bit = 0;
    GetWindowsNameType(strWindowsVersion, &n64Bit);

    iniHelperFile.WriteString(TEXT("General"), TEXT("WinVer"), strWindowsVersion);
    iniHelperFile.WriteInt(TEXT("General"), TEXT("Win64Bit"), n64Bit);

    iniHelperFile.WriteString(TEXT("General"), TEXT("CreateDate"), strLogDate);
    iniHelperFile.WriteString(TEXT("General"), TEXT("CreateHour"), strLogHour);
    iniHelperFile.WriteString(TEXT("General"), TEXT("ModDate"), strLogDate);
    iniHelperFile.WriteString(TEXT("General"), TEXT("ModHour"), strLogHour);

    CString strBuffer, strUserName;
    DWORD dwUsernameSize = 1024;
    GetUserName(strBuffer.GetBuffer(dwUsernameSize), &dwUsernameSize);
    strUserName = strBuffer.GetBuffer(dwUsernameSize);
    strBuffer.ReleaseBuffer();
    iniHelperFile.WriteString(TEXT("General"), TEXT("Author"), strUserName);

    WriteSystemFolders(&iniHelperFile);

    iniHelperFile.WriteString(TEXT("CUSID"), TEXT("CUSID"), GetCurrentUserSID());

    iniHelperFile.WriteString(TEXT("General"), TEXT("Comment"), TEXT(""));

    return TRUE;
}
BOOL AddNewLog(CString& strIniFullPath, CString& strLogName, CString& strNewLogPath)
{
    CIni iniFile;
    CStringArray arrSections;
    BOOL bSectionNameUnique = FALSE;
    int nUniqueIndex = 1;
    CString strTempLogName = strLogName, strLogsFolderReaded;
    SYSTEMTIME systime = { 0 };

    iniFile.SetPathName(strIniFullPath);
    //iniFile.GetSectionNames(&arrSections);

    do
    {

        //if(iniFile.IsSectionExist(strTempLogName)== TRUE)
        if ((IsLogNameExist(&iniFile, strTempLogName) == TRUE) || (iniFile.IsSectionExist(strTempLogName) == TRUE))
        {
            CString strUniqueIndex;
            strUniqueIndex.Format(TEXT("(%d)"), nUniqueIndex);
            strTempLogName = strLogName;
            strTempLogName += strUniqueIndex;
            nUniqueIndex++;
        }
        else
            bSectionNameUnique = TRUE;
    }
    while (bSectionNameUnique == FALSE);

    strLogName = strTempLogName;

    if (VSGetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsFolderReaded) == FALSE)
    {
        CString strExeFolder;
        GetLocalAppData(strExeFolder);
        strLogsFolderReaded = strExeFolder;
        strLogsFolderReaded += TEXT("\\Logs\\");

        VSSetSettings(CAT_UNINSTALLER_APPBAR, VAL_APPBAR_LOGSDIR, strLogsFolderReaded);
    }

    strNewLogPath = strLogsFolderReaded + strLogName + TEXT("\\");

    if (SHCreateDirectory(NULL, strNewLogPath) != ERROR_SUCCESS)
        return FALSE;

    GetLocalTime(&systime);

    CTime timeCreation(systime);
    iniFile.IncreaseUInt(TEXT("General Info"), TEXT("Logs Count"));

    iniFile.WriteString(strLogName, TEXT("Path"), strNewLogPath);

    CString strHour;
    strHour.Format(TEXT("%02d:%02d:%02d"), timeCreation.GetHour(), timeCreation.GetMinute(), timeCreation.GetSecond());
    iniFile.WriteString(strLogName, TEXT("Hour"), strHour);

    CString strDate;
    strDate.Format(TEXT("%02d-%02d-%d"), timeCreation.GetDay(), timeCreation.GetMonth(), timeCreation.GetYear());
    iniFile.WriteString(strLogName, TEXT("Date"), strDate);

    iniFile.WriteString(strLogName, TEXT("Name"), strLogName);
    iniFile.WriteString(strLogName, TEXT("Comment"), TEXT(""));
    iniFile.WriteInt(strLogName, TEXT("Imported"), 0);

    iniFile.WriteInt(strLogName, TEXT("Parsed"), 0);

    CreateHelperInifile(strNewLogPath, strLogName, strDate, strHour);

    return TRUE;
}


BOOL IsLogNameExist(CIni* iniMain, CString& strNewlogName)
{
    CStringArray strSecNames;
    iniMain->GetSectionNames(&strSecNames);
    for (int i = 0; i < strSecNames.GetCount(); i++)
    {
        CString strCurLogName = iniMain->GetString(strSecNames[i], TEXT("Name"));
        if (strCurLogName.CompareNoCase(strNewlogName) == 0)
            return TRUE;
    }
    return FALSE;
}
