#include "stdafx.h"
#include "ProcessUtils.h"
#include "rapidxml.h"
#include "sha256.h"
#include "7zip.h"
#include "InlineTracer.h"
#include <winver.h>

#pragma comment(lib, "Version.lib")

#pragma region String Tools

template<typename Str>
void RemoveSubstring(Str& Target, const Str& SubStr)
{
	size_t Pos;

	while ((Pos = Target.find(SubStr)) != Str::npos)
		Target.erase(Pos, SubStr.size());
}


bool ScanForStringA(const char* buffer, const size_t buffer_size, const char* str, const size_t str_size)
{
	size_t stridx = 0;
	for (size_t i = 0; i < buffer_size; i++)
	{
		if (str[stridx] == buffer[i])
		{
			if (++stridx == str_size) return true;
		}
		else
			stridx = 0;
	}

	return false;
}


bool ScanForStringW(const char* buffer, const size_t buffer_size, const wchar_t* str, const size_t str_size)
{
	return ScanForStringA(buffer, buffer_size, (char*)str, str_size * sizeof(wchar_t));
}

#pragma endregion 

#pragma region Icon Checksum

typedef struct
{
	WORD idReserved; // must be 0
	WORD idType; // 1 = ICON, 2 = CURSOR
	WORD idCount; // number of images (and ICONDIRs)

	// ICONDIR [1...n]
	// ICONIMAGE [1...n]

} ICONHEADER;

//
// An array of ICONDIRs immediately follow the ICONHEADER
//
typedef struct
{
	BYTE bWidth;
	BYTE bHeight;
	BYTE bColorCount;
	BYTE bReserved;
	WORD wPlanes; // for cursors, this field = wXHotSpot
	WORD wBitCount; // for cursors, this field = wYHotSpot
	DWORD dwBytesInRes;
	DWORD dwImageOffset; // file-offset to the start of ICONIMAGE

} ICONDIR;

//
// After the ICONDIRs follow the ICONIMAGE structures -
// consisting of a BITMAPINFOHEADER, (optional) RGBQUAD array, then
// the color and mask bitmap bits (all packed together
//
typedef struct
{
	BITMAPINFOHEADER biHeader; // header for color bitmap (no mask header)
	//RGBQUAD rgbColors[1...n];
	//BYTE bXOR[1]; // DIB bits for color bitmap
	//BYTE bAND[1]; // DIB bits for mask bitmap

} ICONIMAGE;

//
// Write the ICO header to disk
//
static UINT WriteIconHeader(HANDLE hFile, int nImages)
{
	ICONHEADER iconheader;
	DWORD nWritten;

	// Setup the icon header
	iconheader.idReserved = 0; // Must be 0
	iconheader.idType = 1; // Type 1 = ICON (type 2 = CURSOR)
	iconheader.idCount = nImages; // number of ICONDIRs

	// Write the header to disk
	WriteFile(hFile, &iconheader, sizeof(iconheader), &nWritten, 0);

	// following ICONHEADER is a series of ICONDIR structures (idCount of them, in fact)
	return nWritten;
}

//
// Return the number of BYTES the bitmap will take ON DISK
//
static UINT NumBitmapBytes(BITMAP* pBitmap)
{
	int nWidthBytes = pBitmap->bmWidthBytes;

	// bitmap scanlines MUST be a multiple of 4 bytes when stored
	// inside a bitmap resource, so round up if necessary
	if (nWidthBytes & 3)
		nWidthBytes = (nWidthBytes + 4) & ~3;

	return nWidthBytes * pBitmap->bmHeight;
}

//
// Return number of bytes written
//
static UINT WriteIconImageHeader(HANDLE hFile, BITMAP* pbmpColor, BITMAP* pbmpMask)
{
	BITMAPINFOHEADER biHeader;
	DWORD nWritten;
	UINT nImageBytes;

	// calculate how much space the COLOR and MASK bitmaps take
	nImageBytes = NumBitmapBytes(pbmpColor) + NumBitmapBytes(pbmpMask);

	// write the ICONIMAGE to disk (first the BITMAPINFOHEADER)
	ZeroMemory(&biHeader, sizeof(biHeader));

	// Fill in only those fields that are necessary
	biHeader.biSize = sizeof(biHeader);
	biHeader.biWidth = pbmpColor->bmWidth;
	biHeader.biHeight = pbmpColor->bmHeight * 2; // height of color+mono
	biHeader.biPlanes = pbmpColor->bmPlanes;
	biHeader.biBitCount = pbmpColor->bmBitsPixel;
	biHeader.biSizeImage = nImageBytes;

	// write the BITMAPINFOHEADER
	WriteFile(hFile, &biHeader, sizeof(biHeader), &nWritten, 0);

	// write the RGBQUAD color table (for 16 and 256 colour icons)
	if (pbmpColor->bmBitsPixel == 2 || pbmpColor->bmBitsPixel == 8)
	{

	}

	return nWritten;
}

//
// Wrapper around GetIconInfo and GetObject(BITMAP)
//
static BOOL GetIconBitmapInfo(HICON hIcon, ICONINFO* pIconInfo, BITMAP* pbmpColor, BITMAP* pbmpMask)
{
	if (!GetIconInfo(hIcon, pIconInfo))
		return FALSE;

	if (!GetObject(pIconInfo->hbmColor, sizeof(BITMAP), pbmpColor))
		return FALSE;

	if (!GetObject(pIconInfo->hbmMask, sizeof(BITMAP), pbmpMask))
		return FALSE;

	return TRUE;
}

//
// Write one icon directory entry - specify the index of the image
//
static UINT WriteIconDirectoryEntry(HANDLE hFile, int nIdx, HICON hIcon, UINT nImageOffset)
{
	ICONINFO iconInfo;
	ICONDIR iconDir;

	BITMAP bmpColor;
	BITMAP bmpMask;

	DWORD nWritten;
	UINT nColorCount;
	UINT nImageBytes;

	GetIconBitmapInfo(hIcon, &iconInfo, &bmpColor, &bmpMask);

	nImageBytes = NumBitmapBytes(&bmpColor) + NumBitmapBytes(&bmpMask);

	if (bmpColor.bmBitsPixel >= 8)
		nColorCount = 0;
	else
		nColorCount = 1 << (bmpColor.bmBitsPixel * bmpColor.bmPlanes);

	// Create the ICONDIR structure
	iconDir.bWidth = (BYTE)bmpColor.bmWidth;
	iconDir.bHeight = (BYTE)bmpColor.bmHeight;
	iconDir.bColorCount = nColorCount;
	iconDir.bReserved = 0;
	iconDir.wPlanes = bmpColor.bmPlanes;
	iconDir.wBitCount = bmpColor.bmBitsPixel;
	iconDir.dwBytesInRes = sizeof(BITMAPINFOHEADER) + nImageBytes;
	iconDir.dwImageOffset = nImageOffset;

	// Write to disk
	WriteFile(hFile, &iconDir, sizeof(iconDir), &nWritten, 0);

	// Free resources
	DeleteObject(iconInfo.hbmColor);
	DeleteObject(iconInfo.hbmMask);

	return nWritten;
}

static UINT WriteIconData(HANDLE hFile, HBITMAP hBitmap)
{
	BITMAP bmp;
	int i;
	BYTE* pIconData;

	UINT nBitmapBytes;
	DWORD nWritten;

	GetObject(hBitmap, sizeof(BITMAP), &bmp);

	nBitmapBytes = NumBitmapBytes(&bmp);

	pIconData = (BYTE*)malloc(nBitmapBytes);

	GetBitmapBits(hBitmap, nBitmapBytes, pIconData);

	// bitmaps are stored inverted (vertically) when on disk..
	// so write out each line in turn, starting at the bottom + working
	// towards the top of the bitmap. Also, the bitmaps are stored in packed
	// in memory - scanlines are NOT 32bit aligned, just 1-after-the-other
	for (i = bmp.bmHeight - 1; i >= 0; i--)
	{
		// Write the bitmap scanline
		WriteFile(
			hFile,
			pIconData + (i * bmp.bmWidthBytes), // calculate offset to the line
			bmp.bmWidthBytes, // 1 line of BYTES
			&nWritten,
			0);

		// extend to a 32bit boundary (in the file) if necessary
		if (bmp.bmWidthBytes & 3)
		{
			DWORD padding = 0;
			WriteFile(hFile, &padding, 4 - bmp.bmWidthBytes, &nWritten, 0);
		}
	}

	free(pIconData);

	return nBitmapBytes;
}

//
// Create a .ICO file, using the specified array of HICON images
//
BOOL SaveIcon3(TCHAR* szIconFile, HICON hIcon[], int nNumIcons)
{
	HANDLE hFile;
	int i;
	int* pImageOffset;

	if (hIcon == 0 || nNumIcons < 1)
		return FALSE;

	// Save icon to disk:
	hFile = CreateFile(szIconFile, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);

	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;

	//
	// Write the iconheader first of all
	//
	WriteIconHeader(hFile, nNumIcons);

	//
	// Leave space for the IconDir entries
	//
	SetFilePointer(hFile, sizeof(ICONDIR) * nNumIcons, 0, FILE_CURRENT);

	pImageOffset = (int*)malloc(nNumIcons * sizeof(int));

	//
	// Now write the actual icon images!
	//
	for (i = 0; i < nNumIcons; i++)
	{
		ICONINFO iconInfo;
		BITMAP bmpColor, bmpMask;

		GetIconBitmapInfo(hIcon[i], &iconInfo, &bmpColor, &bmpMask);

		// record the file-offset of the icon image for when we write the icon directories
		pImageOffset[i] = SetFilePointer(hFile, 0, 0, FILE_CURRENT);

		// bitmapinfoheader + colortable
		WriteIconImageHeader(hFile, &bmpColor, &bmpMask);

		// color and mask bitmaps
		WriteIconData(hFile, iconInfo.hbmColor);
		WriteIconData(hFile, iconInfo.hbmMask);

		DeleteObject(iconInfo.hbmColor);
		DeleteObject(iconInfo.hbmMask);
	}

	//
	// Lastly, skip back and write the icon directories.
	//
	SetFilePointer(hFile, sizeof(ICONHEADER), 0, FILE_BEGIN);

	for (i = 0; i < nNumIcons; i++)
	{
		WriteIconDirectoryEntry(hFile, i, hIcon[i], pImageOffset[i]);
	}

	free(pImageOffset);

	// finished!
	CloseHandle(hFile);

	return TRUE;
}


uint8_t* get_icon_hash(fs::path const& filename)
{
	HICON hIconLarge;
	HICON hIconSmall;
	BOOL ret;


	if (ExtractIconEx(filename.c_str(), 0, &hIconLarge, &hIconSmall, 1) == 0)
		return nullptr;

	auto temp = GetTemporaryFilename();

	ret = SaveIcon3((TCHAR*)temp.c_str(), &hIconLarge, 1);


	if (ret)
	{
		ReadFileToBuffer file(temp);
		SHA256 ctx;
		ctx.update((const uint8_t*)file.b, file.s);
		return ctx.digest();
	}

	return nullptr;
}
inline bool is_installer_icon(fs::path const& exe)
{
	static const vector<array<uint8_t,32>> precalculated_hashes =
	{
	{148,201,213,39,20,137,97,140,28,69,218,231,101,8,36,54,25,89,240,153,146,15,29,228,99,70,69,20,139,194,33,18,},
	{67,238,58,250,138,61,238,230,91,193,254,45,110,222,152,91,38,94,187,29,169,143,250,144,11,237,77,248,24,65,108,130,},
	{153,87,194,62,61,7,130,29,151,187,112,48,239,64,188,52,176,99,68,73,236,49,61,116,80,160,37,133,67,72,193,81,},
	{148,201,213,39,20,137,97,140,28,69,218,231,101,8,36,54,25,89,240,153,146,15,29,228,99,70,69,20,139,194,33,18,},
	{17,223,15,202,25,151,104,198,27,185,240,167,83,103,248,255,99,114,65,165,210,203,174,136,72,44,181,193,191,2,48,82,},
	{160,35,52,251,3,89,76,202,226,63,224,32,118,196,219,31,159,217,113,2,175,255,114,229,6,218,57,159,152,244,75,141,},
	{68,178,162,158,154,81,20,53,152,239,52,47,155,42,96,224,99,147,194,186,187,221,231,156,159,251,68,114,225,237,187,11,},
	{10,210,71,136,183,42,36,191,175,213,201,10,141,53,42,75,71,34,66,175,56,126,156,145,139,162,225,96,106,204,158,151,},
	{24,248,63,165,219,228,201,136,222,221,86,66,53,64,126,229,123,26,139,243,251,48,213,73,229,135,113,23,12,48,205,38,},
	{117,255,34,156,245,67,9,249,133,102,231,169,134,189,216,67,50,68,147,197,188,17,56,126,247,25,176,50,61,242,171,199,},
	{68,13,3,163,95,210,122,45,114,131,176,52,3,20,21,117,206,72,60,249,106,44,174,74,236,254,123,209,131,136,60,117,},
	{45,95,27,33,31,103,33,223,214,79,104,41,251,77,55,68,97,33,68,74,2,177,38,87,235,166,36,79,51,70,4,10,},
	{54,204,59,39,196,19,208,149,180,187,27,129,146,237,155,163,50,74,8,151,30,162,214,126,74,191,5,249,34,208,41,188,},
	{6,41,173,81,51,240,233,174,128,78,145,212,138,25,125,53,3,250,166,189,157,1,45,76,105,86,111,35,218,26,113,161,},
	{169,146,6,44,183,107,118,21,76,129,11,119,134,75,181,203,31,209,233,70,121,160,106,147,184,23,230,56,76,47,35,190,},
	{131,200,188,143,141,115,28,143,167,211,38,155,158,198,98,19,187,14,192,34,177,95,94,159,52,99,90,86,176,125,213,111,},
	{184,235,191,170,73,157,135,39,157,162,164,120,244,118,226,181,103,37,101,220,85,158,64,77,78,215,86,158,23,139,198,152,},
	{100,102,8,93,217,58,244,105,254,104,228,179,22,123,109,52,77,253,243,103,18,84,36,119,78,159,44,241,204,34,217,104,},
	{6,81,24,56,5,114,23,101,128,21,219,153,127,254,254,249,154,186,225,53,88,61,176,67,47,67,92,201,142,141,97,116,},
	{100,102,8,93,217,58,244,105,254,104,228,179,22,123,109,52,77,253,243,103,18,84,36,119,78,159,44,241,204,34,217,104,},
	{173,143,92,231,140,149,199,9,105,153,235,244,148,115,43,69,155,133,151,152,235,55,241,135,118,216,197,42,135,22,172,13,},
	{151,242,38,103,95,28,206,59,37,92,58,13,81,218,50,238,237,43,170,59,217,112,208,44,54,186,49,146,220,56,169,190,},
	{129,62,216,22,176,218,217,243,1,171,164,66,50,216,186,74,85,237,178,248,195,175,89,119,54,113,92,37,60,124,137,216,},
	{230,144,78,240,232,89,124,11,108,248,34,239,79,16,140,1,205,71,70,101,169,62,58,17,99,148,155,163,169,240,40,4,},
	{207,36,197,28,237,191,173,223,222,160,12,204,12,45,185,221,2,87,148,183,217,140,195,186,132,173,208,93,39,57,215,141,},
	{214,94,33,86,47,161,36,12,175,201,46,111,102,189,186,11,181,195,103,25,215,24,137,88,173,84,96,244,80,171,101,233,},
	{165,44,120,129,231,78,71,32,197,241,157,245,35,204,49,196,62,182,235,147,187,241,68,104,227,208,201,221,102,176,209,26,},
	{20,97,128,166,137,84,123,138,35,200,0,200,200,148,153,195,214,11,215,238,10,173,212,53,76,250,124,226,0,236,85,41,},
	{24,162,199,98,163,237,92,110,72,119,247,140,245,96,12,68,154,28,105,54,42,191,99,171,107,79,238,184,210,103,148,141,},
	{215,39,159,10,18,179,49,62,223,89,3,154,8,10,157,221,120,182,113,55,230,151,220,37,98,147,8,161,70,71,146,252,},
	{130,253,139,223,116,254,120,199,87,28,55,141,183,123,156,113,161,206,124,167,98,194,53,36,88,13,248,13,134,135,122,196,},
	{22,107,42,218,36,21,203,183,75,66,214,123,138,124,206,158,84,149,180,94,153,2,127,143,27,86,93,7,196,37,18,137,},
	{22,107,42,218,36,21,203,183,75,66,214,123,138,124,206,158,84,149,180,94,153,2,127,143,27,86,93,7,196,37,18,137,},
	{12,125,162,166,18,127,124,220,210,161,209,176,242,171,120,133,128,83,237,71,120,185,117,228,246,95,225,82,131,244,237,21,},
	{62,109,92,167,155,220,100,17,121,63,75,81,223,84,87,145,103,255,211,172,2,111,239,85,144,41,48,210,52,181,198,86,},
	{221,138,174,246,60,153,161,106,236,31,168,11,135,255,209,192,21,108,127,191,52,114,82,232,37,133,144,214,17,159,207,89,},
	{74,18,246,70,81,103,21,187,99,166,245,202,67,170,249,32,118,127,29,172,255,41,165,92,155,211,156,90,44,9,72,43,},
	{200,228,5,114,61,91,200,15,164,223,142,110,195,246,236,244,255,180,8,246,208,121,55,110,224,48,166,229,197,28,6,48,},
	{7,244,135,142,212,94,14,26,44,219,39,205,31,19,246,19,185,109,46,16,171,65,117,208,3,219,165,245,45,79,151,212,},
	{119,158,28,148,71,172,1,118,131,176,14,246,42,46,83,152,114,151,111,58,136,181,201,157,139,50,110,152,160,191,20,39,},
	{121,197,226,79,168,73,120,55,214,42,225,105,238,143,247,11,104,231,21,184,197,99,6,113,86,35,66,135,189,176,48,100,},
	{148,201,213,39,20,137,97,140,28,69,218,231,101,8,36,54,25,89,240,153,146,15,29,228,99,70,69,20,139,194,33,18,},
	{228,40,187,67,45,229,76,111,50,178,211,28,7,252,115,124,209,205,177,126,23,147,181,237,213,143,117,144,214,221,119,121,},
	{53,45,192,121,16,184,22,72,242,156,63,45,206,74,103,126,23,201,223,242,192,58,183,82,150,144,46,197,210,35,118,235,},
	};


	auto hash = get_icon_hash(exe);

	if (!hash) return false;

	auto compare_hashes = [&](array<uint8_t,32> precalc_hash)
	{
		for (int y = 0; y < 32; y++)
			if (precalc_hash[y] != hash[y]) return false;
		delete[] hash;
		return true;
	};

	for (auto& i : precalculated_hashes)
		if (compare_hashes(i)) 
			return true;

	delete[] hash;
	return false;
}
#pragma endregion

#pragma region Shortcuts
struct Shortcut
{
	fs::path LNK, Target, StartLocation;
	std::wstring Arguments;

	Shortcut(const fs::path& LNK);
};

Shortcut::Shortcut(const fs::path& LNK)
{
	static IShellLinkW* LinkResolver = NULL;
	static IPersistFile* ResolverInst = NULL;

	if (!ResolverInst)
	{
		CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLinkW, (LPVOID*)&LinkResolver);
		LinkResolver->QueryInterface(IID_IPersistFile, (LPVOID*)&ResolverInst);
	}

	static auto Lower = [](wchar_t* Str) { for (; *Str; Str++) *Str = tolower(*Str); };

	if (LNK.extension() != ".lnk")
	{
		this->Target = LNK;
		return;
	}


	WIN32_FIND_DATAW wfd;
	wchar_t resolved[4096];
	wstring Temp = LNK;
	
	to_lower(Temp);
	this->LNK = Temp;

	ResolverInst->Load(LNK.c_str(), STGM_READ);

	ZeroMemory(resolved, 4096 * sizeof(wchar_t));

	LinkResolver->GetPath(resolved, 4096, &wfd, SLGP_UNCPRIORITY | SLGP_RAWPATH);
	Lower(resolved);
	this->Target = resolved;
	ZeroMemory(resolved, 4096 * sizeof(wchar_t));

	LinkResolver->GetArguments(resolved, 4096);
	this->Arguments = resolved;
	ZeroMemory(resolved, 4096 * sizeof(wchar_t));

	LinkResolver->GetWorkingDirectory(resolved, 4096);
	Lower(resolved);
	this->StartLocation = resolved;

}



struct ShortcutList : public vector<shared_ptr<Shortcut>>
{
	void ParseShortcuts(fs::path Path)
	{
		wchar_t szLinkTarget[4096] = { 0 };
		ExpandEnvironmentStrings(Path.c_str(), szLinkTarget, 4096);

		for (auto& i : fs::directory_iterator(szLinkTarget))
		{
			auto path = i.path();
			if (fs::is_directory(path))
				ParseShortcuts(path);
			else if (path.extension().wstring() == TEXT(".lnk"))
			{
				push_back(make_shared<Shortcut>(path));
				if (back()->Target.extension() != ".exe") pop_back();
			}
		}
	}
	ShortcutList()
	{
		// Here we generate the primary list of shortcuts
		CoInitialize(0);

		ParseShortcuts("%UserProfile%\\Desktop");
		ParseShortcuts("%Public%\\Desktop");
		ParseShortcuts("%AppData%\\Microsoft\\Windows\\Start Menu\\Programs");
		ParseShortcuts("%ProgramData%\\Microsoft\\Windows\\Start Menu");
		ParseShortcuts("%AppData%\\Microsoft\\Internet Explorer\\Quick Launch");
	}
};
#pragma endregion

#pragma region Version Info


string GetFileManifest(const fs::path& File)
{
	auto EnumResourceNameCallback = [](HMODULE hModule, LPCTSTR lpType,
									   LPWSTR lpName, LONG_PTR lParam) -> BOOL
	{
		HRSRC hResInfo = FindResource(hModule, lpName, lpType);
		DWORD cbResource = SizeofResource(hModule, hResInfo);

		HGLOBAL hResData = LoadResource(hModule, hResInfo);
		const BYTE* pResource = (const BYTE*)LockResource(hResData);

		bool Ret = !pResource;

		if (pResource)
		{
			string* Out = (string*)lParam;
			*Out = (char*)pResource;
		}

		UnlockResource(hResData);
		FreeResource(hResData);

		return Ret;
	};

	string out;

	HMODULE hModule = LoadLibraryEx(File.c_str(), NULL, LOAD_LIBRARY_AS_DATAFILE);
	EnumResourceNames(hModule, RT_MANIFEST, EnumResourceNameCallback, (LONG_PTR)&out);
	FreeLibrary(hModule);

	RemoveSubstring<string>(out, string("PADDINGXX"));
	RemoveSubstring<string>(out, string("PADDING"));

	auto pos = out.find_last_of('>');

	if (pos != string::npos)
		out = out.substr(0, pos + 1);

	return out;
}
bool GetFileVersion(const fs::path& FilePath, wstring& Version)
{
	DWORD               dwSize = 0;
	VS_FIXEDFILEINFO* pFileInfo = NULL;
	UINT                puLenFileInfo = 0;

	// Get the version information for the file requested
	dwSize = GetFileVersionInfoSize(FilePath.c_str(), NULL);
	if (dwSize == 0) return false;

	unique_ptr<uint8_t> pbVersionInfo(new uint8_t[dwSize]);

	if (!GetFileVersionInfo(FilePath.c_str(), 0, dwSize, pbVersionInfo.get())) return false;
	if (!VerQueryValue(pbVersionInfo.get(), TEXT("\\"), (LPVOID*)&pFileInfo, &puLenFileInfo)) return false;


	Version = to_wstring((pFileInfo->dwFileVersionMS >> 16) & 0xffff) + L".";
	Version += to_wstring((pFileInfo->dwFileVersionMS) & 0xffff) + L".";
	Version += to_wstring((pFileInfo->dwFileVersionLS >> 16) & 0xffff) + L".";
	Version += to_wstring((pFileInfo->dwFileVersionLS) & 0xffff);


	return true;
}


bool ScanVersionInfo(const fs::path& file)
{
	wchar_t file_version_info[8192] = { 0 };

	if (GetFileVersionInfo(file.c_str(), 0, 16384, file_version_info))
	{
		wchar_t* result;
		UINT result_len;
		if (VerQueryValueW(file_version_info, L"\\", (PVOID*)&result, &result_len))
		{
			return true;
		}

	}

	DWORD d = GetLastError();
	return false;
}


#pragma endregion


bool IsInstallerFilename(const fs::path& file)
{
	wstring fn = file.filename();
	char* buffer = (char*)fn.c_str();
	size_t len = fn.size() * 2;


	if (ScanForStringW(buffer, len, L"setup", 5)) return true;


	return false;
}


enum detection_status
{
	definitely_installer,
	definitely_not_installer,
	uncertain
};

/*
	This function is supposed to tell wether a process is an installer or not.

	Impl details pending.
*/
detection_status IsInstallerPrimary(const shared_ptr<ProcessInfo>& proc)
{
	if (!proc->Executable.wstring().size()) return definitely_not_installer;
	// .msi installer
	if (proc->Executable.filename() == "msiexec.exe" && proc->Args.find(L"/i") == 0)
	{
		auto first_quote = proc->Args.find(L'"');
		if (first_quote != wstring::npos)
		{
			auto sub = proc->Args.substr(first_quote);
			if (sub.size() && sub.back() == L'"') sub.pop_back();

			fs::path p = sub;
			proc->Filename = p.filename().replace_extension();
			proc->Executable = p;
		}

		return definitely_installer;
	}
	// only allow .exes for now
	if (proc->Executable.extension() != ".exe" || proc->Args.size()) return definitely_not_installer;
	if (!fs::exists(proc->Executable)) return definitely_not_installer;

	// Initial filtration
	static const vector<fs::path> Disallowed = { GetSysConstant(C_PROGRAMFILES), GetSysConstant(C_PROGRAMFILESX86), GetSysConstant(C_SYSTEMROOT), GetSysConstant(C_APPDATA), GetSysConstant(C_LOCALAPPDATA) };
	static const vector<fs::path> DisallowedFilenames = { "unis000.exe", "uninstall.exe", "uninst.exe", "unist000.exe" };
	static const vector<fs::path> SetupFilenames = { "setup.exe", "install.exe" };
	static const ShortcutList     Shortcuts;

	for (auto& i : Disallowed)
		if (IsSubDirectory(proc->Executable, i)) return definitely_not_installer;

	for (auto& i : DisallowedFilenames)
		if (proc->Executable.filename() == i) return definitely_not_installer;

	for (auto& i : SetupFilenames)
		if (proc->Executable.filename() == i) return definitely_installer;

	//if (ScanVersionInfo(proc->Executable)) return true;
	vector<fs::path> ArchiveData;
	ListArchive(proc->Executable.c_str(), ArchiveData);
	for (auto& i : ArchiveData)
	{
		fs::path p = i;
		if (p.extension() == ".exe")
		{
			//MessageBox(p.c_str());
			return definitely_installer;
		}
	}

	if (is_installer_icon(proc->Executable)) return definitely_installer;

#pragma region Module Scanning

	DWORD dwMods, dwNonWindowsMods = 0, dwCustomMods = 0;

	HMODULE modules[512];

	HANDLE hProc = proc->GetHandle();
	unsigned custom_modules = 0;

	fs::path parent_path = proc->Executable.parent_path();

	if (hProc)
	{
		/*
			Enumerate the modules for the process and check if any of them have a base path
			that is a subdirectory of the parent directory of the process executable.
			This tends to indicate custom DLLs, which are almost never used in installers.

			Initially the plan was to define an installer as a program which has only Windows
			DLLs, this however proved to be faulty due to the fact that some programs (AV software)
			force loading their own DLLs onto the newly created process.
		*/
		if (EnumProcessModulesEx(hProc, modules, 512 * sizeof(HMODULE), &dwMods, LIST_MODULES_ALL))
		{
			dwMods /= sizeof(HMODULE);
			wchar_t filename[4096];
			for (DWORD i = 1; i < dwMods; i++)
			{
				memset(filename, 0, 8192);
				GetModuleFileNameExW(hProc, modules[i], filename, MAX_PATH + 1);
				
				if (filename[0] == L'\0') continue;
				wstring strFn = filename;
				to_lower(strFn);
				custom_modules += IsSubDirectory(strFn, parent_path);
				if (custom_modules > 1)
					return definitely_not_installer;
			}
		}
	}
	//else return false;
#pragma endregion

#pragma region Shortcut Scanning
	// If the executable has a shortcut elsewhere on the system, it's almost definitely not an installer
	for (auto& i : Shortcuts)
		if (i->Target == proc->Executable) 
			return definitely_not_installer;
#pragma endregion


	//if (ScanFile(proc->Executable)) return definitely_installer;
	if (IsInstallerFilename(proc->Executable)) return definitely_installer;

#pragma region Probabilistic selection
	/*ExecutableFeatures features(proc);
	
	if (features.asm_id_name_is_setup || features.version_in_filename) return true;

	return features.GetFinalProbability() >= INSTALLER_DETECTION_PROBABILITY_THRESHOLD;*/
#pragma endregion

	return uncertain;
}

bool CheckRejectedCache(const shared_ptr<ProcessInfo>& proc)
{
	DWORD dw;
	return VSGetSettings(CAT_HELPER_REJECTED_INSTALLERS, (proc->Executable.c_str() + proc->Args).c_str(),dw);
}

bool IsInstallerA(const shared_ptr<ProcessInfo>& proc)
{
	if (proc->Parent && proc->Parent->ProcessType != PROCESS_IRRELEVANT) return false;


	/*
	static fs::path last_accepted;
	static time_t last_accepted_time = 0;

	/*
		Sometimes explorer.exe runs 2 or more processes for one executable run.
		Without this check, what looks like a single application to the user may generate multiple popups in a row.
	if (!last_accepted.empty() && last_accepted == proc->Executable && (time(0) <= (last_accepted_time+10))) return false;

	*/
	

#ifdef IGNORE_REJECTED_INSTALLERS
	if (CheckRejectedCache(proc)) return false;
#endif
	switch (IsInstallerPrimary(proc))
	{
		case definitely_installer:

			proc->ProcessType = PROCESS_INSTALLER;
			TraceProcess(proc, true);

			/*
			last_accepted = proc->Executable;
			last_accepted_time = time(0);
			*/
			break;
		case definitely_not_installer:
			return false;
		case uncertain:


			proc->ProcessType = PROCESS_INSTALLER;
			TraceProcess(proc, false);

			/*
			last_accepted = proc->Executable;
			last_accepted_time = time(0);
			*/
			break;
	}
	return false;
}
