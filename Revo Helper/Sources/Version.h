
#ifndef VERSION_H
#define VERSION_H

class CVersion
{
	// constructors
public:
	CVersion(LPCTSTR lpszPath = NULL);
	virtual ~CVersion();
	BOOL Init();

	// operations
public:
	BOOL operator> (CVersion& rVersion);

	// attributes
public:
	// fixed info
	BOOL GetFileVersion(CString& strFileVer);
	BOOL GetFileVersion(WORD* pwVersion1,
						WORD* pwVersion2,
						WORD* pwVersion3,
						WORD* pwVersion4);
	BOOL GetProductVersion(CString& strFileVer);
	BOOL GetProductVersion(WORD* pwVersion1,
						   WORD* pwVersion2,
						   WORD* pwVersion3,
						   WORD* pwVersion4);
	BOOL GetFileFlags(DWORD& rdwFlags);
	BOOL GetFileOS(DWORD& rdwOS);
	BOOL GetFileType(DWORD& rdwType);
	BOOL GetFileSubtype(DWORD& rdwType);
	time_t GetTimeStamp();

	// string info
	BOOL GetCompanyName(CString& rCompanyName);
	BOOL GetFileDescription(CString& rFileDescription);
	BOOL GetLegalCopyright(CString& rLegalCopyright);
	BOOL GetProductName(CString& rProductName);
	BOOL GetStringInfo(LPCTSTR lpszKey, CString& rValue);
	BOOL GetLanguageName(CString& rValue);
	BOOL GetLanguageIdString(CString& rValue) { rValue = m_strLangId; return TRUE; }
	BOOL GetStringInfoEx(int nIndex, CString& strKey, CString& strValue);
	INT_PTR GetStringInfoEx();

	CStringArray* m_psaKeys;
	CStringArray* m_psaValues;
	CStringArray* m_psaLanguages;

	// implementation
protected:
	BOOL GetLanguageId(CString& rValue);
	BOOL GetFixedInfo(VS_FIXEDFILEINFO& rFixedInfo);
	void Dump(CString& rFirstLangId,
			  BOOL bFirstLangIdOnly,
			  CStringArray** ppsaKeys = NULL,
			  CStringArray** ppsaValues = NULL,
			  CStringArray** ppsaLanguages = NULL);
	BOOL GetTranslationId(LPVOID lpData, UINT unBlockSize, WORD wLangId,
						  DWORD& dwId, BOOL bPrimaryEnough/*= FALSE*/);

	BYTE* m_pData;
	DWORD				m_dwDataSize;
	VS_FIXEDFILEINFO	m_FixedInfo;
	time_t				m_mtime;
	DWORD				m_dwHandle;
	WORD				m_wFileVersion[4];
	WORD				m_wProductVersion[4];
	DWORD				m_dwFileFlags;
	DWORD				m_dwFileOS;
	DWORD				m_dwFileType;
	DWORD				m_dwFileSubtype;
	DWORD				m_dwLangId;

	CString				m_strPath;
	CString				m_strCompanyName;
	CString				m_strFileDescription;
	CString				m_strLegalCopyright;
	CString				m_strProductName;
	CString				m_strLangId;
};

#endif
///////////////////////////////////////////////////////////////////////////////
