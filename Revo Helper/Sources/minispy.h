/*++

Copyright (c) 1989-2002  Microsoft Corporation

Module Name:

    minispy.h

Abstract:

    Header file which contains the structures, type definitions,
    and constants that are shared between the kernel mode driver,
    minispy.sys, and the user mode executable, minispy.exe.

Environment:

    Kernel and user mode

--*/
#ifndef __MINISPY_H__
#define __MINISPY_H__

#include "AppBarStdafx.h"

//
//  FltMgr's IRP major codes
//

#define IRP_MJ_ACQUIRE_FOR_SECTION_SYNCHRONIZATION  ((UCHAR)-1)
#define IRP_MJ_RELEASE_FOR_SECTION_SYNCHRONIZATION  ((UCHAR)-2)
#define IRP_MJ_ACQUIRE_FOR_MOD_WRITE                ((UCHAR)-3)
#define IRP_MJ_RELEASE_FOR_MOD_WRITE                ((UCHAR)-4)
#define IRP_MJ_ACQUIRE_FOR_CC_FLUSH                 ((UCHAR)-5)
#define IRP_MJ_RELEASE_FOR_CC_FLUSH                 ((UCHAR)-6)
#define IRP_MJ_NOTIFY_STREAM_FO_CREATION            ((UCHAR)-7)

#define IRP_MJ_FAST_IO_CHECK_IF_POSSIBLE            ((UCHAR)-13)
#define IRP_MJ_NETWORK_QUERY_OPEN                   ((UCHAR)-14)
#define IRP_MJ_MDL_READ                             ((UCHAR)-15)
#define IRP_MJ_MDL_READ_COMPLETE                    ((UCHAR)-16)
#define IRP_MJ_PREPARE_MDL_WRITE                    ((UCHAR)-17)
#define IRP_MJ_MDL_WRITE_COMPLETE                   ((UCHAR)-18)
#define IRP_MJ_VOLUME_MOUNT                         ((UCHAR)-19)
#define IRP_MJ_VOLUME_DISMOUNT                      ((UCHAR)-20)

//
//  My own definition for transaction notify command
//

#define IRP_MJ_TRANSACTION_NOTIFY                   ((UCHAR)-40)


//
//  Version definition
//

#define MINISPY_MAJ_VERSION 2
#define MINISPY_MIN_VERSION 0

typedef struct _MINISPYVER {

    USHORT Major;
    USHORT Minor;

} MINISPYVER, *PMINISPYVER;

//
//  Name of minispy's communication server port
//

#define MINISPY_PORT_NAME                   L"\\RevoFltPort"

//
//  Local definitions for passing parameters between the filter and user mode
//

typedef ULONG_PTR FILE_ID;
typedef LONG NTSTATUS;

//
//  The maximum size of a record that can be passed from the filter
//

#define RECORD_SIZE     512

//
//  The maximum size of a buffer that can be passed to the filter
//

#define BUFFER_SIZE     16*65536 //it is devided by PVOID (4 or 8)


//
//  This defines the type of record buffer this is along with certain flags.
//

#define RECORD_TYPE_NORMAL                       0x00000000
#define RECORD_TYPE_FILETAG                      0x00000004

#define RECORD_TYPE_FLAG_STATIC                  0x80000000
#define RECORD_TYPE_FLAG_EXCEED_MEMORY_ALLOWANCE 0x20000000
#define RECORD_TYPE_FLAG_OUT_OF_MEMORY           0x10000000
#define RECORD_TYPE_FLAG_MASK                    0xffff0000

//
//  The fixed data received for RECORD_TYPE_NORMAL
//

//typedef struct _RECORD_DATA {
//
//    LARGE_INTEGER OriginatingTime;
//    LARGE_INTEGER CompletionTime;
//
//    //FILE_ID DeviceObject;
//    //FILE_ID FileObject;
//    //FILE_ID Transaction;
//
//    FILE_ID ProcessId;
//    FILE_ID ThreadId;
//
//    ULONG_PTR Information;
//
//    NTSTATUS Status;
//
//    //ULONG IrpFlags;
//    //ULONG Flags;
//
//    UCHAR CallbackMajorId;
//    UCHAR CallbackMinorId;
//    UCHAR Reserved[2];      // Alignment on IA64
//
//    //PVOID Arg1;
//    //PVOID Arg2;
//    //PVOID Arg3;
//    //PVOID Arg4;
//    //PVOID Arg5;
//    //LARGE_INTEGER Arg6;
//
//} RECORD_DATA, *PRECORD_DATA;

//
//  What information we actually log.
//
//
//typedef struct _LOG_RECORD {
//
//
//    ULONG Length;           // Length of log record.  This Does not include
//    ULONG SequenceNumber;   // space used by other members of RECORD_LIST
//
//    ULONG RecordType;       // The type of log record this is.
//    ULONG Reserved;         // For alignment on IA64
//
//    RECORD_DATA Data;
//    ULONG OldNameLength;
//	PVOID OldName;
//	ULONG NewNameLength;
//	PVOID NewName;
// 
//    WCHAR Name[];           //  This is a null terminated string
//
//} LOG_RECORD, *PLOG_RECORD;

typedef struct _LOG_FILE_RECORD {

    ULONG Length;           // Length of log record.  This Does not include
    ULONG SequenceNumber;   // space used by other members of RECORD_LIST

    ULONG RecordType;       // The type of log record this is.
	ULONG Reserved;         // For alignment on IA64
    
	LARGE_INTEGER OriginatingTime;
    LARGE_INTEGER CompletionTime;

    FILE_ID ProcessId;
    FILE_ID ThreadId;

    ULONG_PTR Information;

    NTSTATUS Status;

	INT IsDirectory;
    UCHAR CallbackMajorId;
    UCHAR CallbackMinorId;
  
	ULONG OldNameLength;
	PVOID OldName;
	
	ULONG NewNameLength;
	PVOID NewName;
 
} LOG_FILE_RECORD, *PLOG_FILE_RECORD;


typedef struct _REG_RECORD {

    ULONG Length;           // Length of log record.  

	FILE_ID ProcessId;
	REG_NOTIFY_CLASS eventType;

	ULONG RegNewDataType;
	ULONG RegOldDataType;
	ULONG RecordType;       // The type of log record this is.
	
	ULONG RegNewDataLen;
	PVOID RegNewData;
	
	ULONG RegOldDataLen;
	PVOID RegOldData;
	
	ULONG RegPathLen;
	PVOID RegPath;
	
	ULONG RegValueNameLen;
	PVOID RegValueName;

}REG_RECORD, *PREG_RECORD;

typedef struct _REGRECORD_LIST {

    LIST_ENTRY List;

    //
    // Must always be last item.  See MAX_LOG_RECORD_LENGTH macro below.
    // Must be aligned on PVOID boundary in this structure. This is because the
    // log records are going to be packed one after another & accessed directly
    // Size of log record must also be multiple of PVOID size to avoid alignment
    // faults while accessing the log records on IA64
    //

    REG_RECORD RegRecord;

} REGRECORD_LIST, *PREGRECORD_LIST;

typedef struct _PID_RECORD {

	FILE_ID ProcessId;
	FILE_ID ProcessParentId;
	USHORT bCreate;
	ULONG RecordType;       // The type of log record this is.

	ULONG Reserved; //For 64 bit alignment

}PID_RECORD, *PPID_RECORD;

typedef struct _PIDRECORD_LIST {

    LIST_ENTRY List;

    //
    // Must always be last item.  See MAX_LOG_RECORD_LENGTH macro below.
    // Must be aligned on PVOID boundary in this structure. This is because the
    // log records are going to be packed one after another & accessed directly
    // Size of log record must also be multiple of PVOID size to avoid alignment
    // faults while accessing the log records on IA64
    //

    PID_RECORD PIDRecord;

} PIDRECORD_LIST, *PPIDRECORD_LIST;

//
//  How the mini-filter manages the log records.
//

typedef struct _RECORD_LIST {

    LIST_ENTRY List;

    //
    // Must always be last item.  See MAX_LOG_RECORD_LENGTH macro below.
    // Must be aligned on PVOID boundary in this structure. This is because the
    // log records are going to be packed one after another & accessed directly
    // Size of log record must also be multiple of PVOID size to avoid alignment
    // faults while accessing the log records on IA64
    //

    LOG_FILE_RECORD LogRecord;

} RECORD_LIST, *PRECORD_LIST;

//
//  Defines the commands between the utility and the filter
//

typedef enum _MINISPY_COMMAND {

    GetMiniSpyLog,
	GetMiniSpyRegLog,
	GetMiniSpyPIDLog,
	StartCapturing,
	StopCapturing,
    GetMiniSpyVersion
} MINISPY_COMMAND;

//
//  Defines the command structure between the utility and the filter.
//

typedef struct _COMMAND_MESSAGE {
    MINISPY_COMMAND Command;
	ULONG NewPid;
    ULONG Reserved;  // Alignment on IA64
    //UCHAR Data[]; //Shit may happen because of that comment!
} COMMAND_MESSAGE, *PCOMMAND_MESSAGE;

//
//  The maximum number of BYTES that can be used to store the file name in the
//  RECORD_LIST structure
//

#define MAX_NAME_SPACE ROUND_TO_SIZE( (RECORD_SIZE - sizeof(RECORD_LIST)), sizeof( PVOID ))

//
//  Returns the number of BYTES unused in the RECORD_LIST structure.  Note that
//  LogRecord->Length already contains the size of LOG_RECORD which is why we
//  have to remove it.
//

#define REMAINING_NAME_SPACE(LogRecord) \
    (ASSERT((LogRecord)->Length >= sizeof(LOG_FILE_RECORD)), \
     (USHORT)(MAX_NAME_SPACE - ((LogRecord)->Length - sizeof(LOG_FILE_RECORD))))

#define MAX_LOG_RECORD_LENGTH  (RECORD_SIZE - FIELD_OFFSET( RECORD_LIST, LogRecord ))


//
//  Macros available in kernel mode which are not available in user mode
//

#ifndef Add2Ptr
#define Add2Ptr(P,I) ((PVOID)((PUCHAR)(P) + (I)))
#endif

#ifndef ROUND_TO_SIZE
#define ROUND_TO_SIZE(_length, _alignment)    \
            (((_length) + ((_alignment)-1)) & ~((_alignment) - 1))
#endif

#ifndef FlagOn
#define FlagOn(_F,_SF)        ((_F) & (_SF))
#endif

#endif /* __MINISPY_H__ */

