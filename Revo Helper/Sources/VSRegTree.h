#ifndef _VSREGTREE_H_INCLUDED_
#define _VSREGTREE_H_INCLUDED_
#pragma once

class VSRegVal;
class VSRegTreeItem;

enum eRegOperation {RegNone, RegCreated, RegSetVal, RegDeleted, RegExclude, RegError, RegNotExist};
enum eRegItemType {RootKey, ChildKey, BrotherKey};
enum eRegWOW6432Type {KeyNoType, Key32Bit, Key64Bit};

#define CURRENT_VERSION_RT	1
#define CURRENT_VERSION_RV	1
#define CURRENT_VERSION_RI	1

typedef struct _REGNODE
{
	LPTSTR								pszRegKeyName;
	struct _REGNODE *					pChild;
	struct _REGNODE *					pBrother;
	struct _REGNODE *					pFather;
	eRegWOW6432Type						eWOW6432Type;
	eRegOperation						eOperation;
	CTypedPtrList<CObList, VSRegVal*>*	m_pListRegVals;

}REGNODE, *PREGNODE;

class VSRegTree : public CObject
{

struct RegExcludePath
{
	CStringArray	arrRegExcludedPath;
	BOOL			bExcludeSubKeys;
};

public:
	DECLARE_SERIAL(VSRegTree)
	
	void Serialize(CArchive &ar); 
	void SerializebyList(CArchive &ar); 
	void RemoveNodeFromTree(PREGNODE pTreeNode);
	
	BOOL SaveRegTreeLog(LPCTSTR pszFilePath);
	BOOL SaveRegTreeLogbyList(LPCTSTR pszFilePath);
	BOOL LoadRegTreeLog(LPCTSTR pszFilePath);

	BOOL AddExludeKey(LPCTSTR pszFullKeyPath);

	PREGNODE CreateKey(LPCTSTR pszFullKeyPath, eRegWOW6432Type eWOW6432Type = KeyNoType);
	PREGNODE MergeCreateKey(LPCTSTR pszFullKeyPath, eRegWOW6432Type eWOW6432Type = KeyNoType, eRegOperation eOperation = RegNone);
	PREGNODE DeleteKey(LPCTSTR pszFullKeyPath, eRegWOW6432Type eWOW6432Type = KeyNoType);
	PREGNODE DeleteKeyVal(LPCTSTR strKeyPath, LPCTSTR strValName, DWORD dwType, BYTE* btData, DWORD dwDataLen, eRegWOW6432Type eWOW6432Type = KeyNoType);
	PREGNODE SetKeyVal(LPCTSTR strKeyPath, LPCTSTR strValName, 
		    	   DWORD dwType, BYTE* btData, DWORD dwDataLen,
				   DWORD dwOldType, BYTE* btOldData, DWORD dwOldDataLen,  eRegWOW6432Type eWOW6432Type = KeyNoType);
	PREGNODE MergeSetKeyVal(LPCTSTR strKeyPath, LPCTSTR strValName,
		DWORD dwType, BYTE* btData, DWORD dwDataLen,
		DWORD dwOldType, BYTE* btOldData, DWORD dwOldDataLen, eRegWOW6432Type eWOW6432Type = KeyNoType, eRegOperation eOperation = RegSetVal);
	
	PREGNODE GetKey(LPCTSTR pszFullKeyPath);
	PREGNODE GetRegTree() { return m_pTree; }
	void	 SetRegTree(PREGNODE pNewTree) { m_pTree = pNewTree; }

	//Helper function that adds child parameter to parent parameter
	PREGNODE AddChildToNode(PREGNODE pParentNode, PREGNODE pChildNode);
	void ExcludeRegKey(LPCTSTR pszFullRegPath, BOOL bExcludeSubKeys);

	void ConvertRegTreeToList(PREGNODE pTreeNode,		
							  CTypedPtrList<CObList, VSRegTreeItem*>* listVals, 
							  int nDeep, eRegItemType eType);
	PREGNODE ConvertListToRegTree(CTypedPtrList<CObList, VSRegTreeItem*>* listRegItems);

	CTypedPtrList<CObList, VSRegTreeItem*>* GetRegList() { return &m_listRegItems; }

	VSRegTree()
	{
		m_pTree = CreateRegNode(_T("REGISTRY"));
		m_nVersion = CURRENT_VERSION_RT;
	}

	~VSRegTree();

	PREGNODE CreateRegNode(LPCTSTR pszKeyName = NULL, 
						   eRegWOW6432Type eKeyWOW6432Type = KeyNoType,
						   eRegOperation eOperation = RegNone,
						   CTypedPtrList<CObList, VSRegVal*>* pListRegVals = NULL,
						   PREGNODE pChild = NULL, 
						   PREGNODE pBrother = NULL, 
						   PREGNODE pFather = NULL);

	//PREGNODE InsertKeyLikeMerge(LPCTSTR pszFullKeyPath,BOOL* bNewlyCreated);
	PREGNODE InsertKey(LPCTSTR pszFullKeyPath);

	void DeleteTreeMem(PREGNODE pTreeNode);
	void EmptyTree();

private:
	void DeleteRegNode(PREGNODE pTreeNode);
	void DeleteRegTreeItemsList();
	void ExcludeKey(PREGNODE pTreeNode, RegExcludePath* pRegExcludePaths,  DWORD dwLevel = 0);

private:
	//always read version first
	INT											m_nVersion;				
	PREGNODE									m_pTree;
	CTypedPtrList<CObList, VSRegTreeItem*>		m_listRegItems;
};

class VSRegTreeItem : public CObject
{
public:
	DECLARE_SERIAL(VSRegTreeItem)
	virtual void Serialize(CArchive &ar); 
	VSRegTreeItem()
	{
		m_nVersion		= CURRENT_VERSION_RI;
		m_nAfterStatus	= 0;
		m_nBackLevels	= 0;
		m_eOperation	= RegNone;
		m_eWOW6432Type	= KeyNoType;
	}
	VSRegTreeItem(LPCTSTR pszItemName, int nAfterStatus, int nBackLevels, eRegWOW6432Type eWOW6432Type, eRegOperation eOp, CTypedPtrList<CObList, VSRegVal*>* listRegVals) :
				  m_strItemName(pszItemName), m_nAfterStatus(nAfterStatus), m_nBackLevels(nBackLevels), m_eWOW6432Type(eWOW6432Type), m_eOperation(eOp),
				  m_nVersion(CURRENT_VERSION_RI) 
				  {
					  m_ListRegVals.AddTail(listRegVals);
				  }

   	operator LPARAM () const
	{
		return reinterpret_cast < LPARAM > ( this );
	}
	static VSRegTreeItem* FromLPARAM(LPARAM lParam)
	{
		VSRegTreeItem * pData =	reinterpret_cast < VSRegTreeItem * > ( lParam );
		ASSERT( pData != NULL );
		return  pData;
	}

	~VSRegTreeItem();

public:
	//always read version first
	INT										m_nVersion;				
	//version 1
	INT										m_nAfterStatus;
	INT										m_nBackLevels;
	eRegWOW6432Type							m_eWOW6432Type;
	eRegOperation							m_eOperation;
	CString									m_strItemName;
	CTypedPtrList<CObList, VSRegVal*>		m_ListRegVals;
	////////////
};

class VSRegVal : public CObject
{
public:
	DECLARE_SERIAL(VSRegVal)
	void Serialize(CArchive &ar); 
	
	VSRegVal()
	{
		m_nVersion = 1;
		m_eOperation = RegNone;
	}

	VSRegVal(LPCTSTR pszValName, DWORD dwNewType, LPBYTE lpNewByteData, DWORD dwNewDataLen, 
								 DWORD dwOldType = 0, LPBYTE lpOldByteData = NULL, DWORD dwOldDataLen = 0) :
								 m_strValName(pszValName), 
								 m_dwNewType(dwNewType),
								 m_dwOldType(dwOldType), 
								 m_nVersion(CURRENT_VERSION_RV), 
								 m_eOperation(RegNone)
	{		
		if(dwNewDataLen)
		{
			m_NewBytesArray.RemoveAll();
			m_NewBytesArray.SetSize(dwNewDataLen);
			CopyMemory(m_NewBytesArray.GetData(), lpNewByteData, dwNewDataLen);
		}
		
		if(dwOldDataLen)
		{
			m_OldBytesArray.RemoveAll();
			m_OldBytesArray.SetSize(dwOldDataLen);
			CopyMemory(m_OldBytesArray.GetData(), lpOldByteData, dwOldDataLen);
		}
	}


//Members
public:
	//always read version first
	INT										m_nVersion;				
	//version 1
	eRegOperation							m_eOperation;

	CString									m_strValName;
	
	DWORD 									m_dwNewType;
	CByteArray								m_NewBytesArray;

	DWORD 									m_dwOldType;
	CByteArray								m_OldBytesArray;
};



#endif /* _VSREGTREE_H_INCLUDED_ */