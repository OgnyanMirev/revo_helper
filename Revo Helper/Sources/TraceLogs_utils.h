#pragma once
#include "ini.h"

#define SYSFOLDERCOUNT 46

#define BUFSIZE 256
#define PRODUCT_PROFESSIONAL 0x00000030
#define VER_SUITE_WH_SERVER  0x00008000

typedef void (WINAPI* PGNSI)(LPSYSTEM_INFO);
typedef BOOL(WINAPI* PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);

struct SystemFolderKey
{
	CString strIniKey;
	int		iCSIDL;
};

BOOL GetWindowsNameType(CString& strFullOSName, int* nIs64Bit);
void WriteSystemFolders(CIni* iniHelperFile);

BOOL CreateHelperInifile(CString& strLogPath, CString& strLogName, CString& strLogDate, CString& strLogHour);

BOOL CreateLogsInitData(CString& strIniFullPath);
BOOL TryToRestoreIniFile(CString& strIniFullPath, CString& strLogsFolder);
BOOL AddNewLog(CString& strIniFullPath, CString& strLogName, CString& strNewLogPath);
BOOL VerifyEmbeddedSignature();

BOOL IsLogNameExist(CIni* iniMain, CString& strNewlogName);