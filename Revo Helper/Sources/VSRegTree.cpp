#include "stdafx.h"
#include "VSRegTree.h"
#include <shlwapi.h>

IMPLEMENT_SERIAL(VSRegVal,CObject, VERSIONABLE_SCHEMA | 1)
IMPLEMENT_SERIAL(VSRegTree,CObject, VERSIONABLE_SCHEMA | 1)
IMPLEMENT_SERIAL(VSRegTreeItem,CObject, VERSIONABLE_SCHEMA | 1)

void VSRegTree::Serialize(CArchive &ar)
{
	CObject::Serialize( ar );

	if( ar.IsStoring() )
	{
		ar << m_nVersion;
		DeleteRegTreeItemsList();
		ConvertRegTreeToList(m_pTree, &m_listRegItems, 0, RootKey);
		m_listRegItems.Serialize(ar);
	}
	else
	{
		ar >> m_nVersion;
		switch (m_nVersion)
		{
			case 1:
				DeleteRegTreeItemsList();
				m_listRegItems.Serialize(ar);
				DeleteRegNode(m_pTree);
				m_pTree = ConvertListToRegTree(&m_listRegItems);
				break;
		}
	}
}
void VSRegTree::SerializebyList(CArchive &ar)
{
	CObject::Serialize( ar );

	if( ar.IsStoring() )
	{
		ar << m_nVersion;
		//DeleteRegTreeItemsList();
		//ConvertRegTreeToList(m_pTree, &m_listRegItems, 0, RootKey);
		m_listRegItems.Serialize(ar);
		DeleteRegTreeItemsList();
	}
	else
	{
		ar >> m_nVersion;
		switch (m_nVersion)
		{
			case 1:
				DeleteRegTreeItemsList();
				m_listRegItems.Serialize(ar);
				DeleteRegNode(m_pTree);
				m_pTree = ConvertListToRegTree(&m_listRegItems);
				break;
		}
	}
}

void VSRegTreeItem::Serialize(CArchive &ar)
{
	CObject::Serialize( ar );

	if( ar.IsStoring() )
	{
		ar << m_nVersion;
		ar << m_strItemName << m_nAfterStatus << m_nBackLevels << m_eWOW6432Type << m_eOperation;
	}
	else
	{
		ar >> m_nVersion;
		switch (m_nVersion)
		{
			case 1:
			ar >> m_strItemName >> m_nAfterStatus >> m_nBackLevels >> (LONG&) m_eWOW6432Type >> (LONG&) m_eOperation;
			break;
		}
	}
	m_ListRegVals.Serialize(ar);
}

void VSRegVal::Serialize(CArchive &ar)
{
	CObject::Serialize( ar );
	if( ar.IsStoring() )
	{
		ar << m_nVersion;
		ar << m_strValName << m_dwNewType << m_dwOldType << (LONG)m_eOperation;
	}
	else
	{
		ar >> m_nVersion;
		switch (m_nVersion)
		{
			case 1:
			ar >> m_strValName >> m_dwNewType >> m_dwOldType >> (LONG&)m_eOperation;
			break;
		}
	}
	m_NewBytesArray.Serialize(ar);
	m_OldBytesArray.Serialize(ar);
}

//Full path where the registry log file will be saved
BOOL VSRegTree::SaveRegTreeLog(LPCTSTR pszFilePath)
{
	 CFile fileData;
	 CFileException e;
	 
	 BOOL bRes = fileData.Open(pszFilePath, CFile::modeWrite|CFile::modeCreate,&e);
	 if(bRes == TRUE)
	 {
		CArchive ar(&fileData,CArchive::store);
		
		Serialize(ar);

		ar.Close();
		fileData.Close();
		
		return TRUE;
	 }
	return FALSE;
}

//Full path where the registry log file will be saved
BOOL VSRegTree::SaveRegTreeLogbyList(LPCTSTR pszFilePath)
{
	 CFile fileData;
	 CFileException e;
	 
	 BOOL bRes = fileData.Open(pszFilePath, CFile::modeWrite|CFile::modeCreate,&e);
	 if(bRes == TRUE)
	 {
		CArchive ar(&fileData,CArchive::store);
		
		SerializebyList(ar);

		ar.Close();
		fileData.Close();
		
		return TRUE;
	 }
	return FALSE;
}


BOOL VSRegTree::LoadRegTreeLog(LPCTSTR pszFilePath)
{
	CFile fileCacheData;
	CFileException e;
	
	BOOL bRes = fileCacheData.Open(pszFilePath, CFile::modeRead,&e);
	if(bRes == TRUE)
	{
		CArchive ar(&fileCacheData,CArchive::load);

		Serialize(ar);

		ar.Close();
		fileCacheData.Close();

		return TRUE;
	}

	return FALSE;
}
PREGNODE VSRegTree::ConvertListToRegTree(CTypedPtrList<CObList, VSRegTreeItem*>* listRegItems)
{
	PREGNODE trCurParent = NULL, trPrevParent = NULL, trUpperBrother = NULL, trRoot = NULL;
	
	int nLastItemBackLevels = 0;
	
	POSITION posData = listRegItems->GetHeadPosition();
	while(posData!=NULL)
	{
		VSRegTreeItem* regElem = (VSRegTreeItem*)listRegItems->GetNext(posData);

		int nGoBack = 0;
		BOOL bGoBackFlag = FALSE; 
		if(regElem->m_nBackLevels < nLastItemBackLevels)
		{
			nGoBack = nLastItemBackLevels - regElem->m_nBackLevels ;
			bGoBackFlag = TRUE; 
			nGoBack++;
		}
		
		while(nGoBack != 0)
		{
			trCurParent = trCurParent->pFather;
			nGoBack--;
		}

		if(bGoBackFlag == TRUE) 
			trPrevParent = trCurParent;

		switch (regElem->m_nAfterStatus)// 0- Root; 1- Child; 2 - Subling
		{
		case RootKey:
			trCurParent = CreateRegNode(regElem->m_strItemName, regElem->m_eWOW6432Type, regElem->m_eOperation, &regElem->m_ListRegVals,NULL,NULL,NULL);
			trRoot = trCurParent;
			break;
		case ChildKey:
			trPrevParent = trCurParent;
			trCurParent = CreateRegNode(regElem->m_strItemName, regElem->m_eWOW6432Type, regElem->m_eOperation, &regElem->m_ListRegVals,NULL,NULL,trCurParent);
			trPrevParent->pChild = trCurParent;
			break;
		case BrotherKey:
			if(trPrevParent)
			{
				trUpperBrother = trPrevParent->pChild;
				while(trUpperBrother->pBrother)
					trUpperBrother = trUpperBrother->pBrother;
				trCurParent = CreateRegNode(regElem->m_strItemName, regElem->m_eWOW6432Type, regElem->m_eOperation, &regElem->m_ListRegVals,NULL,NULL,trPrevParent);
				trUpperBrother->pBrother = trCurParent;
			}
			break;
		}
		nLastItemBackLevels = regElem->m_nBackLevels;
	}

	return trRoot;
}

void VSRegTree::ConvertRegTreeToList(PREGNODE pTree,	
						  CTypedPtrList<CObList, VSRegTreeItem*>* listVals, 
						  int nDeep, eRegItemType eType)
{
	if(pTree == NULL)
		return;

	PREGNODE pNextBrother = NULL;
	PREGNODE pCurrentNode = NULL;

	VSRegTreeItem* listItem = new VSRegTreeItem(pTree->pszRegKeyName, eType, nDeep, pTree->eWOW6432Type, pTree->eOperation, pTree->m_pListRegVals);

	listVals->AddTail(listItem);

	eType = ChildKey;
	ConvertRegTreeToList(pTree->pChild, listVals, ++nDeep, eType);
	nDeep--;
	eType = BrotherKey;
	
	pNextBrother = pTree->pBrother;
	while(pNextBrother != NULL)
	{
		pCurrentNode = pNextBrother;
		pNextBrother = pNextBrother->pBrother;

		VSRegTreeItem* listItem = new VSRegTreeItem(pCurrentNode->pszRegKeyName,
													eType,
													nDeep,
													pCurrentNode->eWOW6432Type,
													pCurrentNode->eOperation,
													pCurrentNode->m_pListRegVals);

		listVals->AddTail(listItem);

		if(pCurrentNode->pChild != NULL)
		{
			eType = ChildKey;
			ConvertRegTreeToList(pCurrentNode->pChild, listVals, ++nDeep, eType);
			nDeep--;
			eType = BrotherKey;
		}
	}
}

//Can create node even with a NULL name
PREGNODE VSRegTree::CreateRegNode(LPCTSTR pszKeyName,
								  eRegWOW6432Type eKeyWOW6432Type,
								  eRegOperation	eOperation,
								  CTypedPtrList<CObList, VSRegVal*>* pListRegVals,
								  PREGNODE pChild, 
								  PREGNODE pBrother, 
								  PREGNODE pFather)
{
	PREGNODE pTreeNode = NULL;

	pTreeNode = new REGNODE;
	if(pTreeNode)
	{
		pTreeNode->pBrother = pBrother;
		pTreeNode->pChild = pChild;
		pTreeNode->pFather = pFather;
		pTreeNode->eOperation = eOperation;
		pTreeNode->eWOW6432Type = eKeyWOW6432Type;
		pTreeNode->pszRegKeyName = NULL;
		pTreeNode->m_pListRegVals = NULL;

		pTreeNode->m_pListRegVals = new CTypedPtrList<CObList, VSRegVal*>;
		if(pListRegVals)
			pTreeNode->m_pListRegVals->AddTail(pListRegVals);

		if(pszKeyName)
		{
			size_t lLen = _tcslen(pszKeyName) + sizeof(TCHAR);
			pTreeNode->pszRegKeyName = new TCHAR[lLen];
			StringCchCopy(pTreeNode->pszRegKeyName, lLen, pszKeyName);
		}
	}
	
	return pTreeNode;
}

//Delete given node and all its sub nodes 
void VSRegTree::DeleteRegNode(PREGNODE pTreeNode)
{
	if(pTreeNode == NULL)
		return;

	PREGNODE pNextBrother = NULL;
	PREGNODE pCurrentNode = NULL;

	DeleteRegNode(pTreeNode->pChild);
	
	pNextBrother = pTreeNode->pBrother;
	while(pNextBrother != NULL)
	{
		pCurrentNode = pNextBrother;
		pNextBrother = pNextBrother->pBrother;
		if(pCurrentNode->pChild != NULL)
		{
			DeleteRegNode(pCurrentNode->pChild);
		}
		if(pCurrentNode->pszRegKeyName)
		{
			delete [] pCurrentNode->pszRegKeyName;
			pCurrentNode->pszRegKeyName = NULL;
		}

		//Delete values of the key
		POSITION pos;
		for (pos = pCurrentNode->m_pListRegVals->GetHeadPosition(); pos != NULL;)
		{
			VSRegVal* pRegVal = (VSRegVal*)pCurrentNode->m_pListRegVals->GetNext(pos);
			delete pRegVal;
			pRegVal = NULL;
		}
		//remove all pointers to values
		if(pCurrentNode->m_pListRegVals)
		{
			pCurrentNode->m_pListRegVals->RemoveAll();
			delete pCurrentNode->m_pListRegVals;
			pCurrentNode->m_pListRegVals = NULL;
		}

		if(pCurrentNode)
		{
			delete pCurrentNode;
			pCurrentNode = NULL;
		}
	}
	
	if(pTreeNode->pszRegKeyName)
	{
		delete [] pTreeNode->pszRegKeyName;
		pTreeNode->pszRegKeyName = NULL;
	}

	if(pTreeNode)
	{
		POSITION pos;
		for (pos = pTreeNode->m_pListRegVals->GetHeadPosition(); pos != NULL;)
		{
			VSRegVal* pRegVal = (VSRegVal*)pTreeNode->m_pListRegVals->GetNext(pos);
			delete pRegVal;
			pRegVal = NULL;
		}		
		pTreeNode->m_pListRegVals->RemoveAll();
		delete pTreeNode->m_pListRegVals;
		pTreeNode->m_pListRegVals = NULL;

		delete pTreeNode;
		pTreeNode = NULL;
	}
}

void VSRegTree::RemoveNodeFromTree(PREGNODE pTreeNode)
{
	if(pTreeNode == NULL)
		return;
	
	PREGNODE pNode = pTreeNode->pFather, pPrevNode = NULL;

	pNode = pNode->pChild;

	//check if the first child is the node we are looking for
	if(pNode == pTreeNode)
	{
		pNode->pFather->pChild = pNode->pBrother;
		pNode->pBrother = NULL;
		DeleteRegNode(pNode);
		return;
	}

	while(pNode)
	{
		pPrevNode = pNode;
		pNode = pNode->pBrother;

		//check if current node is the node we are looking for
		if(pNode == pTreeNode)
		{
			pPrevNode->pBrother = pNode->pBrother;
			pNode->pBrother = NULL;
			DeleteRegNode(pNode);
			return;
		}
	}
	OutputDebugString(_T("ERROR RU 0x00000005"));
}

//Helper function that adds child parameter to parent parameter
PREGNODE VSRegTree::AddChildToNode(PREGNODE pParentNode, PREGNODE pChildNode)
{
	if((pParentNode == NULL) || (pChildNode == NULL))
		return NULL;
	
	PREGNODE pRealChild = pParentNode->pChild;

	//if parent has a child then connect them
	if(pRealChild)
	{
		//connect it with other child
		pParentNode->pChild->pBrother = pRealChild;
	}

	pChildNode->pFather = pParentNode;
	pParentNode->pChild = pChildNode;

	return pChildNode;
}
//////////Key path should begin \REGISTRY\... MACHINE or USER
////////PREGNODE InsertKeyLikeMerge(LPCTSTR pszFullKeyPath,BOOL* bNewlyCreated)
////////{
////////	if(m_pTree == NULL)
////////		return NULL;
////////	
////////	if(pszFullKeyPath == NULL)
////////		return NULL;
////////
////////	PREGNODE pNode = m_pTree, pPrevNode = NULL;
////////
////////	CString strSrc(pszFullKeyPath), resToken;
////////	int curPos = 0;
////////
////////	resToken= strSrc.Tokenize(_T("\\"),curPos);
////////	while (resToken != "")
////////	{
////////		// While there are tokens in "pszFullKeyPath"
////////		if(pNode)
////////		{
////////			if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
////////			{
////////				pPrevNode = pNode;
////////				pNode = pNode->pChild;
////////			}
////////			else
////////			{
////////				if(pNode->pBrother)
////////				{
////////					while(pNode)
////////					{
////////						pPrevNode = pNode;
////////						pNode = pNode->pBrother;
////////						if(pNode)
////////						{
////////							if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
////////							{
////////								pPrevNode = pNode;
////////								pNode = pNode->pChild;
////////								*bNewlyCreated = FALSE;
////////								break;
////////							}
////////						}
////////						else
////////						{
////////							//key does not exists, so create it
////////							pPrevNode->pBrother = CreateRegNode(resToken);
////////							pPrevNode->pBrother->pFather = pPrevNode->pFather;
////////							pPrevNode = pPrevNode->pBrother;
////////							*bNewlyCreated = TRUE;
////////							break;
////////						}
////////					}
////////				}
////////				else
////////				{
////////					//no more brothers to match, so create a brother
////////					pNode->pBrother = CreateRegNode(resToken);
////////					pNode->pBrother->pFather = pNode->pFather;
////////					pPrevNode = pNode->pBrother;
////////					pNode = NULL;
////////					*bNewlyCreated = TRUE;
////////				}
////////			}
////////		}
////////		else
////////		{
////////			pPrevNode->pChild = CreateRegNode(resToken);
////////			pPrevNode->pChild->pFather = pPrevNode;
////////			pPrevNode = pPrevNode->pChild;
////////			*bNewlyCreated = TRUE;
////////		}
////////		// Get next token:   
////////	   resToken= strSrc.Tokenize(_T("\\"),curPos);
////////	}
////////	
////////	return pPrevNode;
////////}

//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::InsertKey(LPCTSTR pszFullKeyPath)
{
	if(m_pTree == NULL)
		return NULL;
	
	if(pszFullKeyPath == NULL)
		return NULL;

	PREGNODE pNode = m_pTree, pPrevNode = NULL;

	CString strSrc(pszFullKeyPath), resToken;
	int curPos = 0;

	resToken= strSrc.Tokenize(_T("\\"),curPos);
	while (resToken != "")
	{
		// While there are tokens in "pszFullKeyPath"
		if(pNode)
		{
			if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
			{
				pPrevNode = pNode;
				pNode = pNode->pChild;
			}
			else
			{
				if(pNode->pBrother)
				{
					while(pNode)
					{
						pPrevNode = pNode;
						pNode = pNode->pBrother;
						if(pNode)
						{
							if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
							{
								pPrevNode = pNode;
								pNode = pNode->pChild;
								break;
							}
						}
						else
						{
							//key does not exists, so create it
							pPrevNode->pBrother = CreateRegNode(resToken);
							pPrevNode->pBrother->pFather = pPrevNode->pFather;
							pPrevNode = pPrevNode->pBrother;
							break;
						}
					}
				}
				else
				{
					//no more brothers to match, so create a brother
					pNode->pBrother = CreateRegNode(resToken);
					pNode->pBrother->pFather = pNode->pFather;
					pPrevNode = pNode->pBrother;
					pNode = NULL;
				}
			}
		}
		else
		{
			pPrevNode->pChild = CreateRegNode(resToken);
			pPrevNode->pChild->pFather = pPrevNode;
			pPrevNode = pPrevNode->pChild;
		}
		// Get next token:   
	   resToken= strSrc.Tokenize(_T("\\"),curPos);
	}
	
	return pPrevNode;
}
//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::GetKey(LPCTSTR pszFullKeyPath)
{
	if(m_pTree == NULL)
		return NULL;
	
	if(pszFullKeyPath == NULL)
		return NULL;

	PREGNODE pNode = m_pTree, pPrevNode = NULL;

	CString strSrc(pszFullKeyPath), resToken;
	int curPos = 0;

	resToken= strSrc.Tokenize(_T("\\"),curPos);
	while (resToken != "")
	{
		// While there are tokens in "pszFullKeyPath"
		if(pNode)
		{
			if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
			{
				pPrevNode = pNode;
				pNode = pNode->pChild;
			}
			else
			{
				if(pNode->pBrother)
				{
					while(pNode)
					{
						pPrevNode = pNode;
						pNode = pNode->pBrother;
						if(pNode)
						{
							if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
							{
								pPrevNode = pNode;
								pNode = pNode->pChild;
								break;
							}
						}
						else
						{
							return NULL;
						}
					}
				}
				else
				{
					return NULL;
				}
			}
		}
		else
		{
			return NULL;
		}
		// Get next token:   
	   resToken= strSrc.Tokenize(_T("\\"),curPos);
	}
	
	return pPrevNode;
}

//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::CreateKey(LPCTSTR pszFullKeyPath, eRegWOW6432Type eWOW6432Type)
{
	PREGNODE pKey = InsertKey(pszFullKeyPath);

	if(pKey == NULL)
		return pKey;

	pKey->eWOW6432Type = eWOW6432Type;

	switch (pKey->eOperation)
	{
	case RegExclude:
		//DO NOT LOG
		break;
	case RegNone:
		{
			if(pKey->m_pListRegVals->GetCount() == 0)
			{
				//if there are no values check if there are no sub keys
				//if no then it is created
				if(pKey->pChild == NULL)
					pKey->eOperation = RegCreated;
			}
			else
			{
				//there are values and the key has none operatin
				//so, it was there before that create - do not log it!
			}
		}
		break;
	case RegCreated:
		//the key is already created so we do not have to do anything!
		break;
	case RegSetVal:
		//never should happen
		pKey->eOperation = RegError;
		break;
	case RegDeleted:
		//Do nothing - just show that during the logging session
		//keys are deleted, it does not matter if they are created again!
		break;
	}

	return pKey;
}
//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::MergeCreateKey(LPCTSTR pszFullKeyPath, eRegWOW6432Type eWOW6432Type , eRegOperation eOperation )
{
	PREGNODE pKey = InsertKey(pszFullKeyPath);

	if (pKey == NULL)
		return pKey;

	pKey->eWOW6432Type = eWOW6432Type;

	if (pKey->eOperation == RegNone || pKey->eOperation == RegError)
		pKey->eOperation = eOperation;

	////////switch (pKey->eOperation)
	////////{
	////////case RegExclude:
	////////	//DO NOT LOG
	////////	break;
	////////case RegNone:
	//////////{
	//////////	if (pKey->m_pListRegVals->GetCount() == 0)
	//////////	{
	//////////		//if there are no values check if there are no sub keys
	//////////		//if no then it is created
	//////////		if (pKey->pChild == NULL)
	//////////			pKey->eOperation = RegCreated;
	//////////	}
	//////////	else
	//////////	{
	//////////		//there are values and the key has none operatin
	//////////		//so, it was there before that create - do not log it!
	//////////	}
	//////////}
	////////break;
	////////case RegCreated:
	////////	//the key is already created so we do not have to do anything!
	////////	break;
	////////case RegSetVal:
	////////	//never should happen
	////////	pKey->eOperation = RegError;
	////////	break;
	////////case RegDeleted:
	////////	//Do nothing - just show that during the logging session
	////////	//keys are deleted, it does not matter if they are created again!
	////////	break;
	////////}

	return pKey;
}

//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::DeleteKey(LPCTSTR pszFullKeyPath, eRegWOW6432Type eWOW6432Type)
{
	PREGNODE pKey = InsertKey(pszFullKeyPath);

	if(pKey == NULL)
		return pKey;

	pKey->eWOW6432Type = eWOW6432Type;

	switch (pKey->eOperation)
	{
	case RegExclude:
		//DO NOT LOG
		break;
	case RegNone:
		//There is no previous operation so mark it as deleted
		pKey->eOperation = RegDeleted;
		break;
	case RegCreated:
		//remove it from the tree
		RemoveNodeFromTree(pKey);
		break;
	case RegSetVal:
		//never should happen
		pKey->eOperation = RegError;
		break;
	case RegDeleted:
		//Do nothing - just show that during the logging session keys are deleted
		break;
	}

	return pKey;
}
//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::DeleteKeyVal(LPCTSTR strKeyPath, LPCTSTR strValName, DWORD dwType, BYTE* btData, DWORD dwDataLen, eRegWOW6432Type eWOW6432Type)
{
	PREGNODE pKey = InsertKey(strKeyPath);

	if(pKey == NULL)
		return pKey;

	pKey->eWOW6432Type = eWOW6432Type;

	POSITION pos;
	for (pos = pKey->m_pListRegVals->GetHeadPosition(); pos != NULL;)
	{
		VSRegVal* pRegVal = (VSRegVal*)pKey->m_pListRegVals->GetNext(pos);
		if(StrCmpI(pRegVal->m_strValName,strValName) == 0)
		{
			switch (pRegVal->m_eOperation)
			{
			case RegNone:
			case RegCreated:
				//never should happen
				pRegVal->m_eOperation = RegError;
				break;
			case RegSetVal:
				pRegVal->m_eOperation = RegDeleted;
				break;
			case RegDeleted:
				//Do nothing - it is already deleted
				break;
			}
			//key value has been found and updated so return to base
			return pKey;
		}		
	}
	if(pKey->eOperation == RegExclude)
		return NULL;

	//the value is not found so create one and add it to the tail in list of values
	VSRegVal* pRegVal = new VSRegVal(strValName, 0, NULL, 0, dwType, btData, dwDataLen);
	pRegVal->m_eOperation = RegDeleted;
	pKey->m_pListRegVals->AddTail(pRegVal);

	return pKey;
}

//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::SetKeyVal(LPCTSTR strKeyPath, LPCTSTR strValName, 
						 DWORD dwType, BYTE* btData, DWORD dwDataLen,
						 DWORD dwOldType, BYTE* btOldData, DWORD dwOldDataLen, eRegWOW6432Type eWOW6432Type)
{
	PREGNODE pKey = InsertKey(strKeyPath);

	if(pKey == NULL)
		return pKey;

	pKey->eWOW6432Type = eWOW6432Type;

	POSITION pos;
	for (pos = pKey->m_pListRegVals->GetHeadPosition(); pos != NULL;)
	{
		VSRegVal* pRegVal = (VSRegVal*)pKey->m_pListRegVals->GetNext(pos);
		if(StrCmpI(pRegVal->m_strValName, strValName) == 0)
		{
			switch (pRegVal->m_eOperation)
			{
			case RegNone:
			case RegCreated:
				//never should happen
				pRegVal->m_eOperation = RegError;
				break;
			case RegDeleted:
				//change from deleted to set and change the new data
				pRegVal->m_eOperation = RegSetVal;
			case RegSetVal:
				{
					//if the value exists change the new data but keep the old data
					pRegVal->m_dwNewType = dwType;
					pRegVal->m_NewBytesArray.RemoveAll();
					pRegVal->m_NewBytesArray.SetSize(dwDataLen);
					CopyMemory(pRegVal->m_NewBytesArray.GetData(), btData, dwDataLen);
				}
				break;
			}
			//key value has been found and updated so return to base
			return pKey;
		}		
	}

	if(pKey->eOperation == RegExclude)
		return NULL;

	//the value is not found so create one and add it to the tail in list of values
	VSRegVal* pRegVal = new VSRegVal(strValName, dwType, btData, dwDataLen, dwOldType, btOldData, dwOldDataLen);
	pRegVal->m_eOperation = RegSetVal;
	pKey->m_pListRegVals->AddTail(pRegVal);

	return pKey;
}
//Key path should begin \REGISTRY\... MACHINE or USER
PREGNODE VSRegTree::MergeSetKeyVal(LPCTSTR strKeyPath, LPCTSTR strValName,
	DWORD dwType, BYTE* btData, DWORD dwDataLen,
	DWORD dwOldType, BYTE* btOldData, DWORD dwOldDataLen, eRegWOW6432Type eWOW6432Type,  eRegOperation eOperation )
{
	PREGNODE pKey = InsertKey(strKeyPath);

	if (pKey == NULL)
		return pKey;

	pKey->eWOW6432Type = eWOW6432Type;

	POSITION pos;
	for (pos = pKey->m_pListRegVals->GetHeadPosition(); pos != NULL;)
	{
		VSRegVal* pRegVal = (VSRegVal*)pKey->m_pListRegVals->GetNext(pos);
		if (StrCmpI(pRegVal->m_strValName, strValName) == 0)
		{
			switch (pRegVal->m_eOperation)
			{
			case RegNone:
			case RegCreated:
				//never should happen
				pRegVal->m_eOperation = RegError;
				break;
			case RegDeleted:
				//change from deleted to set and change the new data
				//pRegVal->m_eOperation = RegSetVal;
				break;
			case RegExclude:
				break;
			case RegSetVal:
			{
				//if the value exists change the new data but keep the old data
				pRegVal->m_dwNewType = dwType;
				pRegVal->m_NewBytesArray.RemoveAll();
				pRegVal->m_NewBytesArray.SetSize(dwDataLen);
				CopyMemory(pRegVal->m_NewBytesArray.GetData(), btData, dwDataLen);
			}
			break;
			}
			//key value has been found and updated so return to base
			return pKey;
		}
	}

	//if (pKey->eOperation == RegExclude)
	//	return NULL;

	//the value is not found so create one and add it to the tail in list of values
	VSRegVal* pRegVal = new VSRegVal(strValName, dwType, btData, dwDataLen, dwOldType, btOldData, dwOldDataLen);
	pRegVal->m_eOperation = eOperation;
	pKey->m_pListRegVals->AddTail(pRegVal);

	return pKey;
}

VSRegTree::~VSRegTree()
{
	DeleteRegNode(m_pTree);
	DeleteRegTreeItemsList();
}

void VSRegTree::DeleteRegTreeItemsList()
{
	POSITION pos;
	for (pos = m_listRegItems.GetHeadPosition(); pos != NULL;)
	{
		VSRegTreeItem* pRegTreeItem = (VSRegTreeItem*)m_listRegItems.GetNext(pos);
		delete pRegTreeItem;
	}		
	m_listRegItems.RemoveAll();
}

VSRegTreeItem::~VSRegTreeItem()
{
	//remove only pointers from the list
	//because values are already deleted (while deleting the tree)
	m_ListRegVals.RemoveAll();
}

BOOL VSRegTree::AddExludeKey(LPCTSTR pszFullKeyPath)
{
	if(m_pTree == NULL)
		return NULL;
	
	if(pszFullKeyPath == NULL)
		return NULL;

	PREGNODE pNode = m_pTree, pPrevNode = NULL;

	CString strSrc(pszFullKeyPath), resToken;
	int curPos = 0;

	resToken= strSrc.Tokenize(_T("\\"),curPos);
	while (resToken != "")
	{
		// While there are tokens in "pszFullKeyPath"
		if(pNode)
		{
			if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
			{
				pPrevNode = pNode;
				pNode = pNode->pChild;
			}
			else
			{
				if(pNode->pBrother)
				{
					while(pNode)
					{
						pPrevNode = pNode;
						pNode = pNode->pBrother;
						if(pNode)
						{
							if(StrCmpI(resToken, pNode->pszRegKeyName) == 0)
							{
								pPrevNode = pNode;
								pNode = pNode->pChild;
								break;
							}
						}
						else
						{
							//key does not exists so create it
							pPrevNode->pBrother = CreateRegNode(resToken, KeyNoType, RegExclude);
							pPrevNode->pBrother->pFather = pPrevNode->pFather;
							pPrevNode = pPrevNode->pBrother;
							break;
						}
					}
				}
				else
				{
					pNode->pBrother = CreateRegNode(resToken, KeyNoType, RegExclude);
					pNode->pBrother->pFather = pNode->pFather;
					pPrevNode = pNode->pBrother;
					pNode = NULL;
				}
			}
		}
		else
		{
			pPrevNode->pChild = CreateRegNode(resToken, KeyNoType, RegExclude);
			pPrevNode->pChild->pFather = pPrevNode;
			pPrevNode = pPrevNode->pChild;
		}
		// Get next token:   
	   resToken= strSrc.Tokenize(_T("\\"),curPos);
	}
	
	return TRUE;
}

//Exlude registry key from m_pTree
//Key path should begin HKEY_LOCAL_MACHINE or HKEY_CURENT_USER or key on that level
void VSRegTree::ExcludeRegKey(LPCTSTR pszFullRegPath, BOOL bExcludeSubKeys)
{
	//Tokanize registry path
	RegExcludePath regKeyInfo;
	regKeyInfo.bExcludeSubKeys = bExcludeSubKeys;

	CString resToken, strSource(pszFullRegPath);

	int curPos= 0;

	resToken = strSource.Tokenize(_T("\\"),curPos);
	while (resToken != "")
	{
	   regKeyInfo.arrRegExcludedPath.Add(resToken);
	   resToken = strSource.Tokenize(_T("\\"),curPos);
	}

	//Because key should begin with REGISTRY
	regKeyInfo.arrRegExcludedPath.InsertAt(0, _T("REGISTRY"));
	ExcludeKey(m_pTree, &regKeyInfo);

}

//Key path should begin REGISTRY\... 
void VSRegTree::ExcludeKey(PREGNODE pTreeNode, RegExcludePath* pRegExcludePaths, DWORD dwLevel)
{
	if(pTreeNode == NULL)
		return;

	PREGNODE pNextBrother = NULL;
	PREGNODE pCurrentNode = NULL;
	pNextBrother = pTreeNode->pBrother;

	//Get excluded key name depending on the level we are on
	CString strRegKey = pRegExcludePaths->arrRegExcludedPath.GetAt(dwLevel);
	//does it mach current key name
	if(::PathMatchSpec(pTreeNode->pszRegKeyName, strRegKey))
	{
		//mark as exclude because you cannot exclude only a key  
		//without excuding its parents
		pTreeNode->eOperation = RegExclude;

		//if we are on the last key - the key we look to exclude
		if(dwLevel == pRegExcludePaths->arrRegExcludedPath.GetCount()-1)
		{
			//if we do not exclude sub keys just mark as excluded this key only
			if(pRegExcludePaths->bExcludeSubKeys)
			{
				//we do not want sub keys either so delete the whole key
				RemoveNodeFromTree(pTreeNode);				
			}
		}
		else
		{
			//we are still iterating to find the final reg key to exclude
			dwLevel++;
			ExcludeKey(pTreeNode->pChild, pRegExcludePaths, dwLevel);
			//back from requrse, so decrease the level
			dwLevel--;
		}

		//if it is not a wildcard then do not go through its brothers - just return 
		if((strRegKey.Find(_T('?')) == -1) && (strRegKey.Find(_T('*')) == -1))
			return;
	}
	
	//we didn't find the key as a first child so search its brothers for match
	while(pNextBrother != NULL)
	{
		pCurrentNode = pNextBrother;
		pNextBrother = pNextBrother->pBrother;
		if(::PathMatchSpec(pCurrentNode->pszRegKeyName, strRegKey))
		{
			//mark as exclude because you cannot exclude only a key  
			//without excuding its parents
			pCurrentNode->eOperation = RegExclude;
	
			//match found so check if it is the final key
			if(dwLevel == pRegExcludePaths->arrRegExcludedPath.GetCount()-1)
			{
				if(pRegExcludePaths->bExcludeSubKeys)
				{
					//we do not want sub keys either so delete the whole key
					RemoveNodeFromTree(pCurrentNode);
				}
			}
			else
			{
				//it matches but it is not the final key so keep searching
				dwLevel++;
				ExcludeKey(pCurrentNode->pChild, pRegExcludePaths, dwLevel);
				dwLevel--;
			}
		}
	}
}

//Delete given node and all its sub nodes 
void VSRegTree::DeleteTreeMem(PREGNODE pTreeNode)
{
	if(pTreeNode == NULL)
		return;

	PREGNODE pNextBrother = NULL;
	PREGNODE pCurrentNode = NULL;

	DeleteTreeMem(pTreeNode->pChild);
	
	pNextBrother = pTreeNode->pBrother;
	while(pNextBrother != NULL)
	{
		pCurrentNode = pNextBrother;
		pNextBrother = pNextBrother->pBrother;
		if(pCurrentNode->pChild != NULL)
		{
			DeleteTreeMem(pCurrentNode->pChild);
		}
		if(pCurrentNode->pszRegKeyName)
		{
			delete [] pCurrentNode->pszRegKeyName;
			pCurrentNode->pszRegKeyName = NULL;
		}

		//////////Delete values of the key
		////////POSITION pos;
		////////for (pos = pCurrentNode->m_pListRegVals->GetHeadPosition(); pos != NULL;)
		////////{
		////////	VSRegVal* pRegVal = (VSRegVal*)pCurrentNode->m_pListRegVals->GetNext(pos);
		////////	delete pRegVal;
		////////	pRegVal = NULL;
		////////}
		//////////remove all pointers to values
		if(pCurrentNode->m_pListRegVals)
		{
			pCurrentNode->m_pListRegVals->RemoveAll();
			delete pCurrentNode->m_pListRegVals;
			pCurrentNode->m_pListRegVals = NULL;
		}

		if(pCurrentNode)
		{
			delete pCurrentNode;
			pCurrentNode = NULL;
		}
	}
	
	if(pTreeNode->pszRegKeyName)
	{
		delete [] pTreeNode->pszRegKeyName;
		pTreeNode->pszRegKeyName = NULL;
	}

	if(pTreeNode)
	{
		/////*POSITION pos;
		////for (pos = pTreeNode->m_pListRegVals->GetHeadPosition(); pos != NULL;)
		////{
		////	VSRegVal* pRegVal = (VSRegVal*)pTreeNode->m_pListRegVals->GetNext(pos);
		////	delete pRegVal;
		////	pRegVal = NULL;
		////}		
		pTreeNode->m_pListRegVals->RemoveAll();
		delete pTreeNode->m_pListRegVals;
		pTreeNode->m_pListRegVals = NULL;

		delete pTreeNode;
		pTreeNode = NULL;
	}
}

void VSRegTree::EmptyTree()
{
	DeleteTreeMem(m_pTree);
	m_pTree = CreateRegNode(_T("REGISTRY"));
}
