#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING

#pragma region Constants
// All the different dialog modes
#define MODE_NEWVERSION 1
#define MODE_ADVERT 2
#define MODE_CONTINUE_UNINSTALL 3
#define MODE_INDIE_UNINSTALL 4
#define MODE_INSTALL_TRACED 5
#define MODE_LICENSE_EXPIRED 6
#define MODE_TRIAL_EXPIRED 7

// All the different types of processes
#define PROCESS_IRRELEVANT 0
#define PROCESS_INSTALLER 1
#define PROCESS_UNINSTALLER 2
#define PROCESS_INHERIT 3 // Inherit the type of the parent process
#define PROCESS_SIBLING 4 // Inherit the type of the first sibling process
#pragma endregion


// If defined, programs detected as installers but rejected by the user will be ignored forever.
// May be useful to prevent repetetive popups.
//#define IGNORE_REJECTED_INSTALLERS



#pragma region PRO/FREE
#define UPDATEPRO4XML _T("https://www.revouninstaller.com/updatepro4help.xml")
#define UPDATEFREEXML _T("https://www.revouninstaller.com/updatefreehelp.xml")

#define REG_UNINSTALL_KEY_PRO L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{67579783-0FB7-4F7B-B881-E5BE47C9DBE0}_is1"
#define REG_UNINSTALL_KEY_FREE L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{A28DBDA2-3CC7-4ADC-8BFE-66D7743C6C97}_is1"

#define MUTEX_NAME_PRO "GlobalRevoHelperProMutex"
#define MUTEX_NAME_FREE "GlobalRevoHelperFreeMutex"

#define HELPER_AUTOSTART_TASK_NAME_PRO L"RevoHelperProStartup"
#define HELPER_AUTOSTART_TASK_NAME_FREE L"RevoHelperFreeStartup"

#define RUEXE_PRO "RevoUninPro.exe"
#define RUEXE_FREE "RevoUnin.exe"


#ifdef PRO_ENABLED
#define REG_UNINSTALL_KEY REG_UNINSTALL_KEY_PRO
#define UPDATE_XML_FILE UPDATEPRO4XML
#define RUEXE RUEXE_PRO
#define MUTEX_NAME MUTEX_NAME_PRO
#define HELPER_AUTOSTART_TASK_NAME HELPER_AUTOSTART_TASK_NAME_PRO
/*
	The maximum number of seconds, after which if a traced process which may be an installer,
	is still uncategorized, the tracing will abort
*/
#define INLINE_TRACER_MAXIMUM_UNCERTAINTY_PERIOD 90
#else
#define REG_UNINSTALL_KEY REG_UNINSTALL_KEY_FREE
#define UPDATE_XML_FILE UPDATEFREEXML
#define RUEXE RUEXE_FREE
#define MUTEX_NAME MUTEX_NAME_FREE
#define HELPER_AUTOSTART_TASK_NAME HELPER_AUTOSTART_TASK_NAME_FREE

#endif

#pragma endregion


#define AppRU_P(x) x ## RUTooltip


#ifdef _DEBUG
#include <iostream>
#endif


#pragma region Constants

#define NOTIFICATION_DURATION_INFINITE      -1



#define MINUTE 60
#define HOUR (MINUTE * 60)
#define DAY (HOUR*24)

#ifdef _DEBUG
extern int SUBSCRIPTION_EXPIRY_WARNING_DAYS, UPDATE_NOTIFICATION_INTERVAL, ADVERT_NOTIFICATION_INTERVAL, SUBSCRIPTION_EXPIRY_WARNING_INTERVAL, SECONDARY_NOTIFICATION_DELAY,
MINIMUM_INTER_NOTIFICATION_INTERVAL, PRE_NOTIFICATION_WAIT_PERIOD, NOTIFICATION_DURATION, EXPIRED_LIC_NOTIFICATION_INTERVAL;
#else
#define SUBSCRIPTION_EXPIRY_WARNING_DAYS 5
#define UPDATE_NOTIFICATION_INTERVAL     (DAY * 3)
#define ADVERT_NOTIFICATION_INTERVAL     (DAY * 3)
#define SUBSCRIPTION_EXPIRY_WARNING_INTERVAL (DAY * 2)
#define SECONDARY_NOTIFICATION_DELAY 3600 //seconds
#define MINIMUM_INTER_NOTIFICATION_INTERVAL 3600 // seconds
#define PRE_NOTIFICATION_WAIT_PERIOD        5 // seconds
#define NOTIFICATION_DURATION               10000 //ms
#define EXPIRED_LIC_NOTIFICATION_INTERVAL (DAY*5)
#endif
#pragma endregion

#ifdef PRO_ENABLED
#define USE_PRO_ICON
#endif
