#ifndef _VSFILELOG_H_INCLUDED_
#define _VSFILELOG_H_INCLUDED_
#pragma once

enum eFileOp {FileCreate, FileDelete, FileRename, FileOverwritten, FileError};

class VSFileLog : public CObject
{
public:
	DECLARE_SERIAL(VSFileLog)
	VSFileLog();
	void Serialize(CArchive &ar); 

public:
	//always read version first
	INT			m_nVersion;				

	CString		m_strFileName;
	CString		m_strRenameName;
	eFileOp		m_eOperation;
	INT			m_nIsDirectory;
};

#endif /* _VSFILELOG_H_INCLUDED_ */