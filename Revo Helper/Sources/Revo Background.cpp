
// Revo Background.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ProcessUtils.h"
#include "DialogTask.h"


/*
	Responsible for managing the dialog tasks in one thread, tracing processes in another
*/
class DialogManager
{
	vector<shared_ptr<DialogTask>> Tasks;
	HANDLE hProcTraceThread, hTaskThread;
public:

	void RunTasks()
	{
		int cycles = 3599;
		while (true)
		{
			cycles++;

			// Check for an update every hour
			if (cycles == (3600 * (1000 / NEW_PROCESS_CHECK_FREQ)))
			{
				auto update = CheckUpdate();
				if (update) 
				{
					AddTask(make_shared<DialogTask>(MODE_NEWVERSION));
					Tasks.back()->update_info = update;
				}
				cycles = 0;
			}
			Sleep(1000);
			if (Tasks.empty()) continue;

			for (int i = 0; i < Tasks.size(); i++)
			{
				if (Tasks[i]->CanBeExecuted())
				{
					dlg.WaitTask();
  					dlg.SetTask(Tasks[i]);
					dlg.ShowWindow(SW_SHOW);
					Tasks.erase(Tasks.begin() + i);
					i--;
				}
			}
		}
	}
	DialogManager()
	{
		// Process tracing is only done for installer and uninstaller detection, thus we only need it in pro
#ifdef PRO_ENABLED
		hProcTraceThread = CreateThread(0, 0, [](LPVOID p) -> DWORD { ((DialogManager*)p)->ProcessTrace(); return 0; }, this, 0, 0);
#endif
		hTaskThread = CreateThread(0, 0, [](LPVOID p) -> DWORD {((DialogManager*)p)->RunTasks(); return 0; }, this, 0, 0);
	}
	void AddTask(shared_ptr<DialogTask> task)
	{
		Tasks.push_back(task);
	}
	~DialogManager()
	{
#ifdef PRO_ENABLED
		TerminateThread(hProcTraceThread, 0);
		CloseHandle(hProcTraceThread);
#endif
		TerminateThread(hTaskThread, 0);
		CloseHandle(hTaskThread);
	}
	void ProcessTrace()
	{
		struct ProcessesInfo
		{
			vector<shared_ptr<ProcessInfo> > NewProcesses, OldProcesses;

			void operator()()
			{
				// Clear the NewProcesses structure for another use
				NewProcesses.clear();
				// Set all OldProcesses->bStillAlive flag to false, this flag will be reset to true
				// on GetProcessInfos if the process is still alive, if not, it should be deleted
				for (auto& i : OldProcesses) i->bStillAlive = false;

				// Get new processes in NewProcesses struct, NewProcesses also get added to the
				// OldProcesses struct if, if a process is enumerated, which is already in the 
				// OldProcesses strcut, its current entry will have its bStillAlive flag set to true
				GetProcessInfos(NewProcesses, &OldProcesses);


				// All OldProcesses which have not had their bStillAlive flag set to true should be deleted
				for (int i = 0; i < OldProcesses.size(); i++)
				{
					if (!OldProcesses[i]->bStillAlive)
					{
						OldProcesses.erase(OldProcesses.begin() + i);
						i--;
					}
				}
			}
			ProcessesInfo()
			{

				// Reserve some memory for the process parsing, NewProcesses doesn't really need
				// so much, but it will be reserved anyway since on the first run, all processes are new
				NewProcesses.reserve(256);
				OldProcesses.reserve(256);
				// First run
				(*this)();
			}
		};


		ProcessesInfo pi;
		int ProcessType;
		while (true)
		{
			// Refresh the process entries
			pi();

			shared_ptr<CInstalledAppData> appdata;

			// Check every new entry
			for (auto& i : pi.NewProcesses)
			{
				ProcessType = GetProcessType(i, appdata);
				if (ProcessType == PROCESS_UNINSTALLER)
				{
					auto task = make_shared<DialogTask>(MODE_INDIE_UNINSTALL);
					task->process = i;
					task->appdata = appdata;
					AddTask(task);
				}
				else if (ProcessType == PROCESS_INSTALLER)
				{
					auto task = make_shared<DialogTask>(MODE_INSTALL_TRACED);
					task->process = i;
					AddTask(task);
				}
			}

			// Controls how frequently we check for new processes
			Sleep(NEW_PROCESS_CHECK_FREQ);
		}
	}
};

//
//int CRevoBackgroundApp::RunBackground()
//{
//	CIniData cIni;
//	{
//		CString strSelectedLangIni;
//
//		if (VSGetSettings(CAT_GENERAL, VAL_LANGUAGE_FILE, strSelectedLangIni) == FALSE)//if there is no setting in the Registry load the default
//		{
//			strSelectedLangIni = _T("english.ini");
//			VSSetSettings(CAT_GENERAL, VAL_LANGUAGE_FILE, strSelectedLangIni);
//		}
//
//		CString IniPath = L"lang\\";
//		IniPath += strSelectedLangIni.GetString();
//
//		cIni.InitData(IniPath);
//	}
//
//	DialogManager mgr;
//
//
//	// Loads RevoIcon and places it in the tray area
//	auto hIcon = LoadIconW(MAKEINTRESOURCE(IDI_ICON1));
//	CMyAppTrayIcon icon(&mgr.dlg);
//	icon.CreateIcon(hIcon, 0, RUTooltip);
//	icon.ShowIcon(0);
//	
//#ifdef PRO_ENABLED
//	auto RestartDataFile = GetSysConstant(C_LOCALAPPDATA) / L"VS Revo Group\\Revo Uninstaller Pro\\data\\onrestart.dat";
//	if (fs::exists(RestartDataFile))
//	{
//		auto task = make_shared<DialogTask>(MODE_CONTINUE_UNINSTALL);
//		task->int_unin_info = make_unique<UninstallInfo>();
//		int WizMode;
//		DWORD dwUninMethod;
//		if(DWReadRestartDataFile(RestartDataFile, WizMode, dwUninMethod, 0, task->int_unin_info.get(), WizMode, &cIni))
//			mgr.AddTask(task);
//	}
//#endif
//
//	mgr.dlg.DoModal();
//
//	icon.HideIcon(0);
//	icon.DeleteIcon(0);
//	return IDOK;
//}
//
//
//// CRevoBackgroundApp initialization
//
//BOOL CRevoBackgroundApp::InitInstance()
//{
//	
//	// InitCommonControlsEx() is required on Windows XP if an application
//	// manifest specifies use of ComCtl32.dll version 6 or later to enable
//	// visual styles.  Otherwise, any window creation will fail.
//	INITCOMMONCONTROLSEX InitCtrls;
//	InitCtrls.dwSize = sizeof(InitCtrls);
//	// Set this to include all the common control classes you want to use
//	// in your application.
//	InitCtrls.dwICC = ICC_WIN95_CLASSES;
//	InitCommonControlsEx(&InitCtrls);
//
//	CWinApp::InitInstance();
//
//
//	AfxEnableControlContainer();
//
//	// Create the shell manager, in case the dialog contains
//	// any shell tree view or shell list view controls.
//	CShellManager *pShellManager = new CShellManager;
//
//	// Activate "Windows Native" visual manager for enabling themes in MFC controls
//	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
//
//	// Standard initialization
//	// If you are not using these features and wish to reduce the size
//	// of your final executable, you should remove from the following
//	// the specific initialization routines you do not need
//	// Change the registry key under which our settings are stored
//	// TODO: You should modify this string to be something appropriate
//	// such as the name of your company or organization
//	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
//
//
//
//	//CRevoBackgroundDlg dlg;
//	//m_pMainWnd = &dlg;
//	INT_PTR nResponse = RunBackground();
//	if (nResponse == IDOK)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with OK
//	}
//	else if (nResponse == IDCANCEL)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with Cancel
//	}
//	else if (nResponse == -1)
//	{
//		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
//		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
//	}
//
//	// Delete the shell manager created above.
//	if (pShellManager != NULL)
//	{
//		delete pShellManager;
//	}
//
//#ifndef _AFXDLL
//	ControlBarCleanUp();
//#endif
//
//	// Since the dialog has been closed, return FALSE so that we exit the
//	//  application, rather than start the application's message pump.
//	return FALSE;
//}

