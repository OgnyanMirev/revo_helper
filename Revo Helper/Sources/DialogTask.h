#pragma once
#include "ProcessUtils.h"
#include "LeftoverScannerWrapper.h"
/*
	Contains all information needed by the main dialog for a prompt to the user
*/
struct DialogTask
{
	// Task mode
	int mode;

	// Specify if task should be discareded
	// Only used whenever a leftover scan discovers no leftovers currently
	bool discard = false;

	// UNIN and INST modes
	shared_ptr<ProcessInfo> process;
	// UNIN mode
	shared_ptr<CInstalledAppData> appdata;
	// INST mode
	//unique_ptr<PausedProcess> paused_process;
	// Update mode
	shared_ptr<UpdateInfo> update_info;
	// Interrupted UNIN mode
	unique_ptr<UninstallInfo> int_unin_info;
	// Advert mode
	shared_ptr<Advertisement> advert_info;

	// UNIN mode values for various leftovers
	unsigned int registry_n, files_mb, files_n;
	
	time_t execution_point = 0;
	const wchar_t* SetToNowKey = nullptr;

	unique_ptr<LeftoverScanner> scanner;
	void InitTrackFinder(shared_ptr<CInstalledAppData> appdata, shared_ptr<ProcessInfo> pi);

	DialogTask(int mode);
	/*
		Some tasks cannot be executed immediately, while others need to do extra work
		when they are signalled that the system is ready to execute them.
		This function solves both problems at the same time.
	*/
	bool CanBeExecuted();

	void SetExecutionPoint(time_t);
};
