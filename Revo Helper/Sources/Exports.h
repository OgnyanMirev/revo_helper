#include "stdafx.h"

//enum event_type : byte
//{
//	notification,
//	advertisement
//};
//struct Event
//{
//	std::vector<std::wstring> Messages;
//	union
//	{
//		std::wstring ButtonName;
//		std::wstring LinkURL;
//	};
//	union
//	{
//		std::wstring Caption;
//		std::wstring ImageURL;
//	};
//	event_type type;
//};

#ifdef DLL
#define API __declspec(dllexport) 
extern "C"
{

#else 
#define API
#endif
	API int __cdecl ListenForEvents(char* buffer, char* button_name);
	API void __cdecl ExecuteEvent();
	API void __cdecl NeverShowAgain();
	API void __cdecl InitStrings(char* strOpenRevo, char* strSettings, char* strExit, char* strNeverShowAgain, char* strToolTip);
	API int __cdecl GetMode();
#ifdef DLL
}
#endif