#include"stdafx.h"
#include "DialogTask.h"


DialogTask::DialogTask(int mode) : mode(mode), execution_point(0) {}

void DialogTask::InitTrackFinder(shared_ptr<CInstalledAppData> appdata, shared_ptr<ProcessInfo> pi)
{
	this->process = pi;
	this->scanner = make_unique<LeftoverScanner>(appdata);
}

bool DialogTask::CanBeExecuted()
{
	if (execution_point)
	{
		if (time(0) < execution_point)
			return false;
	}

	// if MODE_INDIE_UNINSTALL only execute when all uninstaller processes are dead
	if (mode == MODE_INDIE_UNINSTALL)
	{
		if (!process->LiveBranch())
		{
			scanner->Scan();
			discard = !scanner->HasLeftovers(files_n, files_mb, registry_n);
			return true;
		}
		else return false;
	}

	if (SetToNowKey) SetToNow(SetToNowKey);

	return true;
}

void DialogTask::SetExecutionPoint(time_t time_point)
{
	execution_point = time_point;
}
