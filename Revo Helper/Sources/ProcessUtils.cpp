#include "stdafx.h"
#include "ProcessUtils.h"
#include "InstallerDetector.h"
#include <Psapi.h>
#include <thread>
#include <TlHelp32.h>
#include <winioctl.h>
#include <winsvc.h>
#include <SetupAPI.h>
#include <queue>
#include <mutex>


//#pragma comment(lib, "wbemuuid.lib")
#pragma comment(lib, "setupapi.lib")




#define SIOCTL_TYPE 40000
#define IOCTL_GETPROCESS CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)



#pragma region ProcessInfo
unordered_map<DWORD, shared_ptr<ProcessInfo>> ProcessInfo::Processes;
unordered_map<DWORD, unordered_map<wstring, vector<ProcessInfo*>>> ProcessInfo::SiblingMap;


void ProcessInfo::ClearState()
{
	return; // More or less allowing a memory leak, it's contained in that new ProcessInfos with old PIDs will overwrite old ProcessInfos, but it uses more memory than strictly necessary.
	ProcessInfo::SiblingMap.clear();
	ProcessInfo::Processes.clear();
	//ProcessInfo::Roots.clear();
}


void CommandLineToExeArgs(wstring const& CommandLine, wstring& Exe, wstring& Args, wstring* Filename = nullptr)
{
	auto Separator = CommandLine.find(L".exe");

	if (Separator == wstring::npos)
		Args = CommandLine;
	else
	{
		Separator = CommandLine.find(' ', Separator);
		if (Separator != wstring::npos)
		{
			Args = CommandLine.substr(1 + Separator);
			Exe = CommandLine.substr(0, Separator);
		}
		else Exe = CommandLine;
	}
	Exe.erase(remove(Exe.begin(), Exe.end(), L'"'), Exe.end());
	while (Args.size() && Args.back() == ' ') Args.pop_back();
	while (Exe.size() && Exe.back() == ' ') Exe.pop_back();


	/*
		I don't know what this is, but some processes seem to feature it and it doesn't
		get recognized by the downstream algorithms
	*/

	if (Exe.find(L"\\??\\") == 0)
		Exe = Exe.substr(4);

	if (Filename)
	{
		*Filename = fs::path(Exe).filename().replace_extension();
	}

	to_lower(Exe);
}

ProcessInfo::ProcessInfo(wstring const& ExePath, wstring& FullCommandLine, DWORD PID, DWORD ParentPID, HANDLE hProcess)
{
	wstring temp;

	CommandLineToExeArgs(FullCommandLine, temp, Args, &Filename);

	if (ExePath.size())
		Executable = ExePath;
	else Executable = temp;


	if (Executable.filename() == L"msiexec.exe") Executable = Executable.filename();

	this->hProcess = hProcess;
	this->ParentPID = ParentPID;
	this->PID = PID;

	if (this->Processes[ParentPID])
	{
		this->Parent = this->Processes[ParentPID];
		// Inherit process type
		if (this->Parent->ProcessType != PROCESS_IRRELEVANT)
			this->ProcessType = PROCESS_INHERIT;
	}

	bValid = true;
}

HANDLE ProcessInfo::GetHandle(DWORD dwDesiredAccess)
{
	// Handle gets preserved between requests
	if (hProcess != INVALID_HANDLE_VALUE) return hProcess;
	hProcess = OpenProcess(dwDesiredAccess, FALSE, PID);
	return hProcess;
}

ProcessInfo::~ProcessInfo()
{
	if (bValid && hProcess != INVALID_HANDLE_VALUE) CloseHandle(hProcess);
}


#ifdef _DEBUG
void PrintChildren(ProcessInfo* pi, string& out, string prefix = "")
{
	out += prefix;

	out += pi->Executable.filename().string() + ' ' + cvt2utf8(pi->Args) + ' ' + to_string(pi->PID) + " <- " + to_string(pi->ParentPID) + '\n';


	prefix += "    ";
	for (auto& i : pi->Children)
		PrintChildren(i.second.get(), out, prefix);
}

void ProcessInfo::PrintChildren()
{
	string p;
	::PrintChildren(this, p);
	//static ofstream ofs("C:\\Temp\\Process Trees.txt", ios::trunc);

	cout << p << endl;
}
#endif

bool ProcessInfo::Alive()
{
	if (!bStillAlive) return false;

	auto hProc = GetHandle();
	if (hProc == INVALID_HANDLE_VALUE)
	{
		bStillAlive = false;
		return false;
	}

	DWORD ExitCode;

	GetExitCodeProcess(hProc, &ExitCode);
	bStillAlive = ExitCode == STILL_ACTIVE;

	return bStillAlive;
}

bool ProcessInfo::ContainsPIDInternal(DWORD const& PID)
{
	if (PID == this->PID) return true;
	for (auto& i : Children) if (i.second->ContainsPID(PID)) return true;
	return false;
}

bool ProcessInfo::ContainsPID(DWORD const& PID)
{
	if (PID == this->PID) return true;
	for (auto& i : Children) if (i.second->ContainsPID(PID)) return true;
	if (Siblings) for (auto& i : *Siblings) if (i->PID != this->PID && i->ContainsPIDInternal(PID)) return true;
	return false;
}

bool ProcessInfo::LiveBranchInternal()
{
	if (Alive()) return true;
	for (auto& i : Children)
		if (i.second->LiveBranch()) return true;
	return false;
}


bool ProcessInfo::LiveBranch()
{
	if (Alive()) return true;
	for (auto& i : Children)
		if (i.second->LiveBranch()) return true;
	if (Siblings) for (auto& i : *Siblings) if (i->LiveBranchInternal()) return true;
	return false;
}


shared_ptr<ProcessInfo> ProcessInfo::Create(wstring const& ExePath, wstring& FullCommandLine, DWORD PID, DWORD ParentPID, HANDLE hProcess)
{
	auto process = shared_ptr<ProcessInfo>(new ProcessInfo(ExePath, FullCommandLine, PID, ParentPID));

	if (SiblingMap[ParentPID][process->Executable.wstring()].size())
	{
		process->Siblings = &SiblingMap[ParentPID][process->Executable.wstring()];
		process->Siblings->push_back(process.get());
		process->SetProcessType(PROCESS_SIBLING);
	}
	else
	{
		for (auto& i : Processes)
		{
			if (i.second && ParentPID == i.second->ParentPID && process->Executable == i.second->Executable)
			{
				SiblingMap[ParentPID][process->Executable] = { process.get(), i.second.get() };
				process->Siblings = &SiblingMap[ParentPID][process->Executable];
				i.second->Siblings = &SiblingMap[ParentPID][process->Executable];
				process->SetProcessType(PROCESS_SIBLING);
			}
		}
	}


	if (process->Parent)
		process->Parent->Children[process->PID] = process;


#ifdef _DEBUG
	static ofstream ofs("C:\\Temp\\TestedProcesses.txt", ios::trunc);

	ofs << ParentPID << ' ' << PID << ' ' << cvt2utf8(FullCommandLine) << endl;

	if (process->Parent)
		ofs << PID << " child of " << process->Parent->PID << endl;

	ofs.flush();
#endif


	Processes[PID] = process;

	return process;
}


void ProcessInfo::SetProcessType(int NewType)
{
	ProcessType = NewType;
	for (auto& i : Children)
		i.second->SetProcessType(NewType);
}

shared_ptr<ProcessInfo> ProcessInfo::GetServicesProcessTree()
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 ProcessEntry;
	memset(&ProcessEntry, 0, sizeof(ProcessEntry));
	ProcessEntry.dwSize = sizeof(ProcessEntry);


	shared_ptr<ProcessInfo> ServicesBase = nullptr;

	static wstring serv_ws = L"services.exe", dummy;

	const static vector<wstring> included_processes = 
	{
		L"svchost.exe",
		L"msiexec.exe",
		L"spoolsv.exe",
		L"rundll32.exe",
	};

	if (Process32First(hSnapshot, &ProcessEntry)) do
	{
		if (ServicesBase)
		{
			if (ServicesBase->ContainsPID(ProcessEntry.th32ParentProcessID))
				for (auto& i : included_processes)
					if (i == ProcessEntry.szExeFile)
					{
						wstring tmp = ProcessEntry.szExeFile;
						Create(dummy, tmp, ProcessEntry.th32ProcessID, ProcessEntry.th32ParentProcessID);
					}
		}
		else if (ProcessEntry.szExeFile == serv_ws)
			ServicesBase = Create(dummy, serv_ws, ProcessEntry.th32ProcessID, ProcessEntry.th32ParentProcessID);
	}
	while (Process32Next(hSnapshot, &ProcessEntry));


	CloseHandle(hSnapshot);

	return ServicesBase;
}

#pragma endregion

#ifdef PRO_ENABLED
#ifdef _DEBUG
void IsInstaller(const shared_ptr<ProcessInfo>& proc)
{
	static ofstream dbgstrm("C:\\Temp\\Detector.txt", ios::trunc);

	if (IsInstallerA(proc))
	{
		dbgstrm << "INSTALLER: " << proc->Executable << ' ' << proc->PID << endl;
		dbgstrm.flush();
	}
	else
	{
		dbgstrm << "NOT INSTALLER: " << proc->Executable << ' ' << proc->PID << endl;
		dbgstrm.flush();
	}
}
#else
#define IsInstaller IsInstallerA
#endif
#endif

void GetProcessType(const shared_ptr<ProcessInfo>& proc, shared_ptr<CInstalledAppData>& Data)
{
	if (proc->ProcessType != PROCESS_IRRELEVANT) return;

	if (!proc->Executable.wstring().size())
	{
		proc->ProcessType = PROCESS_IRRELEVANT;
		return;
	}

	bool not_installer = false;

	// Check if it is an uninstaller
	for (auto& i : *GetCache())
	{
		if (i->UninstallStringPath.wstring().size())
		{
			bool usp_eq_pe = i->UninstallStringPath == proc->Executable,
				usp_eq_pef = i->UninstallStringPath == proc->Executable.filename(),
				usa_eq_pa = i->UninstallStringArguments == proc->Args,
				paf_usp_ne_npos = proc->Args.find(i->UninstallStringPath) != wstring::npos;

			//if (i->UninstallStringPath == L"msiexec.exe") continue;

			if (((usp_eq_pe || usp_eq_pef) && usa_eq_pa) || paf_usp_ne_npos)
			{
				if (DetectedUninstallerFromRevo(i->strAppName))
				{
					proc->ProcessType = PROCESS_IRRELEVANT;
					return;
				}
#ifdef _DEBUG
				wcout << proc->Executable << L' ' << proc->Args << endl;
#endif
				Data = i;
				proc->ProcessType = PROCESS_UNINSTALLER;
				return;
			}
		}

#ifdef PRO_ENABLED
		if (GlobalActivated())
		{
			// If the executable is in a subdirectory of a known program's install location, it's probably not an installer
			if (!not_installer)
			{
				if (IsSubDirectory(proc->Executable, i->strInstallLocation.MakeLower().GetString()))
					not_installer = true;
			}
			if (!not_installer)
				IsInstaller(proc);
		}
#endif
	}
}


shared_ptr<ProcessInfo> KeListenForProcesses()
{
	static struct KernelLink
	{
		HANDLE hDevice, hListenerThread;
		char ReadBuffer[16384];
		wstring dummy;
		queue<shared_ptr<ProcessInfo>> Queue;
		mutex m;
		KernelLink()
		{
			if (!LoadDevice()) return;

			hDevice = CreateFile(L"\\\\.\\RevoDetector", GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			hListenerThread = CreateThread(0, 0, [](PVOID p)->DWORD 
			{
				KernelLink& link = *(KernelLink*)p;
				while (true)
				{
					DWORD dwBytesRead = 0;

					//memset(ReadBuffer, 0, 16384);

					if (DeviceIoControl(link.hDevice, IOCTL_GETPROCESS, NULL, 0, link.ReadBuffer, sizeof(link.ReadBuffer), &dwBytesRead, NULL))
					{
						memset(link.ReadBuffer + dwBytesRead, 0, 8);

						struct Temp
						{
							DWORD PID, ParentPID;
						}*t = (Temp*)link.ReadBuffer;

						if (!t->PID) continue; // return KeListenForProcesses();
						wstring CommandLine = (wchar_t*)(link.ReadBuffer + sizeof(Temp));


						std::lock_guard l(link.m);
						link.Queue.push(ProcessInfo::Create(link.dummy, CommandLine, t->PID, t->ParentPID));
					}
				}
				return 0;
			}, this, 0, 0);
		}
		~KernelLink()
		{
			TerminateThread(hListenerThread,0);
			CloseHandle(hListenerThread);

			UnloadDevice();
			if (hDevice != INVALID_HANDLE_VALUE)
				CloseHandle(hDevice);
		}

	private:

		wstring GetDriverPath()
		{
			return GetWorkingDirectory() / "RevoProcessDetector.inf";
		}

		bool LoadDevice()
		{
			bool ret = true;
			SC_HANDLE sc_manager, rpd_service;

			auto driver_path = GetDriverPath();

			if (!fs::exists(driver_path))
			{
				ErrorMsg(L"Couldn't find driver.");
				return false;
			}
			InstallHinfSectionW(0, 0, (L"DefaultInstall 132 " + driver_path).c_str(), SW_HIDE);

			sc_manager = OpenSCManager(0, 0, SC_MANAGER_ALL_ACCESS);
			rpd_service = OpenService(sc_manager, L"RevoProcessDetector", SERVICE_START);
			if (!StartService(rpd_service, 0, 0))
			{
				auto ec = GetLastError();

				if (ec != ERROR_SERVICE_ALREADY_RUNNING)
				{
					ErrorMsg(L"Error " + to_wstring(ec) + L" when trying to start Process Detector service.");
					ret = false;
				}

			}
			CloseServiceHandle(sc_manager);
			CloseServiceHandle(rpd_service);
			return ret;
		}
		void UnloadDevice()
		{
		}
	} link;

	if (link.hDevice == INVALID_HANDLE_VALUE) return nullptr;

	bool state_not_cleared = true;
	while (link.Queue.empty())
	{
		if (state_not_cleared)
		{
			ProcessInfo::ClearState();
			state_not_cleared = false;
		}
		Sleep(20);
	}
	std::lock_guard l(link.m);
	auto proc = link.Queue.front();
	link.Queue.pop();

	return proc;
}

shared_ptr<ProcessInfo> ListenForProcess()
{
	return KeListenForProcesses();
}
