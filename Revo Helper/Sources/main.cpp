#include "stdafx.h"
#include "ProcessUtils.h"
#include "DialogTask.h"
#include "gui.h"
#ifdef _DEBUG
#include <iostream>
#endif
DWORD NeverShowAgainVar = 0;
shared_ptr<DialogTask> CurrentTask;

#ifdef _DEBUG
#include <sstream>

int SUBSCRIPTION_EXPIRY_WARNING_DAYS, UPDATE_NOTIFICATION_INTERVAL, ADVERT_NOTIFICATION_INTERVAL, SUBSCRIPTION_EXPIRY_WARNING_INTERVAL, SECONDARY_NOTIFICATION_DELAY,
MINIMUM_INTER_NOTIFICATION_INTERVAL, PRE_NOTIFICATION_WAIT_PERIOD, NOTIFICATION_DURATION, EXPIRED_LIC_NOTIFICATION_INTERVAL;

void InitializeGlobalVariables()
{
	ifstream ifs(GetWorkingDirectory() / "config.txt");
	string line, l,r;
	unordered_map<string, int> vals;


	while (getline(ifs, line))
	{
		stringstream ss(line);

		getline(ss, l, '=');
		getline(ss, r, '=');

		vals[l] = stoi(r);
	}

#define init_val(x) x = vals[#x]; cout << "[config] " << #x << " = " << vals[#x] << endl;

	init_val(SUBSCRIPTION_EXPIRY_WARNING_DAYS);
	init_val(UPDATE_NOTIFICATION_INTERVAL);
	init_val(ADVERT_NOTIFICATION_INTERVAL);
	init_val(SUBSCRIPTION_EXPIRY_WARNING_INTERVAL);
	init_val(SECONDARY_NOTIFICATION_DELAY);
	init_val(MINIMUM_INTER_NOTIFICATION_INTERVAL);
	init_val(PRE_NOTIFICATION_WAIT_PERIOD);
	init_val(NOTIFICATION_DURATION);
	init_val(EXPIRED_LIC_NOTIFICATION_INTERVAL);
#undef init_val
}
#endif

void NeverShowAgainEx(int Value)
{
	NeverShowAgainVar |= (1 << Value);
	VSSetSettings(CAT_HELPER, VAL_NEVER_SHOW_AGAIN, NeverShowAgainVar);
}

void NeverShowAgain()
{
	NeverShowAgainEx(CurrentTask->mode);
}


/*
	Mostly deals with the scheduling of events.
	Doesn't strictly need to be a class but it simplifies things.
*/
class event_manager
{
	vector<shared_ptr<DialogTask>> Tasks;
	HANDLE hListenThread;

	vector<time_t> AllocatedTimePoints;

	static bool QNeverShowAgain(int Mode)
	{
		return (NeverShowAgainVar & (1 << Mode));
	}


	void ThListenForProcesses()
	{
		shared_ptr<CInstalledAppData> appdata;

		while (true)
		{
			auto proc = ::ListenForProcess();
			GetProcessType(proc, appdata);
			if (proc->ProcessType == PROCESS_UNINSTALLER)
			{
				auto task = make_shared<DialogTask>(MODE_INDIE_UNINSTALL);
				task->InitTrackFinder(appdata, proc);
				AddTask(task);
			}
		}
	}

	void AddTask(shared_ptr<DialogTask> task)
	{
		if (!(NeverShowAgainVar & (1 << task->mode)))
		{
#ifdef _DEBUG
			string taskmode;

			switch (task->mode)
			{
#define F(x) case x: taskmode = #x; break
				F(MODE_ADVERT);
				F(MODE_CONTINUE_UNINSTALL);
				F(MODE_INDIE_UNINSTALL);
				F(MODE_LICENSE_EXPIRED);
				F(MODE_NEWVERSION);
			}
			cout << "Creating task " << taskmode << " execution point " << task->execution_point << endl;
#undef F
#endif
			Tasks.push_back(task);
		}
	}



	bool IsInRange(time_t a, time_t b, unsigned Range)
	{
		if (abs(a - b) < Range) return true;
		return false;
	}

	time_t AllocateClosestAvailableTimePoint(time_t tp)
	{
		time_t t0 = time(0);

		if (tp <= t0) tp = t0+PRE_NOTIFICATION_WAIT_PERIOD;

		for (int i = 0; i < AllocatedTimePoints.size(); i++)
		{
			//cout << "Time point test for x: " << tp << " y: " << AllocatedTimePoints[i] ;
			if (IsInRange(tp, AllocatedTimePoints[i], MINIMUM_INTER_NOTIFICATION_INTERVAL))
			{
				tp = AllocatedTimePoints[i] + MINIMUM_INTER_NOTIFICATION_INTERVAL;
				//cout << " in range, moving to " << tp << endl;

			}
			//else cout << " not in range." << endl;
		}
		AllocatedTimePoints.push_back(tp);
		return tp;
	}

	bool TaskExists(int TaskId)
	{
		if (QNeverShowAgain(TaskId)) return true;
		for (auto& i : Tasks) if (i->mode == TaskId) return true;
		return false;
	}

	time_t GetLastTaskExecution(const wchar_t* Key)
	{
		QWORD TimePoint = 0;
		VSGetSettings(CAT_HELPER, Key, TimePoint);
		return TimePoint;
	}

	shared_ptr<DialogTask> GetTaskUpdate()
	{
		if (!TaskExists(MODE_NEWVERSION))
		{
			auto target = GetLastTaskExecution(VAL_LAST_UPDATE_NOTIFICATION) + UPDATE_NOTIFICATION_INTERVAL;
			if (target > (time(0) + HOUR * 5)) return nullptr;
			auto best_possible_target = AllocateClosestAvailableTimePoint(target);

			auto update = CheckUpdate();
			if (!update) return nullptr;

			auto task = make_shared<DialogTask>(MODE_NEWVERSION);
			task->SetExecutionPoint(best_possible_target);
			task->update_info = update;
			task->SetToNowKey = update->SetToNowKey;
			return task;
		}
		return nullptr;
	}

	shared_ptr<DialogTask> GetTaskUnfinishedUn()
	{
		if (!GlobalActivated()) return nullptr;
		
		if (!TaskExists(MODE_CONTINUE_UNINSTALL))
		{
			auto RestartDataFile = GetSysConstant(C_LOCALAPPDATA) / L"VS Revo Group\\Revo Uninstaller Pro\\data\\onrestart.dat";
			if (fs::exists(RestartDataFile))
			{
#ifdef _DEBUG
				cout << "onrestart.dat exists." << endl;
#endif
				auto task = make_shared<DialogTask>(MODE_CONTINUE_UNINSTALL);
				task->int_unin_info = make_unique<UninstallInfo>();
				int WizMode;
				DWORD dwUninMethod;
				if (DWReadRestartDataFile(RestartDataFile, WizMode, dwUninMethod, 0, task->int_unin_info.get(), WizMode))
				{

					time_t execution_point = 0;
					CString name;
					if (VSGetSettings(CAT_HELPER, VAL_UNFINISHED_UN_SHOWN, name))
					{
						if (name == task->int_unin_info->m_strAppName)
						{
							/*QWORD UnfinishedUnTimpestamp = 0;
							if (VSGetSettings(CAT_HELPER, VAL_UNFINISHED_UN_DELAYED, UnfinishedUnTimpestamp))
							{
								execution_point = UnfinishedUnTimpestamp + DAY;
								VSDeleteSettings(CAT_HELPER, VAL_UNFINISHED_UN_DELAYED);
							}
							else return nullptr;*/
							return nullptr;
						}
					}
#ifdef _DEBUG
					cout << "unfinished un created." << endl;
#endif
					task->SetExecutionPoint(AllocateClosestAvailableTimePoint(execution_point));
					return task;
				}
			}
		}
		return nullptr;
	}

	shared_ptr<DialogTask> GetTaskAdvert()
	{
		if (!TaskExists(MODE_ADVERT))
		{
			auto advert = GetAdvert();
			if (advert)
			{
				auto task = make_shared<DialogTask>(MODE_ADVERT);

				task->SetExecutionPoint(AllocateClosestAvailableTimePoint(0));

				task->advert_info = advert;
				task->SetToNowKey = advert->SetToNowKey;
				return task;
			}
		}
		return nullptr;
	}

#ifdef PRO_ENABLED
	shared_ptr<DialogTask> GetTaskExpiredTrial()
	{
		if (!TaskExists(MODE_TRIAL_EXPIRED))
		{
			auto exec = GetLastTaskExecution(VAL_LAST_TRIAL_EXPIRED_NOTIFICATION) + SUBSCRIPTION_EXPIRY_WARNING_INTERVAL;


			auto t0 = time(0);
			if (exec < t0)
			{

				auto task = make_shared<DialogTask>(MODE_TRIAL_EXPIRED);
				task->SetExecutionPoint(AllocateClosestAvailableTimePoint(t0));
				task->SetToNowKey = VAL_LAST_TRIAL_EXPIRED_NOTIFICATION;
				return task;
			}
		}

		return nullptr;
	}

	shared_ptr<DialogTask> GetTaskExpiredLic()
	{
		DWORD dummy;
		if (VSGetSettings(CAT_GENERAL, VAL_TRIALEXP, dummy))
		{
			//VSDeleteSettings(CAT_HELPER, VAL_TRIAL_EXPIRED);
			return GetTaskExpiredTrial();
		}
		if (!TaskExists(MODE_LICENSE_EXPIRED))
		{
#ifdef _DEBUG
			cout << "Activation: " << GlobalActivated() << endl;
#endif

			if (!GlobalActivated())
			{
				auto exec = GetLastTaskExecution(VAL_LAST_SUBSCRIPTION_EXPIRED_NOTIFICATION) + SUBSCRIPTION_EXPIRY_WARNING_INTERVAL;
				
				auto t0 = time(0);
				if (exec < t0)
				{

					auto task = make_shared<DialogTask>(MODE_LICENSE_EXPIRED);
					task->SetExecutionPoint(AllocateClosestAvailableTimePoint(t0));
					task->SetToNowKey = VAL_LAST_SUBSCRIPTION_EXPIRED_NOTIFICATION;
					return task;
				}
			}
		}
		return nullptr;
	}
#endif
	void TryCreateTask(shared_ptr<DialogTask> task)
	{
		if (task) AddTask(task);
	}
	void CheckForBasicTasks()
	{
		/*
#ifdef PRO_ENABLED
		if (!GlobalActivated())
		{
			auto task = make_shared<DialogTask>(MODE_LICENSE_EXPIRED);
			Tasks.push_back(task);
		}
		else
		{
			activated = true;
			[&]()
			{
				auto RestartDataFile = GetSysConstant(C_LOCALAPPDATA) / L"VS Revo Group\\Revo Uninstaller Pro\\data\\onrestart.dat";
				if (fs::exists(RestartDataFile))
				{
					auto task = make_shared<DialogTask>(MODE_CONTINUE_UNINSTALL);
					task->int_unin_info = make_unique<UninstallInfo>();
					int WizMode;
					DWORD dwUninMethod;
					if (DWReadRestartDataFile(RestartDataFile, WizMode, dwUninMethod, 0, task->int_unin_info.get(), WizMode))
					{
						CString name;
						if (VSGetSettings(CAT_HELPER, VAL_UNFINISHED_UN_SHOWN, name))
						{
							if (name == task->int_unin_info->m_strAppName)
							{
								QWORD UnfinishedUnTimpestamp = 0;
								if (VSGetSettings(CAT_HELPER, VAL_UNFINISHED_UN_DELAYED, UnfinishedUnTimpestamp))
									task->SetExecutionPoint(UnfinishedUnTimpestamp + DAY);
								else return;
							}
							else VSSetSettings(CAT_HELPER, VAL_UNFINISHED_UN_SHOWN, task->int_unin_info->m_strAppName);

						}
						AddTask(task);
					}
				}
			}();
		}
#endif

		auto update = CheckUpdate();
		if (update)
		{
			auto task = make_shared<DialogTask>(MODE_NEWVERSION);
			if (Tasks.size()) task->SetExecutionPoint(time(0) + SECONDARY_NOTIFICATION_DELAY);
			task->update_info = update;
			task->stn_key = update->stn_key;
			AddTask(task);
		}

		if (!update)
		{
			auto advert = GetAdvert();
			if (advert)
			{
				auto task = make_shared<DialogTask>(MODE_ADVERT);
				task->advert_info = advert;
				task->stn_key = advert->stn_key;
				AddTask(task);
			}
		}
		*/
#ifdef PRO_ENABLED
		TryCreateTask(GetTaskExpiredLic());
#endif
		TryCreateTask(GetTaskUnfinishedUn());
		TryCreateTask(GetTaskUpdate());
		TryCreateTask(GetTaskAdvert());
	}

public:

	event_manager()
	{
		CheckForBasicTasks();

		bool NoDriver = (NeverShowAgainVar & (1 << MODE_INDIE_UNINSTALL)) && (!GlobalActivated() || (NeverShowAgainVar & (1 << MODE_INSTALL_TRACED)));


		/*
			Process listener thread.
		*/
		if (!NoDriver)
			hListenThread = CreateThread(0, 0, [](PVOID p) ->DWORD
		{
			((event_manager*)p)->ThListenForProcesses(); return 0;
		}, this, 0, 0);
	}
	~event_manager()
	{
		if (hListenThread != INVALID_HANDLE_VALUE)
		{
			TerminateThread(hListenThread, 0);
			CloseHandle(hListenThread);
		}
	}
	shared_ptr<DialogTask> GetTask(bool& running)
	{
		int CFTCounter = 0;
		while (running)
		{
#ifdef _DEBUG
			//cout << "Current time: " << time(0) << endl;
#endif
			for (int i = 0; i < Tasks.size(); i++)
			{
				if (Tasks[i]->CanBeExecuted())
				{
					auto task = Tasks[i];
					Tasks.erase(Tasks.begin() + i);
					if (!task->discard) return task;
					i--;
				}
			}

			Sleep(2000);
			if (CFTCounter++ >= 120)
			{
				CheckForBasicTasks();
				CFTCounter = 0;
			}
		}
		return nullptr;
	}
};


void ProcessEvent(notification_callback_data data)
{
	if (data.never_show_again) ::NeverShowAgain();
	
	if (data.execute)
	{
		if (CurrentTask)
		{
			switch (CurrentTask->mode)
			{
				case MODE_NEWVERSION:
					InvokeRevo(L"/update");
					break;
				case MODE_CONTINUE_UNINSTALL:
					InvokeRevo(L"/continue");
					break;
				case MODE_INDIE_UNINSTALL:
					CurrentTask->scanner->CleanLeftovers();
					break;
			}
		}
	}
}

void UpdateSubscriptionDialog(notification_callback_data)
{
	//if (cbd.never_show_again)
	//	NeverShowAgain();
	InvokeRevo(L"/subscription");
}

void OpenBuyNowPage(notification_callback_data)
{
	ShellExecuteA(NULL, "open", "https://www.revouninstaller.com/buy-now-revo-uninstaller-pro/", 0, 0, SW_HIDE);
}
#ifdef _DEBUG
#include <iostream>
void CreateConsole()
{
	if (!AllocConsole())
	{
		// Add some error handling here.
		// You can call GetLastError() to get more info about the error.
		return;
	}

	// std::cout, std::clog, std::cerr, std::cin
	FILE* fDummy;
	freopen_s(&fDummy, "CONOUT$", "w", stdout);
	freopen_s(&fDummy, "CONOUT$", "w", stderr);
	freopen_s(&fDummy, "CONIN$", "r", stdin);
	std::cout.clear();
	std::clog.clear();
	std::cerr.clear();
	std::cin.clear();

	// std::wcout, std::wclog, std::wcerr, std::wcin
	HANDLE hConOut = CreateFile(_T("CONOUT$"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	HANDLE hConIn =  CreateFile(_T("CONIN$"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	SetStdHandle(STD_OUTPUT_HANDLE, hConOut);
	SetStdHandle(STD_ERROR_HANDLE, hConOut);
	SetStdHandle(STD_INPUT_HANDLE, hConIn);
	std::wcout.clear();
	std::wclog.clear();
	std::wcerr.clear();
	std::wcin.clear();
}
#endif


int main()
{
	if (!ShouldRun()) return 0;


#ifdef _DEBUG
	CreateConsole();
	InitializeGlobalVariables();
#endif



	// Basic initialization
	VSGetSettings(CAT_HELPER, VAL_NEVER_SHOW_AGAIN, NeverShowAgainVar);

	check_set_autorun();
	

	bool running = true;
	/*
		Main GUI thread
		Creates a popoup if a task is created.
	*/
	CloseHandle(CreateThread(0, 0, [](PVOID p)->DWORD
	{
		create_tray_icon();
		*((bool*)p) = false;
		return 0;
	}, &running, 0, 0));

	event_manager mgr;



	while (running)
	{
		CurrentTask = mgr.GetTask(running);

		auto& task = CurrentTask;
		if (task)
			switch (task->mode)
			{
				case MODE_ADVERT:
					create_advert(task->advert_info->ImageURL, task->advert_info->LinkURL);
					break;
				case MODE_NEWVERSION:
					create_notification(LangString(strid::new_version_title), LangString(strid::update_button), ProcessEvent,
										{ LangString(strid::current_version_str) + (L": " + task->update_info->CurrentVersion),
										  LangString(strid::new_version_str) + (L": " + task->update_info->NewVersion)
#ifdef PRO_ENABLED
										, LangString(strid::release_date_str) + (L": " + task->update_info->Date) 
#endif
										}, L"", MODE_NEWVERSION);
					break;
				case MODE_CONTINUE_UNINSTALL:
					//create_notification(LangString(strid::unfinished_uninstall_title), LangString(strid::continue_button), ProcessEvent
					//					, { LangString(strid::program_str) + (L": " + wstring(task->int_unin_info->m_strAppName.GetString())) }, L"");
					create_continue_uninstall_dlg( // Continue -> invoke revo to continue
												  []()
					{
						InvokeRevo(L"/continue");
					},
												  []()
					{ // Later -> set delay timestamp - deprecated
			  //VSSetSettings(CAT_HELPER, VAL_UNFINISHED_UN_DELAYED, (QWORD)time(0)); 
					},
						[]()
					{ // Never -> delete onrestart.dat
						error_code ec;
						fs::remove(GetSysConstant(C_LOCALAPPDATA) / L"VS Revo Group\\Revo Uninstaller Pro\\data\\onrestart.dat", ec);
					},

					{
						(LangString(program_str) + L": ") + task->int_unin_info->m_strAppName.GetString()
					}, L"");

					VSSetSettings(CAT_HELPER, VAL_UNFINISHED_UN_SHOWN, task->int_unin_info->m_strAppName);
					break;
					/*case MODE_INSTALL_TRACED:
						create_notification(LangString(strid::untraced_install_title), LangString(strid::trace_button), ProcessEvent,
											{ LangString(strid::program_str) + (L": " + task->process->Executable.filename().wstring()) }, L"", MODE_INSTALL_TRACED);
						break;*/
				case MODE_INDIE_UNINSTALL:
					create_notification(LangString(strid::uninstall_leftovers_title), LangString(strid::clean_button), ProcessEvent,
										{ LangString(strid::program_str) + (L": " + wstring(task->scanner->m_appData->strAppName.GetString())),
										  LangString(strid::leftover_files_str) + (L": " + to_wstring(task->files_mb) + L"MB"),
										  LangString(strid::registry_items_str) + (L": " + wstring(to_wstring(task->registry_n))) }, L"", MODE_INDIE_UNINSTALL);
					break;

#ifdef PRO_ENABLED
				case MODE_LICENSE_EXPIRED:
				{
					CString ExpiredOn;
					wstring exp_date;
					if (VSGetSettings(CAT_HELPER, VAL_EXPIREDON, ExpiredOn))
						exp_date = ExpiredOn.GetString();
					else
					{
						DWORD iDay = 1, iMonth = 1, iYear = 1970;

						if (!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPDAY, iDay) ||
							!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPMONTH, iMonth) ||
							!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPYEAR, iYear)) break;
						exp_date = to_wstring(iDay) + L'/' + to_wstring(iMonth) + L'/' + to_wstring(iYear);
					}


					create_notification(LangString(subscription_expired_title), LangString(update_button),
										UpdateSubscriptionDialog, { LangString(expired_on_str) + L": " + exp_date }, L"", MODE_LICENSE_EXPIRED);

					break;
				}
				case MODE_TRIAL_EXPIRED:
				{
					DWORD iDay = 1, iMonth = 1, iYear = 1970;

					if (!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPDAY, iDay) ||
						!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPMONTH, iMonth) ||
						!VSGetSettings(CAT_GENERAL, VAL_TRIALEXPYEAR, iYear)) break;
					auto exp_date = to_wstring(iDay) + L'/' + to_wstring(iMonth) + L'/' + to_wstring(iYear);

					create_notification(LangString(trial_expired_title), LangString(buy_now_str),
										OpenBuyNowPage, { LangString(expired_on_str) + L": " + exp_date }, L"", MODE_TRIAL_EXPIRED);

					break;
				}
#endif
		}
 	}


	return 0;
}

