#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/picture.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/paint/graphics.hpp>
#include <nana/gui/widgets/checkbox.hpp>
#include <nana/gui/widgets/menu.hpp>
#include <nana/gui/notifier.hpp>
#include <nana/gui/widgets/textbox.hpp>

#include <thread>
#include <vector>
#include <Windows.h>
#include <versionhelpers.h>
#include <Shlobj.h>
#include "language.h"
#include "dirutils.h"

using namespace nana;
using namespace nana::API;
using namespace std;
namespace fs = std::experimental::filesystem::v1;
#include "gui.h"

#define TOOLBAR_SIZE 30
#define SLIDE_PIXEL_INCREMENT 5
#define SLIDE_INCREMENT_TIMER_MS 5
#ifdef PRO_ENABLED
#define HIGHLIGHT_COLOR 0x0078d7
#define HIGHLIGHT_COLOR_R 0
#define HIGHLIGHT_COLOR_G 120
#define HIGHLIGHT_COLOR_B 215
#else
#define HIGHLIGHT_COLOR 0xc8c8c3
#define HIGHLIGHT_COLOR_R 216
#define HIGHLIGHT_COLOR_G 216
#define HIGHLIGHT_COLOR_B 211
#endif
#define DEFAULT_BUTTON_WIDTH 84


void NeverShowAgainEx(int Mode);




nana::color const& get_hl_color()
{
    static struct _
    {
        nana::color c;
        _()
        {
            c = c.from_rgb(HIGHLIGHT_COLOR_R, HIGHLIGHT_COLOR_G, HIGHLIGHT_COLOR_B);
        }
    } c;
    return c.c;

}

paint::image const& get_ru_icon()
{
    static struct _
    {
        paint::image img;
        _()
        {
            wchar_t fn[4096] = { 0 };
            GetModuleFileName(0, fn, 4096);
            img.open(fn);
        }
    } img;
    return img.img;
}

paint::font get_default_font(double size = 16)
{
    return nana::paint::font{ "", size,{ 600 } };
}


/*
    Implements the toolbar at the top of the popup as a reusable module
*/
struct rh_toolbar : public panel<true>
{
    template<typename T>
    rh_toolbar(form& f, nana::paint::image const& p, T OnClose = nullptr) : panel<true>(f, rectangle(0, 0, f.size().width, TOOLBAR_SIZE)),
        btn_close(*this, rectangle(f.size().width - TOOLBAR_SIZE, -2, TOOLBAR_SIZE, TOOLBAR_SIZE+2)),
        prog_name(*this, rectangle(TOOLBAR_SIZE + 10, 0, f.size().width - (TOOLBAR_SIZE * 2), TOOLBAR_SIZE)),
        icon(*this, rectangle(2, 2, TOOLBAR_SIZE - 4, TOOLBAR_SIZE - 4))
    {
        bgcolor(get_hl_color());
        btn_close.transparent(true);
        btn_close.fgcolor(colors::white);
        //if (OnClose != nullptr)
        btn_close.events().click(OnClose);

        btn_close.events().click(API::exit);
        btn_close.typeface(get_default_font(16));
        btn_close.text_align(align::center);
        btn_close.caption("X");
        icon.load(p);
        icon.transparent(true);
        icon.stretchable(true);
        prog_name.caption(LangString(strid::tool_tip_str));
        prog_name.transparent(true);
        prog_name.typeface(get_default_font());
    }
    picture icon;
    label prog_name, btn_close;
};


float scaling_factor = 0;

template<typename T, typename OnCloseFn>
void create_dlg(T extra_stuff, nana::size s = { 350,250 }, unsigned NotificationDuration = NOTIFICATION_DURATION, OnCloseFn OnClose = []() {})
{
    if (scaling_factor == 0)
    {
        auto hLib = LoadLibrary(L"User32.dll");

        if (hLib)
        {
            auto get_dpi_func = (UINT(*)())GetProcAddress(hLib, "GetDpiForSystem");

            if (get_dpi_func)
            {
                SetProcessDPIAware();
                scaling_factor = get_dpi_func();
                scaling_factor /= 100;
            }
        }

        if (scaling_factor == 0)
        {
            HKEY hKey;
            if (RegOpenKey(HKEY_CURRENT_USER, L"Control Panel\\Desktop", &hKey) == ERROR_SUCCESS)
            {
                DWORD sf_data;
                DWORD sf_data_size = sizeof(sf_data);
                DWORD dwType = REG_QWORD;
                if (RegQueryValueExW(hKey, L"LogPixels", NULL, &dwType, (LPBYTE)&sf_data, &sf_data_size) == ERROR_SUCCESS)
                {
                    scaling_factor = sf_data;
                    scaling_factor /= 100;
                }
            }
        }

        if (scaling_factor == 0)
            scaling_factor = 1;
    }




    auto hMonitor = MonitorFromPoint({ 0,0 }, MONITOR_DEFAULTTOPRIMARY);

    MONITORINFO mi;
    memset(&mi, 0, sizeof(mi));
    mi.cbSize = sizeof(mi);
    GetMonitorInfo(hMonitor, &mi);


    rectangle initial(mi.rcWork.right - s.width*scaling_factor, mi.rcWork.bottom, s.width, s.height);

    struct sform : public form
    {
        using form::form;
        void exit()
        {
            API::exit();
        }

    };
    
    sform f(initial, { 0, 0, 1, 0, 0, 0, 0 });

    f.typeface(get_default_font());
    rh_toolbar tbar(f, get_ru_icon(), OnClose);
    panel<true> info_panel(f, rectangle(0, TOOLBAR_SIZE, initial.width, initial.height - TOOLBAR_SIZE));
    info_panel.bgcolor(colors::white);
    extra_stuff(info_panel);
    f.events().key_release([](arg_keyboard k) { if (k.key == keyboard::escape) API::exit(); });

    f.scale(scaling_factor);

    s = f.size();
    auto p = f.pos();
    rectangle target;
    target.x = mi.rcWork.right - s.width;
    target.width = s.width;
    target.height = s.height;
    target.y = mi.rcWork.bottom - target.height;

    struct _ 
    {
        sform* f;
        rectangle* initial, * target;
        bool exited = false;
        unsigned& NotificationDuration;
    } params{ &f, &initial, &target, false, NotificationDuration };


    HANDLE hThread = CreateThread(0, 0, [](PVOID p) ->DWORD
    {
        _* f = (_*)p;
        rectangle original = *f->initial;

        // Slider loop, just move the window on a timer
        while (f->initial->y > f->target->y)
        {
            f->initial->y -= SLIDE_PIXEL_INCREMENT;
            f->f->move(f->initial->x, f->initial->y);
            Sleep(SLIDE_INCREMENT_TIMER_MS);
        }
        // If we overshoot the target due to a bad increment, go back
        if (f->initial->y < f->target->y)
            f->f->move(f->target->x, f->target->y);
        
        if (f->NotificationDuration == NOTIFICATION_DURATION_INFINITE)
        {
            while (f->NotificationDuration == NOTIFICATION_DURATION_INFINITE)
                Sleep(20);
        }
        else Sleep(f->NotificationDuration);

        if (!f->exited)
        {
            while (f->initial->y < original.y)
            {
                f->initial->y += SLIDE_PIXEL_INCREMENT;
                f->f->move(f->initial->x, f->initial->y);
                Sleep(SLIDE_INCREMENT_TIMER_MS);
            }

            f->f->close();
        }
        return 0;
    }, &params, 0, 0);
    f.show();
    exec();
    params.exited = true;
    //WaitForSingleObject(hThread, INFINITE);
    TerminateThread(hThread, 0);
    CloseHandle(hThread);
}


template<typename T>
void create_dlg(T extra_stuff, nana::size s = { 350,250 }, unsigned NotificationDuration = NOTIFICATION_DURATION)
{
    create_dlg(extra_stuff, s, NotificationDuration, []() {});
}

void create_advert(string const& img_link, string& link)
{
    while (link.size() && link.back() == ' ') link.pop_back();
    while (link.size() && link[0] == ' ') link.erase(link.begin()+ 0);

    wchar_t temp_path[4096] = { 0 };
    GetTempPath(4096, temp_path);
    GetTempFileName(temp_path, 0, 0, temp_path);

    fs::path img_file = temp_path;

    img_file.replace_extension(".bmp");

    wstring img_link_ws = wstring(img_link.begin(), img_link.end());

    URLDownloadToFileW(0, img_link_ws.c_str(), img_file.c_str(), 0, 0);

    if (!fs::exists(img_file)) return;
    paint::image img;
    img.open(img_file.wstring());
    auto s = img.size();
    picture* pic;
    create_dlg([&](panel<true>& f)
    {
        pic = new picture(f, rectangle(0, 0, s.width, s.height));
        pic->load(img);
        pic->cursor(nana::cursor::hand);
        pic->events().click([&]() { ShellExecuteA(NULL, "open", link.c_str(), 0, 0, SW_HIDE); API::exit(); });
        pic->stretchable(true);
    }, { s.width, s.height + TOOLBAR_SIZE });
    delete pic;
    img.close();
    fs::remove(img_file);
}


template<typename T>
void scale_font(T& obj, int font_size)
{
    rectangle rect;

    nana::size s = obj.size();
    auto p = obj.pos();

    nana::label l0(obj, rectangle(0, 0, s.width, s.height));
    l0.caption(obj.caption());

    rect.width = s.width;
    rect.height = s.height;
    rect.x = p.x;
    rect.y = p.y;


    while (true)
    {
        l0.typeface(get_default_font(font_size));
        s = l0.measure(0);


        if (s.width >= rect.width && font_size > 1)
            font_size--;
        else break;
    }

    obj.typeface(get_default_font(font_size-1));

}
struct custom_button
{
    nana::panel<true> panel;
    nana::button button;

    custom_button(nana::panel<true>& parent, string caption, rectangle rect, int font_size = 13) : panel(parent, rect), button(parent, rect)
    {
        this->panel.bgcolor(get_hl_color());

        this->button.transparent(true);
        this->button.bgcolor(get_hl_color());
        this->button.typeface(get_default_font(font_size));
        this->button.edge_effects(false);
        this->button.borderless(true);
        this->button.caption(caption);

        scale_font(button, font_size);
    }
private:
};


void create_notification(wstring const& caption, wstring const& button_caption, void(*button_function)(notification_callback_data),
                         vector<wstring> const& msgs, wstring const& img_path, int Mode)
{
    vector<label*> messages;
    checkbox* nsa_cbox;
    picture* pic;
    //button* multi_btn;
    //panel<true>* btn_panel;
    custom_button* button;
    auto img = get_ru_icon();
    create_dlg([&](panel<true>& f)
    {
        label* lbl = new label(f, rectangle(20, 5, f.size().width - 40, 52));
        scale_font(*lbl, 13);
        lbl->caption(caption);
        lbl->text_align(align::center);
        messages.push_back(lbl);

        rectangle labels_start(154, 60, 171, 15);

        for (auto& i : msgs)
        {
            lbl = new label(f, labels_start);
            lbl->caption(i);
            lbl->typeface(get_default_font(9));
            lbl->text_align(align::left);
            labels_start.y += 13 + 15;
            messages.push_back(lbl);
        }

        pic = new picture(f, rectangle(18, 60, 108, 108));
        pic->load(img);
        pic->stretchable(true);
        pic->transparent(true);

        nsa_cbox = new checkbox(f, rectangle(20, 190, 160, 19));
        nsa_cbox->transparent(true);
        nsa_cbox->caption(LangString(strid::never_show_again_str));
        auto bc = cvt2utf8(button_caption);
        button = new custom_button(f, bc, rectangle(255, 177, DEFAULT_BUTTON_WIDTH, 32));
        button->button.events().click([&]() { button_function({ true, false }); API::exit(); });
    });

    if (nsa_cbox->checked()) NeverShowAgainEx(Mode);
    for (auto& i : messages) delete i;
    delete nsa_cbox;
    delete button;
    delete pic;
}



template<typename T>
void assign_text(T* whatever_ptr, string const& utf8)
{
    whatever_ptr->caption(utf8);
}


void create_continue_uninstall_dlg(void(*continue_function)(), void(*later_function)(), void (*never_function)(), vector<wstring> const& msgs, wstring const& img_path)
{
    vector<label*> messages;
    checkbox* nsa_cbox;
    picture* pic;
    //button* multi_btn;
    //panel<true>* btn_panel;
    custom_button* continue_button, *later_button, *never_button;
    auto img = get_ru_icon();
    create_dlg([&](panel<true>& f)
    {
        label* lbl = new label(f, rectangle(20, 5, f.size().width - 40, 32));
        lbl->caption(LangString(unfinished_uninstall_title));
        lbl->typeface(get_default_font(15));
        lbl->text_align(align::center);
        messages.push_back(lbl);

        rectangle labels_start(154, 50, 171, 15);

        for (auto& i : msgs)
        {
            lbl = new label(f, labels_start);
            lbl->caption(i);
            lbl->typeface(get_default_font(9));
            lbl->text_align(align::left);
            labels_start.y += 13 + 15;
            messages.push_back(lbl);
        }

        pic = new picture(f, rectangle(18, 50, 108, 108));
        pic->load(img);
        pic->stretchable(true);
        pic->transparent(true);

        nsa_cbox = new checkbox(f, rectangle(20, 190, 160, 19));
        nsa_cbox->transparent(true);
        nsa_cbox->caption(LangString(strid::never_show_again_str));

        continue_button = new custom_button(f, LangStringUTF8(strid::continue_button), rectangle(245, 177, DEFAULT_BUTTON_WIDTH+10, 32));
        continue_button->button.events().click(continue_function);
        continue_button->button.events().click(API::exit);

        later_button = new custom_button(f, LangStringUTF8(strid::later_button), rectangle(245, 177 - 32 - 3, DEFAULT_BUTTON_WIDTH+10, 32));
        later_button->button.events().click(later_function);
        later_button->button.events().click(API::exit);

        never_button = new custom_button(f, LangStringUTF8(strid::never_button), rectangle(245, 177 - 32 - 3 - 32 - 3, DEFAULT_BUTTON_WIDTH+10, 32));
        never_button->button.events().click(never_function);
        never_button->button.events().click(API::exit);

    });

    if (nsa_cbox->checked()) NeverShowAgainEx(MODE_CONTINUE_UNINSTALL);

    for (auto& i : messages) delete i;
    delete nsa_cbox;
    delete continue_button;
    delete pic;
    delete later_button;
    delete never_button;
}


bool InvokeRevo(wstring const&, HANDLE*h=NULL);
void create_tray_icon()
{
    char fn[4096] = { 0 };
    GetModuleFileNameA(0, fn, 4096);

    form f(rectangle(0,0,0,0), { 0, 0, 1, 0, 0, 0, 0 });
    notifier n(f);
    n.events().mouse_up([](arg_mouse v) {
        static struct _
        {
            nana::menu m;

            void set()
            {
                m.clear();
#define add_item(a,b) m.append(LangStringUTF8(a), b)
                add_item(strid::open_revo_str, [](auto) { InvokeRevo({}); });
                add_item(strid::settings_str, [&](auto)
                {
                    HANDLE hProcess;
                    if (InvokeRevo(L"/settings", &hProcess))
                    {
                        WaitForSingleObject(hProcess, INFINITE);
                        if (LangRefresh())
                        {
                            set();
                        }
                    }
                });
                add_item(strid::exit_str, [](auto) { API::exit_all(); /*ExitProcess(0);*/ });
#undef add_item
            }

            _() { set(); }
        } m;
        if (v.right_button)
            menu_popuper(m.m, v.window_handle, v.pos, mouse::any_button)(v);
    });
    n.events().dbl_click([](arg_mouse) { InvokeRevo({}); });
    n.icon(fn);

    n.text(LangStringUTF8(strid::tool_tip_str)
#ifdef PRO_ENABLED
           + " Pro");
#else
    );
#endif
    f.show();
    exec();
}




void create_tracing_query_dialog(wstring& product_name, wstring const& executable_name, tracing_dialog_data& data)
{
    nana::label* l1;
    custom_button* button;
    nana::textbox* log_name = nullptr;
    nana::checkbox* nsa_checkbox = nullptr;
    thread* t = nullptr;
    //data.abort = true;
    int current_state = 0;

    unsigned NotificationDuration = NOTIFICATION_DURATION_INFINITE;

    /*
        There will be 3 dialogs in total:
        1. Would you like to trace [PROGRAM]?
        2. Tracing [PROGRAM]...
        3. Save log as 
    */
    create_dlg([&](panel<true>& f)
    {
        // Functionality for the first dialog
        l1 = new nana::label(f, rectangle(5, 5, 270, 30));
        l1->caption(LangStringUTF8(strid::would_you_like_to_trace_str) + ' ' + cvt2utf8(executable_name) + '?');
        l1->text_align(nana::align::left, nana::align_v::center);
        //l1->typeface(get_default_font(12));
        nsa_checkbox = new nana::checkbox(f, rectangle(5,35,280,20));
        nsa_checkbox->transparent(true);
        nsa_checkbox->caption(LangStringUTF8(never_show_again_str));
        button = new custom_button(f, LangStringUTF8(strid::trace_button), rectangle(273, 7, 75, 30));



        // This thread continuously checks if the application being traced has exited until the TRACE button is pressed, if the application exits, destroy the dialog.
        t = new thread([&]()
        {
            while (current_state == 0)
            {
                if (data.done)
                {
                    NotificationDuration = 0;
                    data.abort = true;
                    return;
                }
                Sleep(10);
            }
        });


        // TRACE button
        button->button.events().click([&]()
        {
            current_state++;
            // Functionality for the second dialog

            delete nsa_checkbox;
            nsa_checkbox = 0;
            t->join();
            delete t;

            /*
                The current thread will be dstroyed as soon as we clear the triggering button's event chain as this thread is intrinsically tied to its originating event.
                Therefore we jump execution to a new thread and allow this one to exit before clearing the button's event chain.

                I've realized that this can be done simply by switching off of current_state to decide what to do, with the same handler for every state, 
                but at this point I don't want to change already debugged code and risk breaking something, even if it is needlessly complicated.
            */
            t = new thread([&]()
            {
                wstring caption_base = LangString(tracing_str) + (L' ' + executable_name);
                button->button.caption(LangString(stop_button));
                Sleep(10);
                button->button.events().click.clear();

                // STOP TRACING button
                button->button.events().click([&]()
                {
                    if (MessageBox(0, LangString(confirm_stop_trace_str).c_str(), LangString(confirm_str).c_str(), MB_YESNO) == IDYES)
                        data.done = true;
                });


                wstring dots;
                int counter = 0;
                while (!data.done && !data.abort)
                {
                    if (counter == 5)
                    {
                        dots += '.';
                        l1->caption(caption_base + dots);
                        if (dots.size() == 3) dots.clear();
                        counter = 0;
                    }
                    else counter++;

                    Sleep(100);
                }

                if (data.abort)
                    API::exit();
                else if (data.done)
                {
                    current_state++;

                    button->button.caption(LangString(save_button));
                    l1->move({ 3,3,280, 17 });
                    l1->caption(LangStringUTF8(strid::save_log_as_str));

                    log_name = new nana::textbox(f, rectangle(3, 20, 280, 25));
                    log_name->caption(product_name);
                    log_name->multi_lines(false);
                    
                    product_name.clear();

                    button->panel.move(rectangle(285, 20, 60, 25));
                    button->button.move(rectangle(285, 20, 60, 25));
                    button->button.events().click.clear();


                    log_name->scale(scaling_factor);
                    button->button.scale(scaling_factor);
                    button->panel.scale(scaling_factor);
                    l1->scale(scaling_factor);

                    // SAVE button
                    button->button.events().click([&](arg_click const& ev)
                    {
                        product_name = log_name->caption_wstring();

                        if (!product_name.size())
                        {
                            MessageBox(0, LangString(strid::log_name_cannot_be_empty_str).c_str(), LangString(strid::error_str).c_str(), MB_OK);
                            return;
                        }

                        
                        wchar_t* product_name_buf = new wchar_t[product_name.size()+1];
                        product_name_buf[product_name.size()] = 0;
                        memcpy(product_name_buf, product_name.c_str(), product_name.size() * sizeof(wchar_t));

                        int ret = PathCleanupSpec(0, product_name_buf);

                        if (ret & PCS_REMOVEDCHAR || ret & PCS_REPLACEDCHAR)
                        {
                            MessageBox(0, LangString(strid::log_name_contained_invalid_chars_str).c_str(), LangString(strid::error_str).c_str(), MB_OK);
                            log_name->caption(product_name_buf);
                            delete[] product_name_buf;
                            return;
                        }
                        
                        delete[] product_name_buf;
                        API::exit();
                    });
                }
            });

        });
    }, nana::size(350, 90), NotificationDuration, 
        [&](arg_click const& ev)
    {
        static bool mutex = false;
        if (mutex)
        {
            ev.stop_propagation();
            return;
        }
        mutex = true;
        button->button.enabled(false);
        switch (current_state)
        {
            case 0:
            {
                if (MessageBox(0, LangString(confirm_abort_trace_str).c_str(), LangString(confirm_str).c_str(), MB_YESNO) == IDYES)
                    data.abort = true;
                else ev.stop_propagation();
                break;
            }
            case 1:
            {
                if (MessageBox(0, LangString(confirm_abort_trace_str).c_str(), LangString(confirm_str).c_str(), MB_YESNO) == IDYES)
                    data.abort = true;
                else ev.stop_propagation();
                break;
            }
            case 2:
            {
                if (MessageBox(0, LangString(confirm_discard_log_data_str).c_str(), LangString(confirm_str).c_str(), MB_YESNO) == IDYES)
                    data.abort = true;
                else ev.stop_propagation();

                break;
            }

            default:
                data.abort = true;
                break;
        }
        button->button.enabled(true);
        mutex = false;
    });

    if (nsa_checkbox)
    {
        if (nsa_checkbox->checked())
            NeverShowAgainEx(MODE_INSTALL_TRACED);
        delete nsa_checkbox;
    }

    delete l1;
    delete button;
    if (log_name) delete log_name;
    if (t)
    {
        current_state = -1;
        t->join();
        delete t;
    }
}
