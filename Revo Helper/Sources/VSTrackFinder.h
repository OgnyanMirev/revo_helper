#ifndef _VSTRACKFINDER_H_INCLUDED_
#define _VSTRACKFINDER_H_INCLUDED_
#pragma once

#include "VSRegTree.h"
#include "UninstallerUtils.h"


enum eUninstallMode { uninSafeMode, uninModerateMode, uninAdvancedMode };

struct FoundRegData
{
	FoundRegData() { dwDataSize = 0; pbtData = NULL; }
	CString strRootName;
	CString strKeyName;
	CString strFullKeyPath;
	CString strValueName;
	DWORD	dwType;
	BYTE* pbtData;
	DWORD	dwDataSize;
};
typedef CList<FoundRegData*, FoundRegData*> RegDataList;

class CVSTrackFinder
{
private:
	BOOL SearchRegistry(HKEY hKey, LPCTSTR pszKeyName, LPTSTR pszFullRegKeyName, CStringArray& arrToSearchFor, VSRegTree* pResultTree, DWORD* pdwFoundCount,
						CStringArray* parrIgnoredKeys = NULL, BOOL b64Bit = FALSE, int nDepthLevels = -1, int nBreakIfEqualToCount = -1,
						BOOL bKeys = TRUE, BOOL bValues = TRUE, BOOL bData = TRUE, BOOL bMatchCase = TRUE);
	BOOL SearchRegistry(HKEY hKey, LPCTSTR pszKeyName, LPTSTR pszFullRegKeyName, CStringArray& arrToSearchFor, RegDataList* listFoundData, DWORD* pdwFoundCount,
						CStringArray* parrIgnoredKeys, BOOL b64Bit, int nDepthLevels, int nBreakIfEqualToCount,
						BOOL bKeys, BOOL bValues, BOOL bData, BOOL bMatchCase, BOOL bIgnoreParentVals = FALSE);

	BOOL SearchInRegistryPath(HKEY hKey, LPCTSTR pszKeyName, LPTSTR pszFullRegKeyName, LPTSTR pszSearchedRegPath, CStringArray& arrToSearchFor, VSRegTree* pResultTree, DWORD* pdwFoundCount, int nMatchCount,
							  CStringArray* parrIgnoredKeys = NULL, BOOL b64Bit = FALSE, int nDepthLevels = -1, int nBreakIfEqualToCount = -1, BOOL bMatchCase = TRUE);

	BOOL SearchLeftoverFilesFolders(CStringArray* arrSearchPattern, BOOL fMatchInPath = FALSE, CStringArray* arrMatch = NULL);

	BOOL FindStartMenuLeftovers(LPCTSTR pSearchFolder, CStringArray& arrTargetPath, LPCTSTR pCommonFilesFolder, LPCTSTR pProgramFilesFolder, LPCTSTR pCommonFilesFolderX86, LPCTSTR pProgramFilesFolderX86, CStringArray& arrResult);
	BOOL FindFileOrFolder(LPCTSTR pFolder, CStringArray* arrNames, BOOL fFolder, CStringArray* pArray, BOOL fRecursive = TRUE, BOOL bMatchCase = TRUE, int nDepthLevels = -1);

	BOOL IsPatternAllowedForSearch(LPCTSTR szPattern);
	BOOL IsListInString(CStringArray* arrStringList, LPCTSTR pszString, BOOL bMatchCase = FALSE);
	BOOL IsStringInList(LPCTSTR pszString, CStringArray* arrStringList, BOOL bMatchCase = TRUE);
	BOOL GetStartMenuItems(LPCTSTR pFolder, CStringArray* pArray);
	BOOL GetShellLinkTarget(LPCTSTR pszLinkFile, LPTSTR pszTargetPath, DWORD buffSize);
	BOOL SquashGUID(LPCWSTR in, LPWSTR out);
	BOOL UnsquashGUID(LPCWSTR in, LPWSTR out);
	BOOL IsInSharedDlls(LPCTSTR pszPath);
	BOOL IsStringMatchToListDrive(CStringArray* arrStringList, LPCTSTR pszString, int nMatchCounter);

	void InitCommonSearchPatterns();
	void InitMSIInfo();
	BOOL SearchCommonRegPaths();
	BOOL SearchCmnPtrnInRegKeySoftware();
	BOOL SearchMatchAppNameInRegPath();
	BOOL SearchMatchKeyNameInRegPath();
	BOOL SearchInRegKeyClasses();
	BOOL PutFoldersOfMSIAppForSearch();

	BOOL FindMSIGUIDFromProductsKey(CStringArray& arrFolders, LPCTSTR pszAppName = NULL);
	BOOL FindMSIGUIDFromComponentsKey(CStringArray& arrFolders);
	BOOL CheckIfMSIGUIDIsExistProduct(CString& strMSIData);
	BOOL FindMSIUInfoFromUninstallKey(LPCTSTR pszMSI_GUID, UninstallInfo* pInfo);

	int ExtractPureStrings(LPCTSTR pszSource, CStringArray& arrResult);

	void SearchForMSILeftovers();

	void MatchInDrivePath(LPCTSTR pFolder, CStringArray* arrNames, CStringArray* pResultArray,
						  CStringArray* arrIgnoreList, int nMatchCounter, int nDepthLevel, int nBreakCount);

	void FindAllDataInFolder(LPCTSTR pFolder, CStringArray* arrNames, CStringArray* pResultArray,
							 CStringArray* arrIgnoreList, int nDepthLevel, int nBreakCount,
							 BOOL bNoCompare, BOOL bMatchCase, BOOL bFolders, BOOL bFiles);

	static unsigned __cdecl ScanThread(LPVOID pThis)
	{
		((CVSTrackFinder*)pThis)->Scan();

		((CVSTrackFinder*)pThis)->m_pJobsDoneEvent->SetEvent();

		return 0;
	};

	void ConvertMultySzToArray(TCHAR* pszMultySz, DWORD dwSize, CStringArray& arrResult);
	void RemoveLastKeyName(LPTSTR pszPath);
	void GetAllPathsFromString(LPCTSTR pszStringPaths, CStringArray* arrPaths);

	void CheckForPinnedShortcuts(bool ExtendedSearch = false);

	void UninstallModeSafe();
	void UninstallModeModerate();
	void UninstallModeAdvanced();

	void ShellFree(void* p);
	void ExtractMissingFromStartMenu();

	void GetExactFoldersForSearch(CStringArray& arrToPutInto);
	void GetAllTypeFoldersForSearch(CStringArray& arrToPutInto);

	void PutFoundFilesFolders(CStringArray& arrFileFolderPaths);
	void PutFoundFilesFolders(LPCTSTR lpszFileFolderPaths, BOOL bTurnOnProtection = TRUE);

	void Scan();

	BOOL IsFolderAllowedForSearch(LPCTSTR pszFolderName);
	void InitCSIDLs();

	CString GetCurrentUserSID();
	void SearchInHKCUExplorerFileExt(RegDataList* pExtDatalist, eRegWOW6432Type eKeyWOW, REGSAM regPermissions);
	BOOL MarkFoundParentKeyAsCreated(CString& strFullPath, CStringArray* parrIgnore);
	BOOL GetFileNameFromPath(CString& strPath, CString& strRes);
	BOOL AddParentFolderAsFound(CString& strFolderName);
	BOOL AddParentInstallLocAsFound(CString& strFolderName);

	void AddBackSlashToPaths(CStringArray& arrModFolders);
	BOOL NotInTheArray(CStringArray& arrTypeLibs, CString& strTypeLib);
	void CheckIsSpecProgram();
	void AddAdobeFlashData();
	void AddJavaUpdateData();

	void UninstallingWinApp();

	void UsePublisherDataIfSingleApp(CString& strInstallLocation);
	BOOL SameNameinOtherProgramFiles(CString& strOtherProgramFilesDir, CString& strPublisherName);
	void ClearRegTreeFromApps();

	//Parameters
private:
	CEvent* m_pJobsDoneEvent;

	CWinThread* m_thScanThred;

	eUninstallMode		m_uninModerateMode;

	CStringArray		m_arrSearchPattern;
	CStringArray		m_arrStartBefore, m_arrStartAfter;
	CStringArray		m_arrAllStartBefore, m_arrAllStartAfter;

	CStringArray		m_arrSMRemovedTargets;
	CStringArray		m_arrSMAllRemovedTargets;

	CStringArray		m_arrMSIComponentNames;

	CStringArray		m_arrFinalFolders;
	CStringArray		m_arrAllKindFolders;

	CStringArray		m_arrStartMenuFileNames;

	CString				m_strCSIDL_PROGRAM_FILES_COMMON;// C:\Program Files\Common
	CString				m_strCSIDL_WINDOWS;// C:\Windows
	CString				m_strCSIDL_PROGRAM_FILES;// C:\Program Files
	CString				m_strCSIDL_PERSONAL;// My Documents
	CString				m_strCSIDL_PROGRAMS;//Start Menu\Programs
	CString				m_strCSIDL_STARTMENU;// <user name>\Start Menu
	CString				m_strCSIDL_DESKTOPDIRECTORY;// <user name>\Desktop
	//CString				m_strCSIDL_COMMON_DESKTOPDIRECTORY;// All Users\Desktop
	CString				m_strCSIDL_COMMON_STARTMENU;// All Users\Start Menu
	CString				m_strCSIDL_COMMON_PROGRAMS;// All Users\Start Menu\Programs
	CString				m_strCSIDL_COMMON_DESKTOPDIRECTORY;// All Users\Desktop
	//CString				m_strCSIDL_COMMON_STARTMENU;// All Users\Start Menu
	CString				m_strCSIDL_LOCAL_APPDATA;// <user name>\Local Settings\Applicaiton Data (non roaming)
	CString				m_strCSIDL_COMMON_APPDATA;// All Users\Application Data
	CString				m_strCSIDL_APPDATA;// <user name>\Application Data
	CString				m_strCSIDL_COMMON_DOCUMENTS;// All Users\Documents
	CString				m_strCSIDL_CONTROLS;// My Computer\Control Panel
	CString				m_strCSIDL_PROFILE; // USERPROFILE
	CString				m_strCSIDL_COMMON_STARTUP;// All Users\Startup
	CString				m_strCSIDL_STARTUP;// Start Menu\Programs\Startup
	CString				m_strCSIDL_MYMUSIC;// "My Music" folder
	CString				m_strCSIDL_MYVIDEO;// "My Videos" folder
	CString				m_strCSIDL_MYPICTURES;// C:\Program Files\My Pictures
	CString				m_strCSIDL_COMMON_TEMPLATES;// All Users\Templates
	CString				m_strCSIDL_COMMON_MUSIC;// All Users\My Music
	CString				m_strCSIDL_COMMON_PICTURES;// All Users\My Pictures
	CString				m_strCSIDL_COMMON_VIDEO;// All Users\My Video
	CString				m_strCSIDL_RESOURCES;// Resource Direcotry
	CString				m_strCSIDL_COMMON_FAVORITES;// Valid only for Windows NT systems
	CString				m_strCSIDL_FAVORITES;// C:\Documents and Settings\username\Favorites
	//CString				m_strCSIDL_COMMON_APPDATA;// All Users
	CString				m_strCSIDL_PROGRAM_FILESX86;// %ProgramFilesx86%
	CString				m_strCSIDL_PROGRAM_FILES_COMMONX86;// x86 Program Files\Common on RISC
	CString				m_strCSIDL_SYSTEMX86;// x86 system directory on RISC

	BOOL				m_bSpecialProgram;// special programs have special rules for better uninstall like, add additional folders for search.
	BOOL				m_bPopularBrowser; // in case we are unisntalling browser do not do deep sacan for some foders because that finds extenions and modules


public:
	CVSTrackFinder();
	CVSTrackFinder(LPCTSTR pszAppName, LPCTSTR pszInstallLocation,
				   eUninstallMode uMode = uninModerateMode, BOOL b64bit = FALSE);

	~CVSTrackFinder();

	//Basic operation calling sequence
	BOOL FindBuiltInUninstallers(LPCTSTR pszInstallPath, LPCTSTR pszAppName, CPtrList* pBuiltInList);

	DWORD CreateSystemRestorePoint();
	//Scan Start menu before running built-in uninstaller (if any)
	void StartMenuScan(BOOL bBeforeBuiltIn = TRUE);
	//Run here the built-in uninstaller (if any)
	//Call StartMenuScan(FALSE) (if there is a built-in unisntaller)
	//Call StartScan
	CEvent* StartScan(eUninstallMode eMode = uninModerateMode);

	//Helpers
	void StopScan();
	void PutFoldersForSearch(LPCTSTR lpszFolderPath);
	void PutFoldersForSearch(CStringArray& arrFolderPaths);
	void ResetFoldersForSearch()
	{
		m_arrFinalFolders.RemoveAll();
		m_arrAllKindFolders.RemoveAll();
	}
	void ResetSearchPattern() { m_arrSearchPattern.RemoveAll(); }
	int AddSearchPattern(CStringArray& arrPattern);
	int AddSearchPattern(LPCTSTR pszPattern);


	//Parameters
public:
	//input info here
	HKEY				m_hRootKey;

	CString				m_strAppName;
	CString				m_strUninstString;
	CString				m_strKeyName;
	CString				m_strFullKeyPath;
	CString				m_strInstallLocation;
	CString				m_strVersion;
	CString				m_strCompany;
	CString				m_strWebSite;
	CString				m_strComment;
	CString				m_strMSIGUID;
	CString				m_strDispIconFileName;
	CString				m_strDisplayIconPath;

	FILETIME			m_ftDate;

	BOOL				m_bMSI;
	BOOL				m_b64Bit;

	//Results are here
	VSRegTree* m_pTree;
	CStringArray		m_arrFoundFilesFolders;

	CStringArray* m_parrFUProgNames;
	CStringArray* m_parrFUPaths;
	CString				m_strAppNameAlpha;
	BOOL				m_bUninstallingWinApp;
	BOOL				m_bUninstallingEdgeExt;
	//only used when we unisntall Win App
	CString				m_strInternalName;

};

#endif /* _VSTRACKFINDER_H_INCLUDED_ */