
// Portions of code borrowed from Sven Wiegand's
// "CFileVersionInfo - Retrieving a File's full Version Information Resource"
// http://www.codeproject.com/file/fileversioninfo.asp


#include "stdafx.h"
#include "winver.h"
#include <time.h>
#include <sys\types.h>
#include <sys\stat.h>
#include "version.h"

#pragma message("automatic link to VERSION.LIB")
#pragma comment(lib, "version.lib")


///////////////////////////////////////////////////////////////////////////////
// ctor
CVersion::CVersion(LPCTSTR lpszPath)
{
	m_strPath = _T("");

	if (lpszPath && lpszPath[0] != 0)
	{
		m_strPath = lpszPath;
	}
	else
	{
		TCHAR szPathName[MAX_PATH * 2];

		szPathName[0] = _T('\0');
		::GetModuleFileName(AfxGetInstanceHandle(), szPathName, sizeof(szPathName) / sizeof(TCHAR) - 1);
		m_strPath = szPathName;
	}

	m_pData = NULL;
	m_dwHandle = 0;

	for (int i = 0; i < 4; i++)
	{
		m_wFileVersion[i] = 0;
		m_wProductVersion[i] = 0;
	}

	m_dwFileFlags = 0;
	m_dwFileOS = 0;
	m_dwFileType = 0;
	m_dwFileSubtype = 0;
	m_strCompanyName = _T("");
	m_strFileDescription = _T("");
	m_strLegalCopyright = _T("");
	m_strProductName = _T("");
	m_strLangId = _T("");

	m_psaKeys = NULL;;
	m_psaValues = NULL;
	m_psaLanguages = NULL;
}

///////////////////////////////////////////////////////////////////////////////
// dtor
CVersion::~CVersion()
{
	if (m_pData != NULL)
		delete[] m_pData;
	m_pData = NULL;
	if (m_psaKeys)
		delete m_psaKeys;
	m_psaKeys = NULL;
	if (m_psaValues)
		delete m_psaValues;
	m_psaValues = NULL;
	if (m_psaLanguages)
		delete m_psaLanguages;
	m_psaLanguages = NULL;
}

///////////////////////////////////////////////////////////////////////////////
// Init
BOOL CVersion::Init()
{
	DWORD dwHandle;
	DWORD dwSize;
	BOOL rc;
	struct _stat buf;

	// get file stats
	if (_tstat(m_strPath, &buf) == 0)
		m_mtime = buf.st_mtime;
	else
		m_mtime = 0;

	if (m_mtime == 0)
		return FALSE;

	dwSize = ::GetFileVersionInfoSize((LPTSTR)(LPCTSTR)m_strPath, &dwHandle);
	if (dwSize == 0)
		return FALSE;

	m_pData = new BYTE[dwSize + 1];
	ASSERT(m_pData);
	memset(m_pData, 0, dwSize + 1);
	m_dwDataSize = dwSize;

	rc = ::GetFileVersionInfo((LPTSTR)(LPCTSTR)m_strPath, dwHandle, dwSize, m_pData);
	if (!rc)
		return FALSE;

	CString strj;
	Dump(strj, FALSE, &m_psaKeys, &m_psaValues, &m_psaLanguages);

	//#ifdef _DEBUG
	//	int k;
	//	for (k = 0; k < m_psaKeys->GetSize(); k++)
	//	{
	//		TRACE(_T("%2d:  <%s> = <%s>\n"), k, (*m_psaKeys)[k], (*m_psaValues)[k]);
	//	}
	//	for (k = 0; k < m_psaLanguages->GetSize(); k++)
	//	{
	//		TRACE(_T("%2d:  language = <%s>\n"), k, (*m_psaLanguages)[k]);
	//	}
	//#endif

		// get fixed info

	if (GetFixedInfo(m_FixedInfo))
	{
		m_wFileVersion[0] = HIWORD(m_FixedInfo.dwFileVersionMS);
		m_wFileVersion[1] = LOWORD(m_FixedInfo.dwFileVersionMS);
		m_wFileVersion[2] = HIWORD(m_FixedInfo.dwFileVersionLS);
		m_wFileVersion[3] = LOWORD(m_FixedInfo.dwFileVersionLS);

		m_wProductVersion[0] = HIWORD(m_FixedInfo.dwProductVersionMS);
		m_wProductVersion[1] = LOWORD(m_FixedInfo.dwProductVersionMS);
		m_wProductVersion[2] = HIWORD(m_FixedInfo.dwProductVersionLS);
		m_wProductVersion[3] = LOWORD(m_FixedInfo.dwProductVersionLS);

		m_dwFileFlags = m_FixedInfo.dwFileFlags;
		m_dwFileOS = m_FixedInfo.dwFileOS;
		m_dwFileType = m_FixedInfo.dwFileType;
		m_dwFileSubtype = m_FixedInfo.dwFileSubtype;
	}
	else
	{
		return FALSE;
	}

	CString strl;
	GetLanguageId(strl);

	// get string info

	GetStringInfo(_T("CompanyName"), m_strCompanyName);
	GetStringInfo(_T("FileDescription"), m_strFileDescription);
	GetStringInfo(_T("LegalCopyright"), m_strLegalCopyright);
	GetStringInfo(_T("ProductName"), m_strProductName);

	TRACE(_T("m_strFileDescription=%s\n"), m_strFileDescription);

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetTimeStamp
time_t CVersion::GetTimeStamp()
{
	return m_mtime;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileVersion
BOOL CVersion::GetFileVersion(CString& strFileVer)
{
	strFileVer.Format(_T("%u.%u.%u.%u"),
					  m_wFileVersion[0],
					  m_wFileVersion[1],
					  m_wFileVersion[2],
					  m_wFileVersion[3]);
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileVersion
BOOL CVersion::GetFileVersion(WORD* pwVersion1,
							  WORD* pwVersion2,
							  WORD* pwVersion3,
							  WORD* pwVersion4)
{
	*pwVersion1 = m_wFileVersion[0];
	*pwVersion2 = m_wFileVersion[1];
	*pwVersion3 = m_wFileVersion[2];
	*pwVersion4 = m_wFileVersion[3];
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetProductVersion
BOOL CVersion::GetProductVersion(CString& strFileVer)
{
	strFileVer.Format(_T("%u.%u.%u.%u"),
					  m_wProductVersion[0],
					  m_wProductVersion[1],
					  m_wProductVersion[2],
					  m_wProductVersion[3]);
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetProductVersion
BOOL CVersion::GetProductVersion(WORD* pwVersion1,
								 WORD* pwVersion2,
								 WORD* pwVersion3,
								 WORD* pwVersion4)
{
	*pwVersion1 = m_wProductVersion[0];
	*pwVersion2 = m_wProductVersion[1];
	*pwVersion3 = m_wProductVersion[2];
	*pwVersion4 = m_wProductVersion[3];
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileFlags
BOOL CVersion::GetFileFlags(DWORD& rdwFlags)
{
	rdwFlags = m_dwFileFlags;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileOS
BOOL CVersion::GetFileOS(DWORD& rdwOS)
{
	rdwOS = m_dwFileOS;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileType
BOOL CVersion::GetFileType(DWORD& rdwType)
{
	rdwType = m_dwFileType;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileSubtype
BOOL CVersion::GetFileSubtype(DWORD& rdwType)
{
	rdwType = m_dwFileSubtype;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetCompanyName
BOOL CVersion::GetCompanyName(CString& rCompanyName)
{
	rCompanyName = m_strCompanyName;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFileDescription
BOOL CVersion::GetFileDescription(CString& rFileDescription)
{
	rFileDescription = m_strFileDescription;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetLegalCopyright
BOOL CVersion::GetLegalCopyright(CString& rLegalCopyright)
{
	rLegalCopyright = m_strLegalCopyright;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetProductName
BOOL CVersion::GetProductName(CString& rProductName)
{
	rProductName = m_strProductName;
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetFixedInfo
BOOL CVersion::GetFixedInfo(VS_FIXEDFILEINFO& rFixedInfo)
{
	BOOL rc;
	UINT nLength;
	VS_FIXEDFILEINFO* pFixedInfo = NULL;

	memset(&rFixedInfo, 0, sizeof(VS_FIXEDFILEINFO));

	if (!m_pData)
		return FALSE;

	rc = ::VerQueryValue(m_pData, _T("\\"), (void**)&pFixedInfo, &nLength);

	if (rc && pFixedInfo)
		memcpy(&rFixedInfo, pFixedInfo, sizeof(VS_FIXEDFILEINFO));

	return rc;
}

///////////////////////////////////////////////////////////////////////////////
// GetTranslationId
BOOL CVersion::GetTranslationId(LPVOID lpData, UINT unBlockSize, WORD wLangId,
								DWORD& dwId, BOOL bPrimaryEnough/*= FALSE*/)
{
	for (LPWORD lpwData = (LPWORD)lpData;
		 (LPBYTE)lpwData < ((LPBYTE)lpData) + unBlockSize;
		 lpwData += 2)
	{
		if (*lpwData == wLangId)
		{
			dwId = *((DWORD*)lpwData);
			return TRUE;
		}
	}

	if (!bPrimaryEnough)
		return FALSE;

	for (LPWORD lpwData = (LPWORD)lpData;
		 (LPBYTE)lpwData < ((LPBYTE)lpData) + unBlockSize;
		 lpwData += 2)
	{
		if (((*lpwData) & 0x00FF) == (wLangId & 0x00FF))
		{
			dwId = *((DWORD*)lpwData);
			return TRUE;
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////
// GetLanguageId
BOOL CVersion::GetLanguageId(CString& rValue)
{
	TRACE(_T("in CVersion::GetLanguageId\n"));

	BOOL rc = TRUE;
	LPVOID lpInfo;
	UINT nInfoLen;

	rValue = _T("");

	if (!m_pData)
		return FALSE;

	m_dwLangId = 0;

	// find best matching language and codepage
	if (::VerQueryValue(m_pData, _T("\\VarFileInfo\\Translation"), &lpInfo,
						&nInfoLen))
	{
		// 1. Check if version information is available that matches the
		//    user's main and sub language.
		if (!GetTranslationId(lpInfo, nInfoLen, GetUserDefaultLangID(),
							  m_dwLangId, FALSE))
		{
			// 2. Check if version information is available that matches the
			//    user's main language.
			if (!GetTranslationId(lpInfo, nInfoLen, GetUserDefaultLangID(),
								  m_dwLangId, TRUE))
			{
				// 3. Check if version information marked as "neutral"
				//    (LANG_NEUTRAL) is available.
				if (!GetTranslationId(lpInfo, nInfoLen, MAKELANGID(LANG_NEUTRAL,
																   SUBLANG_NEUTRAL), m_dwLangId, TRUE))
				{
					// 4. Check if version information in english language
					//    (LANG_ENGLISH) is available.
					if (!GetTranslationId(lpInfo, nInfoLen, MAKELANGID(LANG_ENGLISH,
																	   SUBLANG_NEUTRAL), m_dwLangId, TRUE))
					{
						// 5. use the first language from the language list
						TRACE(_T("using first language from language list\n"));
						m_dwLangId = *((DWORD*)lpInfo);
					}
					else
					{
						TRACE(_T("using LANG_ENGLISH version info\n"));
					}
				}
				else
				{
					TRACE(_T("using LANG_NEUTRAL version info\n"));
				}
			}
			else
			{
				TRACE(_T("using version info for user's main language\n"));
			}
		}
		else
		{
			TRACE(_T("using version info for user's main and sub language\n"));
		}
	}
	else
	{
		TRACE(_T("ERROR: VerQueryValue failed\n"));
	}

	// hex must be caps to match language strings from Dump()
	m_strLangId.Format(_T("%04X%04X"), LOWORD(m_dwLangId), HIWORD(m_dwLangId));
	rValue = m_strLangId;

	// do some sanity checks - if either FileVersion or FileDescription
	// is not available, use language from first StringFileInfo structure

	CString str;
	if (!GetStringInfo(_T("FileVersion"), str) ||
		!GetStringInfo(_T("FileDescription"), str))
	{
		Dump(rValue, TRUE);
		m_dwLangId = _tcstoul(rValue, NULL, 16);
		WORD w1 = LOWORD(m_dwLangId);
		WORD w2 = HIWORD(m_dwLangId);
		m_dwLangId = (w1 << 16) + w2;

		// hex must be caps to match language strings from Dump()
		m_strLangId.Format(_T("%04X%04X"), LOWORD(m_dwLangId), HIWORD(m_dwLangId));
	}

	TRACE(_T("m_strLangId=%s\n"), m_strLangId);

	return rc;
}

///////////////////////////////////////////////////////////////////////////////
// GetLanguageName
BOOL CVersion::GetLanguageName(CString& rValue)
{
	rValue = _T("");

	if (!m_pData)
		return FALSE;

	TCHAR lang[_MAX_PATH];
	::VerLanguageName(m_dwLangId & 0xFFFF, lang, sizeof(lang) / sizeof(TCHAR) - 1);
	lang[sizeof(lang) / sizeof(TCHAR) - 1] = 0;
	rValue = lang;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetStringInfo
BOOL CVersion::GetStringInfo(LPCTSTR lpszKey, CString& rValue)
{
	UINT nLength;
	LPTSTR lpszValue = NULL;

	rValue = _T("");

	if (!m_pData)
		return FALSE;

	ASSERT(!m_strLangId.IsEmpty());

	CString Key;
	Key.Format(_T("\\StringFileInfo\\%s\\%s"), m_strLangId, lpszKey);

	TRACE(_T("using language key <%s>\n"), Key);

	TCHAR* pszKey = new TCHAR[Key.GetLength() + 100];
	_tcscpy(pszKey, Key);

	BOOL rc = ::VerQueryValue(m_pData, pszKey, (void**)&lpszValue, &nLength);

	if (rc)
	{
		if (lpszValue)
		{
			TRACE(_T("lpszValue=<%s>\n"), lpszValue);
			rValue = lpszValue;
		}
	}
	else
	{
		TRACE(_T("ERROR  no value found for key <%s>\n"), Key);
	}

	if (pszKey)
		delete[] pszKey;

	return rc;
}

///////////////////////////////////////////////////////////////////////////////
// GetStringInfoEx
BOOL CVersion::GetStringInfoEx(int nIndex, CString& strKey, CString& strValue)
{
	strKey = _T("");
	strValue = _T("");

	if (m_psaKeys && (nIndex < m_psaKeys->GetSize()))
	{
		strKey = (*m_psaKeys)[nIndex];
		strValue = (*m_psaValues)[nIndex];
	}
	else
		return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// GetStringInfoEx
INT_PTR  CVersion::GetStringInfoEx()
{
	INT_PTR n = 0;

	if (m_psaKeys)
		n = m_psaKeys->GetSize();

	return n;
}

///////////////////////////////////////////////////////////////////////////////
// WideCharToString

CString WideCharToString(void* pszW)
{
	CString str = _T("");

#ifdef _UNICODE
	str = (LPTSTR)pszW;
	return str;
#endif

	size_t cbAnsi, cCharacters;

	// if input is null then just return the same.
	if (pszW == NULL)
	{
		return _T("");
	}

	cCharacters = wcslen((LPCOLESTR)pszW) + 1;

	// Determine number of bytes to be allocated for ANSI string.
	// An ANSI string can have at most 2 bytes per character
	// (for Double Byte Character Strings)
	cbAnsi = cCharacters * 2;

	TCHAR* MultiByteStr = new TCHAR[cbAnsi * 2];	// buffer for new string
	if (!MultiByteStr)
	{
		ASSERT(FALSE);
		return _T("");
	}

	// convert to ANSI.
	if (!WideCharToMultiByte(CP_ACP, 0, (LPCOLESTR)pszW, (INT)cCharacters,
							 (LPSTR)MultiByteStr, (INT)cbAnsi, NULL, NULL))
		str = _T("Error");
	else
		str = MultiByteStr;

	if (MultiByteStr)
		delete[] MultiByteStr;

	return str;
}

///////////////////////////////////////////////////////////////////////////////
// Dump

void CVersion::Dump(CString& rFirstLangId,
					BOOL bFirstLangIdOnly,
					CStringArray** ppsaKeys,
					CStringArray** ppsaValues,
					CStringArray** ppsaLanguages)
{

	TRACE(_T("in CVersion::Dump\n"));

	rFirstLangId = _T("");

	if (!m_pData)
		return;

	if (!bFirstLangIdOnly)
	{
		if (ppsaKeys)
			*ppsaKeys = new CStringArray;
		if (ppsaValues)
			*ppsaValues = new CStringArray;
		if (ppsaLanguages)
			*ppsaLanguages = new CStringArray;
	}

	WORD* wBuf = (WORD*)m_pData;
	DWORD dwVersionInfoLength = wBuf[0];	// wLength
	WORD wVersionInfoValueLength = wBuf[1];	// wValueLength
	TRACE(_T("dwVersionInfoLength=0x%X\n"), dwVersionInfoLength);
	TRACE(_T("m_dwDataSize=0x%X\n"), m_dwDataSize);

	if (dwVersionInfoLength > 50000)
	{
		ASSERT(FALSE);
		return;
	}

	CString strVersionInfoKey;
	DWORD dwKeyLen;
	BOOL bUnicode = FALSE;
	DWORD dwWordOffset = 2;
	DWORD dwByteOffset = 4;

	// first check if this is unicode (NT, WIn2000, XP)
	if (m_pData[6] == 'V')
	{
		bUnicode = TRUE;
		dwWordOffset = 3;
		dwByteOffset = 6;
		TRACE(_T("is Unicode\n"));
		//strVersionInfoKey = (LPTSTR)&m_pData[dwByteOffset];
		strVersionInfoKey = WideCharToString(&wBuf[dwWordOffset]);
		dwKeyLen = strVersionInfoKey.GetLength() + 1; //wcslen((LPCOLESTR)&wBuf[dwWordOffset]);// + 1;
		dwKeyLen *= 2;
	}
	else
	{
		strVersionInfoKey = (LPTSTR)&m_pData[dwByteOffset];
		dwKeyLen = strVersionInfoKey.GetLength() + 1; //strlen((LPCTSTR)&wBuf[dwWordOffset]);// + 1;
	}

	TRACE(_T("VS_VERSION_INFO=<%s> at 0x%X\n"), strVersionInfoKey, dwWordOffset);

	// make sure string is VS_VERSION_INFO
	if (strVersionInfoKey != _T("VS_VERSION_INFO"))
	{
		ASSERT(FALSE);
		return;
	}

	TRACE(_T("dwKeyLen=%u  wVersionInfoValueLength=%u\n"), dwKeyLen, wVersionInfoValueLength);

	DWORD dwPad = 3;	// set pad to 3 to align to 4-byte boundary (aka 32-bit alignment)
	dwByteOffset += dwKeyLen + dwPad;
	dwByteOffset &= ~dwPad;
	TRACE(_T("after key length added:  dwByteOffset=0x%X\n"), dwByteOffset);
	dwByteOffset += wVersionInfoValueLength + dwPad;
	dwByteOffset &= ~dwPad;
	TRACE(_T("after value length added:  dwByteOffset=0x%X\n"), dwByteOffset);

	dwWordOffset = dwByteOffset / 2;

	int nFileInfo = 100;

	// loop to read StringFileInfo and VarFileInfo structures
	while ((dwByteOffset < dwVersionInfoLength) && (nFileInfo > 0))
	{
		WORD wFileInfoLength = wBuf[dwWordOffset];	// size of StringFileInfo or VarFileInfo block
		DWORD dwFileInfoEnd = dwByteOffset + wFileInfoLength;
		TRACE(_T("dwFileInfoEnd=0x%X\n"), dwFileInfoEnd);

		if (wFileInfoLength > 40000)
		{
			ASSERT(FALSE);
			return;
		}

		CString strEntry;
		strEntry = _T("");

		if (bUnicode)
		{
			//strEntry = (LPTSTR)&m_pData[dwByteOffset+6];
			strEntry = WideCharToString(&wBuf[dwWordOffset + 3]);
			dwKeyLen = strEntry.GetLength() + 1;
			dwKeyLen *= 2;
			dwByteOffset += 6;
		}
		else
		{
			strEntry = (LPTSTR)&m_pData[dwByteOffset + 4];
			dwKeyLen = strEntry.GetLength() + 1;
			dwByteOffset += 4;
		}

		TRACE(_T("dwKeyLen=%d\n"), dwKeyLen);
		TRACE(_T("===== <%s> at 0x%X =====\n"), strEntry, dwByteOffset);

		dwByteOffset += dwKeyLen + dwPad;
		dwByteOffset &= ~dwPad;
		dwWordOffset = dwByteOffset / 2;

		///////////////////////////////////////////////////////////////////////
		if (strEntry == _T("StringFileInfo"))
		{
			int nStringTable = 200;
			while ((dwByteOffset < dwFileInfoEnd) && (nStringTable > 0))
			{
				TRACE(_T("===== StringTable at 0x%X =====\n"), dwByteOffset);

				WORD wStringTableLength = wBuf[dwWordOffset];	// StringTable wLength
				DWORD dwStringTableEnd = dwByteOffset + wStringTableLength;

				if (wStringTableLength > 30000)
				{
					ASSERT(FALSE);
					return;
				}

				CString strLanguage;
				strLanguage = _T("");

				if (bUnicode)
				{
					//strLanguage = (LPTSTR)&m_pData[dwByteOffset+6];
					strLanguage = WideCharToString(&wBuf[dwWordOffset + 3]);
					dwKeyLen = strLanguage.GetLength() + 1;
					dwKeyLen *= 2;
					dwByteOffset += 6;
				}
				else
				{
					strLanguage = (LPTSTR)&m_pData[dwByteOffset + 4];
					dwKeyLen = strLanguage.GetLength() + 1;
					dwByteOffset += 4;
				}

				TRACE(_T("dwKeyLen=%d\n"), dwKeyLen);
				strLanguage.MakeUpper();	// make hex all caps
				TRACE(_T("language=<%s> at 0x%X\n"), strLanguage, dwByteOffset);

				rFirstLangId = strLanguage;
				if (bFirstLangIdOnly)
				{
					TRACE(_T("rFirstLangId=<%s>\n"), rFirstLangId);
					TRACE(_T("===== exiting Dump =====\n"));
					return;
				}

				TRACE(_T("1: dwByteOffset=0x%X\n"), dwByteOffset);
				dwByteOffset += dwKeyLen + dwPad;
				TRACE(_T("2: dwByteOffset=0x%X\n"), dwByteOffset);
				dwByteOffset &= ~dwPad;
				TRACE(_T("3: dwByteOffset=0x%X\n"), dwByteOffset);
				dwWordOffset = dwByteOffset / 2;

				TRACE(_T("dumping String: dwStringTableEnd=0x%X\n"), dwStringTableEnd);

				int nStringCount = 500;
				while ((dwByteOffset < dwStringTableEnd) && (nStringCount > 0))
				{
					TRACE(_T("string at 0x%X\n"), dwByteOffset);

					WORD wStringLength = wBuf[dwWordOffset];	// String wLength
					DWORD dwStringEnd = dwByteOffset + wStringLength;
					TRACE(_T("dwStringEnd=0x%X\n"), dwStringEnd);

					if (wStringLength > 10000)
					{
						ASSERT(FALSE);
						return;
					}

					CString strKey;
					strKey = _T("");

					if (bUnicode)
					{
						//strKey = (LPTSTR)&m_pData[dwByteOffset+6];
						strKey = WideCharToString(&wBuf[dwWordOffset + 3]);
						dwKeyLen = strKey.GetLength() + 1;//wcslen((LPCOLESTR)&wBuf[dwByteOffset+6]) + 1;
						dwKeyLen *= 2;
						dwByteOffset += 6;
					}
					else
					{
						strKey = (LPTSTR)&m_pData[dwByteOffset + 4];
						dwKeyLen = strKey.GetLength() + 1; //strlen((LPCTSTR)&wBuf[dwByteOffset+4]) + 1;
						dwByteOffset += 4;
					}

					TRACE(_T("dwKeyLen=%d\n"), dwKeyLen);
					TRACE(_T("key=<%s> at 0x%X\n"), strKey, dwByteOffset);

					TRACE(_T("4: dwByteOffset=0x%X\n"), dwByteOffset);
					dwByteOffset += dwKeyLen + dwPad;
					TRACE(_T("5: dwByteOffset=0x%X\n"), dwByteOffset);
					dwByteOffset &= ~dwPad;
					TRACE(_T("6: dwByteOffset=0x%X\n"), dwByteOffset);
					dwWordOffset = dwByteOffset / 2;

					CString strValue;
					strValue = _T("");

					if (dwByteOffset < dwStringEnd)
					{
						if (m_pData[dwByteOffset] != 0)
						{
							if (bUnicode)
							{
								//strValue = (LPTSTR)&m_pData[dwByteOffset];
								strValue = WideCharToString(&wBuf[dwWordOffset]);
								dwKeyLen = strValue.GetLength() + 1; //wcslen((LPCOLESTR)&wBuf[dwByteOffset+6]) + 1;
								dwKeyLen *= 2;
								//dwByteOffset += 6;
							}
							else
							{
								strValue = (LPTSTR)&m_pData[dwByteOffset];
								dwKeyLen = strValue.GetLength() + 1; //strlen((LPCTSTR)&wBuf[dwByteOffset+4]) + 1;
								//dwByteOffset += 4;
							}

							TRACE(_T("dwKeyLen=%d\n"), dwKeyLen);
							TRACE(_T("value=<%s> at 0x%X\n"), strValue, dwByteOffset);

							TRACE(_T("7: dwByteOffset=0x%X\n"), dwByteOffset);
							dwByteOffset += dwKeyLen;
						}
						//else
						{
							dwByteOffset = dwStringEnd;
						}

						dwByteOffset += dwPad;

						TRACE(_T("8: dwByteOffset=0x%X\n"), dwByteOffset);
						dwByteOffset &= ~dwPad;
						TRACE(_T("9: dwByteOffset=0x%X\n"), dwByteOffset);
						dwWordOffset = dwByteOffset / 2;
					}

					//if (!strValue.IsEmpty())
					{
						TRACE(_T("FINAL:  strKey=<%s>  strValue=<%s>\n"), strKey, strValue);
						CString str;
						str = strLanguage;
						str += "|";
						str += strKey;
						if (ppsaKeys)
							(*ppsaKeys)->Add(str);
						if (ppsaValues)
							(*ppsaValues)->Add(strValue);
					}

					nStringCount--;
				}
				ASSERT(nStringCount > 0);

				nStringTable--;
			}
			ASSERT(nStringTable > 0);
		}
		///////////////////////////////////////////////////////////////////////
		else if (strEntry == _T("VarFileInfo"))
		{
			int nVarCount = 100;
			while ((dwByteOffset < dwFileInfoEnd) && (nVarCount > 0))
			{
				TRACE(_T("===== Var at 0x%X =====\n"), dwByteOffset);

				WORD wVarLength = wBuf[dwWordOffset];
				DWORD dwVarEnd = dwByteOffset + wVarLength;
				TRACE(_T("dwVarEnd=0x%X\n"), dwVarEnd);

				if (wVarLength > 10000)
				{
					ASSERT(FALSE);
					return;
				}

				CString strVar;
				strVar = _T("");

				if (bUnicode)
				{
					//strVar = (LPTSTR)&m_pData[dwByteOffset+6];
					strVar = WideCharToString(&wBuf[dwWordOffset + 3]);
					dwKeyLen = strVar.GetLength() + 1; //wcslen((LPCOLESTR)&wBuf[6]) + 1;
					dwKeyLen *= 2;
					dwByteOffset += 6;
				}
				else
				{
					strVar = (LPTSTR)&m_pData[dwByteOffset + 4];
					dwKeyLen = strVar.GetLength() + 1; //strlen((LPCTSTR)&wBuf[4]) + 1;
					dwByteOffset += 4;
				}

				TRACE(_T("var:  <%s>\n"), strVar);

				if (strVar != "Translation")
				{
					ASSERT(FALSE);
					break;
				}

				TRACE(_T("A: dwByteOffset=0x%X\n"), dwByteOffset);
				dwByteOffset += dwKeyLen + dwPad;
				TRACE(_T("B: dwByteOffset=0x%X\n"), dwByteOffset);
				dwByteOffset &= ~dwPad;
				TRACE(_T("C: dwByteOffset=0x%X\n"), dwByteOffset);
				dwWordOffset = dwByteOffset / 2;

				CString strLanguage;
				int nValueCount = 100;

				while ((dwByteOffset < dwVarEnd) && (nValueCount > 0))
				{
					WORD w1 = wBuf[dwWordOffset];
					WORD w2 = wBuf[dwWordOffset + 1];
					DWORD dwValue = (DWORD)(w1 << 16) + w2;
					TRACE(_T("dwValue=0x%08X\n"), dwValue);

					// hex must be caps
					strLanguage.Format(_T("%04X%04X"), w1, w2);

					if (ppsaLanguages)
						(*ppsaLanguages)->Add(strLanguage);

					dwWordOffset += 2;
					dwByteOffset += 4;
					nValueCount--;
				}

				nVarCount--;
			}
			ASSERT(nVarCount > 0);
		}
		else
		{
			// not a StringFileInfo or VarFileInfo structure
			TRACE(_T("ERROR  unknown structure\n"));
			//break;

			dwByteOffset = dwFileInfoEnd;
			dwWordOffset = dwByteOffset / 2;
		}

		nFileInfo--;
	}
	ASSERT(nFileInfo > 0);

	TRACE(_T("===== exiting Dump =====\n"));
}

///////////////////////////////////////////////////////////////////////////////
// operator >
BOOL CVersion::operator > (CVersion& rVersion)
{
	WORD nThis1, nThis2, nThis3, nThis4;
	WORD nVersion1, nVersion2, nVersion3, nVersion4;

	GetFileVersion(&nThis1, &nThis2, &nThis3, &nThis4);
	rVersion.GetFileVersion(&nVersion1, &nVersion2, &nVersion3, &nVersion4);

	if (nThis1 > nVersion1)
		return TRUE;
	else if (nThis1 < nVersion1)
		return FALSE;

	if (nThis2 > nVersion2)
		return TRUE;
	else if (nThis2 < nVersion2)
		return FALSE;

	if (nThis3 > nVersion3)
		return TRUE;
	else if (nThis3 < nVersion3)
		return FALSE;

	if (nThis4 > nVersion4)
		return TRUE;
	else
		return FALSE;
}


