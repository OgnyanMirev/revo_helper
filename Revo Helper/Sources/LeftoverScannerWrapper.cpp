#include "stdafx.h"
#include "LeftoverScannerWrapper.h"

void LeftoverScanner::Scan()
{
	CString strText;
	auto m_oScanner = m_trackFinder.get();
	//auto Temp = CIAD2UninstallInfo(this->m_appData.get());
	///auto m_pBinUninstallerData = &Temp;

	auto pUnFirstApp = m_appData.get();

	m_oScanner->m_strAppName = pUnFirstApp->strAppName; //m_pBinUninstallerData->m_strAppName;
	m_oScanner->m_strInstallLocation = pUnFirstApp->strInstallLocation;//m_pBinUninstallerData->m_strInstallLocation;
	m_oScanner->m_strUninstString = pUnFirstApp->strUninstString; //m_pBinUninstallerData->m_strUninstString;
	m_oScanner->m_strKeyName = pUnFirstApp->strKeyName; //m_pBinUninstallerData->m_strKeyName;
	//m_oScanner.m_strFullKeyPath = pUnFirstApp //m_pBinUninstallerData->m_strFullKeyPath;
	if (pUnFirstApp->bIsMSI)
		m_oScanner->m_strMSIGUID = pUnFirstApp->strKeyName; //m_pBinUninstallerData->m_strMSIGUID;
	m_oScanner->m_bMSI = pUnFirstApp->bIsMSI; //m_pBinUninstallerData->m_bMSI;
	m_oScanner->m_b64Bit = pUnFirstApp->b64Bit; //m_pBinUninstallerData->m_b64Bit;
	CString strDIFullPath = pUnFirstApp->strIconPath;
	CString strDIFullPath2 = strDIFullPath;
	CString strDIFileName;
	if (strDIFullPath.IsEmpty() == FALSE)
	{
		DWORD dwFlags = 0;
		if (pUnFirstApp->b64Bit)
			dwFlags = KEY_WOW64_64KEY;
		else
			dwFlags = KEY_WOW64_32KEY;
		REGSAM flagsOpen = KEY_READ | dwFlags;

		CString strFullPathNoRoot, strDisplayIcon = TEXT("");
		//HKEY hkeyRoot = GetRootFromPath(pUnFirstApp->m_strFullKeyPath);
		//RemoveRootFromPath(m_pBinUninstallerData->m_strFullKeyPath, strFullPathNoRoot);

		HKEY root;
		GetRootHKEYByName(pUnFirstApp->strRoot, root);
		CRegistry regAppData(root);
		if (regAppData.Open(pUnFirstApp->strFullKeyName, flagsOpen))
		{
			if (regAppData.GetStringValue(TEXT("DisplayIcon"), strDisplayIcon) == TRUE)
			{
				int nIconPos = strDisplayIcon.Find(TEXT(','));
				if (nIconPos != -1)
				{
					CString strNewDisplayIcon = strDisplayIcon.Left(nIconPos);
					strDisplayIcon = strNewDisplayIcon;

				}
			}
		}

		if (pUnFirstApp->strIconPath.CompareNoCase(strDisplayIcon) == 0)
		{
			LPTSTR pBuffer = strDIFullPath.GetBuffer();
			//::PathRemoveBackslash(pBuffer);
			::PathRemoveExtension(pBuffer);
			strDIFileName = ::PathFindFileName(pBuffer);
			strDIFullPath.ReleaseBuffer();

			PathRemoveFileSpec(strDIFullPath2.GetBuffer());
			strDIFullPath2.ReleaseBuffer();
			m_oScanner->m_strDisplayIconPath = strDIFullPath2;
			m_oScanner->m_strDispIconFileName = strDIFileName;
		}
		else
		{
			strDIFullPath = strDisplayIcon;
			strDIFullPath2 = strDIFullPath;

			LPTSTR pBuffer = strDIFullPath.GetBuffer();
			//::PathRemoveBackslash(pBuffer);
			::PathRemoveExtension(pBuffer);
			strDIFileName = ::PathFindFileName(pBuffer);
			strDIFullPath.ReleaseBuffer();

			PathRemoveFileSpec(strDIFullPath2.GetBuffer());
			strDIFullPath2.ReleaseBuffer();
			m_oScanner->m_strDisplayIconPath = strDIFullPath2;
			m_oScanner->m_strDispIconFileName = strDIFileName;
			/*	m_oScanner->m_strDisplayIconPath.Empty();
				m_oScanner->m_strDispIconFileName.Empty();*/
		}
		m_oScanner->StartMenuScan(FALSE);
		auto ev = m_oScanner->StartScan(uninAdvancedMode);
		WaitForSingleObject(ev->m_hObject, INFINITE);
	}
}


void CountCreatedItems(PREGNODE pTreeNode, int* pKeysCounter, int* pValuesCounter)
{
	if ((pTreeNode == NULL) || (pKeysCounter == NULL) || (pValuesCounter == NULL))
		return;

	PREGNODE pNextBrother = NULL;
	PREGNODE pCurrentNode = NULL;

	if ((pTreeNode->eOperation == RegCreated) || (pTreeNode->eOperation == RegDeleted))
		(*pKeysCounter)++;
	if ((pTreeNode->eOperation == RegCreated) || (pTreeNode->eOperation == RegNone) || (pTreeNode->eOperation == RegDeleted))
		(*pValuesCounter) += pTreeNode->m_pListRegVals->GetCount();

	CountCreatedItems(pTreeNode->pChild, pKeysCounter, pValuesCounter);

	pNextBrother = pTreeNode->pBrother;
	while (pNextBrother != NULL)
	{
		pCurrentNode = pNextBrother;
		pNextBrother = pNextBrother->pBrother;

		if ((pCurrentNode->eOperation == RegCreated) || (pCurrentNode->eOperation == RegDeleted))
			(*pKeysCounter)++;
		if ((pCurrentNode->eOperation == RegCreated) || (pCurrentNode->eOperation == RegNone) || (pCurrentNode->eOperation == RegDeleted))
			(*pValuesCounter) += pCurrentNode->m_pListRegVals->GetCount();

		if (pCurrentNode->pChild != NULL)
			CountCreatedItems(pCurrentNode->pChild, pKeysCounter, pValuesCounter);

	}
}

bool LeftoverScanner::HasLeftovers(unsigned& files_n, unsigned& total_size, unsigned& registry_n)
{
	files_n = 0;
	total_size = 0;
	registry_n = 0;

	vector<wstring> ws;
	for (int i = 0; i < m_trackFinder->m_arrFoundFilesFolders.GetSize(); i++)
		for (auto& y : fs::recursive_directory_iterator(m_trackFinder->m_arrFoundFilesFolders[i].GetString()))
			ws.push_back(y.path().wstring());
	

	for (auto& i : ws)
	{
		if (fs::is_regular_file(i))
		{
			total_size += fs::file_size(i);
			files_n++;
		}
	}
	total_size /= (1024 * 1024);

	PREGNODE pRoot = m_trackFinder->m_pTree->GetRegTree();
	int nRegItems = 0;
	int nValuesCount = 0;
	CountCreatedItems(pRoot, &nRegItems, &nValuesCount);
	
	registry_n = nRegItems + nValuesCount;

	return true;
}
LeftoverScanner::LeftoverScanner(shared_ptr<CInstalledAppData> appdata)
{
	this->m_appData = appdata;
	this->m_trackFinder = make_unique<CVSTrackFinder>();
}

void LeftoverScanner::CleanLeftovers()
{
	auto filename = GetTemporaryFilename();

	if (WriteIAppDataToFile(filename, m_appData))
	{
		if (!InvokeRevo(L"/leftovers " + filename.wstring()))
		{
			ErrorMsg(L"Unable to start " + LangString(strid::tool_tip_str) + L".");
		}
	}
	else
	{
		ErrorMsg(L"Unexpected error.");
	}
}
