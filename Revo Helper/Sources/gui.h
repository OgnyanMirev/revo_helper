#pragma once
#include <string>
#include <vector>


struct notification_callback_data
{
    bool execute : 1, never_show_again : 1;
};


struct tracing_dialog_data
{
    bool& done;
    bool abort = false;
};

void create_notification(wstring const& caption, wstring const& button_caption, void(*button_function)(notification_callback_data),
                         vector<wstring> const& msgs, wstring const& img_path, int Mode);

void create_advert(string const& img_link, string& link);

void create_tray_icon();

void create_tracing_query_dialog(wstring& product_name, wstring const& executable_name, tracing_dialog_data& data);

void create_continue_uninstall_dlg(void(*continue_function)(), void (*later_function)(), void (*never_function)(),  vector<wstring> const& msgs, wstring const& img_path);