#include "AppBarStdafx.h"
#include "VSFileLog.h"

IMPLEMENT_SERIAL(VSFileLog, CObject, VERSIONABLE_SCHEMA | 1)

void VSFileLog::Serialize(CArchive &ar)
{
	CObject::Serialize( ar );
	if( ar.IsStoring() )
	{
		ar << m_nVersion;
		ar << m_strFileName << m_strRenameName << m_eOperation << m_nIsDirectory;
	}
	else
	{
		ar >> m_nVersion;
		switch (m_nVersion)
		{
			case 1:
				ar >> m_strFileName >> m_strRenameName >> (LONG&) m_eOperation >> m_nIsDirectory; 
				break;
		}
	}
}

VSFileLog::VSFileLog()
{
	m_eOperation = FileError;
	m_nVersion = 1;
}