// A bunch of useless code I didn't want to outright delete
// Mostly for dealing with processes before I introduced the driver


void GetProcessInfosWMI(vector<shared_ptr<ProcessInfo> >& info, vector<shared_ptr<ProcessInfo>>* OldInfos)
{
	static struct Data
	{
		Data()
		{
			//MessageBox(L"GPI Data init.");
			BSTR strNetworkResource = L"\\\\.\\root\\CIMV2";

			IWbemLocator* pLoc = NULL;

			HRESULT hres;
#ifndef MANAGED_CLIENT
			hres = CoInitializeEx(0, COINIT_MULTITHREADED);
			if (FAILED(hres))
				return;

			hres = CoInitializeSecurity(
				NULL,
				-1,                          // COM authentication
				NULL,                        // Authentication services
				NULL,                        // Reserved
				RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication
				RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation
				NULL,                        // Authentication info
				EOAC_NONE,                   // Additional capabilities
				NULL                         // Reserved
			);

			if (FAILED(hres))
				return;                    // Program has failed.
#endif
			//MessageBox(L"GPI CCI.");

			hres = CoCreateInstance(CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER, IID_IWbemLocator, (LPVOID*)&pLoc);

			if (FAILED(hres))
				return;                 // Program has failed.

			//MessageBox(L"GPI pLoc->ConnectServer.");
			hres = pLoc->ConnectServer(
				strNetworkResource,      // Object path of WMI namespace
				NULL,                    // User name. NULL = current user
				NULL,                    // User password. NULL = current
				0,                       // Locale. NULL indicates current
				NULL,                    // Security flags.
				0,                       // Authority (e.g. Kerberos)
				0,                       // Context object
				&pSvc                    // pointer to IWbemServices proxy
			);

			if (FAILED(hres))
			{
				pLoc->Release();
				return;                // Program has failed.
			}

			//MessageBox(L"GPI CSPB.");
			hres = CoSetProxyBlanket(
				pSvc,                        // Indicates the proxy to set
				RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
				RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
				NULL,                        // Server principal name
				RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx
				RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
				NULL,                        // client identity
				EOAC_NONE                    // proxy capabilities
			);

			if (FAILED(hres))
			{
				pSvc->Release();
				pLoc->Release();
				return;               // Program has failed.
			}

			// Use the IWbemServices pointer to make requests of WMI ----
			bValid = true;
		}
		void ParseProcesses(vector<shared_ptr<ProcessInfo> >& info, vector<shared_ptr<ProcessInfo> >* OldInfos)
		{
			//MessageBox(L"GPI Data parse.");

			if (!bValid) throw "WQL connection failed.";
			auto hres = pSvc->ExecQuery(L"WQL", L"SELECT CommandLine,ProcessId,ParentProcessId,ExecutablePath FROM Win32_Process WHERE CommandLine != NULL",
										WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, NULL, &pEnumerator);

			if (FAILED(hres))
			{
				return;               // Program has failed.
			}

			// Get the data from the WQL sentence
			IWbemClassObject* pclsObj = NULL;
			ULONG uReturn = 0;

			while (pEnumerator)
			{
				HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);

				if (0 == uReturn || FAILED(hr))
					break;

				VARIANT vtProp;
				wstring CMDLN, ExePath;
				DWORD PID = 0, ParentPID = 0;
				hr = pclsObj->Get(L"ProcessId", 0, &vtProp, 0, 0);// Uint32
				if (!FAILED(hr))
				{
					if ((vtProp.vt != VT_NULL) && (vtProp.vt != VT_EMPTY) && (~vtProp.vt & VT_ARRAY))  PID = vtProp.uintVal;//info.push_back(make_shared<ProcessInfo>(wstring(), vtProp.uintVal));
				}
				VariantClear(&vtProp);

				if (PID)
				{
					hr = pclsObj->Get(L"ParentProcessId", 0, &vtProp, 0, 0);// Uint32
					if (!FAILED(hr))
					{
						if ((vtProp.vt != VT_NULL) && (vtProp.vt != VT_EMPTY) && (~vtProp.vt & VT_ARRAY))  ParentPID = vtProp.uintVal;
					}
					VariantClear(&vtProp);


					hr = pclsObj->Get(L"ExecutablePath", 0, &vtProp, 0, 0);// String
					if (!FAILED(hr))
					{
						if ((vtProp.vt != VT_NULL) && (vtProp.vt != VT_EMPTY) && !(vtProp.vt & VT_ARRAY)) ExePath = vtProp.bstrVal;
					}
					VariantClear(&vtProp);

					to_lower(ExePath);
					if (!IsValidExePath(ExePath))
					{
						pclsObj->Release();
						continue;
					}


					/*
					If OldInfos is specified, check if the process is in OldInfos
					If it is, only set its still alive flag to true and continue to the next one
					*/
					if (OldInfos)
					{
						if ([&]()
						{
							for (auto& i : *OldInfos)
							{
								if (i->PID == PID && i->ParentPID == ParentPID)
								{
									i->bStillAlive = true;
										return true;
								}
							}
							return false;
						}())
						{
							pclsObj->Release();
							continue;
						}
					}

					hr = pclsObj->Get(L"CommandLine", 0, &vtProp, 0, 0);// String
					if (!FAILED(hr))
					{
						if ((vtProp.vt != VT_NULL) && (vtProp.vt != VT_EMPTY) && !(vtProp.vt & VT_ARRAY)) CMDLN = vtProp.bstrVal;
						//MessageBox(vtProp.bstrVal);
					}
					VariantClear(&vtProp);


					auto pi = make_shared<ProcessInfo>(ExePath, CMDLN, PID, ParentPID);
					if (!pi->bValid) continue;
					info.push_back(pi);
					if (OldInfos)
						OldInfos->push_back(pi);
					for (auto& i : info)
						if (i->PID == ParentPID) i->Children.push_back(info.back());
				}
				pclsObj->Release();
				pclsObj = NULL;
			}
			pEnumerator->Release();
		}
	private:
		bool bValid = false;
		IEnumWbemClassObject* pEnumerator = NULL;
		IWbemServices* pSvc = NULL;

	} d;
	d.ParseProcesses(info, OldInfos);
}


shared_ptr<ProcessInfo> WMIListenForProcesses()
{
	struct ProcessesInfo
	{
		vector<shared_ptr<ProcessInfo> > NewProcesses, OldProcesses;

		void operator()()
		{
			// Clear the NewProcesses structure for another use
			NewProcesses.clear();
			// Set all OldProcesses->bStillAlive flag to false, this flag will be reset to true
			// on GetProcessInfos if the process is still alive, if not, it should be deleted
			for (auto& i : OldProcesses) i->bStillAlive = false;

			// Get new processes in NewProcesses struct, NewProcesses also get added to the
			// OldProcesses struct if, if a process is enumerated, which is already in the 
			// OldProcesses strcut, its current entry will have its bStillAlive flag set to true
			GetProcessInfos(NewProcesses, &OldProcesses);

			//OldProcesses.clear();
			// All OldProcesses which have not had their bStillAlive flag set to true should be deleted
			for (int i = 0; i < OldProcesses.size(); i++)
			{
				if (!OldProcesses[i]->bStillAlive)
				{
					OldProcesses.erase(OldProcesses.begin() + i);
					i--;
				}
			}
		}
		ProcessesInfo()
		{
			// Reserve some memory for the process parsing, NewProcesses doesn't really need
			// so much, but it will be reserved anyway since on the first run, all processes are new
			NewProcesses.reserve(256);
			OldProcesses.reserve(256);
			// First run
			(*this)();
		}
	};
	ProcessesInfo pi;

	return nullptr
}




#pragma region Installer Detector Trash
#pragma region Hyperparameters
#define INSTALLER_DETECTION_PROBABILITY_THRESHOLD 0.75
#define INSTALLER_ICON_WEIGHT .80

#define TOTAL_WEIGHTS (0.8 + 0.9)
#pragma endregion







struct ExecutableFeatures
{
	void ExtractManifestFeatures()
	{
		string manifest = GetFileManifest(proc->Executable);
		if (manifest.size())
		{
			using namespace rapidxml;

			try
			{
				xml_document<char> doc;
				doc.parse<parse_default>((char*)manifest.c_str());
				auto n = doc.first_node("assembly");
				auto f = [&](char* c)
				{
					return n->first_node(c);
				};

				if (n)
				{
					auto id = n->first_node("assemblyIdentity");
					if (id)
					{
						auto name = id->first_attribute("name");
						if (name)
						{
							if (name->value() == string("setup.exe"))
							{
								asm_id_name_is_setup = true;
							}
						}
					}
				}
			}
			catch (...) {}
		}
	}
	shared_ptr<ProcessInfo> proc;
	bool has_inst_icon = false, matching_setup_desc = false, spaceless_filename = true, asm_id_name_is_setup = false,
		version_in_filename = false;
	float final_prob, string_scan_prob;
	ExecutableFeatures(shared_ptr<ProcessInfo> proc) : proc(proc)
	{
		ExtractManifestFeatures();
		has_inst_icon = is_installer_icon(proc->Executable);
		for (auto& i : proc->Executable.filename().wstring()) if (i == L' ') { spaceless_filename = false; break; }

		wstring Version;
		if (GetFileVersion(proc->Executable, Version))
		{
			int idx = 0;
			wstring fn = proc->Executable.filename();

			for (int i = 0; i < fn.size(); i++)
			{
				if (Version[idx] == fn[i] || (Version[idx] == L'.' && fn[i] == L'-') || (Version[idx] == L'.' && fn[i] == L'_'))
				{
					if (++idx == Version.size())
					{
						version_in_filename = true;
						break;
					}
				}
				else idx = 0;
			}
			//version_in_filename = fn.find(Version) != wstring::npos;
		}
	}

	float GetFinalProbability()
	{
		return has_inst_icon | matching_setup_desc;
	}
};

//__declspec(dllimport)
bool IsInnoSetupInstaller(const wchar_t*) { return false; }




//bool IsSimpleInstaller(const shared_ptr<ProcessInfo>& proc)
//{
//	return IsInnoSetupInstaller(proc->Executable.c_str()) | IsInstallShieldInstaller(proc) | IsNullsoftInstaller(proc);
//} 


bool ScanFile(const fs::path& file)
{
	static const vector<wstring> unicode = { L"InstallShield", L"Inno Setup", L"Nullsoft", L"OPatchInstall" };
	static const vector<string> ascii = { "InstallShield", "Inno Setup", "Nullsoft", "OPatchInstall" };

	ifstream ifs(file, ios::binary | ios::ate);
	if (!ifs.good()) return false;


	size_t bsize = ifs.tellg();
	ifs.seekg(ios::beg);
	//unique_ptr<char> buffer(new char[bsize]);

	const size_t MB = 1000000;

	char buffer[MB];
	size_t to_read;

	while (bsize)
	{

		if (bsize > MB)
		{
			to_read = MB;
			bsize -= MB;
		}
		else
		{
			to_read = bsize;
			bsize = 0;
		}

		ifs.read(buffer, to_read);

		for (auto& i : ascii)
			if (ScanForStringA(buffer, to_read, i.c_str(), i.size())) return true;

		for (auto& i : unicode)
			if (ScanForStringW(buffer, to_read, i.c_str(), i.size())) return true;
	}

	return false;
}


#pragma endregion



#pragma region Full Executable Scanning

inline float scan_compute_prob(fs::path const& exe_path)
{
	static const vector<pair<string, float>> installer_patterns =
	{
	{"0FALSE", 0.610778},
	{"lower", 0.610778},
	{"higher or equal", 0.610778},
	{"' is ", 0.610778},
	{"OPatchInstall: Comparing if the property '", 0.610778},
	{"higher", 0.610778},
	{"equal", 0.610778},
	{"' = '", 0.610778},
	{"lower or equal", 0.610778},
	{"than '", 0.610778},
	{"`.rdata", 0.523169},
	{"@.data", 0.480506},
	{"x ATH", 0.239521},
	{"l$ E3", 0.239521},
	{"A]A\\]", 0.239521},
	{"L$hH+", 0.239521},
	{"OPatchInstall: CActionCreateTempFolder::execute starts", 0.233533},
	{">`doesn't exist", 0.233533},
	{"OPatchInstall: Done executing the IfTrue list", 0.233533},
	{"false", 0.233533},
	{"OPatchInstall: CActionIf::execute ends", 0.233533},
	{"OPatchInstall: Thrown CDetectionExceptionInvalidCoditionStatementElement. Invalid Condition Statement element found in script", 0.233533},
	{"is not", 0.233533},
	{"OPatchInstall: Executing the IfFalse list", 0.233533},
	{"0 equal to '", 0.233533},
	{"OPatchInstall: The property '", 0.233533},
	{"exists", 0.233533},
	{"OPatchInstall: Evaluation of the condition complete, the result is ", 0.233533},
	{"OPatchInstall: Done executing the IfFalse list", 0.233533},
	{"OPatchInstall: CActionIf::execute starts", 0.233533},
	{"OPatchInstall: Begin evaluation of the condition", 0.233533},
	{"OPatchInstall: Executing the IfTrue list", 0.233533},
	{"UATAUH", 0.209581},
	{"@.reloc", 0.166581},
	{"gfffffffH", 0.156391},
	{"t$ H95", 0.155689},
	{"HthHt:Ht", 0.143713},
	{"VQSPW", 0.137725},
	{"Vjzh\\", 0.137725},
	{"SQVPW", 0.137725},
	{"SQPh0", 0.137725},
	{"SQSSSPW", 0.137725},
	{"OPatchInstall: The property to get the size is '", 0.125749},
	{"OPatchInstall: The file was deleted succesfully", 0.125749},
	{"list<T> too long", 0.125749},
	{"OPatchInstall: The folder was already there", 0.125749},
	{"' doesn't exist", 0.125749},
	{"OPatchInstall: Failed to delete the file", 0.125749},
	{"OPatchInstall: CActionFileProperties::execute starts", 0.125749},
	{"' already exsits, will be renaming it to a temporary file", 0.125749},
	{"OPatchInstall: CActionGetStandardFolder::execute starts", 0.125749},
	{"OPatchInstall: CActionGetFreeDiskSpace::execute starts", 0.125749},
	{"OPatchInstall: CActionCreateTempFile::execute ends", 0.125749},
	{"OPatchInstall: CActionCopyFile::initFromElement starts", 0.125749},
	{"OPatchInstall: CActionFileProperties::execute ends", 0.125749},
	{"OPatchInstall: Will delete the file '", 0.125749},
	{"shell32.dll", 0.125749},
	{"OPatchInstall: The property to get the MD5 hash is '", 0.125749},
	{"OPatchInstall: Will retrieve the CSIDL '", 0.125749},
	{"OPatchInstall: CActionCopyFile::initFromElement ends", 0.125749},
	{"SHGetSpecialFolderPathA", 0.125749},
	{"OPatchInstall: The folder is '", 0.125749},
	{"OPatchInstall: The API SHGetSpecialFolderPathA could not be called in this system", 0.125749},
	{"OPatchInstall: CActionCreateFolder::execute starts", 0.125749},
	{"OPatchInstall: CActionGetStandardFolder::initFromElement ends", 0.125749},
	{"OPatchInstall: CActionCreateTempFile::execute starts", 0.125749},
	{"' exists", 0.125749},
	{"OPatchInstall: CActionCreateFolder::execute ends", 0.125749},
	{"OPatchInstall: Will create the folder '", 0.125749},
	{"OPatchInstall: CActionGetFreeDiskSpace::initFromElement starts", 0.125749},
	{"%u%u%u%u", 0.125749},
	{"OPatchInstall: Will set the property '", 0.125749},
	{"OPatchInstall: CActionGetStandardFolder::initFromElement starts", 0.125749},
	{"' to the path '", 0.125749},
	{"OPatchInstall: Getting the size of the root drive of '", 0.125749},
	{"OPatchInstall: CActionGetStandardFolder::execute ends", 0.125749},
	{"OPatchInstall: CActionCreateTempFolder::execute ends", 0.125749},
	{"OPatchInstall: The free disk space is ", 0.125749},
	{"' to '", 0.125749},
	{"OPatchInstall: Copying file '", 0.125749},
	{"OPatchInstall: The property to get the version is '", 0.125749},
	{"OPatchInstall: The file '", 0.125749},
	{"OPatchInstall: Will get the free disk space for '", 0.125749},
	{"OPatchInstall: CActionGetFreeDiskSpace::initFromElement ends", 0.125749},
	{"OPatchInstall: Will copy '", 0.125749},
	{"OPatchInstall: CActionGetFreeDiskSpace::execute ends", 0.125749},
	{"gperty '", 0.107784},
	{"L$0H3", 0.102894},
	{"OPatchInstall: CActionShellExecute::execute ends", 0.101796},
	{"OPatchInstall: Thrown CDetectionExceptionNotImplemented", 0.101796},
	{"OPatchInstall: CActionShellExecute::execute starts", 0.101796},
	{"' and from directory '", 0.101796},
	{"OPatchInstall: CActionFile::initFromElements ends", 0.101796},
	{"OPatchInstall: CActionExecuteDllAction::initFromElement ends", 0.101796},
	{"OPatchInstall: CActionFile::initFromElement starts", 0.101796},
	{"OPatchInstall: Will store the temp file path in '", 0.101796},
	{"' and the extension '", 0.101796},
	{"OPatchInstall: Thrown CDetectionExceptionOutOfMemory", 0.101796},
	{"OPatchInstall: CActionDllLoad::execute starts", 0.101796},
	{"OPatchInstall: Thrown CDetectionExceptionInvalidActionElement. Invalid Action element found in script", 0.101796},
	{"0OPIUninitialize", 0.101796},
	{"OPatchInstall: CActionLaunchURL::initFromElement starts", 0.101796},
	{"OPatchInstall: Will call into the action '", 0.101796},
	{"OPatchInstall: CActionExecute::initFromElement ends", 0.101796},
	{"OPatchInstall: CActionExecute::initFromElement starts", 0.101796},
	{"OPatchInstall: CActionDllLoad::initFromElement ends", 0.101796},
	{"OPatchInstall: CActionFileProperties::initFromElement starts", 0.101796},
	{"' and set the property '", 0.101796},
	{"OPatchInstall: Thrown CDetectionExceptionWin32(", 0.101796},
	{"OPatchInstall: CActionExecute::execute starts", 0.101796},
	{"OPatchInstall: Restoring the current directory to '", 0.101796},
	{"OPatchInstall: CActionDllLoad::execute ends", 0.101796},
	{"OPatchInstall: CActionDllLoad::initFromElement starts", 0.101796},
	{"OPatchInstall: Will load the dll '", 0.101796},
	{"' in the dll attached to the property '", 0.101796},
	{"OPatchInstall: Will call the action '", 0.101796},
	{"invalid map/set<T> iterator", 0.101796},
	{"OPatchInstall: Thrown CDetectionExceptionCOM(", 0.101796},
	{"OPatchInstall: Will execute the command line '", 0.101796},
	{"OPatchInstall: CActionExecuteDllAction::initFromElement starts", 0.101796},
	{"0map/set<T> too long", 0.101796},
	{"OPatchInstall: ShellExec '", 0.101796},
	{"OPatchInstall: Will launch the command '", 0.101796},
	{"OPatchInstall: CActionExecuteDllAction::execute ends", 0.101796},
	{"OPatchInstall: Calling into the action function", 0.101796},
	{"OPatchInstall: The current directory is '", 0.101796},
	{"OPatchInstall: CActionExecuteDllAction::execute starts", 0.101796},
	{"OPatchInstall: Loading the dll '", 0.101796},
	{"OPatchInstall: CActionFileProperties::initFromElement ends", 0.101796},
	{"OPatchInstall: CActionCreateTempFile::initFromElement ends", 0.101796},
	{"OPatchInstall: CActionCreateTempFile::initFromElement starts", 0.101796},
	{"OPatchInstall: CActionLaunchURL::initFromElement ends", 0.101796},
	{"OPatchInstall: Done calling into the action function", 0.101796},
	{"OPatchInstall: CActionExecute::execute ends", 0.101796},
	{"' using the base name '", 0.101796},
	{"OPatchInstall: Will check for file '", 0.101796},
	{"OPatchInstall: Changing the current directory to '", 0.101796},
	{"OPatchInstall: Launching command line '", 0.101796},
	{"9=@^D", 0.0898204},
	{"9=D^D", 0.0898204},
	{"C9=<^D", 0.0898204},
	{".wixburn8", 0.0898204},
	{"9YX~!", 0.0838323},
	{".ndata", 0.0764973},
	{"bad allocation", 0.0718563},
	{"@.tls", 0.0686642},
	{"tTj\\V", 0.0658683},
	{"w'8]", 0.0598802},
	{"d allocation", 0.0598802},
	{"`.data", 0.0598802},
	{"PK00.", 0.0598802},
	{"tsh8\"", 0.0598802},
	{"tdSh$\"", 0.0598802},
	{"QQSVW", 0.0598802},
	{"SUVWh@", 0.0598802},
	{"SVWj", 0.0538922},
	{"QSVW3", 0.0538922},
	{"H SVWH", -0.0513447},
	{"[^A\\", -0.0537897},
	{"0[^_]A\\", -0.0537897},
	{"[^A\\A]A^", -0.0562347},
	{"AUATWVSH", -0.0562347},
	{"System.Collections.Concurrent.dll                     : 4.6.26820.01", -0.0586797},
	{"Tool chain:", -0.0586797},
	{"Build Type: ret", -0.0586797},
	{"ATVSH", -0.0586797},
	{"System.Collections.dll                                : 4.6.26820.01", -0.0586797},
	{"Framework:", -0.0586797},
	{"PA_A^A]A\\_^[", -0.0611247},
	{"|$ ATH", -0.0635697},
	{"A_A^_^]", -0.0651655},
	{"System.ComponentModel.Annotations.dll                 : 4.6.26820.01", -0.0684597},
	{"[^_]A\\A]", -0.0684597},
	{"System.ComponentModel.DataAnnotations.dll             : 4.6.26820.1", -0.0684597},
	{"System.Collections.Immutable.dll                      : 4.6.26820.01", -0.0684597},
	{"System.Collections.NonGeneric.dll                     : 4.6.26820.01", -0.0684597},
	{"System.ComponentModel.dll                             : 4.6.26820.01", -0.0684597},
	{"System.Collections.Specialized.dll                    : 4.6.26820.01", -0.0684597},
	{"H[^_]A\\A]A^A_", -0.0733496},
	{"gfffffffH+", -0.0757946},
	{"@SWAV", -0.0757946},
	{"@SWATAV", -0.0757946},
	{"(A^A\\_[", -0.0757946},
	{"A^_[", -0.0757946},
	{"([^_]A\\A]A^A_", -0.0782396},
	{"TCUI-Shell.dll                                        : 1.24.1812.10001", -0.0806846},
	{"XboxExperienceServices.dll                            : 1.24.1812.10001", -0.0806846},
	{"System.Configuration.dll                              : 4.6.26820.1", -0.0806846},
	{"System.ComponentModel.Primitives.dll                  : 4.6.26820.01", -0.0806846},
	{"winsdkfb.dll                                          : 1.24.1812.10001", -0.0806846},
	{"System.Console.dll                                    : 4.6.26820.01", -0.0806846},
	{"TCUI-Tracing.dll                                      : 1.24.1812.10001", -0.0806846},
	{"System.ComponentModel.EventBasedAsync.dll             : 4.6.26820.01", -0.0806846},
	{"System.ComponentModel.TypeConverter.dll               : 4.6.26820.01", -0.0806846},
	{"TCUI-Resources.dll                                    : 1.24.1812.10001", -0.0806846},
	{"TCUI-Toolkit.dll                                      : 1.24.1812.10001", -0.0806846},
	{"tem.Web.HttpUtility.dll                            : 4.6.26820.01", -0.0831296},
	{"UWAVAW", -0.0831296},
	{"@UWATAU", -0.0831296},
	{"9\\$@A", -0.0831296},
	{"UVWAVAW", -0.0831296},
	{"t$8fA", -0.0831296},
	{"D$ E3", -0.0831296},
	{"(A]A\\_]", -0.0831296},
	{"HA]_^[", -0.0831296},
	{"l$ VWAV", -0.0831296},
	{"D9l$p", -0.0831296},
	{"9\\$@@", -0.0831296},
	{"8A_A^_]", -0.0831296},
	{"@SVWAU", -0.0831296},
	{"|$@@2", -0.0831296},
	{"stem.Security.Cryptography.Primitives.dll           : 4.6.26820.01", -0.0855746},
	{"AUATSH", -0.0880196},
	{"AUATH", -0.0904645},
	{"AVAUATUWVSH", -0.0929095},
	{"ATWVSH", -0.0929095},
	{".dll                          : 4.6.28529.05", -0.0953545},
	{"(A\\A]", -0.0953545},
	{"ected-at", -0.0977995},
	{"ng: gzipH", -0.0977995},
	{"(12:protA", -0.0977995},
	{"([^_]", -0.0977995},
	{"http1", -0.0977995},
	{"localhosH", -0.0977995},
	{"UVWAVAWH", -0.0993949},
	{"System.Diagnostics.Contracts.dll                      : 4.6.26820.01", -0.105134},
	{"System.dll                                            : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.DiagnosticSource.dll               : 4.6.26820.01", -0.105134},
	{"System.Drawing.dll                                    : 4.6.26820.1", -0.105134},
	{"VWUSH", -0.105134},
	{"System.Core.dll                                       : 4.6.26820.1", -0.105134},
	{"System.Diagnostics.Tracing.dll                        : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.Process.dll                        : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.Tools.dll                          : 4.6.26820.01", -0.105134},
	{"System.Data.dll                                       : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.TextWriterTraceListener.dll        : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.StackTrace.dll                     : 4.6.26820.01", -0.105134},
	{"System.Data.SqlClient.dll                             : 4.6.26820.01", -0.105134},
	{"                           : 4.6.26820.1", -0.105134},
	{"System.Diagnostics.FileVersionInfo.dll                : 4.6.26820.01", -0.105134},
	{"System.Data.Common.dll                                : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.Debug.dll                          : 4.6.26820.01", -0.105134},
	{"System.Drawing.Primitives.dll                         : 4.6.26820.01", -0.105134},
	{"System.Diagnostics.TraceSource.dll                    : 4.6.26820.01", -0.105134},
	{"System.Globalization.dll                              : 4.6.26820.01", -0.107579},
	{"System.Globalization.Extensions.dll                   : 4.6.26820.01", -0.107579},
	{"System.IO.Compression.FileSystem.dll                  : 4.6.26820.1", -0.107579},
	{"System.IO.FileSystem.Primitives.dll                   : 4.6.26820.01", -0.107579},
	{"System.IO.Compression.dll                             : 4.6.26820.01", -0.107579},
	{"System.IO.dll                                         : 4.6.26820.01", -0.107579},
	{"System.IO.FileSystem.dll                              : 4.6.26820.01", -0.107579},
	{"System.IO.FileSystem.Watcher.dll                      : 4.6.26820.01", -0.107579},
	{"System.Dynamic.Runtime.dll                            : 4.6.26820.01", -0.107579},
	{"System.IO.MemoryMappedFiles.dll                       : 4.6.26820.01", -0.107579},
	{"System.IO.Compression.ZipFile.dll                     : 4.6.26820.01", -0.107579},
	{"System.IO.Pipes.AccessControl.dll                     : 4.6.26820.01", -0.107579},
	{"System.IO.FileSystem.DriveInfo.dll                    : 4.6.26820.01", -0.107579},
	{"System.IO.IsolatedStorage.dll                         : 4.6.26820.01", -0.107579},
	{"System.Globalization.Calendars.dll                    : 4.6.26820.01", -0.107579},
	{"System.IO.Pipes.dll                                   : 4.6.26820.01", -0.107579},
	{"t$x@8h", -0.110024},
	{"D$09P }", -0.110024},
	{"D$0f9P s", -0.110024},
	{")D$ L", -0.110024},
	{"T$HI;", -0.110024},
	{"AVAUATVSH", -0.112469},
	{")D$ H", -0.117359},
	{"ATUWVSH", -0.119804},
	{"Version.Plugin.Abstractions.dll                       : 1.0.0.0", -0.124694},
	{"Version.Plugin.dll                                    : 1.0.0.0", -0.124694},
	{"VWAVH", -0.124694},
	{"Xamarin.Auth.dll                                      : 1.6.0.1", -0.124694},
	{"[^_]A\\", -0.127139},
	{"L$HH3", -0.132029},
	{"AVAUATWVSH", -0.134474},
	{"UWVSH", -0.139364},
	{"System.IO.Ports.dll                                   : 4.6.26820.01", -0.146699},
	{"WindowsBase.dll                                       : 4.6.28529.5", -0.168704},
	{"System.Xml.XPath.XDocument.dll                        : 4.6.28529.05", -0.173594},
	{"System.Xml.XPath.dll                                  : 4.6.28529.05", -0.173594},
	{"HA]^][", -0.176039},
	{"SUVAUH", -0.176039},
	{"XA]^][", -0.176039},
	{"uSystem.IO.Ports.dll                                   : 4.6.26820.01", -0.176039},
	{".pdata", -0.184019},
	{".reloc", -0.192056},
	{"0B.debug", -0.193154},
	{".edata", -0.198044},
	{"0@.idata", -0.198044},
	{"@B/70", -0.202934},
	{"t$XI;", -0.227384},
	{"D$0Hk", -0.227384},
	{"|$ Ik", -0.227384},
	{"l$ AV", -0.239609},
	{"HcP<H", -0.264059},
	{"`@.pdata", -0.276284},
	{"[^_]A\\A]A^A_", -0.276284},
	{"`@.buildid5", -0.303178},
	{"AUATUWVSH", -0.317848},
	{"System.Net.dll                                        : 4.6.26820.1", -0.322738},
	{"System.Linq.dll                                       : 4.6.26820.01", -0.322738},
	{"System.Net.Http.dll                                   : 4.6.26820.01", -0.322738},
	{"System.Net.Http.Rtc.dll                               : 4.6.26820.01", -0.322738},
	{"System.Memory.dll                                     : 4.6.26820.01", -0.322738},
	{"System.Linq.Expressions.dll                           : 4.6.26820.01", -0.322738},
	{"System.Linq.Queryable.dll                             : 4.6.26820.01", -0.322738},
	{"System.IO.UnmanagedMemoryStream.dll                   : 4.6.26820.01", -0.322738},
	{"System.Linq.Parallel.dll                              : 4.6.26820.01", -0.322738},
	{"`A_A^A]A\\_^[", -0.327628},
	{"L$8H;", -0.327628},
	{"0@.pdata", -0.327628},
	{"t$HH;", -0.339853},
	{"D8$9u", -0.347188},
	{"L$PH3", -0.347188},
	{"VWATAVAW", -0.359413},
	{"AWAVAUATUWVSH", -0.359413},
	{"@A_A^A\\_^", -0.359413},
	{"@A_A^A]A\\_^[", -0.369193},
	{"SVWATAUAVAW", -0.381418},
	{"A_A^_", -0.397684},
	{"WAVAW", -0.427873},
	{"TrackingDLL.dll                                       : ", -0.511002},
	{"0@.bss", -0.613692},
	{"0@.xdata", -0.616137},
	{"P`.data", -0.632154},
	{"System.Net.Sockets.dll                                : 4.6.26820.01", -0.647922},
	{"System.Net.ServicePoint.dll                           : 4.6.26820.01", -0.647922},
	{"System.Net.WebProxy.dll                               : 4.6.26820.01", -0.647922},
	{"System.Net.WebSockets.Client.dll                      : 4.6.26820.01", -0.647922},
	{"System.Numerics.dll                                   : 4.6.26820.1", -0.647922},
	{"System.Net.WebClient.dll                              : 4.6.26820.01", -0.647922},
	{"System.Net.WebSockets.dll                             : 4.6.26820.01", -0.647922},
	{"System.Net.WebHeaderCollection.dll                    : 4.6.26820.01", -0.647922},
	{"System.Net.Primitives.dll                             : 4.6.26820.01", -0.650367},
	{"System.Net.HttpListener.dll                           : 4.6.26820.01", -0.650367},
	{"System.Net.NameResolution.dll                         : 4.6.26820.01", -0.650367},
	{"System.Net.Security.dll                               : 4.6.26820.01", -0.650367},
	{"System.Net.Requests.dll                               : 4.6.26820.01", -0.650367},
	{"System.Net.Ping.dll                                   : 4.6.26820.01", -0.650367},
	{"System.Net.Mail.dll                                   : 4.6.26820.01", -0.650367},
	{"System.Net.NetworkInformation.dll                     : 4.6.26820.01", -0.650367},
	{"System.Private.Reflection.Core.dll                    : 4.6.27015.02", -0.750611},
	{"System.Reflection.Emit.ILGeneration.dll               : 4.6.26820.01", -0.750611},
	{"System.Reflection.Emit.dll                            : 4.6.26820.01", -0.750611},
	{"System.Reflection.Primitives.dll                      : 4.6.26820.01", -0.750611},
	{"System.Private.Reflection.Metadata.dll                : 4.6.27015.02", -0.750611},
	{"System.Private.Interop.Extensions.dll                 : 4.6.27015.2", -0.750611},
	{"System.Reflection.DispatchProxy.dll                   : 4.6.26820.01", -0.750611},
	{"System.Reflection.dll                                 : 4.6.26820.01", -0.750611},
	{"System.Reflection.Metadata.dll                        : 4.6.26820.01", -0.750611},
	{"System.Reflection.Extensions.dll                      : 4.6.26820.01", -0.750611},
	{"System.Private.MCG.dll                                : 4.6.27015.2", -0.750611},
	{"System.Reflection.Emit.Lightweight.dll                : 4.6.26820.01", -0.750611},
	{"System.Private.Reflection.Execution.dll               : 4.6.27015.02", -0.750611},
	{"System.Private.Debug.dll                              : 4.6.27015.02 built by: PROJECTNREL", -0.753056},
	{"System.Private.CoreLib.DeveloperExperience.dll        : 4.6.27015.2", -0.753056},
	{"System.Private.CoreLib.InteropServices.dll            : 4.6.27015.2", -0.753056},
	{"System.Private.DispatchProxy.dll                      : 4.6.27015.2", -0.753056},
	{"System.Private.Xml.dll                                : 4.6.26820.01", -0.753056},
	{"System.Private.Reflection.Metadata.Ecma335.dll        : 4.6.26820.01", -0.753056},
	{"System.Private.CoreLib.dll                            : 4.6.27008.0", -0.753056},
	{"System.ObjectModel.dll                                : 4.6.26820.01", -0.753056},
	{"System.Private.DataContractSerialization.dll          : 4.6.26820.01", -0.753056},
	{"System.Private.Threading.dll                          : 4.6.27015.02", -0.753056},
	{"System.Private.Xml.Linq.dll                           : 4.6.26820.01", -0.753056},
	{"System.Private.Uri.dll                                : 4.6.26820.01", -0.753056},
	{"System.Private.ILToolchain.dll                        : 4.6.27015.2", -0.753056},
	{"System.Numerics.Vectors.dll                           : 4.6.26820.01", -0.753056},
	{"System.Private.CoreLib.Augments.dll                   : 4.6.27015.2", -0.753056},
	{"System.Private.CoreLib.Threading.dll                  : 4.6.27015.2", -0.753056},
	{"System.Numerics.Vectors.WindowsRuntime.dll            : 4.6.26820.01", -0.753056},
	{"System.Private.CompilerServices.ICastable.dll         : 4.6.27015.2", -0.753056},
	{"System.Private.StackTraceMetadata.dll                 : 4.6.27015.02", -0.753056},
	{"System.Private.CoreLib.WinRTInterop.dll               : 4.6.27015.2", -0.753056},
	{"System.Private.CoreLib.DynamicDelegate.dll            : 4.6.27015.2", -0.753056},
	{"System.Private.WinRTInterop.CoreLib.dll               : 4.6.27015.02 built by: PROJECTNREL", -0.753056},
	{"System.Reflection.Context.dll                         : 4.6.26820.01", -0.753056},
	{"System.Security.Cryptography.Csp.dll                  : 4.6.26820.01", -0.762836},
	{"System.Security.Cryptography.Encoding.dll             : 4.6.26820.01", -0.762836},
	{"System.Security.Cryptography.Cng.dll                  : 4.6.26820.01", -0.762836},
	{"System.Security.Cryptography.Primitives.dll           : 4.6.26820.01", -0.762836},
	{"System.Runtime.Serialization.dll                      : 4.6.26820.1", -0.765281},
	{"System.Runtime.WindowsRuntime.dll                     : 4.6.26820.01", -0.765281},
	{"System.Resources.ResourceManager.dll                  : 4.6.26820.01", -0.765281},
	{"System.Runtime.WindowsRuntime.UI.Xaml.dll             : 4.6.26820.01", -0.765281},
	{"System.Runtime.InteropServices.dll                    : 4.6.26820.01", -0.765281},
	{"System.Runtime.InteropServices.RuntimeInformation.dll : 4.6.26820.01", -0.765281},
	{"System.Runtime.Serialization.Formatters.dll           : 4.6.26820.01", -0.765281},
	{"System.Resources.Writer.dll                           : 4.6.26820.01", -0.765281},
	{"System.Security.Cryptography.Algorithms.dll           : 4.6.26820.01", -0.765281},
	{"System.Security.AccessControl.dll                     : 4.6.26820.01", -0.765281},
	{"System.Runtime.Serialization.Xml.dll                  : 4.6.26820.01", -0.765281},
	{"System.Runtime.Handles.dll                            : 4.6.26820.01", -0.765281},
	{"System.Runtime.Serialization.Json.dll                 : 4.6.26820.01", -0.765281},
	{"System.Security.Claims.dll                            : 4.6.26820.01", -0.765281},
	{"System.Reflection.TypeExtensions.dll                  : 4.6.26820.01", -0.765281},
	{"System.Runtime.Serialization.Primitives.dll           : 4.6.26820.01", -0.765281},
	{"System.Runtime.Numerics.dll                           : 4.6.26820.01", -0.765281},
	{"System.Runtime.dll                                    : 4.6.26820.01", -0.765281},
	{"System.Runtime.InteropServices.WindowsRuntime.dll     : 4.6.26820.01", -0.765281},
	{"System.Resources.Reader.dll                           : 4.6.26820.01", -0.765281},
	{"System.Runtime.CompilerServices.VisualC.dll           : 4.6.26820.01", -0.765281},
	{"System.Runtime.Extensions.dll                         : 4.6.26820.01", -0.765281},
	{"System.Private.DeveloperExperience.AppX.dll           : 4.6.0.0 built by: PROJECTNREL", -0.784841},
	{"System.Private.ServiceModel.dll                       : 4.6.26823.01", -0.784841},
	{"System.Runtime.CompilerServices.Unsafe.dll            : ", -0.797066},
	{"System.Web.dll                                        : 4.6.26820.1", -0.831296},
	{"System.Transactions.dll                               : 4.6.26820.1", -0.831296},
	{"System.Web.HttpUtility.dll                            : 4.6.26820.01", -0.831296},
	{"System.Transactions.Local.dll                         : 4.6.26820.01", -0.831296},
	{"System.Threading.Timer.dll                            : 4.6.26820.01", -0.831296},
	{"System.ValueTuple.dll                                 : 4.6.26820.01", -0.831296},
	{"System.Threading.Thread.dll                           : 4.6.26820.01", -0.833741},
	{"System.Threading.ThreadPool.dll                       : 4.6.26820.01", -0.833741},
	{"System.Threading.Tasks.Extensions.dll                 : 4.6.26820.01", -0.833741},
	{"System.Threading.Tasks.Parallel.dll                   : 4.6.26820.01", -0.833741},
	{"System.Threading.Tasks.dll                            : 4.6.26820.01", -0.833741},
	{"System.Threading.Overlapped.dll                       : 4.6.26820.01", -0.836186},
	{"System.Threading.Tasks.Dataflow.dll                   : 4.6.26820.01", -0.836186},
	{"System.Text.RegularExpressions.dll                    : 4.6.26820.01", -0.838631},
	{"System.Text.Encoding.Extensions.dll                   : 4.6.26820.01", -0.838631},
	{"System.Threading.dll                                  : 4.6.26820.01", -0.838631},
	{"System.Text.Encoding.dll                              : 4.6.26820.01", -0.841076},
	{"System.Text.Encoding.CodePages.dll                    : 4.6.26820.01", -0.841076},
	{"System.ServiceProcess.dll                             : 4.6.26820.1", -0.841076},
	{"System.Security.SecureString.dll                      : 4.6.26820.01", -0.843521},
	{"System.ServiceModel.Web.dll                           : 4.6.26820.1", -0.843521},
	{"System.Security.Principal.Windows.dll                 : 4.6.26820.01", -0.843521},
	{"System.Security.Principal.dll                         : 4.6.26820.01", -0.845966},
	{"System.Security.dll                                   : 4.6.26820.1", -0.845966},
	{"System.Security.Cryptography.X509Certificates.dll     : 4.6.26820.01", -0.848411},
	{"System.ServiceModel.Security.dll                      : 4.6.26823.01", -0.875306},
	{"System.ServiceModel.Primitives.dll                    : 4.6.26823.01", -0.875306},
	{"System.ServiceModel.NetTcp.dll                        : 4.6.26823.01", -0.875306},
	{"System.ServiceModel.Duplex.dll                        : 4.6.26823.01", -0.875306},
	{"System.ServiceModel.dll                               : 4.6.26823.1", -0.875306},
	{"System.ServiceModel.Http.dll                          : 4.6.26823.01", -0.875306},
	{"WindowsBase.dll                                       : 4.6.26820.1", -0.91198},
	{"System.Xml.XPath.XDocument.dll                        : 4.6.26820.01", -0.91198},
	{"System.Xml.XmlDocument.dll                            : 4.6.26820.01", -0.914425},
	{"System.Windows.dll                                    : 4.6.26820.1", -0.914425},
	{"System.Xml.Serialization.dll                          : 4.6.26820.1", -0.914425},
	{"System.Xml.XmlSerializer.dll                          : 4.6.26820.01", -0.914425},
	{"System.Xml.Linq.dll                                   : 4.6.26820.1", -0.914425},
	{"System.Xml.XDocument.dll                              : 4.6.26820.01", -0.914425},
	{"System.Xml.XPath.dll                                  : 4.6.26820.01", -0.914425},
	{"System.Xml.dll                                        : 4.6.26820.1", -0.914425},
	{"System.Xml.ReaderWriter.dll                           : 4.6.26820.01", -0.914425},
	};

	ReadFileToBuffer file(exe_path, 100000000);
	if (!file.b) return 0;

	vector<int> detected(installer_patterns.size());

	size_t dsize = detected.size();
	float total = 0;
	int total_n = 0;
	for (size_t i = 0; i < file.s; i++)
	{
		for (size_t y = 0; y < dsize; y++)
		{
			if (file.b[i] == installer_patterns[y].first[detected[y]])
			{
				if (++detected[y] == installer_patterns[y].first.size())
				{
					total += installer_patterns[y].second;
					total_n++;
					detected[y] = 0;
				}
			}
			else detected[y] = 0;
		}
	}

	return total / total_n;
}

#pragma endregion





#pragma region PausedProcess // Useless
/*
	Pauses process on initialization
	Process can be unpaused using ResumeProcess
*/
/*
PausedProcess::PausedProcess(shared_ptr<ProcessInfo> const& pi)
{
	return;
	THREADENTRY32 te32;
	te32.dwSize = sizeof(te32);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, pi->PID), hThread;

	if (Thread32First(hSnapshot, &te32))
	{
		do
		{
			if (te32.th32OwnerProcessID == pi->PID)
			{
				if (hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, te32.th32ThreadID))
				{
					SuspendThread(hThread);
					Threads.push_back(hThread);
				}
			}
		} while (Thread32Next(hSnapshot, &te32));
	}
}
void PausedProcess::ResumeProcess()
{
	for (auto& i : Threads)
		ResumeThread(i);
}
PausedProcess::~PausedProcess()
{
	ResumeProcess();
	for (auto& i : Threads)
		CloseHandle(i);
}
*/
#pragma endregion

BOOL IsElevated(HANDLE hProcess)
{
	BOOL fRet = FALSE;
	HANDLE hToken = NULL;
	if (OpenProcessToken(hProcess, TOKEN_QUERY, &hToken))
	{
		TOKEN_ELEVATION Elevation;
		DWORD cbSize = sizeof(TOKEN_ELEVATION);
		if (GetTokenInformation(hToken, TokenElevation, &Elevation, sizeof(Elevation), &cbSize))
		{
			fRet = Elevation.TokenIsElevated;
		}
	}
	if (hToken)
		CloseHandle(hToken);
	return fRet;
}


bool IsValidExePath(const wstring& ExePath)
{
	//return true;
	const static vector<wstring> Disallowed =
	{
		GetSysConstant(C_SYSTEMROOT)
	},
		DisallowedExceptions =
	{
		GetSysConstant(C_SYSTEMROOT) / "system32\\msiexec.exe"
	};

	if ([&]()
	{
		for (auto& i : DisallowedExceptions)
			if (ExePath == i) return false;
			return true;
	}())
	{
		for (auto& i : Disallowed)
			if (ExePath.find(i) == 0) return false;
	}

		return true;
}


bool IsValidChar(char const& c)
{
	return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' || c <= '9');
}

vector<string> GetStrings(const std::byte* Binary, size_t const& BinSize, const unsigned min_length = 20, const bool lower = true)
{
	vector<string> output;
	unsigned string_len = 0;
	for (unsigned i = 0; i < BinSize; i++)
	{
		if (IsValidChar((uint8_t)Binary[i])) string_len++;
		else
		{
			if (string_len > min_length)
			{
				output.push_back(string((char*)&Binary[i - string_len]));
				output.back().resize(string_len);
				if (lower) to_lower(output.back());
			}
			string_len = 0;
		}
	}

	return move(output);
}

bool MatchesStrings(ReadFileToBuffer const& file, vector<string> const& strs, const bool lower = true)
{
	auto fstrs = GetStrings((std::byte*)file.b, file.s, 10);

	for (auto& x : fstrs)
		for (auto& y : strs)
			if (x.find(y) != string::npos) return true;
	return false;
}




#define VALID_CHAR(x) (x >= ' ' && x <= '~')
bool task_exists(wchar_t* tn)
{
	HRESULT hr = S_OK;


	///////////////////////////////////////////////////////////////////
	// Call CoInitialize to initialize the COM library and then
	// call CoCreateInstance to get the Task Scheduler object.
	///////////////////////////////////////////////////////////////////
	ITaskScheduler* pITS;
	hr = CoInitialize(NULL);
	if (SUCCEEDED(hr))
	{
		hr = CoCreateInstance(CLSID_CTaskScheduler,
							  NULL,
							  CLSCTX_INPROC_SERVER,
							  IID_ITaskScheduler,
							  (void**)&pITS);
		if (FAILED(hr))
		{
			CoUninitialize();
			return 1;
		}
	}
	else
	{
		return 1;
	}


	///////////////////////////////////////////////////////////////////
	// Call ITaskScheduler::Activate to get the Task object.
	///////////////////////////////////////////////////////////////////
	ITask* pITask;
	hr = pITS->Activate(tn,
						IID_ITask,
						(IUnknown**)&pITask);

	// Release ITaskScheduler interface.
	pITS->Release();

	if (FAILED(hr))
	{
		CoUninitialize();
		return false;
	}


	///////////////////////////////////////////////////////////////////
	// Call ITask::GetStatus. Note that this method is 
	// inherited from IScheduledWorkItem.
	///////////////////////////////////////////////////////////////////
	HRESULT phrStatus;

	hr = pITask->GetStatus(&phrStatus);

	// Release the ITask interface.
	pITask->Release();

	if (FAILED(hr))
	{
		CoUninitialize();
		return false;
	}

	CoUninitialize();

	return phrStatus != SCHED_S_TASK_NOT_SCHEDULED;
}
